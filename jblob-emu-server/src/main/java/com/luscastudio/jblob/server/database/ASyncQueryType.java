package com.luscastudio.jblob.server.database;

/**
 * Created by Lucas on 24/10/2016.
 */
public enum ASyncQueryType {
    UPDATE,
    QUERY,
    NONE
}
