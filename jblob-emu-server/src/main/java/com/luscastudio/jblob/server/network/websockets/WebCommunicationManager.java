//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.network.websockets;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

import java.net.InetSocketAddress;

/**
 * Created by Lucas on 12/03/2017 at 18:35.
 */
public class WebCommunicationManager {
    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;

    public WebCommunicationManager(int bossGroupThreadCount, int workerGroupThreadCount){
        this.bossGroup = new NioEventLoopGroup(bossGroupThreadCount);
        this.workerGroup = new NioEventLoopGroup(workerGroupThreadCount);

        ServerBootstrap bootstrap  = new ServerBootstrap();
        bootstrap.group(bossGroup, workerGroup);
        bootstrap.channel(NioServerSocketChannel.class);
        bootstrap.handler(new LoggingHandler(LogLevel.DEBUG));
        bootstrap.childHandler(new WebSocketConnectionInitializer());

        bootstrap.bind(new InetSocketAddress("0.0.0.0", 3030)).addListener(future -> {
           if(!future.isSuccess())
               System.out.print("Error when starting thi shit");
        });
    }

    public static void main(String[] args){
        new WebCommunicationManager(16, 10);
    }
}
