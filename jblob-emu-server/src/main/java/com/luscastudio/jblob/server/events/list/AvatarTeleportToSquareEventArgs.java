//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.api.utils.engine.IPoint;
import com.luscastudio.jblob.server.events.EventArgs;

/**
 * Created by Lucas on 09/03/2017 at 00:17.
 */
public class AvatarTeleportToSquareEventArgs extends EventArgs {

    private IPoint p;

    public AvatarTeleportToSquareEventArgs(IPoint p) {
        this.p = p;
    }

    public IPoint getP() {
        return p;
    }
}
