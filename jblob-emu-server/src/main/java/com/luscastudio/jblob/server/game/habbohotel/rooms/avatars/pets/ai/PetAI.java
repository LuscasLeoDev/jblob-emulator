package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.ai;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.api.utils.time.DateTimeUtils;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.server.ChatComposer;
import com.luscastudio.jblob.server.events.EventDelegate;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;

import java.util.List;

/**
 * Created by Lucas on 25/02/2017 at 16:15.
 */
public class PetAI {
    private IRoomPetAvatar petAvatar;
    private int nextFreeAction = 0;
    private boolean busy = false;

    private List<EventDelegate> delegates;

    public PetAI(IRoomPetAvatar petAvatar){
        this.petAvatar = petAvatar;
        this.delegates = BCollect.newList();
    }

    private boolean isBusy(){
        return this.nextFreeAction > DateTimeUtils.getUnixTimestampInt() || this.busy;
    }

    public void onCycle(){
        if(isBusy())
            return;
        this.walkToRandomPlayer();

        this.checkDelegates();
    }

    private void walkToRandomPlayer(){
        RoomPlayerAvatar playerAvatar = BCollect.getRandomThing(BCollect.newList(this.petAvatar.getRoom().getRoomAvatarService().getPlayersAvatars()));

        if(playerAvatar == null)
            return;

        if(playerAvatar.getPath().isWalking())
            return;

        Point p = BCollect.getRandomThing(this.petAvatar.getRoom().getRoomMap().getWalkableSquares(playerAvatar.getSetPosition().getPoint(), 1));

        if(p == null)
            return;

        this.petAvatar.moveTo(p);
        this.busy = true;

        EventDelegate petStandDelegate = this.petAvatar.getEventHandler().on("avatar.stand.on.square", (delegate, eventArgs) -> {
            if(this.petAvatar.getSetPosition().getPoint().getDistance(playerAvatar.getPosition().getPoint()) <= 1 && !this.petAvatar.getPath().isWalking()){
                this.petAvatar.getRoom().sendMessage(new ChatComposer(this.petAvatar.getVirtualId(), 0,0, playerAvatar.getName() + ", vamos brincar?"));

                this.petAvatar.getStatusses().set("sit", "0");
                this.petAvatar.getStatusses().set("gst", "sml", 4);
                this.petAvatar.getPosition().setRotation(this.petAvatar.getPosition().getPoint().getRotation(playerAvatar.getPosition().getPoint()));
                this.petAvatar.getPosition().setHeadRotation(this.petAvatar.getPosition().getRotation());
            }

            this.busy = false;
            this.nextFreeAction = DateTimeUtils.getUnixTimestampInt() + 6;
            delegate.cancel();
        });

        this.delegates.add(petStandDelegate);

        EventDelegate futureError = playerAvatar.getEventHandler().on("avatar.leave.room avatar.move", (delegate, eventArgs) -> {
            petStandDelegate.cancel();
            this.busy = false;
            delegate.cancel();
        });

        this.delegates.add(futureError);
    }

    private void checkDelegates() {
        this.delegates.removeIf(EventDelegate::isCancelled);
    }

    private void cancelDelegates(){
        this.delegates.forEach(EventDelegate::cancel);
    }

    public void dispose() {
        this.cancelDelegates();
        this.delegates.clear();
    }
}
