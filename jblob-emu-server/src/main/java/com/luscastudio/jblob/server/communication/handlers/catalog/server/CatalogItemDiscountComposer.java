package com.luscastudio.jblob.server.communication.handlers.catalog.server;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.server.MessageComposer;

import java.util.List;

/**
 * Created by Lucas on 08/12/2016.
 */

public class CatalogItemDiscountComposer extends MessageComposer {
    @Override
    public String id() {
        return "CatalogItemDiscountMessageComposer";
    }

    public CatalogItemDiscountComposer(int maxAmount, int int1, int int2, int int3, List<Integer> ints){
        message.putInt(maxAmount);
        message.putInt(int1);
        message.putInt(int2);
        message.putInt(int3);

        message.putInt(ints.size());
        for(int i : ints){
            message.putInt(i);
        }
    }

    public CatalogItemDiscountComposer(){
        this(300, 0, 0, 0, BCollect.newList());
    }
}
