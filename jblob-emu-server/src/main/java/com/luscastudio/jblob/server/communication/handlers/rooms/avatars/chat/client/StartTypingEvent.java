package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.server.UserTypingComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 17/12/2016.
 */

public class StartTypingEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        IRoomAvatar avatar = room.getRoomAvatarService().getRoomAvatarByUserId(session.getAvatar().getId());
        if(avatar == null)
            return;

        room.sendMessage(new UserTypingComposer(avatar.getVirtualId(), true));

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
