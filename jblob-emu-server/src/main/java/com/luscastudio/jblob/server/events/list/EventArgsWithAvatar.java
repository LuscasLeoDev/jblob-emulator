package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;

/**
 * Created by Lucas on 08/03/2017 at 13:59.
 */
public interface EventArgsWithAvatar {
    IRoomAvatar getAvatar();
}
