package com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata;

import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.IItemMessageParser;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.SimpleItemMessageParser;

/**
 * Created by Lucas on 09/10/2016.
 */

public class SimpleRoomItemExtradata implements IRoomItemExtradata {


    protected FurniProperties properties;
    protected String extradata;
    private SimpleItemMessageParser messageParser;

    public SimpleRoomItemExtradata(String extradata) {
        this.extradata = extradata;
    }


    @Override
    public String toString() {
        return this.getExtradata();
    }

    public int getExtradataInt() {
        if (!NumberHelper.isInteger(extradata))
            return 0;
        return Integer.parseInt(extradata);
    }

    @Override
    public synchronized String getExtradata() {
        return this.extradata;
    }

    @Override
    public String getExtradataToDb() {
        return this.extradata;
    }


    @Override
    public synchronized void setExtradata(String extradata) {
        this.extradata = extradata;
    }

    @Override
    public void init(FurniProperties properties) {
        this.properties = properties;
        this.messageParser = new SimpleItemMessageParser(properties);
    }

    @Override
    public IItemMessageParser getMessageParser() {
        return this.messageParser;
    }

    public void toggle() {
        if (this.getExtradataInt() >= properties.getBase().getInteractionCount())
            this.extradata = "0";
        else
            this.extradata = String.valueOf(this.getExtradataInt() + 1);
    }

}
