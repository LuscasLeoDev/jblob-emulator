package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.WiredTriggerConfigComposer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;

import java.util.Collection;

/**
 * Created by Lucas on 14/02/2017 at 23:35.
 */
public abstract class TriggerWiredBox extends WiredBoxRoomItem implements ISavableWiredTrigger {
    public TriggerWiredBox(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
    }

    @Override
    protected MessageComposer getWiredConfigComposer() {
        return new WiredTriggerConfigComposer(this);
    }

    @Override
    public Collection<IRoomItem> getIncompatibleItems() {
        return BCollect.newList();
    }
}
