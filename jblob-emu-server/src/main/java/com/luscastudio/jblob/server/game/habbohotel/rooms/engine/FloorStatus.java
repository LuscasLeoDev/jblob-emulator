package com.luscastudio.jblob.server.game.habbohotel.rooms.engine;

/**
 * Created by Lucas on 08/10/2016.
 */
public enum FloorStatus {
    BLOCKED,
    FREE
}
