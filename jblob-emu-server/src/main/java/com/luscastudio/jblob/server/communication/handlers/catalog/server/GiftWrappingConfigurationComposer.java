package com.luscastudio.jblob.server.communication.handlers.catalog.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 31/01/2017 at 18:15.
 */
public class GiftWrappingConfigurationComposer extends MessageComposer {
    @Override
    public String id() {
        return "GiftWrappingConfigurationMessageComposer";
    }

    public GiftWrappingConfigurationComposer(){

        message.putBool(false);
        message.putInt(10); //Gift Wraps Price

        message.putInt(10); //New Gift Types count
        {
            message.putInt(3372);
            message.putInt(3373);
            message.putInt(3374);

            message.putInt(3375);
            message.putInt(3376);
            message.putInt(3377);

            message.putInt(3378);
            message.putInt(3379);
            message.putInt(3380);

            message.putInt(3381);
        }

        message.putInt(11); //Old Gift Types Wrap Decoration count
        {
            message.putInt(0);
            message.putInt(1);
            message.putInt(2);
            message.putInt(3);
            message.putInt(4);
            message.putInt(5);
            message.putInt(6);
            message.putInt(7);
            message.putInt(8);
            message.putInt(9);
            message.putInt(10);
        }


        message.putInt(11); //New Gift Types Wrap Decoration count
        {
            message.putInt(0);
            message.putInt(1);
            message.putInt(2);
            message.putInt(3);
            message.putInt(4);
            message.putInt(5);
            message.putInt(6);
            message.putInt(7);
            message.putInt(8);
            message.putInt(9);
            message.putInt(10);
        }

        message.putInt(7); //Old gift types count count
        {
            message.putInt(187);
            message.putInt(188);
            message.putInt(189);

            message.putInt(190);
            message.putInt(191);
            message.putInt(192);

            message.putInt(193);
        }


    }
}
