package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;

/**
 * Created by Lucas on 22/01/2017 at 21:22.
 */
public class PetInformationComposer extends MessageComposer {
    @Override
    public String id() {
        return "PetInformationMessageComposer";
    }

    public PetInformationComposer(IRoomPetAvatar avatar) {

        avatar.composePetAvatarInformation(message);
    }
}
