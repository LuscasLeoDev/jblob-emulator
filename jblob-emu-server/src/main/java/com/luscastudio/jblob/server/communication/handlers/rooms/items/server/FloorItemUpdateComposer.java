package com.luscastudio.jblob.server.communication.handlers.rooms.items.server;

/**
 * Created by Lucas on 10/10/2016.
 */

import com.luscastudio.jblob.server.communication.handlers.rooms.items.ItemSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;

public class FloorItemUpdateComposer extends MessageComposer {
    public FloorItemUpdateComposer(IRoomItem item) {
        ItemSerializer.serializeFloorItem(message, item);
    }

    @Override
    public String id() {
        return "ObjectUpdateMessageComposer";
    }
}
