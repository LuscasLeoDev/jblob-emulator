//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.api.utils.engine.IPoint;
import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;

/**
 * Created by Lucas on 08/03/2017 at 13:55.
 */
public class AvatarMoveEventArgs extends EventArgs {
    private IPoint point;
    private IRoomAvatar avatar;

    public AvatarMoveEventArgs(IPoint point, IRoomAvatar avatar) {
        this.point = point;
        this.avatar = avatar;
    }

    public AvatarMoveEventArgs(IPoint point) {
        this.point = point;
    }

    public IPoint getPoint() {
        return point;
    }

    public IRoomAvatar getAvatar() {
        return avatar;
    }
}
