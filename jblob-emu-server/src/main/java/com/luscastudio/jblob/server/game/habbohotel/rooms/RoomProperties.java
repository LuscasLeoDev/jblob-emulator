package com.luscastudio.jblob.server.game.habbohotel.rooms;

import com.google.common.reflect.TypeToken;
import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.json.JSONUtils;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.boot.JBlobSettings;
import com.luscastudio.jblob.server.game.habbohotel.rooms.models.RoomModel;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 03/10/2016.
 */

public class RoomProperties {

    private int id;
    private String name;
    private String description;
    private int ownerId;

    private String modelName;

    private int lockType;
    private String password;

    private RoomType type;

    private Room room;

    private int maxUsers;

    private int tradeType;

    private int categoryId;
    private List<String> tags;
    private String wallData;
    private String floorData;
    private String landscapeData;

    private int allowPets;
    private int allowPetsEating;
    private int hideWall;
    private int wallThickness;
    private int floorThickness;
    private int allowWalkTrough;

    private int whoCanMute;
    private int whoCanKick;
    private int whoCanBan;

    private int chatMode;
    private int chatSize;
    private int chatSpeed;
    private int chatExtraFlood;
    private int chatDistance;

    private boolean muted;
    private boolean staffPicked;

    private RoomModel model;

    private Map<Integer, Integer> avatarRights;

    private List<Integer> avatarLikes;

    //Todo: add groups, room promotion, etc

    public RoomProperties(int id, String name, String description, int ownerId, String modelName, int lockType, String password, int maxUsers, int tradeType, int categoryId, String wallData, String floorData, String landscapeData, RoomType type, int allowPets, int allowPetsEating, int hideWall, int wallThickness, int floorThickness, int allowWalkTrough, int whoCanMute, int whoCanKick, int whoCanBan, int chatMode, int chatSize, int chatSpeed, int chatExtraFlood, int chatDistance, String jsonAvatarsLike, String jsonAvatarsRight, String tagsData, RoomModel model){

        this.id = id;
        this.name = name;
        this.description = description;
        this.ownerId = ownerId;
        this.modelName = modelName;
        this.lockType = lockType;
        this.password = password;
        this.maxUsers = maxUsers;
        this.tradeType = tradeType;
        this.categoryId = categoryId;
        this.wallData = wallData;
        this.floorData = floorData;
        this.landscapeData = landscapeData;
        this.type = type;
        this.allowPets = allowPets;
        this.allowPetsEating = allowPetsEating;
        this.hideWall = hideWall;
        this.wallThickness = wallThickness;
        this.floorThickness = floorThickness;
        this.allowWalkTrough = allowWalkTrough;
        this.whoCanMute = whoCanMute;
        this.whoCanKick = whoCanKick;
        this.whoCanBan = whoCanBan;
        this.chatMode = chatMode;
        this.chatSize = chatSize;
        this.chatSpeed = chatSpeed;
        this.chatExtraFlood = chatExtraFlood;
        this.chatDistance = chatDistance;

        this.avatarRights =  JSONUtils.toMap(jsonAvatarsRight, new TypeToken<Map<Integer, Integer>>(){}.getType());
        if(this.avatarRights == null)
            this.avatarRights = BCollect.newMap();

        this.avatarLikes = JSONUtils.toList(jsonAvatarsLike, new TypeToken<List<Integer>>(){}.getType());
        if(this.avatarLikes == null)
            this.avatarLikes = BCollect.newList();

        tags = BCollect.newList();
        for (String tag : tagsData.split(JBlobSettings.ROOM_PROPERTIES_TAG_DELIMITER))
            tags.add(tag);

        this.model = model;

    }

    public RoomProperties(ResultSet set, RoomModel model) throws SQLException {

        this.model = model;

        this.id = set.getInt("id");
        this.name = set.getString("name");
        this.description = set.getString("description");
        this.ownerId = set.getInt("owner_id");
        this.modelName = set.getString("model_name");
        this.lockType = set.getInt("lock_type");
        this.password = set.getString("password");
        this.maxUsers = set.getInt("max_users");
        this.tradeType = set.getInt("trade_type");
        this.categoryId = set.getInt("category_id");

        this.wallData = set.getString("wallpaper_data");
        this.floorData = set.getString("floor_data");
        this.landscapeData = set.getString("landscape_data");

        this.type = set.getInt("is_private") == 0 ? RoomType.PUBLIC : RoomType.PRIVATE;

        this.allowPets = set.getInt("allow_pets");
        this.allowPetsEating = set.getInt("allow_pets_eating");

        this.hideWall = set.getInt("hide_wall");
        this.wallThickness = set.getInt("wall_thickness");
        this.floorThickness = set.getInt("floor_thickness");

        this.allowWalkTrough = set.getInt("allow_walk_trough");

        this.whoCanMute = set.getInt("who_can_mute");
        this.whoCanKick = set.getInt("who_can_kick");
        this.whoCanBan = set.getInt("who_can_ban");

        this.chatMode = set.getInt("chat_mode");
        this.chatSize = set.getInt("chat_size");
        this.chatSpeed = set.getInt("chat_speed");
        this.chatExtraFlood = set.getInt("chat_extra_flood");
        this.chatDistance = set.getInt("chat_distance");

        this.avatarRights = JSONUtils.toMap(set.getString("avatars_rights"), new TypeToken<Map<Integer, Integer>>(){}.getType());
        this.avatarLikes = JSONUtils.toList(set.getString("avatars_likes"), new TypeToken<List<Integer>>(){}.getType());

        if(this.avatarRights == null)
            this.avatarRights = BCollect.newMap();

        if(this.avatarLikes == null)
            this.avatarLikes = BCollect.newList();

        tags = BCollect.newList();
        for (String tag : set.getString("tags").split(JBlobSettings.ROOM_PROPERTIES_TAG_DELIMITER))
            tags.add(tag);
    }

    public boolean isActive() {
        return room != null;
    }

    public int getAvatarRank(int avatarId){
        if(avatarId == this.ownerId)
            return 2;//change this later;
        if(!this.avatarRights.containsKey(avatarId))
            return 0;

        return this.avatarRights.get(avatarId);
    }

    public void setAvatarRank(int avatarId, int rankId){
        if(!this.avatarRights.containsKey(avatarId))
            this.avatarRights.put(avatarId, rankId);
        else
            this.avatarRights.replace(avatarId, rankId);
    }

    public boolean avatarHasRight(int avatarId, String permissionName){
        return JBlob.getGame().getPermissionManager().validateRight(this.getAvatarRank(avatarId), permissionName);
    }

    public Map<Integer, Integer> getAvatarRights() {
        return avatarRights;
    }

    public String getOwnerName() {
        IAvatarDataCache cache = JBlob.getGame().getUserCacheManager().getUserCache(ownerId);
        if (cache == null)
            return "Unknown #" + ownerId;

        return cache.getUsername();
    }



    public int getUsersNow() {
        if (room == null)
            return 0;

        return room.getUsersNow();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public String getModelName() {
        return modelName;
    }

    public int getLockType() {
        return lockType;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public int getMaxUsers() {
        return maxUsers;
    }

    public int getScore() {
        return avatarLikes.size();
    }

    public int getTradeType() {
        return tradeType;
    }

    public List<String> getTags() {
        return tags;
    }

    public List<Integer> getAvatarLikes() {
        return avatarLikes;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public RoomType getType() {
        return type;
    }

    public String getFloorData() {
        return floorData;
    }

    public String getLandscapeData() {
        return landscapeData;
    }

    public String getPassword() {
        return password;
    }

    public String getWallData() {
        return wallData;
    }

    public int getAllowPets() {
        return allowPets;
    }

    public int getAllowPetsEating() {
        return allowPetsEating;
    }

    public int getFloorThickness() {
        return floorThickness;
    }

    public int getHideWall() {
        return hideWall;
    }

    public int getWallThickness() {
        return wallThickness;
    }

    public int getAllowWalkTrough() {
        return allowWalkTrough;
    }

    public int getChatDistance() {
        return chatDistance;
    }

    public int getChatExtraFlood() {
        return chatExtraFlood;
    }

    public int getChatMode() {
        return chatMode;
    }

    public int getChatSize() {
        return chatSize;
    }

    public int getChatSpeed() {
        return chatSpeed;
    }

    public int getWhoCanBan() {
        return whoCanBan;
    }

    public int getWhoCanKick() {
        return whoCanKick;
    }

    public int getWhoCanMute() {
        return whoCanMute;
    }

    public boolean isMuted() {
        return muted;
    }

    public boolean isStaffPicked() {
        return staffPicked;
    }

    public RoomModel getModel() {
        return model;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setDescription(String description) {
        this.description = description;
    }

    public void setMuted(boolean muted) {
        this.muted = muted;
    }

    public void setLockType(int lockType) {
        this.lockType = lockType;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setMaxUsers(int maxUsers) {
        this.maxUsers = maxUsers;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void setTradeType(int tradeType) {
        this.tradeType = tradeType;
    }

    public void setAllowPets(boolean allowPets) {
        this.allowPets = allowPets ? 1 : 0;
    }

    public void setAllowPetsEating(boolean allowPetsEating) {
        this.allowPetsEating = allowPetsEating ? 1 : 0;
    }

    public void setAllowWalkTrough(boolean allowWalkTrough) {
        this.allowWalkTrough = allowWalkTrough ? 1 : 0;
    }

    public void setHideWall(boolean hideWall) {
        this.hideWall = hideWall ? 1 : 0;
    }

    public void setWallThickness(int wallThickness) {
        this.wallThickness = wallThickness;
    }

    public void setFloorThickness(int floorThickness) {
        this.floorThickness = floorThickness;
    }

    public void setChatMode(int chatMode) {
        this.chatMode = chatMode;
    }

    public void setChatSize(int chatSize) {
        this.chatSize = chatSize;
    }

    public void setChatSpeed(int chatSpeed) {
        this.chatSpeed = chatSpeed;
    }

    public void setChatDistance(int chatDistance) {
        this.chatDistance = chatDistance;
    }

    public void setChatExtraFlood(int chatExtraFlood) {
        this.chatExtraFlood = chatExtraFlood;
    }

    public void setWhoCanMute(int whoCanMute) {
        this.whoCanMute = whoCanMute;
    }

    public void setWhoCanKick(int whoCanKick) {
        this.whoCanKick = whoCanKick;
    }

    public void setWhoCanBan(int whoCanBan) {
        this.whoCanBan = whoCanBan;
    }

    public void setWallData(String wallData) {
        this.wallData = wallData;
    }

    public void setFloorData(String floorData) {
        this.floorData = floorData;
    }

    public void setLandscapeData(String landscapeData) {
        this.landscapeData = landscapeData;
    }
}
