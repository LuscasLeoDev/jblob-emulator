package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars;

import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.events.EventHandler;
import com.luscastudio.jblob.server.events.list.AvatarStartMoveEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.AvatarObjectPosition;
import com.luscastudio.jblob.server.game.habbohotel.rooms.IAvatarObjectPosition;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.statusses.RoomAvatarStatusses;
import com.luscastudio.jblob.server.game.habbohotel.rooms.engine.path.RoomAvatarPath;
import com.luscastudio.jblob.server.game.habbohotel.rooms.engine.validator.SimpleAvatarPathValidator;

/**
 * Created by Lucas on 22/01/2017 at 15:13.
 */
public abstract class RoomAvatar implements IRoomAvatar {

    private Room room;
    private IAvatarObjectPosition position;
    private IAvatarObjectPosition setPosition;
    private RoomAvatarPath path;
    private boolean updateNeeded;
    private int virtualId;
    private RoomAvatarStatusses statusses;
    private boolean idle;
    private EventHandler eventHandler;
    private double aditionalHeight;
    private int effectId;

    public RoomAvatar(Room room, int virtualId){
        this.room = room;
        this.virtualId = virtualId;

        this.position = new AvatarObjectPosition(0, 0, 0, 0, 0);
        this.setPosition = new AvatarObjectPosition(0, 0, 0, 0, 0);
        this.room = room;
        this.statusses = new RoomAvatarStatusses();
        this.virtualId = virtualId;
        this.path = new RoomAvatarPath(this, room, new SimpleAvatarPathValidator(room, this));

        this.eventHandler = new EventHandler();
    }


    //region IRoomAvatar Methods
    @Override
    public int getVirtualId() {
        return virtualId;
    }

    @Override
    public abstract int getId();

    @Override
    public abstract  String getName();

    @Override
    public abstract  String getMotto();

    @Override
    public abstract  String getFigureString();

    @Override
    public RoomAvatarStatusses getStatusses() {
        return statusses;
    }

    @Override
    public IAvatarObjectPosition getPosition() {
        return position;
    }

    @Override
    public IAvatarObjectPosition getSetPosition() {
        return setPosition;
    }


    @Override
    public abstract RoomAvatarType getType();

    @Override
    public abstract int getAvatarType();

    public void setPosition(int x, int y, double z, int direction) {
        position.setPosition(x, y, z, direction);
    }


    @Override
    public void setPosition(int x, int y, double z) {
        position.setPosition(x, y, z);
    }

    @Override
    public void setSetPosition(int x, int y, double z) {
        setPosition.setPosition(x, y, z);
    }

    @Override
    public RoomAvatarPath getPath() {
        return this.path;
    }

    @Override
    public boolean updateNeeded() {
        return updateNeeded;
    }

    @Override
    public boolean updateNeeded(boolean b) {
        return this.updateNeeded = b;
    }

    @Override
    public Room getRoom() {
        return this.room;
    }

    @Override
    public void moveTo(Point p) {
        EventArgs args = room.getEventHandler().fireEvent("room.avatar.auto.start.move", new AvatarStartMoveEventArgs(this));

        if(args.isCancelled())
            return;

        if(p.equals(this.getSetPosition().getPoint()))
            return;

        getPath().setGoal(p);
        getPath().clearPath();
        getPath().recall(true);
    }

    @Override
    public void clearMovement() {
        this.path.clearPath();
        this.path.setWalking(false);

        this.updateNeeded(true);
        this.position.setPosition(this.setPosition.getX(), this.setPosition.getY(), this.setPosition.getZ());
    }

    public abstract void composeAvatar(PacketWriting message);

    //endregion



    public boolean isIdle() {
        return idle;
    }

    public void setIdle(boolean idle) {
        this.idle = idle;
    }

    public EventHandler getEventHandler() {
        return this.eventHandler;
    }

    @Override
    public String toString() {
        return "[ " + this.getType() +"] [ " + position.getX() + ", " + position.getY() +
                " ]" + "set[ " + setPosition.getX() + ", " + setPosition.getY() + " ]";
    }

    @Override
    public double getAditionalHeight() {
        return aditionalHeight;
    }

    @Override
    public double setAditionalHeight(double height) {
        return aditionalHeight = height;
    }

    @Override
    public int getEffectId() {
        return effectId;
    }

    @Override
    public void setEffectId(int effectId) {
        this.effectId = effectId;
    }

    public synchronized void dispose(){
        this.room = null;
        this.position = null;
        this.setPosition = null;
        this.statusses.dispose();
        this.statusses = null;
        this.path = null;
        this.eventHandler.dispose();
        this.eventHandler = null;
    }
}
