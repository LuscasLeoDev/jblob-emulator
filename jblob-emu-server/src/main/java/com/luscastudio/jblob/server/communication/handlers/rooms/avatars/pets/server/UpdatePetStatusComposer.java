//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 13/04/2017 at 11:17.
 */
public class UpdatePetStatusComposer extends MessageComposer {

    @Override
    public String id() {
        return "UpdatePetStatusMessageComposer";
    }

    public UpdatePetStatusComposer(int id, int someInt, boolean canBreed, boolean someBool, boolean isDead, boolean otherSomeBool) {
        message.putInt(id);
        message.putInt(someInt);
        message.putBool(canBreed);
        message.putBool(someBool);
        message.putBool(isDead);
        message.putBool(otherSomeBool);
    }
}
