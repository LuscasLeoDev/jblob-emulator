package com.luscastudio.jblob.server.game.habbohotel.catalog.bundles;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.notification.server.BroadcastMessageAlertComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.EnforceCategoryUpdateComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomForwardComposer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogDeal;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogDealItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomType;
import com.luscastudio.jblob.server.game.habbohotel.rooms.models.RoomModel;

import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 16/02/2017 at 19:22.
 */
public class CatalogBundleDeal extends CatalogDeal {
    private RoomBundle bundle;
    private Map<Integer, List<BundleFurniProperties>> bundleItems;
    public CatalogBundleDeal(int id, String name, int pageId, int creditsCost, int currencyCost, int currencyId, boolean offerEnabled, boolean allowsGift, boolean isForRent, int clubLevel, String custom, int offerId) throws Exception {
        super(id, name, pageId, creditsCost, currencyCost, currencyId, offerEnabled, allowsGift, isForRent, clubLevel, custom, offerId);

        if(!NumberHelper.isInteger(custom)) {
            BLogger.warn("Invalid bundle room ID on deal #" + this.getId(), this.getClass());
            throw new Exception("Invalid bundle room ID on deal #" + this.getId());
        }

        int bundleId = Integer.parseInt(custom);

        this.bundle = JBlob.getGame().getCatalogManager().getBundle(bundleId);
        if(this.bundle == null) {
            BLogger.warn("Bundle data is null on deal #" + this.getId(), this.getClass());
            throw new Exception("Bundle data is null on deal #" + this.getId());
        }

        this.bundleItems = BCollect.newMap();
        this.parseBundleItems();
    }

    private void parseBundleItems() {
        this.bundleItems.clear();

        for (BundleFurniProperties item : bundle.getBundleItems().values()) {
            if(!this.bundleItems.containsKey(item.getBase().getId()))
                this.bundleItems.put(item.getBase().getId(), BCollect.newList());

            this.bundleItems.get(item.getBase().getId()).add(item);
        }
    }

    @Override
    public boolean tryPurchase(HabboAvatar avatar, String pageExtradata, int amount, List<MessageComposer> resultComposers) {


        RoomModel model = JBlob.getGame().getModelManager().getModel(bundle.getModelName());
        if(model == null){
            avatar.getSession().sendMessage(new BroadcastMessageAlertComposer("<b>Error!</b>\nThe Room Model for this bundle was not found!\nContact some staff about this problem!"));
            BLogger.warn("Room Model '" + bundle.getModelName() + "' was not found for bundle #" + bundle.getId(), this.getClass());
            return false;
        }

        String name = bundle.getName().replace("%username%", avatar.getUsername());
        String desc = bundle.getDescription().replace("%username%", avatar.getUsername());
        RoomProperties roomData = JBlob.getGame().getRoomManager().createRoom(name, desc, avatar.getId(), bundle.getModelName(), bundle.getLockType(), bundle.getPassword(), bundle.getMaxUsers(), bundle.getTradeType(), 0, bundle.getWallData(), bundle.getFloorData(), bundle.getLandscapeData(), RoomType.PRIVATE, bundle.getAllowPets(), bundle.getAllowPetsEating(), bundle.getHideWall(), bundle.getWallThickness(), bundle.getFloorThickness(), bundle.getAllowWalkTrough(), bundle.getWhoCanMute(), bundle.getWhoCanKick(), bundle.getWhoCanBan(), bundle.getChatMode(), bundle.getChatSize(), bundle.getChatSpeed(), bundle.getChatExtraFlood(), bundle.getChatDistance(), "", "", "", model);

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            String query = "INSERT INTO avatars_items_data (owner_id, user_id, room_id, base_id, extradata, group_id, is_rare, x, y, z, rotation, wall_coordinate) VALUES ";
            {
                boolean first = true;
                for (BundleFurniProperties items : bundle.getBundleItems().values()) {
                    if(!first)
                        query += ", ";

                    query += "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    first = false;
                }
            }


            DBConnPrepare prepare = reactor.prepare(query, true);

            {
                boolean first = true;
                int i = 0;
                for (BundleFurniProperties item : bundle.getBundleItems().values()) {

                    prepare.setInt(i * 12 + 1, avatar.getId());
                    prepare.setInt(i * 12 + 2, avatar.getId());
                    prepare.setInt(i * 12 + 3, roomData.getId());
                    prepare.setInt(i * 12 + 4, item.getBase().getId());
                    prepare.setString(i * 12 + 5, item.getExtradata());
                    prepare.setInt(i * 12 + 6, 0);
                    prepare.setInt(i * 12 + 7, item.isRare() ? 1 : 0);
                    prepare.setInt(i * 12 + 8, item.getX());
                    prepare.setInt(i * 12 + 9, item.getY());
                    prepare.setString(i * 12 + 10, NumberHelper.doubleToString(item.getZ()));
                    prepare.setInt(i * 12 + 11, item.getRotation());
                    prepare.setString(i * 12 + 12, item.getWallCoordinate());
                    i++;
                }
            }

            prepare.runInsertKeys();

            resultComposers.add(new RoomForwardComposer(roomData.getId()));
            //resultComposers.add(new EnforceCategoryUpdateComposer(roomData.getId()));
        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }

        return super.tryPurchase(avatar, pageExtradata, amount, resultComposers);
    }

    @Override
    public void parsePacket(PacketWriting message) {
        message.putInt(this.getId());
        message.putString(this.getName());
        message.putBool(this.isForRent());

        message.putInt(this.getCreditsCost());
        message.putInt(this.getCurrencyCost());
        message.putInt(this.getCurrencyId());

        message.putBool(this.isAllowsGift());

        //region Writing Deal Items
        message.putInt(this.getItems().size() + this.bundleItems.size());

        for (List<BundleFurniProperties> bundleItemList : this.bundleItems.values()) {
            BundleFurniProperties bundleItem = bundleItemList.get(0);
            message.putString(bundleItem.getBase().getType());

            message.putInt(bundleItem.getBase().getSpriteId());
            message.putString(""); //Extradata
            message.putInt(bundleItemList.size());
            message.putBool(false); //Limited
        }

        for (CatalogDealItem item : this.getItems().values()) {

            message.putString(item.getType());

            item.parsePageComposer(message);
        }

        message.putInt(this.getClubLevel());
        message.putBool(this.isOfferEnabled());
    }
}
