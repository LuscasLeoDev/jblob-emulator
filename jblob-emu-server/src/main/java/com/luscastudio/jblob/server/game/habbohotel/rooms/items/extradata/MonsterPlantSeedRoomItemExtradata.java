package com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata;

import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.MonsterPlantSeedMessageParser;

/**
 * Created by Lucas on 06/02/2017 at 19:04.
 */
public class MonsterPlantSeedRoomItemExtradata extends SimpleRoomItemExtradata {

    private FurniProperties properties;
    private MonsterPlantSeedMessageParser messageParser;

    private int rarityLevel;
    private String state;

    public MonsterPlantSeedRoomItemExtradata(String extradata) {
        super(extradata);

        int i = 0;
        this.rarityLevel = 0;
        this.state = "";
        for(String s : extradata.split("\t")){
            switch(i){
                case 0:
                    if(!NumberHelper.isInteger(s))
                        this.rarityLevel = 0;
                    else
                        this.rarityLevel = Integer.parseInt(s);
                    break;

                case 1:
                    this.state = s;
                    break;
            }
        }
    }

    @Override
    public void init(FurniProperties properties) {
        this.properties = properties;
        this.messageParser = new MonsterPlantSeedMessageParser(this);
    }

    @Override
    public MonsterPlantSeedMessageParser getMessageParser() {
        return messageParser;
    }

    public int getRarityLevel() {
        return NumberHelper.isInteger(this.getExtradata()) ? Integer.parseInt(this.getExtradata()) : 0;
    }

    @Override
    public String getExtradataToDb() {
        return this.rarityLevel + "\t" + this.state;
    }

    public String getState() {
        return state;
    }
}