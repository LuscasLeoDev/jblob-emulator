package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.conditions;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.HideWiredConfigComposer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.WiredSaveData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.ConditionWiredBox;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * Created by Lucas on 24/02/2017 at 01:51.
 */
public class ConditionFurniHasAvatar extends ConditionWiredBox {
    private Map<Integer, IRoomItem> selectedItems;

    public ConditionFurniHasAvatar(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
        this.selectedItems = BCollect.newMap();
    }

    @Override
    public void saveWiredData(PlayerSession session, Collection<Integer> integerList, String stringData, Collection<Integer> selectedFurniList, int someInt) {
        session.sendMessage(new HideWiredConfigComposer());
        this.selectedItems = this.generateSelectedItems(selectedFurniList);
        this.getExtradata().save(new WiredSaveData(null, this.selectedItems.keySet(), null, null));
        this.room.getRoomItemHandlerService().saveItem(this);
    }

    @Override
    public boolean validateCase(IRoomAvatar avatar, IRoomItem item) {
        for (IRoomItem iRoomItem : this.selectedItems.values()) {
            if(this.room.getRoomMap().getRoomAvatars(iRoomItem.getPosition()).size() == 0)
                return false;
        }
        this.getExtradata().toggle();
        this.room.getRoomItemHandlerService().updateItem(this, false);
        return true;
    }

    @Override
    public int getWiredCode() {
        return 7;
    }

    @Override
    public boolean requireAvatar() {
        return false;
    }

    @Override
    public boolean requireTriggeredItem() {
        return false;
    }

    @Override
    public Collection<IRoomItem> getSelectedItems() {
        return this.selectedItems.values();
    }

    @Override
    public String getStringData() {
        return "";
    }

    @Override
    public Collection<Integer> getIntegersData() {
        return Collections.emptyList();
    }

    @Override
    public Collection<IRoomItem> getIncompatibleItems() {
        return Collections.emptyList();
    }

    @Override
    protected void loadWiredData() {
        WiredSaveData saveData = this.getExtradata().getSavedData();
        if(saveData == null)
            return;

        this.selectedItems = this.generateSelectedItems(saveData.getItemsId());
    }
}
