package com.luscastudio.jblob.server.communication.handlers.rooms.items;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Lucas on 10/10/2016.
 */

public class ItemSerializer {
    public static void serializeFloorItem(PacketWriting message, IRoomItem item) {
        message.putInt(item.getId());
        message.putInt(item.getBase().getSpriteId());
        message.putInt(item.getPosition().getX());
        message.putInt(item.getPosition().getY());
        message.putInt(item.getPosition().getRotation());
        message.putString(NumberHelper.doubleToString(item.getPosition().getZ()));
        message.putString(NumberHelper.doubleToString(item.getHeight()));

        message.putInt(1); //Unknown

        message.putInt(item.getMessageParser().getCode());
        item.getMessageParser().parse(message);

        message.putInt(-1); //unknown
        message.putInt(item.getUseButtonCode()); //unknown
        message.putInt(item.getOwnerId()); //unknown

        if (item.getBase().getSpriteId() < 0)
            message.putString(item.getBase().getCustom());
    }

    public static void serializeWallItem(PacketWriting message, IRoomItem roomItem) {
        message.putString(String.valueOf(roomItem.getId()));
        message.putInt(roomItem.getBase().getSpriteId());
        message.putString(roomItem.getWallCoordinate().toString());

        roomItem.getMessageParser().parse(message);

        message.putInt(-1); //Time to Expire

        message.putInt(roomItem.getUseButtonCode());
        message.putInt(roomItem.getOwnerId());
    }

    public static void serializeInventoryFurni(PacketWriting message, FurniProperties furni) {

        message.putInt(furni.getId()); // ID
        message.putString(furni.getBase().getType().toUpperCase());
        message.putInt(furni.getId()); // ID Again?
        message.putInt(furni.getBase().getSpriteId());
        message.putInt(furni.getBase().getInventoryCode());

        message.putInt(furni.getExtradata().getMessageParser().getCode());
        furni.getExtradata().getMessageParser().parse(message);
//todo: add these bools
        message.putBool(furni.getBase().isCanRecycle()); // Allow recycle
        message.putBool(furni.getBase().isCanTrade()); // Allow trade
        message.putBool(furni.getBase().isCanInventoryStack()); // Stack items of the same type
        message.putBool(furni.isRare()); // Is rare?

        message.putInt(-1); // Rentable expiration
        message.putBool(false); //Is Rentable
        message.putInt(-1); // Room id

        if (furni.getBase().getType().toLowerCase().equals("s")) {
            message.putString("");
            message.putInt(0);
        }
    }

    public static void serializeInventoryPet(PacketWriting message, PetData petData) {
        message.putInt(petData.getId());
        message.putString(petData.getName());
        message.putInt(petData.getPetBase().getPetType());
        message.putInt(petData.getRaceId());
        message.putString(petData.getColor() + " " + petData.getCustom().toPacketString());

        message.putInt(petData.getRaceId()); //maybe race name id

        petData.getCustom().serializeCustom(message);


        message.putInt(petData.getLevel());
    }

    public static <T> void serializeItemsCollections(Collection<T> objects, int limitPerPages, SendingComposerDelegate<T> delegate) {


        List<T> items = BCollect.newList();

        Iterator<T> inventoryIt = objects.iterator();

        int i = 0;

        int pages = Math.max(1, (int) Math.ceil(((double) objects.size() / limitPerPages)));

        while (inventoryIt.hasNext()) {
            T prop = inventoryIt.next();

            if (items.size() < limitPerPages)
                items.add(prop);
            else {
                delegate.sendMessage(pages, i, items);
                i++;
                items.clear();
                items.add(prop);
            }
        }
        delegate.sendMessage(pages, i, items);
        i++;
        items.clear();
    }

    public interface SendingComposerDelegate<T> {
        void sendMessage(int pageCount, int pageIndex, Collection<T> objects);
    }
}
