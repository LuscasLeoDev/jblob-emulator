package com.luscastudio.jblob.server.game.habbohotel.rooms;

/**
 * Created by Lucas on 20/01/2017 at 20:15.
 */
public class RoomEnterErrorFuture {

    private int errorCode;
    private String message;

    public RoomEnterErrorFuture(){
        this.errorCode = 0;
        this.message = "";
    }

    public RoomEnterErrorFuture(int errorCode, String message){
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }

    public boolean success() {
        return this.errorCode == 0;
    }
}
