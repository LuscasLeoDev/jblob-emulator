package com.luscastudio.jblob.server.communication.handlers.player.server;

/**
 * Created by Lucas on 10/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;

public class UserRightsComposer extends MessageComposer {
    @Override
    public String id() {
        return "UserRightsMessageComposer";
    }

    public UserRightsComposer(int rank, int clubLevel, boolean ambassador) {
        message.putInt(clubLevel);
        message.putInt(rank);
        message.putBool(ambassador);
    }
}
