package com.luscastudio.jblob.server.communication.handlers.player.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;

/**
 * Created by Lucas on 01/10/2016.
 */
public class PlayerSessionDataComposer extends MessageComposer {
    public PlayerSessionDataComposer(HabboAvatar avatar) {

        message.putInt(avatar.getId());//id
        message.putString(avatar.getUsername());//Username
        message.putString(avatar.getFigure().toString());//Figure
        message.putString(avatar.getFigure().getGender());//GENDER
        message.putString(avatar.getMotto());//Motto

        message.putString("");//unknown
        message.putBool(false);//unknown

        message.putInt(avatar.getRespectCount());//respects
        message.putInt(avatar.getRespectsToGiveCount());//respects to give
        message.putInt(avatar.getRespectsToGiveToPetsCount());//respects to give to pets

        message.putBool(false);//friend stream

        message.putString(String.valueOf(avatar.getLastOnlineTimestamp()));//last online

        message.putBool(false);//can change name

        message.putBool(false);//unknown
    }

    @Override
    public String id() {
        return "UserObjectMessageComposer";
    }
}
