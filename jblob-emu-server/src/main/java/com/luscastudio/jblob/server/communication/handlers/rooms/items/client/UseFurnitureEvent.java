package com.luscastudio.jblob.server.communication.handlers.rooms.items.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;


/**
 * Created by Lucas on 10/10/2016.
 */

public class UseFurnitureEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int itemId = packet.getInt();
        int requestType = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();
        if (room == null)
            return;

        IRoomItem item = room.getRoomItemHandlerService().getRoomItemById(itemId);
        if (item != null)
            item.onPlayerTrigger(session, requestType);

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
