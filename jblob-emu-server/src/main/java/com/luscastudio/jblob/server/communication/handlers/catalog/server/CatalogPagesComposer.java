package com.luscastudio.jblob.server.communication.handlers.catalog.server;

/**
 * Created by Lucas on 28/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogDeal;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogNode;

import java.util.Collection;

public class CatalogPagesComposer extends MessageComposer {
    @Override
    public String id() {
        return "CatalogIndexMessageComposer";
    }

    public CatalogPagesComposer(String catalogType, Collection<CatalogNode> nodes, boolean open) {

        //Write the root page [That might is not showed on screen]

        message.putBool(false);//Visible
        message.putInt(0); //Icon
        message.putInt(-1); //Id
        message.putString("root"); //External access link
        message.putString(""); //Title
        message.putInt(0); //Offers count

        message.putInt(nodes.size()); //Pages count

        for(CatalogNode node : nodes)
            parseNode(node);

        message.putBool(open);
        message.putString(catalogType);

    }

    private void parseNode(CatalogNode node){
        message.putBool(true);//Visible
        message.putInt(node.getIcon()); //Icon
        message.putInt(node.isEnabled() ? node.getId() : -1); //Id
        message.putString(node.getLink()); //External access link
        message.putString(node.getTitle()); //Title

        message.putInt(node.getDeals().size()); //Pages count

        for(CatalogDeal deal : node.getDeals().values()){
            message.putInt(deal.getOfferId());
        }

        message.putInt(node.getChildren().size()); //Offers count

        for(CatalogNode child : node.getChildren().values())
            parseNode(child);

    }
}
