package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.horse;

import com.google.common.reflect.TypeToken;
import com.luscastudio.jblob.api.utils.json.JSONUtils;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.api.utils.time.DateTimeUtils;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryUpdateRequestComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server.AvatarEffectComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server.RoomUsersComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.RemoveFloorItemComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.SlideObjectBundleComposer;
import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.events.EventDelegate;
import com.luscastudio.jblob.server.events.list.AvatarRideRemoveEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.custom.PetCustomPart;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.IPetAppliableItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.RoomPetAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.PetItemRoomItem;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.concurrent.TimeUnit;

/**
 * Created by Lucas on 18/02/2017 at 16:02.
 */
public class HorseRoomPetAvatar extends RoomPetAvatar implements IPetAppliableItem{
    private String ownerName;
    private HorseData horseData;

    private IRoomAvatar ridingAvatar;
    private EventDelegate rideDelegate;

    public HorseRoomPetAvatar(Room room, int virtualId, PetData petData) {
        super(room, virtualId, petData);
        this.ownerName = "";
        this.ridingAvatar = null;
        this.horseData = JSONUtils.fromJson(petData.getExtradata(), new TypeToken<HorseData>(){}.getType());
        if(horseData == null)
            this.horseData = new HorseData();
    }

    @Override
    public void onRespect(HabboAvatar avatar) {

    }

    @Override
    public void onAvatarSay(HabboAvatar avatar, String message) {

    }

    @Override
    public void onEnter() {

    }

    @Override
    public void onLeave() {
        if(this.ridingAvatar != null && this.horseData.getSaddleId() != 0){
            PetCustomPart part = this.getPetData().getCustom().getPart(4);
            if(part != null)
                part.setPartId(this.horseData.getSaddlePartId());
            else
                this.getPetData().getCustom().set(4, new PetCustomPart(3, this.horseData.getSaddlePartId(), 0));

            this.getPetData().getCustom().generateResultForParser();
        }
    }

    @Override
    public String getMotto() {
        return null;
    }

    @Override
    public String getFigureString() {
        return super.getFigureString();
    }

    @Override
    public void composeAvatar(PacketWriting message) {
        message.putInt(this.getPetData().getPetBase().getPetType());
        message.putInt(this.getPetData().getOwnerId());
        message.putString(this.getOwnerName());
        message.putInt(this.getPetData().getRarityLevel()); //RarityLevel
        message.putBool(this.horseData.getSaddleId() != -1); // has saddle
        message.putBool(this.ridingAvatar != null); // is someone ridding
        message.putBool(false); //can breed?
        message.putBool(false); //?
        message.putBool(false); //
        message.putBool(false);
        message.putInt(this.getPetData().getLevel()); // Level
        message.putString("");
    }

    @Override
    public void composePetAvatarInformation(PacketWriting message) {

        message.putInt(this.getId()); //Id
        message.putString(this.getName()); //Name
        message.putInt(this.getPetData().getLevel()); //Level
        message.putInt(this.getPetData().getPetBase().getMaxLevels()); //Max Level

        message.putInt(this.getPetData().getExperience()); //Experience
        message.putInt(this.getPetData().getMaxExperience()); //Max Experience

        message.putInt(this.getPetData().getEnergy()); //Energy
        message.putInt(this.getPetData().getMaxEnergy()); //Max Energy

        //its happiness, dumb
        message.putInt(this.getPetData().getHappiness()); //Happiness
        message.putInt(this.getPetData().getMaxHappiness()); //Max Happiness

        message.putInt(this.getPetData().getRespects()); //Pet Respects
        message.putInt(this.getPetData().getOwnerId()); //Owner Id

        message.putInt((int) TimeUnit.SECONDS.toDays((long)(DateTimeUtils.getUnixTimestampInt() - this.getPetData().getCreateTimestamp()))); //Age in days

        message.putString(this.getOwnerName()); //Owner name

        message.putInt(this.getPetData().getPetBase().getRaceId()); // Pet Race

        message.putBool(this.horseData.getSaddleId() != -1); //Has Saddle
        message.putBool(this.ridingAvatar != null); //Is Ridding

        message.putInt(5);//Horse jump data
        message.putInt(1);
        message.putInt(2);
        message.putInt(3);
        message.putInt(4);
        message.putInt(5);

        message.putInt(this.horseData.anyoneCanRide() ? 1 : -1); //Monster Plant Grown level / Horse Anyone car ride on
        message.putBool(false);
        message.putBool(false); //?
        message.putBool(false); //Monster Plant Is Dead?
        message.putInt(0); //Monster Plant RarityLevel
        message.putInt(0); //Monster Plant Total Life Time
        message.putInt(0); //Monster Plant Life Time
        message.putInt(0); //Monster Plant Time until grow up

        message.putBool(true);
    }

    @Override
    public void onPlace(HabboAvatar placerAvatar) {

    }

    @Override
    public void onRemove(HabboAvatar removerAvatar) {

    }

    public String getOwnerName() {
        if(this.ownerName != null)
            return this.ownerName;

        IAvatarDataCache cache;
        return this.ownerName = (cache = JBlob.getGame().getUserCacheManager().getUserCache(this.petData.getOwnerId())) != null ? cache.getUsername() : "unknown_" + this.petData.getOwnerId();
    }

    @Override
    public synchronized void dispose() {
        this.ridingAvatar = null;
        if(this.rideDelegate != null)
            this.rideDelegate.cancel();
        this.rideDelegate = null;
        super.dispose();
    }

    @Override
    public void applyItem(PetItemRoomItem item) {
        String[] spl = item.getApplyData().split(":");
        if(spl.length < 3)
            return;

        String petType = spl[0];
        String applyType = spl[1];
        String value = spl[2];

        if(!petType.equals(String.valueOf(this.getPetData().getPetBase().getPetType())))
            return;

        switch(applyType){

            default:
                return;
            case "race":

                if(!NumberHelper.isInteger(value))
                    return;

                //It's fucking confuse, but u can understand Looking for the Horse Assets in its swf source
                int raceBreedIndex;
                if(this.getPetData().getRaceId() >= 61)
                    raceBreedIndex = Math.abs((this.getPetData().getRaceId() - 11) % 4 - 2);
                else
                    raceBreedIndex = Math.abs(this.getPetData().getRaceId() % 4 - 2);

                int typeToApply = Integer.parseInt(value);

                this.getPetData().setRaceId(typeToApply + raceBreedIndex);
                JBlob.getGame().getRoomItemFactory().hideFurniProperties(item.getProperties().getId());

                break;

            case "hairstyle":
            case "hairdye":
                spl = value.split(" ");
                if(spl.length < 2)
                    return;
                String[] partTypes = spl[0].split(",");
                String[] partIds;
                String[] partColors;
                if(spl.length > 2){
                    partIds = spl[1].split(",");
                    partColors = spl[2].split(",");
                } else {
                    partIds = null;
                    partColors = spl[1].split(",");
                }


                for(int i = 0; i < partTypes.length; i++){
                    if(!NumberHelper.isInteger(partTypes[i]))
                        continue;

                    if(partColors.length <= i)
                        continue;

                    if(!NumberHelper.isInteger(partColors[i]))
                        continue;

                     int partType = Integer.parseInt(partTypes[i]);
                     int partColor = Integer.parseInt(partColors[i]);
                     int partId = -1;
                     if(partIds != null && partIds.length > i && NumberHelper.isInteger(partIds[i]))
                         partId = Integer.parseInt(partIds[i]);

                     PetCustomPart part = this.getPetData().getCustom().getPart(partType);
                     if(part != null){
                         if(partIds != null)
                             part.setPartId(partId);
                         else
                            part.setColorId(partColor);
                     } else {
                         this.getPetData().getCustom().set(partType, new PetCustomPart(partType, partId, partColor));
                     }
                }

                JBlob.getGame().getRoomItemFactory().hideFurniProperties(item.getProperties().getId());
                this.getPetData().getCustom().generateResultForParser();
                break;



            case "saddle":

                spl = value.split(",");
                if(spl.length < 2)
                    return;
                if(!NumberHelper.isInteger(spl[0]) || !NumberHelper.isInteger(spl[1]))
                    return;

                if(this.ridingAvatar != null)
                    return;

                this.removeSaddle();

                PetCustomPart part = this.getPetData().getCustom().getPart(4);
                int partId = Integer.parseInt(spl[0]);
                int effectId = Integer.parseInt(spl[1]);
                if(part == null)
                    this.getPetData().getCustom().set(4, new PetCustomPart(4, partId, -1));
                else
                    part.setPartId(partId);

                this.horseData.setSaddleId(item.getProperties().getId());
                this.horseData.setSaddleEffectId(effectId);
                this.horseData.setSaddlePartId(partId);

                this.getPetData().getCustom().generateResultForParser();
                this.saveHorseData();
                JBlob.getGame().getRoomItemFactory().hideFurniProperties(item.getProperties().getId());

            break;
        }
        this.getRoom().sendMessage(new RoomUsersComposer(this));
        this.getRoom().getRoomAvatarService().saveAvatar(this);
        item.getExtradata().setExtradata("1");
        this.getRoom().sendMessage(new SlideObjectBundleComposer(item.getPosition(), item.getPosition(), 0).addMoveItem(item.getProperties().getId(), item.getPosition().getZ(), 50));
        this.getRoom().sendMessage(new RemoveFloorItemComposer(item.getProperties().getId(), 0, false, 500));
        this.getRoom().getRoomItemHandlerService().removeRoomItem(item);
    }

    public void removeSaddle() {
        if(this.ridingAvatar != null)
            return;

        if(this.horseData.getSaddleId() != -1){
            FurniProperties oldSaddle = JBlob.getGame().getRoomItemFactory().getFurniProperties(this.horseData.getSaddleId());
            if(oldSaddle != null){
                JBlob.getGame().getRoomItemFactory().showFurniProperties(oldSaddle.getId());
                PlayerSession ownerSession = JBlob.getGame().getSessionManager().getSession(oldSaddle.getOwnerId());
                if(ownerSession != null){
                    ownerSession.getAvatar().getInventory().addRoomItem(oldSaddle);
                    ownerSession.sendQueueMessage(new FurniListNotificationComposer().add(FurniListNotificationComposer.FURNI, oldSaddle.getId()));
                    ownerSession.sendMessage(new InventoryUpdateRequestComposer());
                }
            }

            PetCustomPart part = this.getPetData().getCustom().getPart(4);
            if(part != null)
                part.setPartId(0);
            else
                this.getPetData().getCustom().set(4, new PetCustomPart(4, 0, 0));

            this.getPetData().getCustom().generateResultForParser();
            this.horseData.setSaddleId(-1);
            this.horseData.setSaddleEffectId(0);
            this.horseData.setSaddlePartId(0);

            this.getRoom().sendMessage(new RoomUsersComposer(this));
            this.saveHorseData();
            this.getRoom().getRoomAvatarService().saveAvatar(this);
        }
    }

    public boolean addRidingAvatar(IRoomAvatar avatar) {
        if(this.ridingAvatar != null)
            return false;

        this.ridingAvatar = avatar;
        this.rideDelegate = avatar.getEventHandler().on("avatar.leave.room avatar.walk.on.square avatar.stand.on.square avatar.teleport.to avatar.on.ride.horse", this::onRideEvent);

        this.getRoom().getRoomMap().removeAvatarFromMap(this, this.getSetPosition().getPoint());

        PetCustomPart part = this.getPetData().getCustom().getPart(4);
        if(part != null)
            part.setPartId(0);

        this.getPetData().getCustom().generateResultForParser();
        this.getRoom().sendMessage(new RoomUsersComposer(this));
        return true;

    }

    public void removeRidingAvatar() {
        if(this.rideDelegate != null)
            this.rideDelegate.cancel();

        if(this.ridingAvatar.getPath().isWalking()){
            this.setPosition(this.ridingAvatar.getSetPosition().getX(), this.ridingAvatar.getSetPosition().getY(), this.ridingAvatar.getSetPosition().getZ());
            //this.setPosition(this.ridingAvatar.getPosition().getX(), this.ridingAvatar.getPosition().getY(), this.ridingAvatar.getPosition().getZ(), this.ridingAvatar.getPosition().getRotation());

            this.getStatusses().remove("mv");
            this.updateNeeded(true);
        }

        this.ridingAvatar.getEventHandler().fireEvent("avatar.ride.remove", new AvatarRideRemoveEventArgs());

        this.rideDelegate = null;
        this.ridingAvatar = null;

        this.getRoom().getRoomMap().addAvatarToMap(this, this.getSetPosition().getPoint());

        PetCustomPart part = this.getPetData().getCustom().getPart(4);
        if(part != null)
            part.setPartId(this.getHorseData().getSaddlePartId());
        else
            this.getPetData().getCustom().set(4, new PetCustomPart(4, this.getHorseData().getSaddlePartId(), 0));

        this.getPetData().getCustom().generateResultForParser();
        this.getRoom().sendMessage(new RoomUsersComposer(this));


    }

    public IRoomAvatar getRidingAvatar() {
        return ridingAvatar;
    }

    public HorseData getHorseData() {
        return horseData;
    }

    private void onRideEvent(EventDelegate eventDelegate, EventArgs args) {
        switch(args.getEventKey()){

            case "avatar.on.ride.horse":
                args.cancel();
                break;

            case "avatar.teleport.to":

                this.getRoom().sendMessage(new AvatarEffectComposer(this.ridingAvatar.getVirtualId(), this.horseData.getSaddleEffectId(), 1001));


            case "avatar.stand.on.square":
            case "avatar.walk.on.square":



                this.setPosition(this.ridingAvatar.getPosition().getX(), this.ridingAvatar.getPosition().getY(), this.ridingAvatar.getPosition().getZ(), this.ridingAvatar.getPosition().getRotation());

                this.setSetPosition(this.ridingAvatar.getSetPosition().getX(), this.ridingAvatar.getSetPosition().getY(), this.ridingAvatar.getSetPosition().getZ());


                this.getPosition().setHeadRotation(this.ridingAvatar.getPosition().getHeadRotation());



                if(this.ridingAvatar.getPath().isWalking())
                    this.getStatusses().set("mv", this.getSetPosition().getX() + "," + this.getSetPosition().getY() + "," + NumberHelper.doubleToString(this.getSetPosition().getZ()));
                else
                    this.getStatusses().remove("mv");

                this.updateNeeded(true);
                break;

            case "avatar.leave.room":
                this.removeRidingAvatar();
                break;
        }
    }

    @Override
    public void save(DBConnReactor reactor) {
        super.save(reactor);
    }

    public void saveHorseData() {
        this.getPetData().setExtradata(JSONUtils.toJson(this.horseData, new TypeToken<HorseData>(){}.getType()));
    }
}
