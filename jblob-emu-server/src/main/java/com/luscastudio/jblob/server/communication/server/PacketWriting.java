package com.luscastudio.jblob.server.communication.server;

import com.luscastudio.jblob.api.utils.collect.BCollect;

import java.util.List;

/**
 * Created by Lucas on 01/10/2016.
 */
public class PacketWriting {

    public List<PacketWritingValue> data;


    public PacketWriting() {
        this.data = BCollect.newList();
    }

    public PacketWriting putDouble(double val) {
        data.add(new PacketWritingValue(val));
        return this;
    }

    public PacketWriting putString(String val) {
        data.add(new PacketWritingValue(val));
        return this;
    }

    public PacketWriting putBool(boolean val) {
        data.add(new PacketWritingValue(val));
        return this;
    }

    public PacketWriting putByte(byte val) {
        data.add(new PacketWritingValue(val));
        return this;
    }

    public PacketWriting putByte(Integer val) {
        data.add(new PacketWritingValue(val.byteValue()));
        return this;
    }

    public PacketWriting putShort(short val) {
        data.add(new PacketWritingValue(val));
        return this;
    }

    public PacketWriting putShort(int val) {
        data.add(new PacketWritingValue(((short) val)));
        return this;
    }

    public PacketWriting putInt(int val) {
        data.add(new PacketWritingValue(val));
        return this;
    }


    public enum PacketWritingType {
        BYTE,
        INT,
        SHORT,
        STRING,
        DOUBLE,
        BOOL
    }


}
