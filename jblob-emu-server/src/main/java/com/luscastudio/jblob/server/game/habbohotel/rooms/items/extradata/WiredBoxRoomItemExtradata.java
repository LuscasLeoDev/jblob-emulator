package com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata;

import com.google.common.reflect.TypeToken;
import com.luscastudio.jblob.api.utils.json.JSONUtils;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.WiredSaveData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.IItemMessageParser;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.SimpleItemMessageParser;

/**
 * Created by Lucas on 15/02/2017 at 17:46.
 */
public class WiredBoxRoomItemExtradata implements IRoomItemExtradata {

    private FurniProperties properties;
    private String state;
    private String jsonDataToDb;

    private SimpleItemMessageParser messageParser;

    public WiredBoxRoomItemExtradata(String extradata){
        this.jsonDataToDb = extradata;
    }

    @Override
    public String getExtradata() {
        return state;
    }

    @Override
    public String getExtradataToDb() {
        return jsonDataToDb;
    }

    @Override
    public void setExtradata(String extradata) {
        this.jsonDataToDb = extradata;
    }

    @Override
    public void init(FurniProperties properties) {
        this.properties = properties;
        this.state = "0";
        this.messageParser = new SimpleItemMessageParser(this.properties);
    }

    @Override
    public IItemMessageParser getMessageParser() {
        return this.messageParser;
    }

    public void toggle() {
        if(this.state.equals("0"))
            this.state = "1";
        else
            this.state = "0";
    }

    public WiredSaveData getSavedData(){
        return JSONUtils.fromJson(this.jsonDataToDb, new TypeToken<WiredSaveData>(){}.getType());
    }

    public void save(WiredSaveData data){
        this.jsonDataToDb = JSONUtils.toJson(data, new TypeToken<WiredSaveData>(){}.getType());
    }


    public void clear() {
        this.jsonDataToDb = "";
    }
}
