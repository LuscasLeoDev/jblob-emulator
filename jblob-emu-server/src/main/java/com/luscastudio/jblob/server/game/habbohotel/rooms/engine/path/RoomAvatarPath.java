package com.luscastudio.jblob.server.game.habbohotel.rooms.engine.path;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.engine.validator.IFloorValidator;

import java.util.List;

/**
 * Created by Lucas on 04/10/2016.
 */

public class RoomAvatarPath {

    private List<Point> path;
    private int point;
    private boolean recall;
    private boolean walking;
    private Point goal;
    private IFloorValidator validator;
    private IRoomAvatar avatar;
    private Room room;

    public RoomAvatarPath(IRoomAvatar avatar, Room room, IFloorValidator floorValidator) {
        path = BCollect.newList();
        this.point = 0;
        this.recall = false;
        this.goal = new Point(0, 0);
        this.validator = floorValidator;
    }

    public void setPath(List<Point> path) {
        for(Point p : this.path)
            p.dispose();

        this.path = path;
        this.point = 0;
    }

    public void clearPath() {
        this.path.clear();
        this.point = 0;
    }

    public boolean hasNex() {
        return path != null && path.size() > point;
    }

    public Point next() {
        return path.get(point++);
    }

    public boolean isRecall() {
        return recall;
    }

    public void recall(boolean recall) {
        this.recall = recall;
    }

    public Point getGoal() {
        return goal;
    }

    public void setGoal(Point goal) {
        this.goal = goal;
    }

    public boolean isWalking() {
        return walking;
    }

    public void setWalking(boolean walking) {
        this.walking = walking;
    }

    public IFloorValidator getValidator() {
        return validator;
    }

    public void addStep(Point Point) {
        this.path.add(Point);
    }
}
