package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor;

import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;

/**
 * Created by Lucas on 22/12/2016 at 21:02.
 */
public class GateRoomItem extends SimpleRoomItem {


    public GateRoomItem(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
    }

    @Override
    public boolean canContinuePath(IRoomAvatar avatar, Point from, boolean finalStep) {
        return avatar instanceof RoomPlayerAvatar && this.getExtradata().getExtradata().equals("1");
    }

    @Override
    public boolean canWalk(IRoomAvatar avatar) {
        return this.getExtradata().getExtradata().equals("1");
    }

    @Override
    public boolean canWalk(IRoomAvatar avatar, Point from) {
        return super.canWalk(avatar, from);
    }
}
