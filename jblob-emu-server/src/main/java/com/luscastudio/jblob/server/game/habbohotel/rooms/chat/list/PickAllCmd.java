package com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryUpdateRequestComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.client.FloorHeightMapComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.RemoveFloorItemComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.RemoveWallItemComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.HeightMapComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.IChatCommand;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.permissions.PermissionManagerPermissions;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;
import com.luscastudio.jblob.server.game.sessions.collections.PlayerSessionCollection;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 16/12/2016.
 */

public class PickAllCmd implements IChatCommand {
    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {

        if (!room.getProperties().avatarHasRight(session.getAvatar().getId(), PermissionManagerPermissions.ROOM_COMMAND_PICKALL))
            return false;

        PlayerSessionCollection sessionCollection = new PlayerSessionCollection();
        List<IRoomItem> items = BCollect.newList();

        if (room.getProperties().avatarHasRight(session.getAvatar().getId(), PermissionManagerPermissions.ROOM_OWNER_RIGHT)) {
            Map<Integer, List<IRoomItem>> itemsToUsers = room.getRoomItemHandlerService().removeAll();

            Iterator<Map.Entry<Integer, List<IRoomItem>>> iteratorUsers = itemsToUsers.entrySet().iterator();

            while (iteratorUsers.hasNext()){
                Map.Entry<Integer, List<IRoomItem>> itemsByUser = iteratorUsers.next();

                PlayerSession targetSession = JBlob.getGame().getSessionManager().getSession(itemsByUser.getKey());

                iteratorUsers.remove();

                items.addAll(itemsByUser.getValue());

                if(targetSession == null)
                    continue;

                Iterator<IRoomItem> iteratorItems = itemsByUser.getValue().iterator();
                FurniListNotificationComposer notificationComposer = new FurniListNotificationComposer();
                while (iteratorItems.hasNext()){
                    IRoomItem item = iteratorItems.next();

                    iteratorItems.remove();

                    targetSession.getAvatar().getInventory().addRoomItem(item.getProperties());
                    notificationComposer.add(FurniListNotificationComposer.FURNI, item.getProperties().getId());

                    room.getRoomMap().updateSqHeight(item.getAffectedTiles());
                }

                targetSession.sendMessage(new InventoryUpdateRequestComposer());
                targetSession.sendMessage(notificationComposer);
                sessionCollection.add(targetSession);
            }

        } else {
            items = room.getRoomItemHandlerService().removeUserItems(session.getAvatar().getId());
            Iterator<IRoomItem> itemIterator = items.iterator();
            FurniListNotificationComposer notificationComposer = new FurniListNotificationComposer();
            while (itemIterator.hasNext()) {
                IRoomItem item = itemIterator.next();
                session.getAvatar().getInventory().addRoomItem(item.getProperties());
                notificationComposer.add(FurniListNotificationComposer.FURNI, item.getProperties().getId());
                room.getRoomMap().updateSqHeight(item.getAffectedTiles());
            }
            session.sendMessage(notificationComposer);
            session.sendMessage(new InventoryUpdateRequestComposer());
            sessionCollection.add(session);
        }

        Iterator<IRoomItem> itemIterator = items.iterator();
        int delay = 0;
        int i = 0;
        while(itemIterator.hasNext()) {
            i++;
            if(i > 100) {
                delay += 500;
                i = 0;
            }
            IRoomItem item = itemIterator.next();
            if(item.getBase().getType().equals("i"))
                room.sendMessage(new RemoveWallItemComposer(item.getId(), item.getOwnerId()));
            else
                room.sendMessage(new RemoveFloorItemComposer(item.getId(), item.getOwnerId(), false, delay));

            item.dispose();
        }

        //room.sendQueueMessage(new HeightMapComposer(room.getRoomMap()));

        room.flush();
        sessionCollection.flush();
        items.clear();

        return true;
    }
}
