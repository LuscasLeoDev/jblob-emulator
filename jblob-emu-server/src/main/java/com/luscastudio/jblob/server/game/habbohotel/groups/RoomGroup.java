//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.groups;

/**
 * Created by Lucas on 07/04/2017 at 11:59.
 */
public class RoomGroup {
    private int id;
    private String name;
    private String description;
    private int roomId;
    private int ownerId;
    private int accessType;
    private int createdTimestamp;

    private String badgeData;
    private int colorOne;
    private int colorTwo;

    private boolean hasChat;
    private boolean hasForum;

    private RoomGroupMemberManager memberManager;

    public RoomGroup(int id, String name, String description, int roomId, int ownerId, int accessType, int createdTimestamp, String badgeData, int colorOne, int colorTwo, boolean hasChat, boolean hasForum) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.roomId = roomId;
        this.ownerId = ownerId;
        this.accessType = accessType;
        this.createdTimestamp = createdTimestamp;
        this.badgeData = badgeData;
        this.colorOne = colorOne;
        this.colorTwo = colorTwo;
        this.hasChat = hasChat;
        this.hasForum = hasForum;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public int getAccessType() {
        return accessType;
    }

    public void setAccessType(int accessType) {
        this.accessType = accessType;
    }

    public int getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(int createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public String getBadgeData() {
        return badgeData;
    }

    public void setBadgeData(String badgeData) {
        this.badgeData = badgeData;
    }

    public int getColorOne() {
        return colorOne;
    }

    public void setColorOne(int colorOne) {
        this.colorOne = colorOne;
    }

    public int getColorTwo() {
        return colorTwo;
    }

    public void setColorTwo(int colorTwo) {
        this.colorTwo = colorTwo;
    }

    public boolean isHasChat() {
        return hasChat;
    }

    public void setHasChat(boolean hasChat) {
        this.hasChat = hasChat;
    }

    public boolean isHasForum() {
        return hasForum;
    }

    public void setHasForum(boolean hasForum) {
        this.hasForum = hasForum;
    }
}
