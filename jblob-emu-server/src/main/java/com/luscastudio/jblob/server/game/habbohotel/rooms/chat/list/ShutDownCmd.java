package com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.IChatCommand;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.concurrent.TimeUnit;

/**
 * Created by Lucas on 19/01/2017 at 17:55.
 */
public class ShutDownCmd implements IChatCommand {
    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {

        int delay = Integer.parseInt(params.get(1, "0")) * 1000;

        //String message = params.get(3, true);

        JBlob.getGame().getProcessManager().run(JBlob::shutdown, Math.max(1, delay), TimeUnit.MILLISECONDS);

        return true;

    }
}
