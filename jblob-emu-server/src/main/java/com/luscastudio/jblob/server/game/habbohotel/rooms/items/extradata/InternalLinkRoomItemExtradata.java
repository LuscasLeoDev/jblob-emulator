package com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.IItemMessageParser;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.MapStuffDataParser;

import java.util.Map;

/**
 * Created by Lucas on 25/02/2017 at 21:27.
 */
public class InternalLinkRoomItemExtradata implements IRoomItemExtradata {

    private MapStuffDataParser parser;
    private FurniProperties properties;
    private Map<String, String> data;
    private String extradata;

    public InternalLinkRoomItemExtradata(String extradata){
        this.extradata = extradata;
        this.data = BCollect.newMap();
    }
    public void parse(Map<String, String> data) {
        this.data = data;
        this.parser.setStuffData(data);
    }

    @Override
    public String getExtradata() {
        return "";
    }

    private void parseExtradata() {
        String[] spl = this.extradata.split("\t");
        int len = spl.length;
        for (int i = 0; i < len && i + 1 < len; i+= 2) {
            this.data.put(spl[i], spl[i+1]);
        }
        this.parser.setStuffData(this.data);
    }

    @Override
    public String getExtradataToDb() {
        boolean f = true;
        String str = "";
        for (Map.Entry<String, String> entry : this.data.entrySet()) {
            if(!f)
                str += "\t";
            f = false;

            str += entry.getKey() + "\t" + entry.getValue();
        }

        return str;
    }

    @Override
    public void setExtradata(String extradata) {

    }

    @Override
    public void init(FurniProperties properties) {
        this.properties = properties;
        this.parser = new MapStuffDataParser();
        this.parseExtradata();
    }

    @Override
    public IItemMessageParser getMessageParser() {
        return parser;
    }

    public void toggleState() {
        String state = this.data.get("state");
        if(state == null){
            this.data.put("state", "1");
        } else {
            try{
                int stateInt = Integer.parseInt(this.data.get("state"));
                if(stateInt >= this.properties.getBase().getInteractionCount() - 1)
                    stateInt = 0;
                else
                    stateInt++;

                this.data.replace("state", String.valueOf(stateInt));
            } catch (Exception ignored){
                this.data.put("state", "0");
            }
        }
    }
}
