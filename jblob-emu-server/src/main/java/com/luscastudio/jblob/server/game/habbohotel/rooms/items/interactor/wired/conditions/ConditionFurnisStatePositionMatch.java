package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.conditions;

import com.google.common.reflect.TypeToken;
import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.engine.ObjectPosition;
import com.luscastudio.jblob.api.utils.json.JSONUtils;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.HideWiredConfigComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.WiredConditionConfigComposer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.WiredSaveData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.ConditionWiredBox;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.utils.SavedItemState;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 15/02/2017 at 23:22.
 */
public class ConditionFurnisStatePositionMatch extends ConditionWiredBox {

    private Map<Integer, IRoomItem> selectedRoomItems;
    private Map<Integer, SavedItemState> savedRoomItemStates;
    private List<Integer> integers;

    public ConditionFurnisStatePositionMatch(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
        this.selectedRoomItems = BCollect.newMap();
        this.savedRoomItemStates = BCollect.newMap();
        this.integers = BCollect.newList();
    }

    @Override
    public boolean validateCase(IRoomAvatar avatar, IRoomItem triggeredItem) {
        for (IRoomItem item : this.selectedRoomItems.values()) {
            if(!this.savedRoomItemStates.containsKey(item.getId()))
                continue;

            SavedItemState state = this.savedRoomItemStates.get(item.getId());

            if(this.checkRotation() && item.getPosition().getRotation() != state.getPosition().getRotation())
                return false;

            if(this.checkExtradata() && !item.getExtradata().getExtradata().equals(state.getExtradata()))
                return false;

            if(this.checkPosition() && (item.getPosition().getX() != state.getPosition().getX() || item.getPosition().getY() != state.getPosition().getY()))
                return false;
        }

        this.getExtradata().toggle();
        this.room.getRoomItemHandlerService().updateItem(this, false);
        return true;
    }

    @Override
    public int getWiredCode() {
        return 0;
    }

    @Override
    public boolean requireAvatar() {
        return false;
    }

    @Override
    public boolean requireTriggeredItem() {
        return false;
    }

    @Override
    public Collection<IRoomItem> getSelectedItems() {
        return this.selectedRoomItems.values();
    }

    @Override
    public String getStringData() {
        return "";
    }

    @Override
    public Collection<Integer> getIntegersData() {
        return this.integers;
    }

    @Override
    public Collection<IRoomItem> getIncompatibleItems() {
        return Collections.emptyList();
    }

    @Override
    protected void loadWiredData() {
        try{
            WiredSaveData saveData = this.getExtradata().getSavedData();
            if(saveData != null){
                this.savedRoomItemStates = JSONUtils.fromJson(saveData.getStringData(), new TypeToken<Map<Integer, SavedItemState>>(){}.getType());
                if(this.savedRoomItemStates == null){
                    this.savedRoomItemStates = BCollect.newMap();
                    this.selectedRoomItems = BCollect.newMap();
                } else {
                    this.selectedRoomItems = this.generateSelectedItems(saveData.getItemsId());
                }
                this.integers = saveData.getIntegerList();
            }
        } catch (Exception ignored){

        }
    }

    private boolean checkExtradata(){
        if(this.integers.size() == 0)
            return false;
        return this.integers.get(0) == 1;
    }

    private boolean checkRotation(){
        if(this.integers.size() <= 1)
            return false;
        return this.integers.get(1) == 1;
    }

    private boolean checkPosition(){
        if(this.integers.size() <= 2)
            return false;
        return this.integers.get(2) == 1;
    }


    @Override
    public void saveWiredData(PlayerSession session, Collection<Integer> integerList, String stringData, Collection<Integer> selectedFurniList, int someInt) {
        if(!this.room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_save_wired_config"))
            return;

        this.selectedRoomItems = this.generateSelectedItems(selectedFurniList);
        this.integers = BCollect.newList(integerList);
        this.savedRoomItemStates.clear();

        for (IRoomItem item : this.selectedRoomItems.values()) {
            SavedItemState savedData = new SavedItemState(new ObjectPosition(item.getPosition().getX(), item.getPosition().getY(),0, item.getPosition().getRotation()), item.getExtradata().getExtradata());
            this.savedRoomItemStates.put(item.getProperties().getId(), savedData);
        }

        String savedDataJson = JSONUtils.toJson(this.savedRoomItemStates, new TypeToken<Map<Integer, SavedItemState>>(){}.getType());

        this.getExtradata().save(new WiredSaveData(this.integers, this.selectedRoomItems.keySet(), 0, savedDataJson));
        this.room.getRoomItemHandlerService().saveItem(this);

        session.sendMessage(new HideWiredConfigComposer());
    }
}
