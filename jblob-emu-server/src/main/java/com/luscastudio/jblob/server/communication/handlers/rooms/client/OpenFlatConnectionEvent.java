package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.handshake.server.GenericErrorComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server.RoomControllerLevelComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server.RoomOwnerControllerComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.*;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomEnterErrorFuture;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.forwarding.ForwardProperties;
import com.luscastudio.jblob.server.game.permissions.PermissionManagerPermissions;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;
import com.luscastudio.jblob.server.manager.RoomManager;

/**
 * Created by Lucas on 03/10/2016.
 */

public class OpenFlatConnectionEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int roomId = packet.getInt();
        String password = packet.getString();

        //todo: cancel debug tester
        Room room = JBlob.getGame().getRoomManager().loadRoom(roomId);

        session.getAvatar().getEventHandler().fireEvent("avatar.room.flat.change");

        if(session.getAvatar().getCurrentRoom() != null) {
            session.getAvatar().getCurrentRoom().getRoomAvatarService().removePlayerAvatar(session.getAvatar().getId());
            session.getAvatar().setCurrentRoomId(0);
        }

        if (room == null) {
            session.sendMessage(new CloseFlatConnectionComposer());
            session.sendMessage(new RoomEnterErrorAlertComposer(RoomEnterErrorAlertComposer.ERROR_UNKNOWN));
            return;
        }

        RoomProperties roomData = room.getProperties();

        if(!roomData.avatarHasRight(session.getAvatar().getId(), "room_right_override_lock")) {
            if (roomData.getLockType() == 1) {

                session.sendMessage(new DoorbellComposer(""));
                //Add to EventArgs room door bell queue
                if(room.getRoomAvatarService().addDoorBellAvatar(session.getAvatar().getUsername(), session.getAvatar())) {
                    room.getRoomAvatarService().performAddAvatarToDoorBell(session.getAvatar().getUsername());
                } else{
                    session.sendMessage(new FlatAccessDeniedComposer(""));
                    session.sendMessage(new CloseFlatConnectionComposer());
                }
                return;

            } else if (roomData.getLockType() == 2) {
                if (!roomData.getPassword().equals(password)) {
                    session.sendMessage(new CloseFlatConnectionComposer());
                    session.sendMessage(new GenericErrorComposer(-100002));
                    return;
                }
            }
        }

        ForwardProperties properties = JBlob.getGame().getRoomForwardService().enqueue(roomId, session.getAvatar(), null);

        properties.onError(roomEnterErrorFuture -> {

            if(!roomEnterErrorFuture.success())
                return false;

            session.sendQueueMessage(new OpenFlatConnectionComposer());

            session.sendQueueMessage(new RoomReadyComposer(room.getProperties().getId(), room.getProperties().getModelName()));
            RoomManager.serializeRoomData(session, room);
            session.flush();

            session.sendQueueMessage(new RoomRatingComposer(roomData.getScore(), !roomData.getAvatarLikes().contains(session.getAvatar().getId())));

            //todo: check user rights at room
            if(room.getProperties().avatarHasRight(session.getAvatar().getId(), PermissionManagerPermissions.ROOM_OWNER_RIGHT)) {
                session.sendQueueMessage(new RoomOwnerControllerComposer());
                session.sendQueueMessage(new RoomControllerLevelComposer(4));
            } else {
                session.sendQueueMessage(new RoomControllerLevelComposer(roomData.getAvatarRank(session.getAvatar().getId())));
            }

            session.flush();

            return true;
        });


        /*RoomEnterErrorFuture errorFuture;
        if (!(errorFuture = room.getRoomAvatarService().tryAddPlayerAvatar(session.getAvatar())).success()) {
            session.sendMessage(new CloseFlatConnectionComposer());
            session.sendMessage(new RoomEnterErrorAlertComposer(errorFuture.getErrorCode(), errorFuture.getMessage()));
            return;
        }

        session.sendQueueMessage(new OpenFlatConnectionComposer());

        session.sendQueueMessage(new RoomReadyComposer(room.getProperties().getId(), room.getProperties().getModelName()));
        RoomManager.serializeRoomData(session, room);
        session.flush();

        session.sendQueueMessage(new RoomRatingComposer(roomData.getScore(), false));

        //todo: check user rights at room
        if(room.getProperties().avatarHasRight(session.getAvatar().getId(), PermissionManagerPermissions.ROOM_OWNER_RIGHT)) {
            session.sendQueueMessage(new RoomOwnerControllerComposer());
            session.sendQueueMessage(new RoomControllerLevelComposer(4));
        } else {
            session.sendQueueMessage(new RoomControllerLevelComposer(roomData.getAvatarRank(session.getAvatar().getId())));
        }

        session.flush();*/


    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
