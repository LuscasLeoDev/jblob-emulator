package com.luscastudio.jblob.server.communication.handlers.player.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 09/10/2016.
 */

public class UpdateFigureDataEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {


        String gender = JBlob.getGame().getFilter().filterFigureGender(packet.getString());
        String figureString = JBlob.getGame().getFilter().filterFigureString(packet.getString());

        session.getAvatar().getFigure().update(figureString, gender);

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
