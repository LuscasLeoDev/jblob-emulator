package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.effects;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.server.ChatComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.HideWiredConfigComposer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.WiredSaveData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.EffectWiredBox;
import com.luscastudio.jblob.server.game.habbohotel.rooms.process.RunProcess;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 16/02/2017 at 12:05.
 */
public class EffectShowMessageToAvatar extends EffectWiredBox {

    private String message;
    private int delay;

    private Map<Integer, RoomPlayerAvatar> players;

    private RunProcess process;

    public EffectShowMessageToAvatar(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
        this.message = "";
        this.delay = 0;
        this.players = BCollect.newConcurrentMap();
    }

    @Override
    public void saveWiredData(PlayerSession session, List<Integer> integerList, String stringData, List<Integer> selectedFurniList, int delay, int lastInt1) {
        this.delay = delay;
        this.message = stringData;
        this.getExtradata().save(new WiredSaveData(null, null, this.delay, this.message));
        this.room.getRoomItemHandlerService().saveItem(this);
        session.sendMessage(new HideWiredConfigComposer());
    }

    @Override
    public int getDelay() {
        return this.delay;
    }

    @Override
    public void onWiredTriggered(IRoomAvatar avatarTrigger, IRoomItem triggeredItem) {
        if(this.players.containsKey(avatarTrigger.getVirtualId()))
            return;
        if(avatarTrigger instanceof RoomPlayerAvatar){
            RoomPlayerAvatar playerAvatar = (RoomPlayerAvatar)avatarTrigger;

            this.players.put(playerAvatar.getVirtualId(), playerAvatar);
            playerAvatar.getEventHandler().on("avatar.leave.room", (delegate, eventArgs) -> {
                this.players.remove(avatarTrigger.getVirtualId());
            });

            this.run();
            this.getExtradata().toggle();
            this.room.getRoomItemHandlerService().updateItem(this, false);
        }
    }

    private void run() {
        if(this.process == null){
            this.room.getRoomProcess().enqueue(this.process = new RunProcess(this::showMessage, this.getDelay() * 500));
        }
    }

    private void showMessage(){
        for (RoomPlayerAvatar avatar : this.players.values()) {
            String msg = this.message.replace("%username%", avatar.getName());

            avatar.getAvatar().getSession().sendMessage(new ChatComposer(avatar.getVirtualId(), 34, 0, msg));
        }

        this.players.clear();
        this.process.cancel();
        this.process = null;
    }

    @Override
    public int getWiredCode() {
        return 7;
    }

    @Override
    public boolean requireAvatar() {
        return true;
    }

    @Override
    public boolean requireTriggeredItem() {
        return false;
    }

    @Override
    public Collection<IRoomItem> getSelectedItems() {
        return Collections.emptyList();
    }

    @Override
    public String getStringData() {
        return message;
    }

    @Override
    public Collection<Integer> getIntegersData() {
        return Collections.emptyList();
    }

    @Override
    public Collection<IRoomItem> getIncompatibleItems() {
        return Collections.emptyList();
    }

    @Override
    protected void loadWiredData() {
        try{
            WiredSaveData data = this.getExtradata().getSavedData();
            if(data != null){
                this.delay = data.getDelay();
                this.message = data.getStringData();
            }
        } catch(Exception ignored){

        }
    }

    @Override
    public void dispose() {
        this.delay = 0;
        if(this.process != null){
            this.process.cancel();
            this.process = null;
        }
        this.message = null;
        this.players.clear();
        this.players = null;
        super.dispose();
    }
}
