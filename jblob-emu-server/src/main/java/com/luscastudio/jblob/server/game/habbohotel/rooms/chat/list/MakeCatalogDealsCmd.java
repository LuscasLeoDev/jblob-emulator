package com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.HideWiredConfigComposer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.events.EventDelegate;
import com.luscastudio.jblob.server.events.list.WiredSaveDataEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniBase;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.IChatCommand;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.WiredSaveData;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Lucas on 17/02/2017 at 17:39.
 */
public class MakeCatalogDealsCmd implements IChatCommand {
    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {

        int id = -session.getAvatar().getId();

        session.sendMessage(new CustomWiredTriggerConfigComposer(id));

        session.getAvatar().getEventHandler().fireEvent("command.makedeal.cancel", null);

        session.sendMessage(new BubbleNotificationComposer("debug", "Type the Page id in the Text input.\nYou have 30 minutes to complete the wired box and save."));

        EventDelegate saveDelegate = session.getAvatar().getEventHandler().on("save.wired.trigger", (delegate, eventArgs) -> {
            WiredSaveDataEventArgs args = (WiredSaveDataEventArgs) eventArgs;

            if(args.getItemId() != id)
                return;

            WiredSaveData saveData = args.getSaveData();

            int dealId = this.makeDeals(saveData, room);

            session.sendMessage(new BubbleNotificationComposer("debug", "Process Result code: " + dealId));
            session.sendMessage(new HideWiredConfigComposer());

            delegate.cancel();
        });

        session.getAvatar().getEventHandler().on("command.makedeal.cancel", (delegate, eventArgs) -> {
            saveDelegate.cancel();
            delegate.cancel();
        });

        avatar.getEventHandler().on("avatar.leave.room", (delegate, eventArgs) -> {
            delegate.cancel();
            saveDelegate.cancel();
        });
        JBlob.getGame().getProcessManager().run(saveDelegate::cancel, 30, TimeUnit.MINUTES);

        return true;
    }

    private int makeDeals(WiredSaveData data, Room room){
        int pageId;
        if(!NumberHelper.isInteger(data.getStringData()))
            return -1;

        pageId = Integer.parseInt(data.getStringData());

        Map<FurniBase, Integer> baseToDeal = BCollect.newMap();
        for (Integer integer : data.getItemsId()) {
            IRoomItem item = room.getRoomItemHandlerService().getRoomItemById(integer);
            if(item == null)
                continue;

            if(!baseToDeal.containsKey(item.getProperties().getBase()))
                baseToDeal.put(item.getProperties().getBase(), 0);

            baseToDeal.replace(item.getProperties().getBase(), baseToDeal.get(item.getBase()) + 1);


        }
        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            String insertQuery;
            boolean first;
            int i;
            DBConnPrepare prepare;

            first = true;
            insertQuery = "INSERT INTO catalog_deals (type, name, page_id) VALUES ";
            for (FurniBase ignored : baseToDeal.keySet()) {
                if(!first)
                    insertQuery += ", ";

                insertQuery += "(?, ?, ?)";

                first = false;
            }

            i = 0;

            prepare = reactor.prepare(insertQuery, true);

            for (FurniBase base : baseToDeal.keySet()) {
                prepare.setString(i * 3 + 1, "default");
                prepare.setString(i * 3 + 2, base.getClassName());
                prepare.setInt(i * 3 + 3, pageId);
                i++;
            }

            ResultSet dealsSet = prepare.runInsertKeys();


            insertQuery = "INSERT INTO catalog_deals_items (deal_id, type, base_id, amount) VALUES ";
            first = true;
            for (FurniBase base : baseToDeal.keySet()) {
                if(!first)
                    insertQuery += ", ";

                insertQuery += "(?, ?, ?, ?)";

                first = false;
            }

            List<Integer> keys = BCollect.newList();
            while(dealsSet.next())
                keys.add(dealsSet.getInt(1));

            prepare = reactor.prepare(insertQuery, true);

            Iterator<Integer> iterator = keys.iterator();

            i = 0;
            for (Map.Entry<FurniBase, Integer> entry : baseToDeal.entrySet()) {
                if(!iterator.hasNext())
                    break;

                prepare.setInt(i * 4 + 1, iterator.next());
                prepare.setString(i * 4 + 2, entry.getKey().getType());
                prepare.setInt(i * 4 + 3, entry.getKey().getId());
                prepare.setInt(i * 4 + 4, entry.getValue());

                i++;
            }

            prepare.runInsertKeys();

            return 0;

        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }

        return 1;
    }

    private class CustomWiredTriggerConfigComposer extends MessageComposer {
        @Override
        public String id() {
            return "WiredTriggerConfigMessageComposer";
        }

        public CustomWiredTriggerConfigComposer(int windowId){

            //region header

            message.putBool(false); //Some boolean related to the comment XX about
            message.putInt(700);//Limit of Selectable items

            message.putInt(0);//items ids count


            message.putInt(0);
            message.putInt(windowId);


            message.putString("");

            message.putInt(0); //Integers Data Count

            message.putInt(0); //Comment XX: Something on external flash texts like wiredfurni.pickfurnis. effect/trigger/conditon . x

            //endregion

            message.putInt(13);

            message.putInt(0); //Incompatible wireds on stack list count

        }
    }
}
