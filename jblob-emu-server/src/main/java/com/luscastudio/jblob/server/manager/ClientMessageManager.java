package com.luscastudio.jblob.server.manager;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.catalog.client.*;
import com.luscastudio.jblob.server.communication.handlers.handshake.client.*;
import com.luscastudio.jblob.server.communication.handlers.hotelview.promos.client.GetPromoArticlesEvent;
import com.luscastudio.jblob.server.communication.handlers.hotelview.promos.client.RefreshCampaignEvent;
import com.luscastudio.jblob.server.communication.handlers.messenger.client.*;
import com.luscastudio.jblob.server.communication.handlers.navigator.client.GoToHotelViewEvent;
import com.luscastudio.jblob.server.communication.handlers.navigator.newnav.client.*;
import com.luscastudio.jblob.server.communication.handlers.navigator.savedsearchs.client.SaveNavigatorSearchEvent;
import com.luscastudio.jblob.server.communication.handlers.player.client.ChangeMottoEvent;
import com.luscastudio.jblob.server.communication.handlers.player.client.ScrGetUserInfoEvent;
import com.luscastudio.jblob.server.communication.handlers.player.client.UpdateFigureDataEvent;
import com.luscastudio.jblob.server.communication.handlers.player.currencies.client.GetCreditsInfoEvent;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.client.*;
import com.luscastudio.jblob.server.communication.handlers.player.preferences.client.*;
import com.luscastudio.jblob.server.communication.handlers.player.profile.client.GetRelationshipsEvent;
import com.luscastudio.jblob.server.communication.handlers.player.profile.client.GetSelectedBadgesEvent;
import com.luscastudio.jblob.server.communication.handlers.player.profile.client.GetUserTagsEvent;
import com.luscastudio.jblob.server.communication.handlers.player.profile.client.SetActivatedBadgesEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.client.*;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.client.ActionEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.client.ApplySignEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.client.RespectUserEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.client.SitEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.client.GetPetAvailableCommandsEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.client.GetPetInformationEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.client.MoveRotatePetEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.horse.client.ModifyWhoCanRideHorseEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.horse.client.RemoveSaddleFromHorseEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.horse.client.RideHorseEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.client.*;
import com.luscastudio.jblob.server.communication.handlers.rooms.events.client.GetEventCategoriesEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.groups.client.GetHabboGroupBadgesEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.client.*;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.client.SaveWiredConditionConfigEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.client.SaveWiredEffectConfigEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.client.SaveWiredTriggerConfigEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.pets.client.ApplyPetItemEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.pets.client.RespectPetEvent;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Map;

/**
 * Created by Lucas on 19/09/2016.
 */
public class ClientMessageManager implements IFlushable {

    private Map<String, IMessageEventHandler> handlers;
    private Map<Integer, IMessageEventHandler> handlersIndexes;
    private Map<IMessageEventHandler, Integer> handlersIndexesReverse;
    private short initialHeader;

    public ClientMessageManager(short initialHeader) {
        handlers = BCollect.newMap();
        handlersIndexes = BCollect.newMap();
        handlersIndexesReverse = BCollect.newMap();
        this.initialHeader = initialHeader;
        this.flush();
    }

    //Handshake Incoming Packets
    private void registerHandShake() {
        this.add("GetClientVersionMessageEvent", new ReleaseBuildNameEvent());
        this.add("InitCryptoMessageEvent", new InitCrytoEvent());
        this.add("GenerateSecretKeyMessageEvent", new SecretKeyEvent());
        this.add("SSOTicketMessageEvent", new SSOTicketEvent());
        this.add("UniqueIDMessageEvent", new UniqueIDEvent());
        this.add("InfoRetrieveMessageEvent", new InfoRetrieveEvent());
        this.add("PingMessageEvent", new PingEvent());
    }

    private void registerMessenger() {
        this.add("MessengerInitMessageEvent", new MessengerInitEvent());
        this.add("GetBuddyRequestsMessageEvent", new GetFriendshipRequestEvent());
        this.add("SendMsgMessageEvent", new SendMessengerMessageEvent());
        this.add("FollowFriendMessageEvent", new FollowFriendEvent());

        this.add("RemoveBuddyMessageEvent", new RemoveBuddyEvent());
        this.add("RequestBuddyMessageEvent", new RequestBuddyEvent());
        this.add("AcceptBuddyMessageEvent", new AcceptBuddyEvent());
    }

    private void registerNavigator() {
        this.add("InitializeNewNavigatorMessageEvent", new InitializeNewNavigatorEvent());
        this.add("CanCreateRoomMessageEvent", new CanCreateRoomEvent());
        this.add("NewNavigatorSearchMessageEvent", new NewNavigatorSearchEvent());
        this.add("GetFurnitureAliasesMessageEvent", new GetFurnitureAliasesEvent());
        this.add("GetGuestRoomMessageEvent", new GetGuestRoomEvent());
        this.add("OpenFlatConnectionMessageEvent", new OpenFlatConnectionEvent());
        this.add("GetRoomEntryDataMessageEvent", new GetRoomEntryDataEvent());
        this.add("GoToFlatMessageEvent", new GoToFlatEvent());
        this.add("GoToHotelViewMessageEvent", new GoToHotelViewEvent());
        this.add("MoveAvatarMessageEvent", new MoveAvatarEvent());

        this.add("GetUserFlatCatsMessageEvent", new GetUserFlatCatsEvent());
        this.add("GetNavigatorFlatsMessageEvent", new GetNavigatorFlatsEvent());

        this.add("GetRoomSettingsMessageEvent", new GetRoomSettingsEvent());
        this.add("CreateFlatMessageEvent", new CreateRoomMessageEvent());
        this.add("GetRoomRightsMessageEvent", new GetRoomRightsEvent());

        this.add("SaveNavigatorSearchMessageEvent", new SaveNavigatorSearchEvent());

        this.add("DeleteRoomMessageEvent", new DeleteRoomEvent());
    }

    private void registerHotelView() {
        this.add("RefreshCampaignMessageEvent", new RefreshCampaignEvent());
        this.add("GetPromoArticlesMessageEvent", new GetPromoArticlesEvent());
    }

    private void registerRoom() {
        //Room Users
        this.add("ChatMessageEvent", new ChatEvent());
        this.add("ShoutMessageEvent", new ShoutEvent());
        this.add("WhisperMessageEvent", new WhisperEvent());
        this.add("SitMessageEvent", new SitEvent());
        this.add("ActionMessageEvent", new ActionEvent());
        this.add("ApplySignMessageEvent", new ApplySignEvent());

        this.add("StartTypingMessageEvent", new StartTypingEvent());
        this.add("CancelTypingMessageEvent", new StopTypingEvent());

        this.add("RespectUserMessageEvent", new RespectUserEvent());


        //Groups
        this.add("GetHabboGroupBadgesMessageEvent", new GetHabboGroupBadgesEvent());

        //Events
        this.add("GetEventCategoriesMessageEvent", new GetEventCategoriesEvent());

        //Settings
        this.add("SaveRoomSettingsMessageEvent", new SaveRoomSettingsEvent());

        //Furnis
        this.add("GetStickyNoteMessageEvent", new GetStickyNoteEvent());
        this.add("UpdateStickyNoteMessageEvent", new UpdateStickyNoteEvent());
        this.add("SaveBrandingItemMessageEvent", new SaveBrandingItemEvent());
        this.add("SetTonerMessageEvent", new SetTonerEvent());
        this.add("GetMoodlightConfigMessageEvent", new GetMoodlightConfigEvent());

        //Pets
        this.add("GetPetInformationMessageEvent", new GetPetInformationEvent());
        this.add("GetPetAvailableCommandsMessageEvent", new GetPetAvailableCommandsEvent());
        this.add("MoveRotatePetMessageEvent", new MoveRotatePetEvent());
        this.add("RespectPetMessageEvent", new RespectPetEvent());
        this.add("ApplyPetItemMessageEvent", new ApplyPetItemEvent());

        this.add("RideHorseMessageEvent", new RideHorseEvent());
        this.add("ModifyWhoCanRideHorseMessageEvent", new ModifyWhoCanRideHorseEvent());
        this.add("RemoveSaddleFromHorseMessageEvent", new RemoveSaddleFromHorseEvent());

        //Floor Plan Editor
        this.add("InitializeFloorPlanSessionMessageEvent", new InitializeFloorPlanSessionEvent());
        this.add("FloorPlanEditorRoomPropertiesMessageEvent", new FloorPlanEditorRoomPropertiesEvent());
        this.add("SaveFloorPlanModelMessageEvent", new SaveFloorPlanModelEvent());

        //Door Bell
        this.add("LetUserInMessageEvent", new DoorBellResponseMessageEvent());

        //Wireds
        this.add("SaveWiredTriggerConfigMessageEvent", new SaveWiredTriggerConfigEvent());
        this.add("SaveWiredEffectConfigMessageEvent", new SaveWiredEffectConfigEvent());
        this.add("SaveWiredConditionConfigMessageEvent", new SaveWiredConditionConfigEvent());
    }

    private void registerPlayer() {
        //Avatar  Details
        this.add("UpdateFigureDataMessageEvent", new UpdateFigureDataEvent());
        this.add("ChangeMottoMessageEvent", new ChangeMottoEvent());

        //Inventory

        ///Furnis
        this.add("RequestFurniInventoryMessageEvent", new RequestFurniInventoryEvent());
        this.add("RequestFurniInventoryMessageEvent2", new RequestFurniInventoryEvent());

        ///Badges
        this.add("GetBadgesMessageEvent", new GetBadgesEvent());
        this.add("SetActivatedBadgesMessageEvent", new SetActivatedBadgesEvent());

        ///Pets
        this.add("GetPetInventoryMessageEvent", new GetPetInventoryEvent());
        this.add("PlacePetMessageEvent", new PlacePetEvent());
        this.add("PickUpPetMessageEvent", new PickUpPetEvent());

        //Getting Information
        this.add("GetCreditsInfoMessageEvent", new GetCreditsInfoEvent());
        this.add("GetSelectedBadgesMessageEvent", new GetSelectedBadgesEvent());
        this.add("GetUserTagsMessageEvent", new GetUserTagsEvent());
        this.add("GetRelationshipsMessageEvent", new GetRelationshipsEvent());
        this.add("ScrGetUserInfoMessageEvent", new ScrGetUserInfoEvent());

        //Furnis
        this.add("UseFurnitureMessageEvent", new UseFurnitureEvent());
        this.add("UseWallItemMessageEvent", new UseWallItemEvent());
        this.add("MoveObjectMessageEvent", new MoveFloorItemEvent());
        this.add("PlaceObjectMessageEvent", new PlaceRoomItemEvent());
        this.add("AddStickyNoteMessageEvent", new AddStickyNoteEvent());
        this.add("PickupObjectMessageEvent", new PickUpRoomItemEvent());
        this.add("ApplyDecorationMessageEvent", new ApplyDecorationEvent());
        this.add("MoveWallItemMessageEvent", new MoveWallItemEvent());

        //Preferences
        this.add("SetFriendBarStateMessageEvent", new UpdateAvatarPreferencesEvent());
        this.add("SetSoundSettingsMessageEvent", new SetSoundSettingsEvent());
        this.add("SetUserFocusPreferenceEvent", new SetUserFocusPreferenceEvent());
        this.add("SetMessengerInviteStatusMessageEvent", new SetMessengerInviteStatusEvent());
        this.add("SetChatPreferenceMessageEvent", new SetChatPreferenceEvent());


    }

    private void registerCatalog(){

        this.add("GetCatalogModeMessageEvent", new GetCatalogPagesEvent());
        this.add("GetCatalogPageMessageEvent", new GetCatalogPageEvent());

        this.add("PurchaseFromCatalogMessageEvent", new PurchaseFromCatalogEvent());
        this.add("GetSellablePetBreedsMessageEvent", new GetSellablePetBreedsEvent());
        this.add("CheckPetNameMessageEvent", new CheckPetNameEvent());

        this.add("GetGiftWrappingConfigurationMessageEvent", new GetGiftWrappingConfigurationEvent());
    }

    @Override
    public void flush() {
        this.handlers.clear();

        this.registerHandShake();
        this.registerHotelView();
        this.registerMessenger();
        this.registerNavigator();
        this.registerRoom();
        this.registerPlayer();
        this.registerCatalog();
    }

    @Override
    public void performFlush() {
        
    }

    //region INCOMING HANDLING

    public void add(String name, IMessageEventHandler handler) {
        if (!handlers.containsKey(name)) {
            handlers.put(name, handler);
            int index = handlersIndexes.size();
            handlersIndexes.put(index, handler);
            handlersIndexesReverse.put(handler, index);
        } else {
            int index = handlersIndexesReverse.get(handler);

            IMessageEventHandler oldHandler = handlersIndexes.get(index);

            handlersIndexes.replace(index, handler);
            handlers.replace(name, handler);

            handlersIndexesReverse.remove(oldHandler);
            handlersIndexesReverse.put(handler, index);
        }
    }

    public boolean hasHandler(String name) {
        return this.handlers.containsKey(name);
    }

    public IMessageEventHandler getHandler(String name) {
        return hasHandler(name) ? handlers.get(name) : null;
    }

    public IMessageEventHandler getHandler(int index) {
        return handlersIndexes.size() > index ? handlersIndexes.get(index) : null;
    }

    public void handleMessage(PlayerSession session, MessageEvent msg) {
        short shortheader = msg.getHeader();

        if (session != null && session.getBuild() != null && shortheader != initialHeader) {

            String header = session.getBuild().getIncomingHeader(shortheader);


            //If there is no one handler with that header
            //don't do anything
            if (!this.hasHandler(header)) {
                BLogger.debug("[INCOMING NOT HANDLED] [" + session.getChannel().channel().remoteAddress().toString() +"] [" + header + "] [" + shortheader + "] {" + msg.toString() + "}", this.getClass());
                return;
            }
            BLogger.debug("[INCOMING] [" + session.getChannel().channel().remoteAddress().toString() +"] [" + header + "] [" + shortheader + "] {" + msg.toString() + "}", this.getClass());

            IMessageEventHandler handler = this.getHandler(header);

            try {
                handler.parse(session, msg);
            } catch(Exception e){
                BLogger.error(e, this.getClass());
            }
        } else if(shortheader == initialHeader) {
            BLogger.debug("[DEFAULT INCOMING] [" + session.getChannel().channel().remoteAddress().toString() + "] [" + shortheader + "] {" + msg.toString() + "}", this.getClass());

            IMessageEventHandler handler = getHandler(0); // Default Handler First
            handler.parse(session, msg);

        }
    }

    //endregion
}
