package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.FlatCreatedComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomType;
import com.luscastudio.jblob.server.game.habbohotel.rooms.models.RoomModel;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 04/02/2017 at 14:37.
 */
public class CreateRoomMessageEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        //session.sendMessage(new CanCreateRoomComposer(false, 500));

        if (!JBlob.getGame().getPermissionManager().validateRight(session.getAvatar().getRank(), "p_create_room"))
            return;

        String name = JBlob.getGame().getFilter().filterString(packet.getString());
        String desc = JBlob.getGame().getFilter().filterString(packet.getString());
        String modelName = packet.getString();

        int category = packet.getInt();
        int maxVisitors = packet.getInt();
        int tradeType = packet.getInt();

        RoomModel model = JBlob.getGame().getModelManager().getModel(modelName);

        if (model == null) {
            session.sendMessage(new BubbleNotificationComposer("error").add("message", "Model name '" + modelName + "' not found on server database!"));
            return;
        }

        RoomProperties roomData = JBlob.getGame().getRoomManager().createRoom(name, desc, session.getAvatar().getId(), modelName, tradeType, "", maxVisitors, tradeType, category, "", "", "", RoomType.PRIVATE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", "", model);

        if (roomData == null) {
            session.sendMessage(new BubbleNotificationComposer("error").add("error", "room_create_error"));
            return;
        }

        Room room = JBlob.getGame().getRoomManager().loadRoom(roomData.getId());

        if (room == null) {
            session.sendMessage(new BubbleNotificationComposer("error").add("error", "room_load_error"));
            return;
        }

        //room.getRoomAvatarService().tryAddPlayerAvatar(session.getAvatar());

        session.sendMessage(new FlatCreatedComposer(roomData.getId(), roomData.getName()));

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
