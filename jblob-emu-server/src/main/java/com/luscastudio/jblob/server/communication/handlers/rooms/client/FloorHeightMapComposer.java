package com.luscastudio.jblob.server.communication.handlers.rooms.client;

/**
 * Created by Lucas on 03/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.models.RoomModel;

public class FloorHeightMapComposer extends MessageComposer {
    @Override
    public String id() {
        return "FloorHeightMapMessageComposer";
    }

    public FloorHeightMapComposer(RoomModel model) {
        message.putBool(true);//Unknown
        message.putInt(model.getWallHeight());//Wall Height

        message.putString(model.getModelString());
    }

    public FloorHeightMapComposer(boolean someBool, int wallHeight, String modelData){
        message.putBool(someBool);
        message.putInt(wallHeight);
        message.putString(modelData);
    }
}
