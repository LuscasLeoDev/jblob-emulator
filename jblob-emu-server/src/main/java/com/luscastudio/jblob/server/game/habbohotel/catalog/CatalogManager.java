package com.luscastudio.jblob.server.game.habbohotel.catalog;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.catalog.server.CatalogUpdateComposer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.catalog.bundles.BundleFurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.catalog.bundles.CatalogBundleDeal;
import com.luscastudio.jblob.server.game.habbohotel.catalog.bundles.RoomBundle;
import com.luscastudio.jblob.server.game.habbohotel.catalog.dealitems.*;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniBase;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;
import org.apache.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 28/10/2016.
 */

public class CatalogManager implements IFlushable {

    public static Logger logger = Logger.getLogger(CatalogManager.class);

    private Map<Integer, CatalogNode> nodes;
    private Map<Integer, CatalogNode> pages;
    private Map<Integer, RoomBundle> bundles;

    private Map<String, Class<? extends CatalogDealItem>> dealItemClassesByType;
    private Map<String, Class<? extends CatalogDeal>> dealClassesByType;
    //private Map<String, Class<? extends CatalogDealItem>> dealClassesByInteraction;

    public CatalogManager() {

        this.nodes = BCollect.newLinkedMap();
        this.pages = BCollect.newLinkedMap();
        this.dealItemClassesByType = BCollect.newMap();
        this.dealClassesByType = BCollect.newMap();
        this.bundles = BCollect.newMap();

        this.addDefaultDealTypes();
    }

    private void addDefaultDealTypes() {
        //Deals
        this.dealClassesByType.put("default", CatalogDeal.class);
        this.dealClassesByType.put("bundle", CatalogBundleDeal.class);

        //Deal Items
        this.dealItemClassesByType.put("s", DealItemFurni.class);
        this.dealItemClassesByType.put("i", DealItemFurni.class);

        this.dealItemClassesByType.put("r", DealItemBot.class);
        this.dealItemClassesByType.put("b", DealItemBadge.class);
        this.dealItemClassesByType.put("e", DealItemEffect.class);
        this.dealItemClassesByType.put("h", DealItemHabboClub.class);
        this.dealItemClassesByType.put("p", DealItemPet.class);

        //Teleport
        this.dealItemClassesByType.put("teleport", DealItemTeleport.class);

        //MonsterPlant
        this.dealItemClassesByType.put("monsterplant_seed", DealItemMonsterPlantSeed.class);

        //Badge Display
        this.dealItemClassesByType.put("badge_display", DealItemBadgeDiplay.class);



    }

    @Override
    public void flush() {


        this.pages.clear();
        this.nodes.clear();

        this.dealItemClassesByType.clear();
        this.bundles.clear();
        this.addDefaultDealTypes();


        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

            DBConnPrepare prepare;
            ResultSet set;

            Map<Integer, Map<Integer, CatalogDeal>> dealMap = BCollect.newLinkedMap();
            Map<Integer, Map<Integer, CatalogDealItem>> dealItemMap = BCollect.newMap();

            //region Getting Bundles

            prepare = reactor.prepare("SELECT * FROM bundles_rooms");
            set = prepare.runQuery();

            while(set.next())
                this.bundles.put(set.getInt("id"), new RoomBundle(set));

            prepare = reactor.prepare("SELECT * FROM bundles_items_data");
            set = prepare.runQuery();

            while(set.next()){
                int bundleId = set.getInt("bundle_id");
                if(!this.bundles.containsKey(bundleId))
                    continue;

                FurniBase base = JBlob.getGame().getItemBaseManager().getItemBase(set.getInt("base_id"));
                if(base == null)
                    continue;

                RoomBundle bundle = this.bundles.get(bundleId);
                bundle.getBundleItems().put(set.getInt("id"), new BundleFurniProperties(set.getInt("id"), base, set.getString("extradata"), set.getInt("is_rare") == 1, set.getInt("x"), set.getInt("y"), NumberHelper.parseDouble(set.getString("z")), set.getInt("rotation"), set.getString("wall_coordinate")));
            }

            //endregion

            //region Getting Deal Items

            prepare = reactor.prepare("SELECT * FROM catalog_deals_items WHERE enabled = '1'");

            set = prepare.runQuery();

            while (set.next()) {
                CatalogDealItem dealItem = getDealItem(set);

                if (dealItem == null)
                    continue;

                if (!dealItemMap.containsKey(dealItem.getDealId()))
                    dealItemMap.put(dealItem.getDealId(), BCollect.newMap());

                dealItemMap.get(dealItem.getDealId()).put(dealItem.getId(), dealItem);
            }

            //endregion

            //region Getting Deals

            prepare = reactor.prepare("SELECT * FROM catalog_deals WHERE enabled = '1' ORDER BY order_id ASC");

            set = prepare.runQuery();

            while (set.next()) {
                CatalogDeal deal = getDeal(set);
                if (deal == null)
                    continue;

                if (!dealMap.containsKey(deal.getPageId()))
                    dealMap.put(deal.getPageId(), BCollect.newLinkedMap());

                dealMap.get(deal.getPageId()).put(deal.getId(), deal);

                if (dealItemMap.containsKey(deal.getId()))
                    deal.setItems(dealItemMap.get(deal.getId()));
                else
                    deal.setItems(BCollect.newMap());
            }
            //endregion

            //region Getting Pages
            prepare = reactor.prepare("SELECT * FROM catalog_pages WHERE visible = '1' ORDER BY order_num");
            set = prepare.runQuery();

            while (set.next()) {
                CatalogNode node = this.addNode(set);
                if (dealMap.containsKey(node.getId()))
                    node.setDeals(dealMap.get(node.getId()));
                else
                    node.setDeals(BCollect.newMap());

                this.pages.put(node.getId(), node);
            }
            //endregion

            this.addNodeChildren();

        } catch (Exception e) {
            logger.error("Error while loading Catalog", e);
        }
    }

    @Override
    public void performFlush() {
        JBlob.getGame().getSessionManager().sendMessage(new CatalogUpdateComposer(true));
    }

    private CatalogDealItem getDealItem(ResultSet set) throws SQLException {


        CatalogDealItem deal = generateCatalogDealItem(set.getInt("id"), set.getInt("deal_id"), set.getInt("base_id"), set.getString("extradata"), set.getInt("limited_count"), set.getInt("amount"), set.getString("type"), set.getInt("is_rare") == 1);

        return deal;
    }

    private CatalogDeal getDeal(ResultSet set) throws SQLException {

        String type = set.getString("type");

        Class<? extends CatalogDeal> instance = this.dealClassesByType.get(type);
        if(instance == null)
            return null;

        try{

            Constructor<? extends CatalogDeal> constructor = instance.getConstructor(int.class, String.class, int.class, int.class, int.class, int.class, boolean.class, boolean.class, boolean.class, int.class, String.class, int.class);

            if(constructor == null)
                return null;


            return constructor.newInstance(set.getInt("id"),
                    set.getString("name"),
                    set.getInt("page_id"),
                    set.getInt("cost_credits"),
                    set.getInt("cost_currency"),
                    set.getInt("currency_id"),
                    set.getInt("offer_enabled") == 1,
                    set.getInt("allow_gift") == 1,
                    set.getInt("is_rent") == 1,
                    set.getInt("club_level"),
                    set.getString("custom"),
                    set.getInt("offer_id"));

        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }


        return null;
    }

    private CatalogNode addNode(ResultSet set) throws SQLException {

        String separator = "\\|";
        List<String> textSet = BCollect.newList();
        Collections.addAll(textSet, (set.getString("texts_set").split(separator)));

        List<String> imageSet = BCollect.newList();
        Collections.addAll(imageSet, (set.getString("images_set")).split(separator));


        CatalogNode node = new CatalogNode(set.getInt("id"), set.getString("title"), set.getInt("icon"), set.getString("external_link"), set.getInt("parent_id"), textSet, imageSet, set.getString("model"), set.getInt("is_builder") == 1, set.getString("require_permission"), set.getInt("enabled") == 1, set.getInt("order_num"));

        //region Loading Deals


        //endregion

        this.pages.put(node.getId(), node);

        if (node.getId() == -1)
            this.nodes.put(node.getId(), node);

        return node;
    }

    private void addNodeChildren() {

        for (CatalogNode node : pages.values()) {

            if (node.getParentId() == -1) {
                nodes.put(node.getId(), node);
                continue;
            }

            if (!pages.containsKey(node.getParentId()))
                continue;

            pages.get(node.getParentId()).getChildren().put(node.getId(), node);
        }

    }

    public synchronized Collection<CatalogNode> getPagesForUser(PlayerSession session) {
        return nodes.values();
    }

    public  synchronized CatalogNode getCatalogNode(int cataId) {
        if(!pages.containsKey(cataId))
            return null;

        return pages.get(cataId);
    }



    public static boolean isValidPurchase(CatalogDeal deal, HabboAvatar avatar, int amount, String extradata) {

        //todo: Add Rank or Permission condition

        if(!deal.isOfferEnabled() && amount > 1)
            return false;

        if(deal.getCreditsCost() > avatar.getBalance().getCreditValue())
            return false;

        return deal.getCurrencyCost() <= avatar.getBalance().get(deal.getCurrencyId());
    }

    public boolean tryPurchase(CatalogDeal deal, HabboAvatar avatar, String PageExtradata, int amount, List<MessageComposer> resultComposers) {

        return deal.tryPurchase(avatar, PageExtradata, amount, resultComposers);
    }

    private CatalogDealItem generateCatalogDealItem(int id, int dealId, int baseId, String extradata, int limited_count, int amount, String type, boolean isRare){

        Class<? extends CatalogDealItem> classe = null;



        /*if(base != null)
            classe = dealClassesByInteraction.get(base.getInteractionType());*/
            classe = dealItemClassesByType.get(type);
        if(classe == null)
            return null;


        try {

            Constructor<? extends CatalogDealItem> constructor = classe.getConstructor(int.class, int.class, int.class, String.class, int.class, int.class, String.class, boolean.class);

            if(constructor == null){
                BLogger.warn("The class '" + classe.getName() + "' doesn't have the correct constructor!!", this.getClass());

                return null;
            }

            return constructor.newInstance(id, dealId, baseId, extradata, limited_count, amount, type, isRare);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            BLogger.error(e, this.getClass());
        }

        return null;
    }

    public RoomBundle getBundle(int bundleId) {
        return this.bundles.get(bundleId);
    }

    public int generateCatalogDeal(Room room){
        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare = reactor.prepare("INSERT INTO bundles_rooms (name, description, model_name, lock_type, password, max_users, trade_type, wallpaper_data, landscape_data, floor_data, who_can_kick, who_can_mute, who_can_ban, chat_mode, chat_size, chat_speed, chat_extra_flood, chat_distance, allow_pets, allow_pets_eating, hide_wall, wall_thickness, floor_thickness, allow_walk_trough) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", true);

            prepare.setString(1, room.getProperties().getName());
            prepare.setString(2, room.getProperties().getDescription());
            prepare.setString(3, room.getProperties().getModelName());
            prepare.setInt(4, room.getProperties().getLockType());
            prepare.setString(5, room.getProperties().getPassword());
            prepare.setInt(6, room.getProperties().getMaxUsers());
            prepare.setInt(7, room.getProperties().getTradeType());
            prepare.setString(8, room.getProperties().getWallData());
            prepare.setString(9, room.getProperties().getLandscapeData());
            prepare.setString(10, room.getProperties().getFloorData());
            prepare.setInt(11, room.getProperties().getWhoCanKick());
            prepare.setInt(12, room.getProperties().getWhoCanMute());
            prepare.setInt(13, room.getProperties().getWhoCanBan());
            prepare.setInt(14, room.getProperties().getChatMode());
            prepare.setInt(15, room.getProperties().getChatSize());
            prepare.setInt(16, room.getProperties().getChatSpeed());
            prepare.setInt(17, room.getProperties().getChatExtraFlood());
            prepare.setInt(18, room.getProperties().getChatDistance());
            prepare.setInt(19, room.getProperties().getAllowPets());
            prepare.setInt(20, room.getProperties().getAllowPetsEating());
            prepare.setInt(21, room.getProperties().getHideWall());
            prepare.setInt(22, room.getProperties().getWallThickness());
            prepare.setInt(23, room.getProperties().getFloorThickness());
            prepare.setInt(24, room.getProperties().getAllowWalkTrough());

            int bundleId = prepare.runInsert();

            {
                String insertData = "INSERT INTO bundles_items_data (bundle_id, base_id, extradata, is_rare, x, y, z, rotation, wall_coordinate) VALUES ";

                boolean first = true;
                for(IRoomItem item : room.getRoomItemHandlerService().getItems()){
                    if(!first)
                        insertData += ", ";
                    insertData += "(?, ?, ?, ?, ?, ?, ?, ?, ?)";
                    first = false;
                }
                prepare = reactor.prepare(insertData, true);

                int i = 0;
                first = true;
                for (IRoomItem item : room.getRoomItemHandlerService().getItems()) {
                    prepare.setInt(9 * i + 1, bundleId);
                    prepare.setInt(9 * i + 2, item.getProperties().getBase().getId());
                    prepare.setString(9 * i + 3, item.getProperties().getExtradata().getExtradataToDb());
                    prepare.setInt(9 * i + 4, item.getProperties().isRare() ? 1 : 0);
                    prepare.setInt(9 * i + 5, item.getPosition().getX());
                    prepare.setInt(9 * i + 6, item.getPosition().getY());
                    prepare.setString(9 * i + 7, NumberHelper.doubleToString(item.getPosition().getZ()));
                    prepare.setInt(9 * i + 8, item.getPosition().getRotation());
                    prepare.setString(9 * i + 9, item.getWallCoordinate().toString());
                    i++;
                }

                ResultSet keys = prepare.runInsertKeys();
            }

            return bundleId;


        } catch (Exception e){
            BLogger.error(e, this.getClass());

            return -1;
        }
    }
}