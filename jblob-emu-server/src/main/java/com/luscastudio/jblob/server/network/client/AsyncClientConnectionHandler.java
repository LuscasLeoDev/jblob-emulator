//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.network.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.debug.BLogger;
import io.netty.channel.ChannelHandlerContext;

import java.util.concurrent.TimeUnit;

/**
 * Created by Lucas on 19/03/2017 at 13:49.
 */
public class AsyncClientConnectionHandler extends SyncClientConnectionHandler {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MessageEvent msg) {

        JBlob.getGame().getProcessManager().run(() -> super.channelRead0(ctx, msg), 1, TimeUnit.NANOSECONDS);
    }
}
