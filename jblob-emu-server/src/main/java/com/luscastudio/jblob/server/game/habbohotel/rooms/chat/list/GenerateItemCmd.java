package com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list;

import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.handshake.server.TriggerClientEventComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryUpdateRequestComposer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniBase;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.IChatCommand;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 17/02/2017 at 20:34.
 */
public class GenerateItemCmd implements IChatCommand {
    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {
        if(params.length() < 2)
            return true;

        if(!params.get(1).contains(",")) {
            if (!NumberHelper.isInteger(params.get(1)))
                return true;

            int baseId = Integer.parseInt(params.get(1));

            FurniBase base = JBlob.getGame().getItemBaseManager().getItemBase(baseId);
            if (base == null)
                return true;

            FurniProperties properties = JBlob.getGame().getRoomItemFactory().createFurniProperties(base, session.getAvatar().getId(), "", false, 0);

            session.getAvatar().getInventory().addRoomItem(properties);
            session.sendMessage(new InventoryUpdateRequestComposer());
            session.sendMessage(new FurniListNotificationComposer().add(FurniListNotificationComposer.FURNI, properties.getId()));
            session.sendMessage(new TriggerClientEventComposer("inventory/open/furni/" + properties.getId()));
        } else {
            FurniListNotificationComposer notif = new FurniListNotificationComposer();
            for (String s : params.get(1).split(",")) {
                if (!NumberHelper.isInteger(s))
                    continue;

                int baseId = Integer.parseInt(s);

                FurniBase base = JBlob.getGame().getItemBaseManager().getItemBase(baseId);
                if (base == null)
                    continue;

                FurniProperties properties = JBlob.getGame().getRoomItemFactory().createFurniProperties(base, session.getAvatar().getId(), "", false, 0);

                session.getAvatar().getInventory().addRoomItem(properties);

                notif.add(FurniListNotificationComposer.FURNI, properties.getId());
            }
            session.sendMessage(new InventoryUpdateRequestComposer());
            session.sendMessage(notif);
            session.sendMessage(new TriggerClientEventComposer("inventory/open/furni"));
        }

        return true;
    }
}
