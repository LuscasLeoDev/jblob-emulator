package com.luscastudio.jblob.server.communication.handlers.hotelview.promos.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.landingview.promoarticle.PromoArticleItem;

import java.util.Collection;

/**
 * Created by Lucas on 14/02/2017 at 19:55.
 */
public class PromoArticlesComposer extends MessageComposer {
    @Override
    public String id() {
        return "PromoArticlesMessageComposer";
    }

    public PromoArticlesComposer(Collection<PromoArticleItem> articleList) {
        message.putInt(articleList.size());

        for (PromoArticleItem item : articleList) {
            message.putInt(item.getId());
            message.putString(item.getTitle());
            message.putString(item.getContext());
            message.putString(item.getButtonText());
            message.putInt(item.getButtonType());
            message.putString(item.getButtonLink());
            message.putString(item.getImageUrl());
        }
    }
}
