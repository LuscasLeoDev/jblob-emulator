package com.luscastudio.jblob.server.game.habbohotel.catalog.dealitems;

import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.AddInventoryPetComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogDealItem;
import com.luscastudio.jblob.server.game.habbohotel.inventory.AvatarInventory;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.custom.PetCustomData;
import com.luscastudio.jblob.server.game.habbohotel.pet.PetBase;
import com.luscastudio.jblob.server.game.habbohotel.pet.custom.PetCustomDataValues;

import java.util.Collection;
import java.util.List;

/**
 * Created by Lucas on 08/12/2016.
 */

public class DealItemPet extends CatalogDealItem {

    public DealItemPet(int id, int dealId, int baseId, String extradata, int limitedAmount, int amount, String type, boolean isRare) {
        super(id, dealId, baseId, extradata, limitedAmount, amount, type, isRare);
    }

    @Override
    public boolean generateValue(HabboAvatar avatar, String pageExtradata, List<Object> outValues, int amount) {
        String[] data = pageExtradata.split("\n");

        if(data.length < 3){
            avatar.getSession().sendMessage(new BubbleNotificationComposer("error", "error while parsing your pet data!"));
            return false;
        }

        String name = data[0];
        String strRaceId = data[1];
        String color = data[2];

        if(!NumberHelper.isInteger(strRaceId)){
            avatar.getSession().sendMessage(new BubbleNotificationComposer("error", "error while parsing your pet race!"));
            return false;
        }

        int raceId = Integer.parseInt(strRaceId);

        Collection<PetBase> petBases = JBlob.getGame().getPetManager().getSellableBreeds(this.getExtradata());

        if(petBases == null){
            avatar.getSession().sendMessage(new BubbleNotificationComposer("error", "The pet base wasn't found for '" + this.getExtradata() + "'!"));
            return false;
        }

        PetBase base = null;

        for(PetBase petBase :petBases){
            if(petBase.getRaceId() == raceId)
                base = petBase;
        }

        if(base == null){
            avatar.getSession().sendMessage(new BubbleNotificationComposer("error", "Pet base wasn't found for race id " + raceId + "!"));
            return false;
        }

        int startLevel = 1;
        int startEnergy = 100;
        int startExperience = 0;
        int startHappiness = 100;
        String startCustomData = "";
        PetCustomDataValues dataValues = JBlob.getGame().getPetManager().getPetCustomDataValues(base.getPetType());
        if(dataValues != null){
            startCustomData = dataValues.getRandomInitialCustom();
        }

        PetData petData = JBlob.getGame().getPetManager().createPetData(AvatarInventory.ITEM_NOROOM_ID, name, base, avatar.getId(), color, new PetCustomData(startCustomData), startLevel, 0, startEnergy, startExperience, startHappiness);

        avatar.getInventory().addPet(petData);
        outValues.add(new AddInventoryPetComposer(petData));
        outValues.add(new FurniListNotificationComposer().add(FurniListNotificationComposer.PET, petData.getId()));
        //outValues.add(new TriggerClientEventComposer("inventory/open/pets/" + petData.getId()));
        return true;
    }

    @Override
    public void handleValues(HabboAvatar avatar, List<Object> values) {

    }

    @Override
    public void parsePageComposer(PacketWriting message) {
        super.parsePageComposer(message);
    }
}
