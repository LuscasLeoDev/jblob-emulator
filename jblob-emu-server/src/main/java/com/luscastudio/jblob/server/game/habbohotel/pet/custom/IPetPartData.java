package com.luscastudio.jblob.server.game.habbohotel.pet.custom;

/**
 * Created by Lucas on 08/02/2017 at 22:15.
 */
public interface IPetPartData {

    int getPartType();
    int getPartId();

}
