package com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list;

import com.luscastudio.jblob.api.utils.time.DateTimeUtils;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.notification.server.BroadcastMessageAlertComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.IChatCommand;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 11/12/2016.
 */

public class AboutCmd implements IChatCommand {


    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {


        long rammUsage = (JBlob.getRuntime().totalMemory() - JBlob.getRuntime().freeMemory()) / (1024 * 1024);
        long freeRam = JBlob.getRuntime().totalMemory() / (1024 * 1024);
        int availableCpuCores = JBlob.getRuntime().availableProcessors();
        long ramMax = JBlob.getRuntime().maxMemory() / (1024 * 1024);

        int loadedRooms = JBlob.getGame().getRoomManager().getRoomCount();
        int usersOnline = JBlob.getGame().getSessionManager().getSessions().size();

        int serverUp = DateTimeUtils.getUnixTimestampInt() - JBlob.getGame().getStartedTime();

        int seconds = serverUp % 60;
        int minutes = (serverUp / 60) % 60;
        int hours = ((serverUp / 3600) % 60);
        int days = (serverUp / (3600 * 24) % 24);

        StringBuilder builder;

        builder = new StringBuilder();

        if (days > 0) {
            builder.append("<b>" + days + "</b> days,  ");
            builder.append("<b>" + hours + "</b> hours and  ");
            builder.append("<b>" + minutes + "</b> minutes");
        } else if (hours > 0) {
            builder.append("<b>" + hours + "</b> hours and  ");
            builder.append("<b>" + minutes + "</b> minutes");
        } else if (minutes > 0) {
            builder.append("<b>" + minutes + "</b> minutes and  ");
            builder.append("<b>" + seconds + "</b> seconds");
        } else {
            builder.append("<b>" + seconds + "</b> seconds");
        }

        String timeStr = builder.toString();

        builder = new StringBuilder();
        builder.append("<font color=\"#CCCCCC\"><u>\t\t\t\t\t\t\t\t\t\t\t      </u></font>\n");
        builder.append("<font color=\"#bd3b2f\">µ <b>J</b></font><b><font color=\"#367897\">Blob Emulator©</font></b> - <i>A new concept of Habbo Emulator</i>\n");
        builder.append("\t<font color=\"#CCCCCC\">V1.0 APLHA</u></font>\n");

        builder.append("\n");
        builder.append("<font color=\"#CCCCCC\"><u>\t<font color=\"#7b7b7b\"><b>Statistics</b></font>\t\t\t\t\t\t\t\t     </u></font>\n");
        builder.append("¤ <u>" + usersOnline + "</u> Users online.\n");
        builder.append("¤ <u>" + loadedRooms + "</u> Loaded rooms.\n");
        builder.append("¤ Server up since:\n");
        builder.append("\t" + timeStr + " ago.\n");

        builder.append("\n");
        builder.append("<font color=\"#CCCCCC\"><u>\t<font background-color=\"#FF0000\"color=\"#7b7b7b\"><b>Memory</b></font>\t\t\t\t\t\t\t\t\t      </u></font>\n");
        builder.append("¤ Using <b>" + rammUsage + "</b> of <b>" + freeRam + "</b> MB of ram.\n");
        builder.append("¤ <b>" + availableCpuCores + "</b> CPU Cores available.\n");
        builder.append("¤ Total memory is <b>" + ramMax + "</b> MB.\n");


        builder.append("\n");
        builder.append("<font color=\"#CCCCCC\"><u>\t<font color=\"#7b7b7b\"><b>Credits/Contact</b></font>\t\t\t\t\t\t\t     </u></font>\n");
        builder.append("¤ LuscasLeo\t\t Skype: lucas76leonardo\n");
        builder.append("¤ Wulles\t\t\t Skype: kingkizile\n");
        builder.append("<font size='9' color='#CCCCCC'>Money is the reason we exists—</font>\n");
        builder.append("\t\t\t\t\t<font size='9' color='#CCCCCC'>Everybody know it. its a fact. <i>Kiss Kiss</i>—</font>\n");



        session.sendMessage(new BroadcastMessageAlertComposer(builder.toString()));

        return true;
    }
}
