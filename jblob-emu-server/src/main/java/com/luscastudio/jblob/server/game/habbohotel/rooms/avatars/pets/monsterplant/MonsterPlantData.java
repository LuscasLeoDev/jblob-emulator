package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.monsterplant;

import com.luscastudio.jblob.api.utils.time.DateTimeUtils;

/**
 * Created by Lucas on 09/02/2017 at 17:12.
 */
public class MonsterPlantData {

    private int wellBeingExpireTimestamp;

    private int wellBeingTotalTime;

    private int totalTimeToGrown;
    private int timeToGrownTimeStamp;
    private boolean dead;

    private int startedGrownTimeStamp;

    public MonsterPlantData(int wellBeingTotalTime, int totalTimeToGrown){
        this.dead = false;

        int now = DateTimeUtils.getUnixTimestampInt();

        this.wellBeingTotalTime = wellBeingTotalTime;
        this.wellBeingExpireTimestamp = now + this.wellBeingTotalTime;

        this.totalTimeToGrown = totalTimeToGrown;
        this.timeToGrownTimeStamp = now + this.totalTimeToGrown;
        this.startedGrownTimeStamp = now;
    }

    public int getTotalTimeToGrown() {
        return totalTimeToGrown;
    }

    public int getWellBeingExpireTimestamp() {
        return wellBeingExpireTimestamp;
    }

    public int getWellBeingTotalTime() {
        return wellBeingTotalTime;
    }

    public boolean isDead() {
        return dead;
    }

    public void setWellBeingExpireTimestamp(int wellBeingExpireTimestamp) {
        this.wellBeingExpireTimestamp = wellBeingExpireTimestamp;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public void setTotalTimeToGrown(int totalTimeToGrown) {
        this.totalTimeToGrown = totalTimeToGrown;
    }

    public void setWellBeingTotalTime(int wellBeingTotalTime) {
        this.wellBeingTotalTime = wellBeingTotalTime;
    }

    public int getTimeToGrownTimeStamp() {
        return timeToGrownTimeStamp;
    }

    public void setTimeToGrownTimeStamp(int timeToGrownTimeStamp) {
        this.timeToGrownTimeStamp = timeToGrownTimeStamp;
    }

    public int getStartedGrownTimeStamp() {
        return startedGrownTimeStamp;
    }

    public void setStartedGrownTimeStamp(int startedGrownTimeStamp) {
        this.startedGrownTimeStamp = startedGrownTimeStamp;
    }
}
