package com.luscastudio.jblob.server.manager;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniBase;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.util.Map;

/**
 * Created by Lucas on 09/10/2016.
 */

public class ItemBaseManager implements IFlushable {
    Logger log = Logger.getLogger(this.getClass());
    private Map<Integer, FurniBase> items;

    public ItemBaseManager() {
        this.items = BCollect.newMap();
    }

    @Override
    public void flush() {
        this.items.clear();

        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {
            DBConnPrepare prepare = reactor.prepare("SELECT * FROM items_base");

            ResultSet set = prepare.runQuery();
            while (set.next())
                items.put(set.getInt("id"), new FurniBase(set));

        } catch (Exception e) {
            log.error("Error while getting item base data:", e);
        }
    }

    @Override
    public void performFlush() {

    }

    public FurniBase getItemBase(int id) {
        if (!items.containsKey(id))
            return null;

        return items.get(id);
    }
}
