package com.luscastudio.jblob.server.communication.handlers.player.inventory.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryUpdateRequestComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.RemoveFloorItemComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.RemoveWallItemComposer;
import com.luscastudio.jblob.server.database.ASyncQueryType;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.database.IASyncQueryCallback;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.inventory.AvatarInventory;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.permissions.PermissionManagerPermissions;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.sql.ResultSet;

import static com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer.FURNI;

/**
 * Created by Lucas on 17/10/2016.
 */

public class PickUpRoomItemEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int unknown = packet.getInt();
        int itemId = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();
        if (room == null)
            return;

        IRoomItem roomItem = room.getRoomItemHandlerService().getRoomItemById(itemId);

        if(roomItem == null)
            return;
        //todo: Set permission name as static
        if(roomItem.getOwnerId() != session.getAvatar().getId()
                && !room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_pickup_item")
                && !room.getProperties().avatarHasRight(session.getAvatar().getId(), PermissionManagerPermissions.ROOM_OWNER_RIGHT)
                && !JBlob.getGame().getPermissionManager().validateRight(session.getAvatar().getRank(), "p_pick_user_items"))
            return;

        if(!roomItem.onRemove(session)){
            return;
        }

        room.getRoomItemHandlerService().removeRoomItem(roomItem);

        final FurniProperties properties = roomItem.getProperties();

        final int targetUserId;

        //todo: left static this permission name and preferences
        if(JBlob.getGame().getPermissionManager().validateRight(session.getAvatar().getRank(), "p_pick_user_items") && session.getAvatar().getPreferences().getBool("mod_pick_users_items"))
            targetUserId = session.getAvatar().getId();
        else
            targetUserId = roomItem.getOwnerId();

        try(DBConnReactor reator = JBlob.getGame().getDbConn().getReactor()) {
            DBConnPrepare prepare =reator.prepare("UPDATE avatars_items_data SET room_id = ?, user_id = ? WHERE id = ?");
            prepare.setInt(1, AvatarInventory.ITEM_NOROOM_ID);
            prepare.setInt(2, targetUserId);
            prepare.setInt(3, properties.getId());
            prepare.run();
        } catch(Exception e){
            BLogger.error("Error while adding room item #" + properties.getId() + " to inventory of avatar #" + session.getAvatar().getId(), this.getClass());
        }

        if(targetUserId ==  session.getAvatar().getId()) {
            session.getAvatar().getInventory().addRoomItem(roomItem.getProperties());
            session.sendMessage(new FurniListNotificationComposer().add(FURNI, itemId));
            session.sendMessage(new InventoryUpdateRequestComposer());
        } else {
            PlayerSession targetSession = JBlob.getGame().getSessionManager().getSession(targetUserId);
            if(targetSession != null){
                targetSession.getAvatar().getInventory().addRoomItem(roomItem.getProperties());
                targetSession.sendMessage(new FurniListNotificationComposer().add(FURNI, itemId));
                targetSession.sendMessage(new InventoryUpdateRequestComposer());
            }
        }
        //session.sendMessage(new InventoryAddRoomItemComposer(roomItem.getProperties()));

        if(roomItem.getBase().getType().equals("i"))
            room.sendMessage(new RemoveWallItemComposer(roomItem.getId(), targetUserId));
        else
            room.sendMessage(new RemoveFloorItemComposer(roomItem.getId(), targetUserId));

        roomItem.dispose();

        /*
            note: The Owner id of the item will be the same even if a staff pick up that UNTIL the staff reload its client
             I guess it's more secure
         */
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
