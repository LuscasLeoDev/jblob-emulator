package com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list;

import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.communication.handlers.player.currencies.server.CreditBalanceComposer;
import com.luscastudio.jblob.server.communication.handlers.player.currencies.server.CurrencyComposer;
import com.luscastudio.jblob.server.game.habbohotel.avatar.currency.AvatarCurrencyBalance;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.IChatCommand;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 17/02/2017 at 01:08.
 */
public class GetCurrencyCmd implements IChatCommand {
    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {
        if(params.length() <= 2)
            return true;

        int currencyId;
        int value;
        boolean increase;

        String valueStr = params.get(2);
        if(valueStr.startsWith("+"))
            valueStr = valueStr.substring(1);
        if(!NumberHelper.isInteger(params.get(1)) || !NumberHelper.isInteger(valueStr))
            return true;

        increase = params.get(2).startsWith("+");

        currencyId = Integer.parseInt(params.get(1));
        value = Integer.parseInt(valueStr);

        int newValue;
        session.getAvatar().getBalance().set(currencyId, newValue = (increase ? session.getAvatar().getBalance().get(currencyId) + value : value));

        if(currencyId == AvatarCurrencyBalance.CREDITS_TYPE)
            session.sendMessage(new CreditBalanceComposer(newValue));
        else
            session.sendMessage(new CurrencyComposer(currencyId, newValue));


        return true;
    }
}
