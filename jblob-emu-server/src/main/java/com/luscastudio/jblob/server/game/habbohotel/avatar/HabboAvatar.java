package com.luscastudio.jblob.server.game.habbohotel.avatar;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.events.EventHandler;
import com.luscastudio.jblob.server.game.accounts.PlayerAccount;
import com.luscastudio.jblob.server.game.habbohotel.avatar.currency.AvatarCurrencyBalance;
import com.luscastudio.jblob.server.game.habbohotel.avatar.figure.AvatarFigure;
import com.luscastudio.jblob.server.game.habbohotel.avatar.preference.AvatarPreferences;
import com.luscastudio.jblob.server.game.habbohotel.friendship.AvatarFriendship;
import com.luscastudio.jblob.server.game.habbohotel.inventory.AvatarInventory;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.permissions.AvatarPermissions;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;
import com.luscastudio.jblob.server.process.HabboAvatarExecutorService;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Lucas on 19/09/2016.
 */
public class HabboAvatar implements IAvatarDataCache {

    private PlayerAccount account;
    private int id;
    private String username;
    private String motto;
    private int rank;
    private int homeRoom;

    private AvatarFigure figure;
    private AvatarCurrencyBalance balance;
    private AvatarFriendship friendship;
    private AvatarInventory inventory;
    private boolean online;
    private int currentRoomId;
    private PlayerSession session;

    private HabboAvatarExecutorService executorService;

    private AvatarPreferences preferences;
    private AvatarPermissions permissions;
    private int clubLevel;
    private int achievementScore;

    private int respectCount;

    private int respectsToGiveCount;
    private int respectsToGiveToPetsCount;

    private int lastOnlineTimestamp;

    private EventHandler eventHandler;



    public HabboAvatar(PlayerSession session) {
        this.session = session;
        this.currentRoomId = 0;
        this.online = true;

        this.permissions = new AvatarPermissions();

        this.executorService = new HabboAvatarExecutorService(this);
        this.eventHandler = new EventHandler();
    }

    public HabboAvatar(PlayerSession session, ResultSet set) throws SQLException {
        this(session);
        this.id = set.getInt("id");
        this.username = set.getString("username");
        this.motto = set.getString("motto");
        this.figure = new AvatarFigure(set.getString("figure_string"), set.getString("gender"), this);
        this.homeRoom = set.getInt("home_room");
        this.rank = set.getInt("rank");
        this.clubLevel = set.getInt("club_level");
        this.achievementScore = set.getInt("achievement_score");
        this.respectCount = set.getInt("respect_count");
        this.respectsToGiveCount = set.getInt("respect_give_count");
        this.respectsToGiveToPetsCount = set.getInt("respect_give_pets_count");
        this.lastOnlineTimestamp = set.getInt("last_online_timestamp");
    }

    public Room getCurrentRoom(boolean load) {
        return load ? JBlob.getGame().getRoomManager().loadRoom(currentRoomId) : JBlob.getGame().getRoomManager().getRoom(currentRoomId);
    }

    public Room getCurrentRoom() {
        return getCurrentRoom(false);
    }

    public AvatarFriendship getFriendship() {
        return friendship;
    }

    public void setFriendship(AvatarFriendship friendship) {
        this.friendship = friendship;
    }

    public void setHomeRoom(int homeRoom) {
        this.homeRoom = homeRoom;
    }

    public AvatarFigure getFigure() {
        return figure;
    }

    public void setFigure(AvatarFigure figure) {
        this.figure = figure;
    }

    public void setCurrentRoomId(int currentRoomId) {
        this.currentRoomId = currentRoomId;
    }

    public PlayerSession getSession() {
        return session;
    }

    public AvatarInventory getInventory() {
        return inventory;
    }

    public void setInventory(AvatarInventory inventory) {
        this.inventory = inventory;
    }

    public PlayerAccount getAccount() {
        return account;
    }

    public AvatarCurrencyBalance getBalance() {
        return balance;
    }

    public void setBalance(AvatarCurrencyBalance balance) {
        this.balance = balance;
    }

    public HabboAvatarExecutorService getExecutorService() {
        return executorService;
    }

    public AvatarPreferences getPreferences(){
        return this.preferences;
    }

    public void setPreferences(AvatarPreferences preferences) {
        this.preferences = preferences;
    }

    //region #ACCOUNT

    @Override
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    //region IAvatarDataCache

    @Override
    public String getFigureString() {
        return figure.toString();
    }

    @Override
    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    @Override
    public String getGender() {
        return figure.getGender();
    }

    @Override
    public boolean isInRoom() {
        return currentRoomId != 0;
    }

    @Override
    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public int getRank() {
        return rank;
    }

    //endregion

    //endregion

    public int getClubLevel() {
        return clubLevel;
    }

    public AvatarPermissions getPermissions() {
        return permissions;
    }

    public int getHomeRoom() {
        return homeRoom;
    }

    public void dispose(){
        this.executorService.stop(false); //todo: improve
        if(!this.executorService.isSaving())
            this.executorService.save();
    }

    public void save(){

    }

    public int getAchievementScore() {
        return achievementScore;
    }

    public int getRespectCount() {
        return respectCount;
    }

    public void setRespectCount(int respectCount) {
        this.respectCount = respectCount;
    }

    public int getRespectsToGiveCount() {
        return respectsToGiveCount;
    }

    public void setRespectsToGiveCount(int respectsToGiveCount) {
        this.respectsToGiveCount = respectsToGiveCount;
    }

    public int getRespectsToGiveToPetsCount() {
        return respectsToGiveToPetsCount;
    }

    public void setRespectsToGiveToPetsCount(int respectsToGiveToPetsCount) {
        this.respectsToGiveToPetsCount = respectsToGiveToPetsCount;
    }

    public int getLastOnlineTimestamp() {
        return lastOnlineTimestamp;
    }

    public void setLastOnlineTimestamp(int lastOnlineTimestamp) {
        this.lastOnlineTimestamp = lastOnlineTimestamp;
    }

    public EventHandler getEventHandler() {
        return eventHandler;
    }
}
