package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.chat.server.ChatComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.CloseFlatConnectionComposer;
import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.events.list.AvatarSaysSomethingEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.avatar.preference.AvatarPreferences;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;


/**
 * Created by Lucas on 04/10/2016.
 */

public class ChatEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        String message = packet.getString();
        int colorId = packet.getInt();
        //int messageIndex = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();

        if (room == null) {
            session.sendMessage(new CloseFlatConnectionComposer());
            return;
        }

        IRoomAvatar avatar = room.getRoomAvatarService().getRoomAvatarByUserId(session.getAvatar().getId());

        if (avatar == null) {
            return;
        }


        if(!this.validateChat(session, avatar, room, message) || this.validateCommand(session, avatar, room, message)) {
            return;
        }

        if(!this.handleChat(session, avatar, room, message, colorId))
            return;

        if(JBlob.getGame().getChatCommandManager().handleCommand(session, avatar, room, message)){
            //todo: Add Command Logger
            return;
        }

        this.sendMessage(avatar, room, message, colorId);

    }

    @Override
    public boolean isAsync() {
        return false;
    }

    private boolean validateChat(PlayerSession session, IRoomAvatar avatar, Room room, String message){
        return true;
    }

    private boolean handleChat(PlayerSession session, IRoomAvatar avatar, Room room, String message, int colorId){
        if(session.getAvatar().getPreferences().getIntOrDefault(AvatarPreferences.LAST_CHAT_BUBBLE, 0) != colorId){
            session.getAvatar().getPreferences().setInt(AvatarPreferences.LAST_CHAT_BUBBLE, colorId);
        }

        EventArgs args = room.getEventHandler().fireEvent("avatar.says.something", new AvatarSaysSomethingEventArgs(avatar, message));
        return !args.isCancelled();
    }

    private boolean validateCommand(PlayerSession session, IRoomAvatar roomAvatar, Room room, String message){
        return JBlob.getGame().getChatCommandManager().handleCommand(session, roomAvatar, room, message);

    }

    private void sendMessage(IRoomAvatar avatar, Room room, String message, int colorId){
        //todo: Add Message Logger
        room.sendQueueMessage(new ChatComposer(avatar.getVirtualId(), colorId, 0, message));
        room.flush();
    }
}
