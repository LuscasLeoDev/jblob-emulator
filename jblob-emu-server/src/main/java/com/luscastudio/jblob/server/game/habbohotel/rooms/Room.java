package com.luscastudio.jblob.server.game.habbohotel.rooms;

import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server.RoomUsersComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.client.FloorHeightMapComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.FloorItemsComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.WallItemsComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.*;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.events.EventHandler;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomAvatarService;
import com.luscastudio.jblob.server.game.habbohotel.rooms.engine.RoomMap;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.RoomItemHandlerService;
import com.luscastudio.jblob.server.game.players.IMessageComposerHandler;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;
import java.util.Map;

/**
 * Created by Lucas on 03/10/2016.
 */

public class Room implements IMessageComposerHandler {

    private RoomProperties properties;
    private RoomMap roomMap;

    private RoomAvatarService roomAvatarService;
    private RoomProcess roomProcess;

    private RoomItemHandlerService roomItemHandlerService;

    private EventHandler eventHandler;

    public Room(RoomProperties prop) {
        this.eventHandler = new EventHandler();

        this.roomProcess = new RoomProcess();

        this.properties = prop;
        this.roomMap = new RoomMap(this);

        this.roomItemHandlerService = new RoomItemHandlerService(this);

        this.roomAvatarService = new RoomAvatarService(this);

        this.roomItemHandlerService.init();
        this.roomAvatarService.init();

        this.roomProcess.start();
    }

    public RoomProcess getRoomProcess() {
        return roomProcess;
    }

    public int getUsersNow() {
        return roomAvatarService.getUsersNow();
    }

    public RoomAvatarService getRoomAvatarService() {
        return roomAvatarService;
    }

    public RoomItemHandlerService getRoomItemHandlerService() {
        return roomItemHandlerService;
    }

    public RoomMap getRoomMap() {
        return roomMap;
    }

    public RoomProperties getProperties() {
        return properties;
    }

    public void sendQueuedObjects(PlayerSession session) {

        Collection<IRoomAvatar> allAvatarsList = roomAvatarService.getAllAvatarsList();

        //Sending HeightMap
        session.sendQueueMessage(new HeightMapComposer(this.getRoomMap()));
        session.sendQueueMessage(new FloorHeightMapComposer(this.getProperties().getModel()));

        //Sending Furnis
        session.sendQueueMessage(new FloorItemsComposer(this.getRoomItemHandlerService().getRoomsItemsOwnersName(), this.roomItemHandlerService.getFloorItems()));
        session.sendQueueMessage(new WallItemsComposer(this.roomItemHandlerService.getWallItems()));

        //Sending Users
        session.sendQueueMessage(new RoomUsersComposer(allAvatarsList));
        session.sendQueueMessage(new UserUpdateComposer(allAvatarsList));


        session.sendQueueMessage(new RoomEntryInfoComposer(this.getProperties().getId(), this.getProperties().avatarHasRight(session.getAvatar().getId(), "room_owner_right")));

        session.sendQueueMessage(new RoomVisualizationSettingsComposer(this.getProperties().getHideWall() == 1, this.getProperties().getWallThickness(), this.getProperties().getFloorThickness()));
        session.sendQueueMessage(new RoomEventComposer());

        if(this.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_manage_doorbell")){
            for (String name : this.roomAvatarService.getDoorBellQueue()) {
                session.sendQueueMessage(new DoorbellComposer(name));
            }
        }

    }


    public void sendObjects(PlayerSession session) {
        sendQueuedObjects(session);
        session.flush();
    }

    public EventHandler getEventHandler() {
        return eventHandler;
    }

    //region Message Sending [IMessageComposerHandler]
    @Override
    public IMessageComposerHandler  sendQueueMessage(MessageComposer message) {
        for (Map.Entry<Integer, PlayerSession> avatar : roomAvatarService.getSessions()) {
            avatar.getValue().sendQueueMessage(message);
        }
        return this;
    }

    @Override
    public void flush() {
        for (Map.Entry<Integer, PlayerSession> avatar : roomAvatarService.getSessions()) {
            avatar.getValue().flush();
        }
    }

    @Override
    public void sendMessage(MessageComposer message) {
        for (Map.Entry<Integer, PlayerSession> avatar : roomAvatarService.getSessions()) {
            avatar.getValue().sendMessage(message);
        }
    }

    public void dispose() {

        try {
            this.sendMessage(new CloseFlatConnectionComposer());
            this.getRoomAvatarService().removeAllPlayers();
            this.getRoomProcess().stop();

            this.roomItemHandlerService.dispose();
            this.roomItemHandlerService = null;

            this.roomAvatarService.dispose();
            this.roomAvatarService = null;

            this.roomMap.dispose();
            this.roomMap = null;

            this.eventHandler.dispose();
            this.eventHandler = null;

            this.properties.setRoom(null);
        }catch (Exception e){
            BLogger.error(e, this.getClass());
        }
    }

    //endregion


}
