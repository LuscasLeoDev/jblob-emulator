package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 17/02/2017 at 01:55.
 */
public class EnforceCategoryUpdateComposer extends MessageComposer {
    @Override
    public String id() {
        return "EnforceCategoryUpdateMessageComposer";
    }

    public EnforceCategoryUpdateComposer(int roomId){
        message.putInt(roomId);
    }
}
