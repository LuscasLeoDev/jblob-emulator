//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;

/**
 * Created by Lucas on 08/03/2017 at 15:57.
 */
public class AvatarWalkOnFurniEventArgs extends EventArgs {
    private IRoomItem item;
    private IRoomAvatar avatar;

    public AvatarWalkOnFurniEventArgs(IRoomItem item, IRoomAvatar avatar) {
        this.item = item;
        this.avatar = avatar;
    }

    public AvatarWalkOnFurniEventArgs(IRoomItem item) {
        this.item = item;
    }

    public IRoomItem getItem() {
        return item;
    }

    public IRoomAvatar getAvatar() {
        return avatar;
    }
}
