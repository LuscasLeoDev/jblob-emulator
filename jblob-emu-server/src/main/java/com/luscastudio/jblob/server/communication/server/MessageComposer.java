package com.luscastudio.jblob.server.communication.server;

import com.luscastudio.jblob.server.game.sessions.PlayerSession;
import io.netty.buffer.ByteBuf;

/**
 * Created by Lucas on 18/09/2016.
 */
public abstract class MessageComposer {

    protected PacketWriting message = new PacketWriting();
    private boolean composed = false;

    public abstract String id();

    public void parse(MessageBufferComposer message) {

        for (PacketWritingValue entry : this.message.data) {

            PacketWriting.PacketWritingType type = entry.type;
            PacketWritingValue value = entry;

            switch (type) {
                case BOOL:
                    message.putBool(value.boolValue);
                    break;

                case INT:
                    message.putInt(value.intValue);
                    break;

                case STRING:
                    message.putString(value.strValue);
                    break;

                case SHORT:
                    message.putShort(value.shortValue);
                    break;

                case BYTE:
                    message.putByte(value.byteValue);
                    break;

                case DOUBLE:
                    message.putDouble(value.doubleValue);
                    break;

            }

        }
    }

    public void composePacket() {

        if(!this.composed) {
            this.composed = true;
            this.compose();
        }

        /*MessageBufferComposer composer = new MessageBufferComposer(buffer);
        composer.putInt(-1);
        composer.putShort((short)1);
        composer.content().setInt(0, composer.content().writerIndex() - 4);

        return composer;*/

        /*//Why
        composer.putInt(-1);

        short header = -1;

        //Putting Header
        composer.putShort(header);

        //Putting Content
        this.parse(composer);

        if (!composer.hasLength()) {
            composer.content().setInt(0, composer.content().writerIndex() - 4);
        }*/
    }

    public void compose(){

    }

}
