package com.luscastudio.jblob.server.communication.handlers.messenger.client;

import com.luscastudio.jblob.api.utils.time.DateTimeUtils;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.messenger.server.MessengerMessageComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 02/10/2016.
 */
public class SendMessengerMessageEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int targetId = packet.getInt();
        String message = JBlob.getGame().getFilter().filterMessage(packet.getString());

        PlayerSession targetSession = JBlob.getGame().getSessionManager().getSession(targetId);
        if (targetSession == null)
            return;

        if(targetSession.getAvatar().getFriendship().getFriend(session.getAvatar().getId()) == null)
            return;

        targetSession.sendMessage(new MessengerMessageComposer(session.getAvatar().getId(), message, DateTimeUtils.getUnixTimestampInt()));
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
