package com.luscastudio.jblob.server.communication.handlers.rooms.server;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;

import java.util.Collection;
import java.util.List;

/**
 * Created by Lucas on 02/02/2017 at 18:09.
 */
public class FloorPlanFloorMapComposer extends MessageComposer {
    @Override
    public String id() {
        return "FloorPlanFloorMapMessageComposer";
    }

    public FloorPlanFloorMapComposer(Collection<IRoomItem> items) {
        List<Point> points = BCollect.newList();

        items.forEach(iRoomItem -> iRoomItem.getAffectedTiles().forEach(point -> {if(!points.contains(point)) points.add(point);}));

        message.putInt(points.size());

        points.forEach(point -> message.putInt(point.getX()).putInt(point.getY()));
    }
}
