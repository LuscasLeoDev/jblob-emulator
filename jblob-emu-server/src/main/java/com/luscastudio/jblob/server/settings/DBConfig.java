package com.luscastudio.jblob.server.settings;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;

import java.sql.ResultSet;
import java.util.Map;

/**
 * Created by Lucas on 09/12/2016.
 */

public class DBConfig implements IFlushable{

    private Map<String, String> variables;

    public DBConfig(){
        this.variables = BCollect.newConcurrentMap();

        this.flush();

    }

    public String get(String key){
        return variables.get(key);
    }

    public String getOrDefault(String key, String def){
        String v = get(key);
        return v == null ? def : v;
    }

    public int getInt(String key, int def){
        String v = get(key);

        return v != null && NumberHelper.isInteger(v) ? Integer.parseInt(v) : def;
    }

    public int getInt(String key){
        return getInt(key, 0);
    }

    public void flush(){

        this.variables.clear();

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){

            DBConnPrepare prepare = reactor.prepare("SELECT * FROM server_config");

            ResultSet set = prepare.runQuery();

            while(set.next()){
                String key;
                if(this.variables.containsKey(key = set.getString("name"))) {
                    BLogger.warn("server_config table has twice variables with same name '" + key + "'", this.getClass());
                    continue;
                }

                this.variables.put(key, set.getString("value"));
            }

        }catch (Exception e){
            BLogger.error(e, this.getClass());
        }

    }

    @Override
    public void performFlush() {

    }


}
