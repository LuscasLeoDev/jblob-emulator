package com.luscastudio.jblob.server.game.habbohotel.rooms.items;

import com.luscastudio.jblob.api.utils.engine.ObjectPosition;
import com.luscastudio.jblob.api.utils.engine.Point;

/**
 * Created by Lucas on 22/12/2016 at 23:55.
 */
public class RoomItemPosition extends ObjectPosition implements IRoomItemPosition{

    protected int width;
    protected int length;

    public RoomItemPosition(int width, int length, int x, int y, double z, int rotation) {
        super(x, y, z, rotation);
        this.width = width;
        this.length = length;
    }

    @Override
    public Point getSquareInFront(int index) {
        Point p;
        switch (this.rotation){
            default:
            case 0:
                p = new Point(this.x + index, this.y - 1);
            break;

            case 2:
                p = new Point(this.x + 1, this.y + index);
            break;

            case 4:
                p = new Point(this.x + index, this.y + 1);
            break;

            case 6:
                p = new Point(this.x - 1, this.y + index);
            break;
        }

        return p;
    }

    @Override
    public Point getSquareBehind(int index) {
        Point p;
        switch (this.rotation){
            default:
            case 0:
                p = new Point(this.x + index, this.y + 1);
                break;

            case 2:
                p = new Point(this.x - 1, this.y + index);
                break;

            case 4:
                p = new Point(this.x + index, this.y - 1);
                break;

            case 6:
                p = new Point(this.x + 1, this.y + index);
                break;
        }

        return p;
    }

    @Override
    public Point getSquareLeft(int index) {
        Point p;
        switch (this.rotation){
            default:
            case 0:
                p = new Point(this.x + 1, this.y + index);
                break;

            case 2:
                p = new Point(this.x + index, this.y - 1);
                break;

            case 4:
                p = new Point(this.x - 1, this.y + index);
                break;

            case 6:
                p = new Point(this.x + index, this.y + 1);
                break;
        }

        return p;
    }

    @Override
    public Point getSquareRight(int index) {
        Point p;
        switch (this.rotation){
            default:
            case 0:
                p = new Point(this.x - 1, this.y + index);
                break;

            case 2:
                p = new Point(this.x + index, this.y + 1);
                break;

            case 4:
                p = new Point(this.x + 1, this.y + index);
                break;

            case 6:
                p = new Point(this.x + index, this.y - 1);
                break;
        }

        return p;
    }


}
