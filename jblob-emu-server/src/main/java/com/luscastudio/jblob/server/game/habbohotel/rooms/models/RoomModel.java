package com.luscastudio.jblob.server.game.habbohotel.rooms.models;

import com.luscastudio.jblob.api.utils.engine.IObjectPosition;
import com.luscastudio.jblob.api.utils.engine.ObjectPosition;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Lucas on 03/10/2016.
 */

public class RoomModel {
    private String name;
    private String modelString;
    private int wallHeight;
    private ObjectPosition doorPosition;
    private Logger log = Logger.getLogger(this.getClass());

    private int sizeX;
    private int sizeY;

    private boolean custom;

    public RoomModel(ResultSet set) throws SQLException {
        this.name = set.getString("name");
        this.modelString = set.getString("floor_data").replace("\n", "");
        this.wallHeight = Integer.parseInt(set.getString("wall_height"));

        String[] a = modelString.split("\r");
        this.sizeX = a[0].length();
        this.sizeY = a.length;


        int x = Integer.parseInt(set.getString("door_x"));
        int y = Integer.parseInt(set.getString("door_y"));
        //double z = Double.parseDouble(set.getString("door_z"));
        int doorRotation = set.getInt("door_rotation");

        this.doorPosition = new ObjectPosition(x, y, 0, doorRotation);

        this.custom = set.getInt("custom") == 1;
    }

    public RoomModel(String name, String modelString, int wallHeight, int doorX, int doorY, int doorRotation, boolean custom){
        this.name = name;
        this.modelString = modelString;

        this.doorPosition = new ObjectPosition(doorX, doorY, 0, doorRotation);

        this.wallHeight = wallHeight;

        this.custom = custom;
    }

    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public IObjectPosition getDoorPosition() {
        return doorPosition;
    }

    public String getModelString() {
        return modelString;
    }

    public String getName() {
        return name;
    }

    public int getWallHeight() {
        return wallHeight;
    }

    public int getTotalSize() {
        return sizeX * sizeY;
    }

    public boolean isCustom() {
        return custom;
    }

    public void dispose(){
        this.name = null;
        this.modelString = null;

        this.doorPosition = null;
    }
}
