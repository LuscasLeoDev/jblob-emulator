package com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata;

import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.IItemMessageParser;

/**
 * Created by Lucas on 10/10/2016.
 */
public interface IRoomItemExtradata {

    String getExtradata();

    String getExtradataToDb();

    void setExtradata(String extradata);

    void init(FurniProperties properties);

    IItemMessageParser getMessageParser();
}
