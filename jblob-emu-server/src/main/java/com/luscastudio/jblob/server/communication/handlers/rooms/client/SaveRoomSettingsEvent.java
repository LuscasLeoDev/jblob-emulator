package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.boot.JBlobSettings;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomInfoUpdatedComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomSettingsSavedComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomVisualizationSettingsComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.navigator.flatcats.NavigatorFlatCategory;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;

/**
 * Created by Lucas on 11/12/2016.
 */

public class SaveRoomSettingsEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int roomId = packet.getInt();

        RoomProperties roomData = JBlob.getGame().getRoomManager().getRoomData(roomId);
        if(roomData == null)
            return;

        String name = JBlob.getGame().getFilter().filterMessage(packet.getString());
        String description = JBlob.getGame().getFilter().filterMessage(packet.getString());

        int lockType = packet.getInt();
        String password = packet.getString();
        int maxUsers = packet.getInt();
        int categoryId = packet.getInt();

        List<String> tags = BCollect.newList();

        int tagCount = packet.getInt();

        for(int i = 0; i < tagCount; i++)
            tags.add(packet.getString());

        int tradeType = packet.getInt();

        boolean allowPets = packet.getBool();
        boolean allowPetsEat = packet.getBool();
        boolean roomBlocking = packet.getBool();
        boolean hideWall = packet.getBool();

        int wallThickness = packet.getInt();
        int floorThickness = packet.getInt();

        int whoCanMute = packet.getInt();
        int whoCanKick = packet.getInt();
        int whoCanBan = packet.getInt();

        int chatMode = packet.getInt();
        int chatSize = packet.getInt();
        int chatSpeed = packet.getInt();
        int chatDistance = packet.getInt();
        int chatExtraFlood = packet.getInt();

        if (chatMode < 0 || chatMode > 1)
            chatMode = 0;

        if (chatSize < 0 || chatSize > 2)
            chatSize = 0;

        if (chatSpeed < 0 || chatSpeed > 2)
            chatSpeed = 0;

        if (chatDistance < 0)
            chatDistance = 1;

        if (chatDistance > 99)
            chatDistance = 100;

        if (chatExtraFlood < 0 || chatExtraFlood > 2)
            chatExtraFlood = 0;

        if (tradeType < 0 || tradeType > 2)
            tradeType = 0;

        if (whoCanMute < 0 || whoCanMute > 1)
            whoCanMute = 0;

        if (whoCanKick < 0 || whoCanKick > 1)
            whoCanKick = 0;

        if (whoCanBan < 0 || whoCanBan > 1)
            whoCanBan = 0;

        if (wallThickness < -2 || wallThickness > 1)
            wallThickness = 0;

        if (floorThickness < -2 || floorThickness > 1)
            floorThickness = 0;

        if (name.length() < 1)
            return;

        if (name.length() > 60)
            name = name.substring(0, 60);

        if (lockType == 2 && password.length() == 0)
            lockType = 0;

        NavigatorFlatCategory category = JBlob.getGame().getNavigatorManager().getFlatCategory(categoryId);

        //todo: check if user can use this category
        if(category == null)
            categoryId = JBlob.getGame().getDbConfig().getInt("navigator.default.category.id", 0);

        if(tagCount > JBlob.getGame().getDbConfig().getInt("navigator.room.max.tags"))
            return;

        roomData.setName(name);
        roomData.setDescription(description);

        roomData.setLockType(lockType);
        roomData.setPassword(password);

        roomData.setMaxUsers(maxUsers);
        roomData.setCategoryId(categoryId);
        roomData.setTags(tags);

        roomData.setTradeType(tradeType);

        roomData.setAllowPets(allowPets);
        roomData.setAllowPetsEating(allowPetsEat);

        roomData.setAllowWalkTrough(roomBlocking);


        roomData.setHideWall(hideWall);
        roomData.setWallThickness(wallThickness);
        roomData.setFloorThickness(floorThickness);

        roomData.setChatMode(chatMode);
        roomData.setChatSize(chatSize);
        roomData.setChatSpeed(chatSpeed);
        roomData.setChatDistance(chatDistance);
        roomData.setChatExtraFlood(chatExtraFlood);

        roomData.setWhoCanMute(whoCanMute);
        roomData.setWhoCanKick(whoCanKick);
        roomData.setWhoCanBan(whoCanBan);

        Room room = JBlob.getGame().getRoomManager().getRoom(roomId);
        if(room != null){
            room.sendQueueMessage(new RoomSettingsSavedComposer(roomId));
            room.sendQueueMessage(new RoomInfoUpdatedComposer(roomId));
            room.sendQueueMessage(new RoomVisualizationSettingsComposer(hideWall, wallThickness, floorThickness));
            //todo: Think about the necessity of this:
            //room.sendQueueMessage(new GuestRoomResultComposer(true, false, room.getProperties(), false));
            room.flush();
        }

        if(session.getAvatar().getCurrentRoom() != room){
            session.sendQueueMessage(new RoomSettingsSavedComposer(roomId));
            session.sendQueueMessage(new RoomInfoUpdatedComposer(roomId)).flush();
        }


        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){

            DBConnPrepare prepare = reactor.prepare("UPDATE players_rooms SET name = ?, description = ?, lock_type = ?, password = ?, max_users = ?, trade_type = ?, category_id = ?, tags = ?, who_can_mute = ?, who_can_kick = ?, who_can_ban = ?, chat_mode = ?, chat_size = ?, chat_speed = ?, chat_extra_flood = ?, chat_distance = ?, allow_pets = ?, allow_pets_eating = ?, hide_wall = ?, wall_thickness = ?, floor_thickness = ?, allow_walk_trough  = ? WHERE id = ?");

            prepare.setString(1, roomData.getName());
            prepare.setString(2, roomData.getDescription());

            prepare.setInt(3, roomData.getLockType());
            prepare.setString(4, roomData.getPassword());

            prepare.setInt(5, roomData.getMaxUsers());

            prepare.setInt(6, roomData.getTradeType());

            prepare.setInt(7, roomData.getCategoryId());
            prepare.setString(8, String.join(JBlobSettings.ROOM_PROPERTIES_TAG_DELIMITER, roomData.getTags()));

            prepare.setInt(9, roomData.getWhoCanMute());
            prepare.setInt(10, roomData.getWhoCanKick());
            prepare.setInt(11, roomData.getWhoCanBan());

            prepare.setInt(12, roomData.getChatMode());
            prepare.setInt(13, roomData.getChatSize());
            prepare.setInt(14, roomData.getChatSpeed());
            prepare.setInt(15, roomData.getChatExtraFlood());
            prepare.setInt(16, roomData.getChatDistance());

            prepare.setInt(17, roomData.getAllowPets());
            prepare.setInt(18, roomData.getAllowPetsEating());

            prepare.setInt(19, roomData.getHideWall());
            prepare.setInt(20, roomData.getWallThickness());
            prepare.setInt(21, roomData.getFloorThickness());

            prepare.setInt(22, roomData.getAllowWalkTrough());
            prepare.setInt(23, roomData.getId());

            prepare.runUpdate();


        }catch (Exception e){
            BLogger.error(e, this.getClass());
        }
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
