package com.luscastudio.jblob.server.communication.handlers.rooms.pets.server;

import com.luscastudio.jblob.server.communication.handlers.rooms.items.ItemSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;

/**
 * Created by Lucas on 09/02/2017 at 19:20.
 */
public class RespectPetNotificationComposer extends MessageComposer {
    @Override
    public String id() {
        return "RespectPetNotificationMessageComposer";
    }

    public RespectPetNotificationComposer(IRoomPetAvatar petAvatar){

        message.putInt(petAvatar.getVirtualId());
        message.putInt(0); //not in use

        ItemSerializer.serializeInventoryPet(message, petAvatar.getPetData());
    }
}
