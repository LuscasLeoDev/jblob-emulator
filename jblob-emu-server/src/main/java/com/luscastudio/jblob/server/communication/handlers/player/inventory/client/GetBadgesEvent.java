package com.luscastudio.jblob.server.communication.handlers.player.inventory.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.BadgesComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 10/11/2016.
 */

public class GetBadgesEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        session.sendMessage(new BadgesComposer(session.getAvatar().getInventory().getBadgeList()));
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
