package com.luscastudio.jblob.server.communication.handlers.rooms.server;

/**
 * Created by Lucas on 03/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;

public class RoomEnterErrorAlertComposer extends MessageComposer {

    public static int ERROR_ROOM_FULL = 1;
    public static int ERROR_ROOM_QUEUE_ERROR = 3;
    public static int ERROR_ROOM_BANNED = 4;
    public static int ERROR_UNKNOWN = -1;

    public RoomEnterErrorAlertComposer(int error, String message) {
        this.message.putInt(error);
        this.message.putString(message);
    }

    public RoomEnterErrorAlertComposer(int error) {
        this.message.putInt(error);
        this.message.putString("");
    }

    @Override
    public String id() {
        return "CantConnectMessageComposer";
    }


}
