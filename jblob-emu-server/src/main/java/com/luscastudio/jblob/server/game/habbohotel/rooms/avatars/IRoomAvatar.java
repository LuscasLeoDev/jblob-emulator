package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars;

import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.events.EventHandler;
import com.luscastudio.jblob.server.game.habbohotel.rooms.IAvatarObjectPosition;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.statusses.RoomAvatarStatusses;
import com.luscastudio.jblob.server.game.habbohotel.rooms.engine.path.RoomAvatarPath;

/**
 * Created by Lucas on 03/10/2016.
 */

public interface IRoomAvatar {

    int getVirtualId();

    int getId();

    String getName();

    String getMotto();

    String getFigureString();

    RoomAvatarStatusses getStatusses();

    IAvatarObjectPosition getPosition();

    IAvatarObjectPosition getSetPosition();

    RoomAvatarType getType();

    int getAvatarType();

    void setPosition(int x, int y, double z);

    void setSetPosition(int x, int y, double z);

    boolean updateNeeded();

    boolean updateNeeded(boolean b);

    RoomAvatarPath getPath();

    Room getRoom();

    void moveTo(Point p);

    void clearMovement();

    void composeAvatar(PacketWriting message);

    //Group getGroup();

    void onEnter();

    void onLeave();

    EventHandler getEventHandler();

    void save(DBConnReactor reactor);

    void dispose();

    double getAditionalHeight();

    double setAditionalHeight(double height);

    int getEffectId();

    void setEffectId(int effectId);

    //boolean say(String message, int colorId, int ge)

}
