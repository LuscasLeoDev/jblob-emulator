package com.luscastudio.jblob.server.game.habbohotel.catalog;

import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniBase;

import java.util.List;

/**
 * Created by Lucas on 28/10/2016.
 */

public abstract class CatalogDealItem {

    private int id;
    protected FurniBase base;

    private String extradata;
    private int limitedAmount;
    private int amount;
    private String type;
    private boolean rare;

    public CatalogDealItem(int id, int dealId, int baseId, String extradata, int limitedAmount, int amount, String type, boolean isRare) {
        this.id = id;
        this.base = null;

        this.extradata = extradata;
        this.limitedAmount = limitedAmount;
        this.amount = amount;
        this.type = type;
        this.dealId = dealId;
        this.rare = isRare;
    }

    private int dealId;

    public int getId() {
        return id;
    }

    public FurniBase getBase() {
        return base;
    }

    public String getExtradata() {
        return extradata;
    }

    public int getLimitedAmount() {
        return limitedAmount;
    }

    public boolean isLimited(){
        return limitedAmount > 0;
    }

    public int getDealId() {
        return dealId;
    }

    public String getType() {
        return type;
    }

    public int getAmount() {
        return amount;
    }

    //todo: complete limited system
    public int getLimitedReaming() {
        return 0;
    }

    public void parsePageComposer(PacketWriting message){
        if(this.getBase() == null)
            message.putInt(0);
        else
            message.putInt(this.getBase().getSpriteId());
        message.putString(this.getExtradata());
        message.putInt(this.getAmount());

        message.putBool(this.isLimited());
        if (this.isLimited()) {
            message.putInt(this.getLimitedAmount());
            message.putInt(this.getLimitedReaming());
        }
    }

    public abstract boolean generateValue(HabboAvatar avatar, String pageExtradata, List<Object> outValues, int amount);

    public abstract void handleValues(HabboAvatar avatar, List<Object> values);

    public boolean isRare() {
        return rare;
    }
}
