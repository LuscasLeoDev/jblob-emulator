package com.luscastudio.jblob.server.boot;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.io.AsyncBufferReader;
import com.luscastudio.jblob.api.utils.io.ReadingCallBack;
import com.luscastudio.jblob.api.utils.json.JSONUtils;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.communication.handlers.catalog.server.CatalogUpdateComposer;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.JBlobGame;
import io.netty.util.CharsetUtil;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.nio.charset.Charset;
import java.util.List;

/**
 * Created by Lucas on 13/09/2016.
 */
public class JBlob {

    private static Logger log = Logger.getLogger(JBlob.class);

    private static JBlobGame game;

    private static Runtime runtime;

    private static boolean serverRunning = false;

    private static AsyncBufferReader reader;

    public static JBlobGame getGame() {
        return game;
    }

    public static Runtime getRuntime() {
        return runtime;
    }

    public static void main(String[] args) {

        //Initializing static classes
        JSONUtils.init();

        reader = new AsyncBufferReader(new ReadingCallBack() {
            @Override
            public void onRead(String message) {
                JBlob.handleCommand(message);

                if(JBlob.serverRunning)
                    JBlob.reader.beginRead();
            }
        }, System.in);

        try {
            //PropertyConfigurator.configure(new FileInputStream("./log4j.properties"));
            BasicConfigurator.configure();
        } catch (Exception e) {
            log.error("Error while loading log4j configuration", e);
            //return;
        }

        runtime = Runtime.getRuntime();



        game = new JBlobGame();
        game.init();

        reader.beginRead();

        serverRunning = true;

        while (serverRunning) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void doTest(){

    }

    public static void handleCommand(String cmd) {

        String[] params = cmd.toLowerCase().split(" ");

        switch (params[0]) {
            default:
                //System.out.println("Error! Command '" + cmd + "' not found");
                String text = "Error! Command '" + cmd + "' not found";
                /*System.out.println("\u001b["  // Prefix
                        + "0"        // Brightness
                        + ";"        // Separator
                        + "31"       // Red foreground
                        + "m"        // Suffix
                        + text       // the text to output
                        + "\u001b[m " // Prefix + Suffix to reset color
                );*/

                System.err.println(text);
                BLogger.error("test", JBlob.class);

                break;

            case "refreshbuilds":
                try {
                    game.getBuildManager().flush();
                    System.out.println("Builds reloaded!");
                    break;
                }catch (Exception e){
                    log.error("Error while updating packets: ", e);
                }
            case "refreshpackets":
                game.getMessageEventManager().flush();
                System.out.println("Packets reloaded!");
                break;

            case "refreshmodels":
                game.getModelManager().flush();
                System.out.println("Room Models reloaded!");
                break;

            case "refreshfurnis":
                game.getItemBaseManager().flush();
                System.out.println("Items Base reloaded!");
                break;

            case "refreshnavigator":
                game.getNavigatorManager().flush();
                System.out.println("Navigator reloaded!");
                break;


            case "unloadroom":
                if(params.length <= 1 || !NumberHelper.isInteger(params[1]))
                    break;

                game.getRoomManager().unloadRoom(Integer.parseInt(params[1]));
                break;

            case "refreshcatalog":
                game.getCatalogManager().flush();
                game.getSessionManager().sendMessage(new CatalogUpdateComposer(false));
                System.out.println("Catalog reloaded!");
                break;

            case "refreshpermissions":
                try {
                    game.getPermissionManager().flush();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case "shutdown":
                JBlob.shutdown();
                break;

            case "gc":
                System.gc();
                break;

            case "unloadrooms":
                List<Integer> ids = BCollect.newList(game.getRoomManager().getActiveRooms().keySet());
                for (Integer id : ids) {
                    game.getRoomManager().unloadRoom(id);
                }
                break;
        }
    }

    public static void shutdown() {
        JBlob.serverRunning = false;
        JBlob.game.close();
        JBlob.reader.stop();

        System.exit(0);
    }

    public static Charset getDefaultCharset() {
        return CharsetUtil.UTF_8;
    }
}