package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.horse.client;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server.AvatarEffectComposer;
import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.events.EventDelegate;
import com.luscastudio.jblob.server.events.list.AvatarRideHorseEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.horse.HorseRoomPetAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;
import java.util.Random;

/**
 * Created by Lucas on 19/02/2017 at 23:14.
 */
public class RideHorseEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int petId = packet.getInt();
        boolean ride = packet.getBool();
        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        IRoomPetAvatar petAvatar = room.getRoomAvatarService().getPet(petId);

        if(petAvatar == null || !(petAvatar instanceof HorseRoomPetAvatar))
            return;

        IRoomAvatar avatar = room.getRoomAvatarService().getRoomAvatarByUserId(session.getAvatar().getId());
        if(avatar == null)
            return;


        HorseRoomPetAvatar horse = (HorseRoomPetAvatar)petAvatar;
        if(ride) {
            if(!horse.getHorseData().anyoneCanRide() && horse.getPetData().getOwnerId() != session.getAvatar().getId())
                return;
            EventArgs rideArgs = avatar.getEventHandler().fireEvent("avatar.on.ride.horse", new AvatarRideHorseEventArgs(horse));

            if (rideArgs.isCancelled())
                return;
        }

        Runnable future = () -> {
            if (ride && horse.addRidingAvatar(avatar)) {
                avatar.setAditionalHeight(1);
                room.sendMessage(new AvatarEffectComposer(avatar.getVirtualId(), horse.getHorseData().getSaddleEffectId(), 0));
                room.getRoomMap().removeAvatarFromMap(avatar, avatar.getSetPosition().getPoint());
                avatar.getSetPosition().setPosition(horse.getSetPosition().getX(), horse.getSetPosition().getY(), horse.getSetPosition().getZ());

                avatar.getPosition().setPosition(horse.getPosition().getX(), horse.getPosition().getY(), horse.getPosition().getZ());

                avatar.getPosition().setHeadRotation(horse.getPosition().getHeadRotation());
                avatar.getPosition().setRotation(horse.getPosition().getRotation());

                avatar.getPosition().setPosition(horse.getPosition().getX(), horse.getPosition().getY(), horse.getPosition().getZ(), horse.getPosition().getRotation());

                room.getRoomMap().addAvatarToMap(avatar, avatar.getSetPosition().getPoint());
                horse.getEventHandler().on("avatar.leave.room", (delegate, eventArgs) -> {
                    delegate.cancel();
                    avatar.setAditionalHeight(0);
                    room.sendMessage(new AvatarEffectComposer(avatar.getVirtualId(), 0, 0));
                    avatar.updateNeeded(true);
                });
                avatar.updateNeeded(true);
            } else if (!ride && horse.getRidingAvatar() == avatar) {
                horse.removeRidingAvatar();
                avatar.setAditionalHeight(0);
                room.sendMessage(new AvatarEffectComposer(avatar.getVirtualId(), 0, 0));
                if(!avatar.getPath().isWalking()) {
                    List<Point> points = room.getRoomMap().getWalkableSquares(avatar.getSetPosition().getPoint(), 1);
                    if (points.size() > 0) {
                        avatar.moveTo(BCollect.getRandomThing(points, new Random((int) (Math.random() * 10000))));
                        avatar.getPosition().setPosition(avatar.getPosition().getX(), avatar.getPosition().getY(), avatar.getPosition().getZ() + 1);
                        avatar.getSetPosition().setPosition(avatar.getSetPosition().getX(), avatar.getSetPosition().getY(), avatar.getSetPosition().getZ() + 1);
                    }
                }
            }
        };

        if(avatar.getSetPosition().getPoint().getDistance(horse.getSetPosition().getPoint()) > 1){
            List<Point> points = room.getRoomMap().getWalkableSquares(horse.getPosition().getPoint(), 1);
            if(points.size() == 0)
                return;

            points.sort((o1, o2) -> {
                if(o1.getDistance(avatar.getSetPosition().getPoint()) > o2.getDistance(avatar.getSetPosition().getPoint()))
                    return 1;
                else if(o1.getDistance(avatar.getSetPosition().getPoint()) < o2.getDistance(avatar.getSetPosition().getPoint()))
                    return -1;
                return 0;
            });


            Point goal = points.get(0);
            avatar.moveTo(goal);

            EventDelegate moveDelegate = avatar.getEventHandler().on("avatar.stand.on.square avatar.cancel.move path.create.fail", (delegate, eventArgs) -> {
                synchronized (goal) {
                    switch (eventArgs.getEventKey()) {
                        case "avatar.stand.on.square":
                            if ((avatar.getSetPosition().getPoint().getDistance(horse.getSetPosition().getPoint()) <= 1)) {
                                future.run();
                                delegate.cancel();
                            }
                            break;

                        case "path.create.fail":
                        case "avatar.cancel.move":
                            delegate.cancel();
                            break;
                    }
                }
            });

            horse.getEventHandler().on("avatar.leave.room", (delegate, eventArgs) -> {
                moveDelegate.cancel();
            });

        } else {
            future.run();
        }

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
