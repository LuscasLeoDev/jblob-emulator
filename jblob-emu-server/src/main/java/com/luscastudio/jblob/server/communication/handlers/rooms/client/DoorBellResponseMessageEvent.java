package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.CloseFlatConnectionComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.FlatAccessDeniedComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomEnterErrorAlertComposer;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomEnterErrorFuture;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 13/02/2017 at 23:34.
 */
public class DoorBellResponseMessageEvent implements IMessageEventHandler{
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        String name = packet.getString();
        boolean accept = packet.getBool();

        Room room = session.getAvatar().getCurrentRoom();

        if(room == null)
            return;

        if(!room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_manage_doorbell"))
            return;

        HabboAvatar avatar = room.getRoomAvatarService().removeDoorBellAvatar(name);

        if(avatar == null)
            return;

        if(!accept){
            avatar.getSession().sendMessage(new FlatAccessDeniedComposer(""));
            room.getRoomAvatarService().performDeniedAvatarFromDoorBell(name);

        } else {
            RoomEnterErrorFuture errorFuture = room.getRoomAvatarService().tryAddPlayerAvatar(avatar);
            if(errorFuture.success()) {
                avatar.getSession().sendMessage(new FlatAccessibleComposer(""));
                room.getRoomAvatarService().performAcceptedAvatarFromDoorBell(name);
            } else {
                session.sendMessage(new CloseFlatConnectionComposer());
                session.sendMessage(new RoomEnterErrorAlertComposer(errorFuture.getErrorCode(), errorFuture.getMessage()));
            }
        }

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
