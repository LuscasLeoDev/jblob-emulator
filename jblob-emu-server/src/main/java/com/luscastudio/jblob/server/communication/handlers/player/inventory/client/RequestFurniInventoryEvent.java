package com.luscastudio.jblob.server.communication.handlers.player.inventory.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.boot.JBlobSettings;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryFurnisResponseComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.ItemSerializer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;

/**
 * Created by Lucas on 11/10/2016.
 */

public class RequestFurniInventoryEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int itemByTypeLimit = JBlob.getGame().getDbConfig().getInt("avatar.inventory.furnis.show.maxpertype", JBlobSettings.AVATAR_INVENTORY_FURNIS_SHOW_MAX_PER_TYPE);

        Collection<FurniProperties> userFurnis = session.getAvatar().getInventory().getFurniList();

        ItemSerializer.serializeItemsCollections(userFurnis, itemByTypeLimit, (pageCount, pageIndex, objects) -> session.sendQueueMessage(new InventoryFurnisResponseComposer(pageCount, pageIndex, objects)));

        session.flush();
    }

    @Override
    public boolean isAsync() {
        return false;
    }

}
