package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomForwardComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomReadyComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;
import com.luscastudio.jblob.server.manager.RoomManager;

/**
 * Created by Lucas on 14/02/2017 at 00:12.
 */
public class GoToFlatEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int roomId = packet.getInt();

        Room currentRoom = session.getAvatar().getCurrentRoom();

        if(currentRoom != null && currentRoom.getProperties().getId() == roomId){
            session.sendQueueMessage(new RoomReadyComposer(currentRoom.getProperties().getId(), currentRoom.getProperties().getModelName()));
            RoomManager.serializeRoomData(session, currentRoom);
            session.flush();
            /*RoomPlayerAvatar avatar = currentRoom.getRoomAvatarService().getRoomAvatarByUserId(session.getAvatar().getId());

            currentRoom.getRoomAvatarService().addAvatarEnteringPacketQueue(avatar);

            currentRoom.sendQueuedObjects(session);

            currentRoom.flush();*/
        } else {
            session.sendMessage(new RoomForwardComposer(roomId));
        }

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
