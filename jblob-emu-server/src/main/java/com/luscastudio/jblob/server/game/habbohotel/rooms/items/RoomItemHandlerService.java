package com.luscastudio.jblob.server.game.habbohotel.rooms.items;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.FloorItemUpdateComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.FloorItemsExtradataUpdateComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.WallItemUpdateCompser;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.database.dao.rooms.items.RoomItemHandlerDao;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.process.RunProcess;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.util.*;

/**
 * Created by Lucas on 10/10/2016.
 */

public class RoomItemHandlerService {
    private static Logger log = Logger.getLogger(RoomItemHandlerService.class);
    Room room;

    private Map<Integer, IRoomItem> itemsById;
    private Map<Integer, Map<Integer, IRoomItem>> itemsByUserId;
    private Map<Integer, IRoomItem> itemsToSave;
    private Map<Integer, RoomItemToUpdateData> itemsToUpdate;

    private Map<Integer, String> roomsItemsOwnersName;

    private Map<Integer, IRoomItem> floorItems;
    private Map<Integer, IRoomItem> wallItems;

    public RoomItemHandlerService(Room room) {
        this.room = room;

        this.itemsById = BCollect.newMap();
        this.itemsByUserId = BCollect.newMap();
        this.itemsToUpdate = BCollect.newMap();
        this.itemsToSave = BCollect.newMap();
        this.floorItems = BCollect.newMap();
        this.wallItems = BCollect.newMap();
        this.roomsItemsOwnersName = BCollect.newMap();
    }

    public void init(){
        loadItems();
        this.room.getRoomProcess().enqueue(new RunProcess(this::onCycle, 0, 100));
    }

    public void loadItems() {
        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {
            DBConnPrepare prepare = reactor.prepare("SELECT avatars_items_data.*, players_avatar.username FROM avatars_items_data INNER JOIN players_avatar ON players_avatar.id = avatars_items_data.user_id WHERE room_id = ?");
            prepare.setInt(1, this.room.getProperties().getId());

            ResultSet set = prepare.runQuery();

            while (set.next()) {
                IRoomItem item = JBlob.getGame().getRoomItemFactory().createRoomItemFromDb(set, this.room);
                if (item == null) continue;

                item.setAffectedTiles(room.getRoomMap().getAffectedTilesForItem(item.getBase().getWidth(), item.getBase().getLength(), item.getPosition().getX(), item.getPosition().getY(), item.getPosition().getRotation()));
                this.room.getRoomMap().addItemToMap(item);
                this.itemsById.put(item.getId(), item);

                if (item.getBase().getType().equals("i")) this.wallItems.put(item.getId(), item);
                else this.floorItems.put(item.getId(), item);

                if(!this.itemsByUserId.containsKey(item.getOwnerId()))
                    this.itemsByUserId.put(item.getOwnerId(), BCollect.newMap());

                this.itemsByUserId.get(item.getOwnerId()).put(item.getId(), item);

                if(!this.roomsItemsOwnersName.containsKey(item.getOwnerId()))
                    this.roomsItemsOwnersName.put(item.getOwnerId(), set.getString("username"));
            }

            this.itemsById.values().forEach(iRoomItem -> iRoomItem.onRoomLoad());

        } catch (Exception e) {
            log.error("Error while loading items for room #" + room.getProperties().getId(), e);
        }
    }

    //region Cycling region

    public void onCycle() {
        this.serializeItemsToUpdate();
    }

    private void serializeItemsToUpdate() {
        if (itemsToUpdate.size() == 0) return;
        List<IRoomItem> itemToUpdateBoth = BCollect.newList();
        List<IRoomItem> itemToUpdateExtradata = BCollect.newList();
        for (Map.Entry<Integer, RoomItemToUpdateData> item : itemsToUpdate.entrySet()) {
            if (item.getValue().isBoth())
                itemToUpdateBoth.add(item.getValue().getItem());
            else
                itemToUpdateExtradata.add(item.getValue().getItem());
        }

        for (IRoomItem item : itemToUpdateBoth)
            if(item.getBase().getType().equals("i"))
                room.sendQueueMessage(new WallItemUpdateCompser(item));
        else
                room.sendQueueMessage(new FloorItemUpdateComposer(item));

        if (itemToUpdateExtradata.size() > 0)
            room.sendQueueMessage(new FloorItemsExtradataUpdateComposer(itemToUpdateExtradata));

        room.flush();
        itemsToUpdate.clear();
    }

    //endregion

    public IRoomItem getRoomItemById(int id) {
        if (!itemsById.containsKey(id)) return null;

        return itemsById.get(id);
    }

    public Map<Integer, String> getRoomsItemsOwnersName(){
        return this.roomsItemsOwnersName;
    }

    public List<IRoomItem> getItems() {
        return new LinkedList<>(this.itemsById.values());
    }

    public Map<Integer, IRoomItem> getFloorItems() {
        return floorItems;
    }

    public Map<Integer, IRoomItem> getWallItems() {
        return wallItems;
    }

    public void updateItem(IRoomItem item, boolean both){
        this.updateItem(item, both, false);
    }

    public void updateItem(IRoomItem item, boolean both, boolean immediately) {

        if(immediately){

            if(both){
                if(item.getBase().getType().equals("i"))
                    this.room.sendMessage(new WallItemUpdateCompser(item));
                else
                    this.room.sendMessage(new FloorItemUpdateComposer(item));
            } else {
                this.room.sendMessage(new FloorItemsExtradataUpdateComposer(item));
            }
            return;
        }

        synchronized (this.itemsToUpdate) {
            if (this.itemsToUpdate.containsKey(item.getId())) {
                if (!this.itemsToUpdate.get(item.getId()).isBoth())
                    this.itemsToUpdate.replace(item.getId(), new RoomItemToUpdateData(item, both));
            }
            else
                this.itemsToUpdate.put(item.getId(), new RoomItemToUpdateData(item, both));
        }
    }

    public void saveItem(IRoomItem item) {
        synchronized (this.itemsToSave) {
            if (!this.itemsToSave.containsKey(item.getId())) this.itemsToSave.put(item.getId(), item);
        }
    }


    public synchronized void dispose() {
        //for(IRoomItem item : new LinkedList<IRoomItem>(this.itemsToSave.values()))
        {
            try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

                Collection<IRoomItem> items = this.itemsToSave.values();

                for (IRoomItem item : items) {
                    DBConnPrepare prepare = reactor.prepare("UPDATE avatars_items_data SET x = ?, y = ?, z = ?, rotation = ?, extradata = ? WHERE id = ?");
                    prepare.setInt(1, item.getPosition().getX());
                    prepare.setInt(2, item.getPosition().getY());
                    prepare.setString(3, NumberHelper.doubleToString(item.getPosition().getZ()));
                    prepare.setInt(4, item.getPosition().getRotation());
                    prepare.setString(5, item.getExtradata().getExtradataToDb());

                    prepare.setInt(6, item.getId());

                    prepare.run();
                }

            } catch (Exception e) {
                log.error("Error while saving items from room #" + this.room.getProperties().getId(), e);
            }

            this.clearAll();
        }
    }

    private void clearAll() {
        this.itemsToUpdate.clear();
        this.itemsToSave.clear();
        this.floorItems.clear();
        this.wallItems.clear();
        this.itemsByUserId.clear();
        this.itemsById.clear();
        this.roomsItemsOwnersName.clear();
    }

    //Add wall Item
    public boolean addItemToRoom(IRoomItem roomItem, String wallCoordinate) {
        //If for some reason this same item is already on this room
        if (this.itemsById.containsKey(roomItem.getId())) {
            return false;
        }

        //Else, put this item
        itemsById.put(roomItem.getId(), roomItem);
        wallItems.put(roomItem.getId(), roomItem);

        //Save the item at room on database
        RoomItemHandlerDao.addItemToRoom(room.getProperties().getId(), roomItem.getId(), roomItem.getWallCoordinate().toString());

        return true;
    }

    //Add Floor Item
    public boolean addItemToRoom(IRoomItem roomItem, int x, int y, int rot) {

        //If for some reason this same item is already on this room
        if (this.itemsById.containsKey(roomItem.getId())) {
            return false;
        }

        //Else, put this item
        itemsById.put(roomItem.getId(), roomItem);
        floorItems.put(roomItem.getId(), roomItem);

        //Generate the affected tiles
        roomItem.setAffectedTiles(room.getRoomMap().getAffectedTilesForItem(roomItem, x, y, rot));

        //If some of the item coordinates are not valid, we can't to put this item there.
        if (!room.getRoomMap().addItemToMap(roomItem)) return false;

        //Else, set the item main position
        roomItem.getPosition().setPosition(x, y, room.getRoomMap().getSquareHeightForItem(roomItem), rot);

        //Save the item at room on database
        RoomItemHandlerDao.addItemToRoom(room.getProperties().getId(), roomItem.getId(), roomItem.getPosition().getX(), roomItem.getPosition().getY(), roomItem.getPosition().getZ(), roomItem.getPosition().getRotation());


        //Prepare the packet update for the square stack length
        room.getRoomMap().updateSqHeight(roomItem.getAffectedTiles());

        return true;
    }


    public void removeRoomItem(IRoomItem item) {
        this.itemsById.remove(item.getId());
        this.itemsToSave.remove(item.getId());
        this.itemsToUpdate.remove(item.getId());


        if (item.getBase().getType().equals("i")) {
            this.wallItems.remove(item.getId());
        }
        else {
            this.floorItems.remove(item.getId());
            this.room.getRoomMap().removeItemFromMap(item);
            this.room.getRoomMap().updateSqHeight(item.getAffectedTiles());
        }
        if(this.itemsByUserId.containsKey(item.getOwnerId()))
            this.itemsByUserId.get(item.getOwnerId()).remove(item.getId());
        //s.dispose();
    }

    public synchronized Map<Integer, List<IRoomItem>> removeAll() {

        Map<Integer, List<IRoomItem>> itemsToUsers = BCollect.newMap();

        Iterator<IRoomItem> itemIterator = this.itemsById.values().iterator();

        while (itemIterator.hasNext()){
            IRoomItem item = itemIterator.next();
            if(!itemsToUsers.containsKey(item.getOwnerId()))
                itemsToUsers.put(item.getOwnerId(), BCollect.newList());

            itemsToUsers.get(item.getOwnerId()).add(item);
            itemIterator.remove();

            this.room.getRoomMap().removeItemFromMap(item);
            //this.room.getRoomMap().updateSqHeight(item.getAffectedTiles());
        }

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare = reactor.prepare("UPDATE avatars_items_data SET room_id = '0' WHERE room_id = ?");
            prepare.setInt(1, this.room.getProperties().getId());
            prepare.run();
        } catch (Exception e){
            BLogger.error(e, this.getClass());
            return BCollect.newMap();
        }

        this.itemsByUserId.clear();
        this.floorItems.clear();
        this.wallItems.clear();
        this.itemsToSave.clear();
        this.itemsToUpdate.clear();
        this.itemsById.clear();

        return itemsToUsers;

    }

    public List<IRoomItem> removeUserItems(int avatarId){
        List<IRoomItem> items = BCollect.newList();

        Iterator<IRoomItem> itemIterator = this.itemsById.values().iterator();

        while (itemIterator.hasNext()){
            IRoomItem item = itemIterator.next();
            if(item.getOwnerId() != avatarId)
                continue;

            this.wallItems.remove(item.getId());
            this.floorItems.remove(item.getId());
            this.itemsToSave.remove(item.getId());
            this.itemsToUpdate.remove(item.getId());
            this.room.getRoomMap().updateSqHeight(item.getAffectedTiles());
            item.onRemove();
            itemIterator.remove();
        }

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare = reactor.prepare("UPDATE avatars_items_data SET room_id = '0' WHERE room_id = ?");
            prepare.setInt(1, this.room.getProperties().getId());
            prepare.run();
        } catch (Exception e){
            BLogger.error(e, this.getClass());
            return BCollect.newList();
        }

        this.itemsByUserId.get(avatarId).clear();

        return items;

    }
}