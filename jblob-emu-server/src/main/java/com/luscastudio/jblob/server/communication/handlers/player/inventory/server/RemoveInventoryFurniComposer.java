package com.luscastudio.jblob.server.communication.handlers.player.inventory.server;

/**
 * Created by Lucas on 17/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;

public class RemoveInventoryFurniComposer extends MessageComposer {
    public RemoveInventoryFurniComposer(int id) {
        message.putInt(id);
    }

    @Override
    public String id() {
        return "FurniListRemoveMessageComposer";
    }
}
