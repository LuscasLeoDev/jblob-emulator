package com.luscastudio.jblob.server.game.habbohotel.landingview.promoarticle;

/**
 * Created by Lucas on 14/02/2017 at 19:41.
 */
public class PromoArticleItem {

    private int id;
    private String title;
    private String context;
    private String imageUrl;
    private String buttonText;
    private String buttonLink;
    private int buttonType;

    public PromoArticleItem(int id, String title, String context, String imageUrl, String buttonText, String buttonLink, int buttonType){
        this.id = id;
        this.title = title;
        this.context = context;
        this.imageUrl = imageUrl;
        this.buttonText = buttonText;
        this.buttonLink = buttonLink;
        this.buttonType = buttonType;
    }

    public int getId() {
        return id;
    }

    public int getButtonType() {
        return buttonType;
    }

    public String getContext() {
        return context;
    }

    public String getButtonLink() {
        return buttonLink;
    }

    public String getButtonText() {
        return buttonText;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getTitle() {
        return title;
    }
}
