package com.luscastudio.jblob.server.communication.handlers.catalog.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.catalog.server.CatalogItemDiscountComposer;
import com.luscastudio.jblob.server.communication.handlers.catalog.server.CatalogPagesComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;


/**
 * Created by Lucas on 28/10/2016.
 */

public class GetCatalogPagesEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {


        session.sendMessage(new CatalogPagesComposer("NORMAL", JBlob.getGame().getCatalogManager().getPagesForUser(session), false));
        session.sendMessage(new CatalogItemDiscountComposer());

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
