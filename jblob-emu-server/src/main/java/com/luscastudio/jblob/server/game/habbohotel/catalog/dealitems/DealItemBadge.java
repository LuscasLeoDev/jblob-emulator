package com.luscastudio.jblob.server.game.habbohotel.catalog.dealitems;

import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogDealItem;

import java.util.List;

/**
 * Created by Lucas on 08/12/2016.
 */

public class DealItemBadge extends CatalogDealItem {

    public DealItemBadge (int id, int dealId, int baseId, String extradata, int limitedAmount, int amount, String type, boolean isRare) {
        super(id, dealId, baseId, extradata, limitedAmount, amount, type, isRare);
    }

    @Override
    public void parsePageComposer(PacketWriting message) {
        message.putString(getExtradata());
    }

    @Override
    public boolean generateValue(HabboAvatar avatar, String pageExtradata, List<Object> outValues, int amount) {
        return true;
    }

    @Override
    public void handleValues(HabboAvatar avatar, List<Object> values) {

    }
}
