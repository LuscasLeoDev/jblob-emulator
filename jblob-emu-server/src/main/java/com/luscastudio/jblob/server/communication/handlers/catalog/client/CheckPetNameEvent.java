package com.luscastudio.jblob.server.communication.handlers.catalog.client;

import com.luscastudio.jblob.api.sercurity.StringFilter;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.catalog.server.CheckPetNameComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 31/01/2017 at 15:13.
 */
public class CheckPetNameEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        String name = packet.getString();
        int someInteger = packet.getInt();

        StringFilter.PetNameValidation nameValidation = JBlob.getGame().getFilter().filterPetName(name);


        session.sendMessage(new CheckPetNameComposer(nameValidation.getErrorCode(), nameValidation.getValidationInfo()));

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
