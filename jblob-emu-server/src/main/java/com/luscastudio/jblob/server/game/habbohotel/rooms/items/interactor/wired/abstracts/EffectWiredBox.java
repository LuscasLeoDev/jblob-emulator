package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts;

import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.WiredEffectConfigComposer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;

/**
 * Created by Lucas on 15/02/2017 at 19:41.
 */
public abstract class EffectWiredBox extends WiredBoxRoomItem implements ISavableWiredEffect{
    public EffectWiredBox(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
    }

    @Override
    protected MessageComposer getWiredConfigComposer() {
        return new WiredEffectConfigComposer(this);
    }

    public abstract int getDelay();

    public abstract void onWiredTriggered(IRoomAvatar avatarTrigger, IRoomItem triggeredItem);
}
