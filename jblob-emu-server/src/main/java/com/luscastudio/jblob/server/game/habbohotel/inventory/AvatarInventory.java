package com.luscastudio.jblob.server.game.habbohotel.inventory;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.inventory.badge.Badge;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 16/10/2016.
 */

public class AvatarInventory {

    private static Logger logger = Logger.getLogger(AvatarInventory.class);

    public static int ITEM_NOROOM_ID = 0;

    private Map<Integer, FurniProperties> furnis;
    private Map<Integer, Badge> badges;
    private Map<Integer, PetData> pets;

    private int avatarId;

    //Helpers
    private boolean loading;

    public AvatarInventory(int avatarId) {
        this.avatarId = avatarId;
        this.furnis = BCollect.newConcurrentMap();
        this.badges = BCollect.newConcurrentMap();
        this.pets = BCollect.newConcurrentMap();

        this.load();
    }

    public AvatarInventory(int avatarId, DBConnReactor reactor) throws SQLException {
        this.avatarId = avatarId;
        this.furnis = BCollect.newConcurrentMap();
        this.badges = BCollect.newConcurrentMap();
        this.pets = BCollect.newConcurrentMap();
        this.load(reactor);
    }

    public void load() {

        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

           load(reactor);

        } catch (Exception e) {
            logger.error("Error while parsing Habbo Avatar #" + this.avatarId + " inventory");
        }
    }

    public void load(DBConnReactor reactor) throws SQLException {

        if(this.loading)
            return;
        this.loading = true;

        this.pets.clear();
        this.badges.clear();
        this.furnis.clear();

        DBConnPrepare prepare;
        ResultSet set;

        prepare = reactor.prepare("SELECT * FROM avatars_items_data WHERE user_id = ? AND room_id = ?");
        prepare.setInt(1, this.avatarId);
        prepare.setInt(2, ITEM_NOROOM_ID);

        set = prepare.runQuery();

        while (set.next()) {
            FurniProperties furni = JBlob.getGame().getRoomItemFactory().generateFurniProperties(set);
            if (furni == null)
                continue;

            furnis.put(furni.getId(), furni);
        }

        prepare = reactor.prepare("SELECT * FROM avatars_badges WHERE avatar_id = ?");
        prepare.setInt(1, this.avatarId);

        set = prepare.runQuery();

        while (set.next()) {
            int id;
            this.badges.put(id = set.getInt("id"), new Badge(id, set.getString("badge"), set.getInt("slot_id")));
        }

        prepare = reactor.prepare("SELECT * FROM avatars_pets_data WHERE owner_id = ? AND room_id = ?");
        prepare.setInt(1, this.avatarId);
        prepare.setInt(2, ITEM_NOROOM_ID);

        set = prepare.runQuery();

        while (set.next()) {
            int id;
            this.pets.put(id = set.getInt("id"), JBlob.getGame().getPetManager().parsePetData(set));
        }

        this.loading = false;
    }

    //region Pets Handling

    public PetData getPet(int id){
        return this.pets.get(id);
    }

    public PetData removePet(int id){
        return this.pets.remove(id);
    }

    public void addPet(PetData data){
        this.pets.put(data.getId(), data);
    }

    public Collection getPetList(){
        return this.pets.values();
    }

    //region

    //region Furnis Handling

    public Collection<FurniProperties> getFurniList() {
        return (furnis.values());
    }

    public FurniProperties getFurni(int id) {
        if (!furnis.containsKey(id))
            return null;

        return furnis.get(id);
    }

    public FurniProperties removeRoomItem(int id) {
        return furnis.remove(id);
    }

    public void addRoomItem(FurniProperties properties) {
        if (this.furnis.containsKey(properties.getId()))
            this.furnis.replace(properties.getId(), properties);
        else
            this.furnis.put(properties.getId(), properties);
    }

    public void clearRoomItems(){
        this.furnis.clear();
    }

    //endregion

    //region Badge Handling


    public Collection<Badge> getBadgeList() {
        return (badges.values());
    }

    public List<Badge> getEquippedBadges() {
        List<Badge> list = BCollect.newList();

        for(Badge badge : badges.values())
            if(badge.getSlotId() > 0)
                list.add(badge);

        return list;
    }

    public void clearSelectedBadges() {
        for(Badge b : this.badges.values()){
            b.setSlotId(0);
        }
    }

    public boolean hasBadge(String badgeCode) {
        for(Badge b : this.badges.values()){
            if(b.getCode().equals(badgeCode))
                return true;
        }

        return false;
    }

    public Badge getBadge(String badgeCode) {
        for(Badge b : this.badges.values()){
            if(b.getCode().equals(badgeCode))
                return b;
        }

        return null;
    }

    //endregion
}
