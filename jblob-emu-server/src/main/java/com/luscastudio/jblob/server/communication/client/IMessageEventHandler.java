package com.luscastudio.jblob.server.communication.client;

import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 19/09/2016.
 */
public interface IMessageEventHandler {
    void parse(PlayerSession session, MessageEvent packet);

    boolean isAsync();
}
