package com.luscastudio.jblob.server.game.habbohotel.catalog.dealitems;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryUpdateRequestComposer;
import com.luscastudio.jblob.server.events.list.PurchasedCatalogDealFurniEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogDealItem;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;

import java.util.Iterator;
import java.util.List;

import static com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer.FURNI;

/**
 * Created by Lucas on 08/12/2016.
 */

public class DealItemFurni extends CatalogDealItem {
    public DealItemFurni(int id, int dealId, int baseId, String extradata, int limitedAmount, int amount, String type, boolean isRare) {
        super(id, dealId, baseId, extradata, limitedAmount, amount, type, isRare);
        this.base = JBlob.getGame().getItemBaseManager().getItemBase(baseId);
    }

    @Override
    public String getType() {
        if(this.getBase() != null)
            return this.getBase().getType();
        return super.getType();
    }

    @Override
    public boolean generateValue(HabboAvatar avatar, String pageExtradata, List<Object> outValues, int amount) {

        List<FurniProperties> properties = JBlob.getGame().getRoomItemFactory().createFurniPropertiesList(this.getBase(), avatar.getId(), pageExtradata, 0,  this.isRare(), this.getAmount() * amount);

        FurniListNotificationComposer composer = new FurniListNotificationComposer();
        for(FurniProperties item : properties)
            composer.add(FURNI, item.getId());

        outValues.add(composer);

        outValues.addAll(properties);

        return true;
    }

    @Override
    public void handleValues(HabboAvatar avatar, List<Object> values) {

        Iterator<Object> iterator = values.iterator();

        List<FurniProperties> furnis = BCollect.newList();

        while(iterator.hasNext()){
            Object item = iterator.next();

            if(item instanceof FurniProperties){
                FurniProperties prop = (FurniProperties)item;
                avatar.getInventory().addRoomItem(prop);
                avatar.getEventHandler().fireEvent("catalog.deal.purchase.furni", new PurchasedCatalogDealFurniEventArgs(this, prop));
                iterator.remove();
            }


        }

        values.add(new InventoryUpdateRequestComposer());

    }
}
