package com.luscastudio.jblob.server.game.habbohotel.rooms.chat;

import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 11/12/2016.
 */

public interface IChatCommand {

    boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params);

}
