package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets;

import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomAvatarType;

/**
 * Created by Lucas on 22/01/2017 at 15:10.
 */
public abstract class RoomPetAvatar extends RoomAvatar implements IRoomPetAvatar{

    protected PetData petData;

    public RoomPetAvatar(Room room, int virtualId, PetData petData){
        super(room, virtualId);
        this.petData = petData;
    }

    @Override
    public int getId() {
        return petData.getId();
    }

    @Override
    public String getName() {
        return petData.getName();
    }

    @Override
    public RoomAvatarType getType() {
        return RoomAvatarType.PET;
    }

    @Override
    public int getAvatarType() {
        return 2;
    }

    @Override
    public PetData getPetData() {
        return petData;
    }

    @Override
    public String getFigureString() {
        return this.petData.getPetBase().getPetType() + " " + this.petData.getRaceId() + " " + this.petData.getColor() + " " + this.petData.getCustom().toPacketString();
    }

    @Override
    public abstract void composeAvatar(PacketWriting message);

    public abstract void composePetAvatarInformation(PacketWriting message);

    @Override
    public synchronized void dispose() {
        super.dispose();
        this.petData = null;
    }

    @Override
    public void save(DBConnReactor reactor) {
        try{
            DBConnPrepare prepare = reactor.prepare("UPDATE avatars_pets_data SET x = ?, y = ?, rotation = ?, level = ?, energy = ?, happiness = ?, experience = ?, respects = ?, extradata = ?, custom_data = ?, race_id = ?, room_id = ? WHERE id = ?");
            prepare.setInt(1, this.getSetPosition().getX());
            prepare.setInt(2, this.getSetPosition().getY());
            prepare.setInt(3, this.getPosition().getRotation());

            prepare.setInt(4, this.getPetData().getLevel());
            prepare.setInt(5, this.getPetData().getEnergy());
            prepare.setInt(6, this.getPetData().getHappiness());
            prepare.setInt(7, this.getPetData().getExperience());
            prepare.setInt(8, this.getPetData().getRespects());
            prepare.setString(9, this.getPetData().getExtradata());
            prepare.setString(10, this.getPetData().getCustom().toDbString());
            prepare.setInt(11, this.getPetData().getRealRaceId());
            prepare.setInt(12, this.getPetData().getRoomId());
            prepare.setInt(13, this.getPetData().getId());

            prepare.run();
        }catch (Exception e){
            BLogger.error(e, this.getClass());
        }
    }

    @Override
    public abstract void onPlace(HabboAvatar placerAvatar);

    @Override
    public abstract void onRemove(HabboAvatar removerAvatar);

    @Override
    public void onRespect(HabboAvatar avatar) {
        this.getPetData().setRespects(this.getPetData().getRespects() + 1);
        this.getRoom().getRoomAvatarService().saveAvatar(this);
    }
}
