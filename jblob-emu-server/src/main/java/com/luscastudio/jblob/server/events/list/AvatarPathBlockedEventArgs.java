//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.api.utils.engine.IPoint;
import com.luscastudio.jblob.server.events.EventArgs;

/**
 * Created by Lucas on 08/03/2017 at 13:41.
 */
public class AvatarPathBlockedEventArgs extends EventArgs {
    private IPoint point;

    public AvatarPathBlockedEventArgs(IPoint point) {
        this.point = point;
    }

    public IPoint getPoint() {
        return point;
    }
}
