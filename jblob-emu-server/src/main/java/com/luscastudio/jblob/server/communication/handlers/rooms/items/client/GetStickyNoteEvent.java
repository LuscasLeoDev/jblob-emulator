package com.luscastudio.jblob.server.communication.handlers.rooms.items.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.StickyNoteComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 09/01/2017 at 20:09.
 */
public class GetStickyNoteEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        int itemId = packet.getInt();

        IRoomItem item = room.getRoomItemHandlerService().getRoomItemById(itemId);

        if(item == null)
            return;

        session.sendMessage(new StickyNoteComposer(item.getId(), item.getExtradata().toString()));

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
