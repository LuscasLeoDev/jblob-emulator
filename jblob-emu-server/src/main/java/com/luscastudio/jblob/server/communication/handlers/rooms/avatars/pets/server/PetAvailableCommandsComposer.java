package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

import java.util.Collection;

/**
 * Created by Lucas on 03/02/2017 at 15:47.
 */
public class PetAvailableCommandsComposer extends MessageComposer {
    @Override
    public String id() {
        return "PetAvailableCommandsMessageComposer";
    }

    public PetAvailableCommandsComposer(int petId, Collection<Integer> commands, Collection<Integer> availableCommands){
        message.putInt(petId);

        message.putInt(commands.size());// All Commands

        commands.forEach(i -> message.putInt(i));

        message.putInt(availableCommands.size());// Available Commands

        availableCommands.forEach(i -> message.putInt(i));
    }
}
