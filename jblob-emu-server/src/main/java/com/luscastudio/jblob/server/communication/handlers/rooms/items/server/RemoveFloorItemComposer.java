package com.luscastudio.jblob.server.communication.handlers.rooms.items.server;

/**
 * Created by Lucas on 17/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;

public class RemoveFloorItemComposer extends MessageComposer {
    public RemoveFloorItemComposer(int id, int pickerId, boolean isExpired, int delay) {
        message.putString(String.valueOf(id));
        message.putBool(isExpired);
        message.putInt(pickerId);
        message.putInt(delay);
    }

    public RemoveFloorItemComposer(int id, int pickerId) {
        message.putString(String.valueOf(id));
        message.putBool(false);
        message.putInt(pickerId);
        message.putInt(0);
    }

    @Override
    public String id() {
        return "ObjectRemoveMessageComposer";
    }
}
