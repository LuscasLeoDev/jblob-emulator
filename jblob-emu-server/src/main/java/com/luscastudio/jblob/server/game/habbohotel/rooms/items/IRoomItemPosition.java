package com.luscastudio.jblob.server.game.habbohotel.rooms.items;


import com.luscastudio.jblob.api.utils.engine.Point;

/**
 * Created by Lucas on 23/12/2016 at 13:40.
 */
public interface IRoomItemPosition {

    int getX();
    int getY();
    double getZ();
    int getRotation();

    Point getSquareInFront(int index);
    Point getSquareBehind(int index);
    Point getSquareLeft(int index);
    Point getSquareRight(int index);

}
