package com.luscastudio.jblob.server.communication.handlers.catalog.client;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.catalog.server.PurchaseErrorComposer;
import com.luscastudio.jblob.server.communication.handlers.catalog.server.PurchaseOKComposer;
import com.luscastudio.jblob.server.communication.handlers.player.currencies.server.CreditBalanceComposer;
import com.luscastudio.jblob.server.communication.handlers.player.currencies.server.CurrencyNotificationComposer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogDeal;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogManager;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogNode;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * Created by Lucas on 07/12/2016.
 */

public class PurchaseFromCatalogEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int pageId = packet.getInt();
        int dealId = packet.getInt();

        String extraData = packet.getString();

        int amount = packet.getInt();

        CatalogNode page = JBlob.getGame().getCatalogManager().getCatalogNode(pageId);

        if (page == null)
            return;

        CatalogDeal deal = page.getDeal(dealId);

        if (deal == null)
            return;

        if (!CatalogManager.isValidPurchase(deal, session.getAvatar(), amount, extraData))
            return;

        //JBlob.getGame().getProcessManager().run(() -> {

        List<MessageComposer> composers = BCollect.newList();
        if (!JBlob.getGame().getCatalogManager().tryPurchase(deal, session.getAvatar(), extraData, amount, composers)) {
            session.sendQueueMessage(new PurchaseErrorComposer(0));
            return;
        }

        if (deal.getCreditsCost() > 0)
            session.sendQueueMessage(new CreditBalanceComposer(session.getAvatar().getBalance().getCreditValue()));

        if (deal.getCurrencyCost() != 0) {
            int userBalance = session.getAvatar().getBalance().get(deal.getCurrencyId());
            session.sendQueueMessage(new CurrencyNotificationComposer(deal.getCurrencyId(), userBalance, 0));
        }

        for (MessageComposer composer : composers)
            session.sendQueueMessage(composer);

        session.flush();


        session.sendQueueMessage(new PurchaseOKComposer(deal)).flush();
        //}, 0, TimeUnit.MILLISECONDS);

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
