package com.luscastudio.jblob.server.game.habbohotel.rooms.chat;

/**
 * Created by Lucas on 11/12/2016.
 */

public class CommandParams {

    public static String COMMAND_PARAMS_SEPARATOR = " ";

    String[] params;

    public CommandParams(String command){
        this.params = command.split(COMMAND_PARAMS_SEPARATOR);
    }

    public CommandParams(String[] params){
        this.params = params;
    }

    public String get(int index, boolean tillEnd){
        int i = index;
        String r = "";
        while(i < params.length){
            r += params[i];
            if(i < params.length)
                r += COMMAND_PARAMS_SEPARATOR;
            i++;
        }

        return r;
    }

    public String get(int index, String def){
        if(params.length <= index)
            return def;

        return params[index];
    }

    public String get(int index){
        if(params.length <= index)
            return "";

        return params[index];
    }

    public int length(){
        return this.params.length;
    }

}
