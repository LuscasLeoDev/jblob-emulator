package com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.notification.server.MaintenanceStatusComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.IChatCommand;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 20/01/2017 at 01:22.
 */
public class MaintenanceAlertCmd implements IChatCommand {
    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {

        JBlob.getGame().getSessionManager().sendMessage(new MaintenanceStatusComposer(false, 20, 80));

        return true;
    }
}
