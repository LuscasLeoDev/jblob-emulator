package com.luscastudio.jblob.server.game.habbohotel.rooms.engine.validator;

import com.luscastudio.jblob.api.utils.engine.Point;

/**
 * Created by Lucas on 09/10/2016.
 */
public interface IFloorValidator {

    boolean validate(Point from, Point to);

    boolean validatePathStep(Point from, Point to, boolean finalPath);
}
