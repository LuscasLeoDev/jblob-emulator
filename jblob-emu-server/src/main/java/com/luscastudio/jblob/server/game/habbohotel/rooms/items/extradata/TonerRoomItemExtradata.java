package com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata;

import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.TonerRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.IItemMessageParser;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.TonerItemMessageParser;

import java.util.List;

/**
 * Created by Lucas on 20/02/2017 at 19:48.
 */
public class TonerRoomItemExtradata implements IRoomItemExtradata{
    private FurniProperties properties;
    private TonerItemMessageParser parser;
    private String extradata;

    private int enabled;
    private int hue;
    private int saturation;
    private int lightness;

    public TonerRoomItemExtradata(String exradata){
        this.extradata = exradata;
        this.enabled = 0;
        this.hue = 0;
        this.saturation = 0;
        this.lightness = 0;
    }

    @Override
    public String getExtradata() {
        return "";
    }

    @Override
    public String getExtradataToDb() {
        return this.enabled + " " + this.hue + " " + this.saturation + " " + this.lightness;
    }

    @Override
    public void setExtradata(String extradata) {

    }

    @Override
    public void init(FurniProperties properties) {
        this.properties = properties;
        this.parser = new TonerItemMessageParser(this);

        String[] spl = this.extradata.split(" ");
        for (int i = 0; i < spl.length; i++) {
            if(!NumberHelper.isInteger(spl[i]))
                continue;

            int value = Integer.parseInt(spl[i]);

            switch(i){
                case 0:
                    this.enabled = value;
                break;
                case 1:
                    this.hue = value;
                break;
                case 2:
                    this.saturation = value;
                break;
                case 3:
                    this.lightness = value;
                break;

            }
        }
    }

    @Override
    public IItemMessageParser getMessageParser() {
        return this.parser;
    }

    public int getHue() {
        return hue;
    }

    public int getLightness() {
        return lightness;
    }

    public int getSaturation() {
        return saturation;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public void setTonerData(int hue, int saturation, int lightness) {
        this.hue = hue;
        this.saturation = saturation;
        this.lightness = lightness;
    }
}
