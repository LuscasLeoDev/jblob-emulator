package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor;

import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;

/**
 * Created by Lucas on 19/02/2017 at 12:57.
 */
public class PetItemRoomItem extends SimpleRoomItem {
    public PetItemRoomItem(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);
    }

    public String getApplyData() {
        return this.getProperties().getBase().getCustom();
    }
}
