package com.luscastudio.jblob.server.game.habbohotel.navigator.tabs;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.game.habbohotel.navigator.categories.NavigatorCategory;
import com.luscastudio.jblob.server.game.habbohotel.navigator.search.NavigatorCategorySearchResult;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 12/02/2017 at 13:24.
 */
public class NavigatorTopLevel {
    private int id;
    private String name;
    private String permissionName;

    private Map<Integer, NavigatorCategory> categoryMap;

    public NavigatorTopLevel(int id, String name, String permissionName){
        this.id = id;
        this.name = name;
        this.permissionName = permissionName;

        this.categoryMap = BCollect.newMap();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void addCategory(NavigatorCategory category) {
        this.categoryMap.put(category.getId(), category);
    }

    public List<NavigatorCategorySearchResult> getResults(PlayerSession session, String search) {
        List<NavigatorCategorySearchResult> results = BCollect.newList();

        for (NavigatorCategory navigatorCategory : this.categoryMap.values()) {
            results.add(new NavigatorCategorySearchResult(navigatorCategory, navigatorCategory.getRoomsData(session, search)));
        }

        return results;
    }
}
