package com.luscastudio.jblob.server.communication.handlers.rooms.items.server;

import com.luscastudio.jblob.server.communication.handlers.rooms.items.ItemSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;

/**
 * Created by Lucas on 20/12/2016 at 01:13.
 */
public class AddWallItemComposer extends MessageComposer {
    @Override
    public String id() {
        return "ItemAddMessageComposer";
    }

    public AddWallItemComposer(IRoomItem roomItem, String ownerName){
        ItemSerializer.serializeWallItem(message, roomItem);

        message.putString(ownerName);
    }
}
