package com.luscastudio.jblob.server.manager;

import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.api.utils.io.Configuration;
import com.luscastudio.jblob.server.boot.JBlobSettings;
import com.luscastudio.jblob.server.debug.BLogger;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by Lucas on 04/10/2016.
 */

public class ProcessManager implements IFlushable {

    private ScheduledExecutorService processManager;
    private int threadCount;
    private Configuration config;

    public ProcessManager(Configuration config) {
        this.threadCount = config.getInt("server.process.thread.count", JBlobSettings.SERVER_PROCESS_THREAD_COUNT);
        this.config = config;
    }

    public void flush() {
        processManager = Executors.newScheduledThreadPool(threadCount, r -> {
            this.threadCount--;

            Thread thread = new Thread(r);

            thread.setName("Process Manager Thread #" + threadCount);

            thread.setUncaughtExceptionHandler((t, e) -> {
                BLogger.error("Error in EventArgs thread [" + t.getName() + "] :", ProcessManager.class);
            });

            return thread;

        });
    }

    @Override
    public void performFlush() {

    }

    public ScheduledFuture runLoop(Runnable run, int delay, int cycle, TimeUnit unit) {
        return this.processManager.scheduleAtFixedRate(run, (long) delay, (long) cycle, unit);
    }

    public ScheduledFuture run(Runnable run, int delay, TimeUnit unit) {
        return this.processManager.schedule(run, delay, unit);
    }


    public void close() {
        this.processManager.shutdown();
    }
}
