package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.utils;

import com.luscastudio.jblob.api.utils.engine.ObjectPosition;

/**
 * Created by Lucas on 15/02/2017 at 23:40.
 */
public class SavedItemState {
    private ObjectPosition position;
    private String extradata;
    public SavedItemState(ObjectPosition position, String extradata){
        this.position = position;
        this.extradata = extradata;
    }

    public ObjectPosition getPosition() {
        return position;
    }

    public String getExtradata() {
        return extradata;
    }
}
