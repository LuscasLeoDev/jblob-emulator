package com.luscastudio.jblob.server.communication.handlers.rooms.items.server;

import com.luscastudio.jblob.server.communication.handlers.rooms.items.ItemSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;

import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 10/10/2016.
 */

public class WallItemsComposer extends MessageComposer {
    public WallItemsComposer(Map<Integer, String> ownersNames, List<IRoomItem> items) {

        message.putInt(ownersNames.size()); //Furnis Owners len

        for(Map.Entry<Integer, String> entry : ownersNames.entrySet()){
            message.putInt(entry.getKey());
            message.putString(entry.getValue());
        }

        message.putInt(items.size());

        for(IRoomItem item : items)
            ItemSerializer.serializeWallItem(message, item);

    }

    public WallItemsComposer(Map<Integer, String> ownersNames, Map<Integer, IRoomItem> items) {

        message.putInt(ownersNames.size()); //Furnis Owners len

        for(Map.Entry<Integer, String> entry : ownersNames.entrySet()){
            message.putInt(entry.getKey());
            message.putString(entry.getValue());
        }

        message.putInt(items.size());

        for(IRoomItem item : items.values())
            ItemSerializer.serializeWallItem(message, item);
    }

     public WallItemsComposer(List<IRoomItem> items) {

        message.putInt(0); //Owners ids and names

        message.putInt(items.size());

        for(IRoomItem item : items)
            ItemSerializer.serializeWallItem(message, item);

    }

    public WallItemsComposer(Map<Integer, IRoomItem> items) {

        message.putInt(0); //Owners ids and names

        message.putInt(items.size());

        for(IRoomItem item : items.values())
            ItemSerializer.serializeWallItem(message, item);
    }



    @Override
    public String id() {
        return "ItemsMessageComposer";
    }
}
