package com.luscastudio.jblob.server.game.sessions;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.builds.ClientBuild;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.communication.server.MessageComposerBoxed;
import com.luscastudio.jblob.server.encryption.DiffieHellman;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.players.IMessageComposerHandler;
import com.luscastudio.jblob.server.game.players.MachineId;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.AttributeKey;

/**
 * Created by Lucas on 19/09/2016.
 */
public class PlayerSession implements IMessageComposerHandler {

    public static AttributeKey<PlayerSession> CLIENT_ATTR = AttributeKey.valueOf("PlayerSession.attr");

    private ChannelHandlerContext channel;
    private MachineId machineId;
    private HabboAvatar habboAvatar;
    private DiffieHellman diffieHellman;
    private ClientBuild build;

    public PlayerSession(ChannelHandlerContext ctx) {
        this.channel = ctx;
        diffieHellman = new DiffieHellman();
    }

    public void sendMessage(MessageComposer message) {
        this.sendMessage(message, true);
    }

    @Override
    public IMessageComposerHandler sendQueueMessage(MessageComposer message) {
        return this.sendMessage(message, false);
    }

    public void flush() {
        this.channel.channel().flush();
    }

    public PlayerSession sendMessage(MessageComposer message, boolean flush) {
        this.channel.channel().write(new MessageComposerBoxed(this, message));
        if (flush)
            this.channel.channel().flush();
        return this;
    }

    public void disconnect() {
        this.channel.disconnect();
        onDisconnect();
    }

    public void onDisconnect() {
        JBlob.getGame().getSessionManager().onSessionDisconnect(this);
        if (this.getAvatar() != null) {

            this.getAvatar().getEventHandler().fireEvent("session.disconnect");

            if (this.getAvatar().isInRoom()) {
                this.getAvatar().getCurrentRoom().getRoomAvatarService().removePlayerAvatar(this.getAvatar());
            }
            this.getAvatar().setOnline(false);
            JBlob.getGame().getUserCacheManager().onSessionDisconnect(this);
            habboAvatar.getFriendship().onDisconnect();

            this.dispose();
        }
    }

    public MachineId getMachineId() {
        return machineId;
    }

    public void setMachineId(String deviceId, String fingerprint, String systemInfo) {
        this.machineId = new MachineId(deviceId, fingerprint, systemInfo);


    }

    public DiffieHellman getDiffieHellman() {
        return diffieHellman;
    }

    public ClientBuild getBuild() {
        if(this.build != null && JBlob.getGame().getBuildManager().getBuildCount() != build.getLastUpdate())
            this.build = JBlob.getGame().getBuildManager().getBuild(this.build.getBuildName());
        return build;
    }

    public void setBuild(ClientBuild build) {
        this.build = build;
    }

    public HabboAvatar getAvatar() {
        return habboAvatar;
    }

    public void setHabboAvatar(HabboAvatar habboAvatar) {
        this.habboAvatar = habboAvatar;
    }

    public static AttributeKey<PlayerSession> getClientAttr() {
        return CLIENT_ATTR;
    }

    public ChannelHandlerContext getChannel() {
        return channel;
    }

    public void dispose(){
        this.getAvatar().dispose();//todo: Improve this shit
    }
}
