package com.luscastudio.jblob.server.game.habbohotel.catalog.dealitems;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.RoomItemFactory;

import java.util.List;
import java.util.Random;

/**
 * Created by Lucas on 07/02/2017 at 15:55.
 */
public class DealItemMonsterPlantSeed extends DealItemFurni {
    public DealItemMonsterPlantSeed(int id, int dealId, int baseId, String extradata, int limitedAmount, int amount, String type, boolean isRare) {
        super(id, dealId, baseId, extradata, limitedAmount, amount, type, isRare);
    }

    @Override
    public boolean generateValue(HabboAvatar avatar, String pageExtradata, List<Object> outValues, int amount) {

        int minRarityLevel = 0;
        int maxRarityLevel = 5;
        int i = 0;

        String[] lvlsStr = this.getBase().getCustom().split("-");

        if(lvlsStr.length >= 2){
            try{
                minRarityLevel = Integer.parseInt(lvlsStr[0]);
                maxRarityLevel = Integer.parseInt(lvlsStr[1]);
            } catch (Exception ignored){

            }
        }

        Random random = new Random(this.hashCode() + this.getAmount());

        List<RoomItemFactory.FurniPropertiesCreateData> createDatas = BCollect.newList();

        while (i++ < amount * this.getAmount()){
            int level = (random.nextInt(maxRarityLevel * 1000 + 1 - minRarityLevel * 1000) + minRarityLevel * 1000) / 1000;

            createDatas.add(new RoomItemFactory.FurniPropertiesCreateData(avatar.getId(), base, String.valueOf(level),false ,0));
        }
        List<FurniProperties> properties = JBlob.getGame().getRoomItemFactory().createFurniPropertiesList(createDatas);
        outValues.addAll(properties);
        FurniListNotificationComposer composer = new FurniListNotificationComposer();
        for(FurniProperties property : properties){
            composer.add(FurniListNotificationComposer.FURNI, property.getId());
        }

        outValues.add(composer);
        return true;
    }

    @Override
    public String getType() {
        return this.getBase().getType();
    }

    @Override
    public void handleValues(HabboAvatar avatar, List<Object> values) {
        super.handleValues(avatar, values);
    }
}
