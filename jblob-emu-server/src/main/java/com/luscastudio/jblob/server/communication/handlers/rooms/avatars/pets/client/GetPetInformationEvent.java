package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.server.PetAvailableCommandsComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.pets.server.PetInformationComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 22/01/2017 at 21:13.
 */
public class GetPetInformationEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        Room room = session.getAvatar().getCurrentRoom();

        if(room == null)
            return;

        int petId = packet.getInt();

        IRoomPetAvatar petAvatar = room.getRoomAvatarService().getPet(petId);
        if(petAvatar == null)
            return;

        String ownerName;
        if(session.getAvatar().getId() == petAvatar.getPetData().getOwnerId())
            ownerName = session.getAvatar().getUsername();
        else {
            IAvatarDataCache cache = JBlob.getGame().getUserCacheManager().getUserCache(petAvatar.getPetData().getOwnerId());
            ownerName = cache != null ? cache.getUsername() : "Unknown_" + petAvatar.getPetData().getOwnerId();
        }
        session.sendQueueMessage(new PetInformationComposer(petAvatar));
        session.sendQueueMessage(new PetAvailableCommandsComposer(petAvatar.getId(), petAvatar.getPetData().getPetBase().getCommands(), petAvatar.getPetData().getAvaliableCommands())).flush();

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
