package com.luscastudio.jblob.server.game.habbohotel.inventory.pets.custom;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.server.PacketWriting;

import java.util.Map;

/**
 * Created by Lucas on 04/02/2017 at 15:41.
 */
public class PetCustomData {

    private Map<Integer, PetCustomPart> parts;

    private String resultForParser;

    public PetCustomData(Map<Integer, PetCustomPart> customDataMap){
        this.parts = customDataMap;
        this.generateResultForParser();
    }

    public PetCustomData(String partsStr) {
        this.parts = BCollect.newMap();

        for (String partsArray : partsStr.split(";")) {

            String[] clotheData = partsArray.split(",");

            if (clotheData.length < 3)
                continue;

            try {
                int partType = Integer.parseInt(clotheData[0]);
                if (this.parts.containsKey(partType))
                    continue;

                int partId = Integer.parseInt(clotheData[1]);
                int partColor = Integer.parseInt(clotheData[2]);

                PetCustomPart part = new PetCustomPart(partType, partId, partColor);
                this.parts.put(partType, part);

            } catch (Exception e) {
            }

            this.generateResultForParser();
        }
    }

    public void generateResultForParser() {
        this.resultForParser = this.parts.size() + " ";


        boolean first = true;
        for (Map.Entry<Integer, PetCustomPart> entry : this.parts.entrySet()) {
            this.resultForParser += (!first ? " " : "") + entry.getKey() + " " + entry.getValue().getPartId() + " " + entry.getValue().getColorId();
            first = false;
        }
    }

    public String toPacketString() {
        return this.resultForParser;
    }

    public String toDbString() {
        String r = "";
        boolean first = true;
        for (Map.Entry<Integer, PetCustomPart> entry : this.parts.entrySet()) {
            r += (!first ? ";" : "") + entry.getKey() + "," + entry.getValue().getPartId() + "," + entry.getValue().getColorId();
            first = false;
        }

        return r;
    }

    public void serializeCustom(PacketWriting message) {
        message.putInt(parts.size());/// count of a set of 3 int
        for (Map.Entry<Integer, PetCustomPart> entry : this.parts.entrySet()) {
            message.putInt(entry.getKey());
            message.putInt(entry.getValue().getPartId());
            message.putInt(entry.getValue().getColorId());
        }
    }

    public void set(int partType, PetCustomPart part){
        if(this.parts.containsKey(partType))
            this.parts.replace(partType, part);
        else
            this.parts.put(partType, part);
    }

    public PetCustomPart getPart(int partType) {
        return this.parts.get(partType);
    }
}
