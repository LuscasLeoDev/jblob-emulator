package com.luscastudio.jblob.server.communication.server;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.debug.BLogger;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufHolder;

/**
 * Created by Lucas on 19/09/2016.
 */
public class MessageBufferComposer implements ByteBufHolder {

    private ByteBuf buffer;

    public MessageBufferComposer(ByteBuf buffer) {
        this.buffer = buffer;
    }

    public boolean hasLength() {
        return (this.buffer.getInt(0) > -1);
    }

    public MessageBufferComposer putShort(short v) {
        try {
            buffer.writeShort(v);
        } catch (Exception e) {
            this.onException(e);
        } finally {
            return this;
        }
    }

    public MessageBufferComposer putInt(int v) {
        try {
            buffer.writeInt(v);
        } catch (Exception e) {
            this.onException(e);
        } finally {
            return this;
        }
    }

    public MessageBufferComposer putString(String val) {

        try {
            if (val == null)
                val = "";

            byte[] strbuffer = val.getBytes(JBlob.getDefaultCharset());
            buffer.writeShort(strbuffer.length);
            buffer.writeBytes(strbuffer);
        } catch (Exception e) {
            this.onException(e);
        } finally {
            return this;
        }
    }

    public MessageBufferComposer putBool(Boolean b) {
        try {
            buffer.writeByte(b ? 1 : 0);
        } catch (Exception e) {
            this.onException(e);
        } finally {
            return this;
        }
    }

    public MessageBufferComposer putByte(byte b) {
        try {
            buffer.writeByte(b);
        } catch (Exception e) {
            this.onException(e);
        } finally {
            return this;
        }
    }

    public MessageBufferComposer putDouble(double d) {
        String raw = String.valueOf(Math.round(d));

        if (raw.length() == 1)
            raw += ".0";

        return this.putString(raw);
    }

    private void onException(Exception e) {
        BLogger.error(e, this.getClass());
    }

    @Override
    public ByteBuf content() {
        return this.buffer;
    }

    @Override
    public MessageBufferComposer copy() {
        return new MessageBufferComposer(this.buffer.copy());
    }

    @Override
    public MessageBufferComposer duplicate() {
        return new MessageBufferComposer(this.buffer.duplicate());
    }

    @Override
    public int refCnt() {
        return this.buffer.refCnt();
    }

    @Override
    public MessageBufferComposer retain() {
        return new MessageBufferComposer(this.buffer.retain());
    }

    @Override
    public ByteBufHolder retain(int increment) {
        return new MessageBufferComposer(this.buffer.retain(increment));
    }

    @Override
    public boolean release() {
        return this.buffer.release();
    }

    @Override
    public boolean release(int decrement) {
        return this.buffer.release(decrement);
    }
}
