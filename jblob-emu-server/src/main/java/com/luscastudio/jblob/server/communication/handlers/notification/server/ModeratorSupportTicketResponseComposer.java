package com.luscastudio.jblob.server.communication.handlers.notification.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 16/12/2016.
 */

public class ModeratorSupportTicketResponseComposer extends MessageComposer {
    @Override
    public String id() {
        return "ModeratorSupportTicketResponseMessageComposer";
    }

    public ModeratorSupportTicketResponseComposer(int result, String text){
        message.putInt(result);
        message.putString(text);
    }

    public ModeratorSupportTicketResponseComposer(String text){
        this(0, text);
    }
}
