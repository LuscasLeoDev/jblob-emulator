package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.triggers;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.HideWiredConfigComposer;
import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.events.EventDelegate;
import com.luscastudio.jblob.server.events.list.AvatarWalkOnFurniEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.WiredSaveData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.TriggerWiredBox;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * Created by Lucas on 14/02/2017 at 23:49.
 */
public class TriggerAvatarWalksOnFurni extends TriggerWiredBox {

    private Map<Integer, IRoomItem> selectedItems;
    EventDelegate delegate;

    public TriggerAvatarWalksOnFurni(FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoordinate) {
        super(properties, room, x, y, z, rotation, wallCoordinate);

        this.selectedItems = BCollect.newConcurrentMap();

        this.delegate = this.room.getEventHandler().on("avatar.walk.on.furni", this::onEvent);
    }

    private void onEvent(EventDelegate delegate, EventArgs args) {
        try {
            AvatarWalkOnFurniEventArgs eventArgs = (AvatarWalkOnFurniEventArgs) args;
            IRoomItem item = eventArgs.getItem();
            IRoomAvatar avatar = eventArgs.getAvatar();
            if(avatar == null)
                return;
            if (!this.selectedItems.containsKey(item.getProperties().getId()))
                return;

            this.getExtradata().toggle();
            this.room.getRoomItemHandlerService().updateItem(this, false);

            if(this.triggerConditionBoxes(avatar, item))
                this.triggerEffectBoxes(avatar, item);

        } catch (Exception ignored){

        }
    }

    @Override
    public int getWiredCode() {
        return 1; //Trigger Avatar Walks on Furni
    }

    @Override
    public boolean requireAvatar() {
        return true;
    }

    @Override
    public boolean requireTriggeredItem() {
        return false;
    }

    @Override
    public Collection<IRoomItem> getSelectedItems() {
        return selectedItems.values();
    }

    @Override
    public String getStringData() {
        return "";
    }

    @Override
    public Collection<Integer> getIntegersData() {
        return Collections.emptyList();
    }

    @Override
    public Collection<IRoomItem> getIncompatibleItems() {
        return Collections.emptyList();
    }

    @Override
    protected void loadWiredData() {
        try{
            WiredSaveData data = this.getExtradata().getSavedData();
            if(data != null) {
                this.selectedItems = this.generateSelectedItems(data.getItemsId());
            } else {
                this.selectedItems = BCollect.newMap();
            }
        }catch (Exception e){
            this.selectedItems = BCollect.newMap();
        }
    }

    @Override
    public void saveWiredData(PlayerSession session, Collection<Integer> integerList, String stringData, Collection<Integer> selectedFurniList, int someInt) {
        if(!this.room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_save_wired_config"))
            return;
        this.selectedItems.clear();

        this.selectedItems = this.generateSelectedItems(selectedFurniList);

        this.getExtradata().save(new WiredSaveData(null, this.selectedItems.keySet(), 0, ""));
        this.room.getRoomItemHandlerService().saveItem(this);

        session.sendMessage(new HideWiredConfigComposer());
    }

    @Override
    public void dispose() {
        this.delegate.cancel();
        super.dispose();
    }
}
