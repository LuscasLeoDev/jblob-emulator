//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.game.habbohotel.rooms.forwarding;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.engine.IObject2DPosition;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomEnterErrorFuture;
import io.netty.util.concurrent.Future;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * Created by Lucas on 08/05/2017 at 11:00.
 */
public class RoomForwardService {

    //private Map<RoomForwardService, Future<Boolean>> futureMap;
    private BlockingQueue<ForwardProperties> queue;
    private ScheduledFuture queueScheduled;

    public RoomForwardService(){
        this.queue = BCollect.newBlockingQueue();
    }

    public void init() {
        this.queueScheduled = JBlob.getGame().getProcessManager().runLoop(this::cycleQueues, 1, 200, TimeUnit.MILLISECONDS);
    }

    private synchronized void cycleQueues(){

        try {
            ForwardProperties properties = queue.take();

            this.forwardUser(properties);


        } catch (InterruptedException e) {
            BLogger.error(e, this.getClass());
        }
    }

    private void forwardUser(ForwardProperties properties){
        if(properties.getAvatar().getCurrentRoom() != null) {
            properties.callError(new RoomEnterErrorFuture(4, "AVATAR_IN_ROOM"));
            return;
        }

        Room room = JBlob.getGame().getRoomManager().loadRoom(properties.getRoomId());
        if(room == null) {
            properties.callError(new RoomEnterErrorFuture(5, "UNKNOWN_ROOM"));
            return;
        }

        RoomEnterErrorFuture roomEnterErrorFuture;
        if(properties.getPosition() != null)
            roomEnterErrorFuture = room.getRoomAvatarService().tryAddPlayerAvatar(properties.getAvatar(), properties.getPosition());
        else
            roomEnterErrorFuture = room.getRoomAvatarService().tryAddPlayerAvatar(properties.getAvatar());

        if(!roomEnterErrorFuture.success()){
            properties.callError(roomEnterErrorFuture);
        } else {
            properties.callError(new RoomEnterErrorFuture());
        }

    }

    public ForwardProperties enqueue(int roomId, HabboAvatar avatar, IObject2DPosition position){
        ForwardProperties forwardProperties = new ForwardProperties(roomId, position, avatar);
        try {
            this.queue.put(forwardProperties);
        } catch (InterruptedException e) {
            BLogger.error(e, this.getClass());
            return null;
        }
        return forwardProperties;
    }


}
