package com.luscastudio.jblob.server.communication.handlers.handshake.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.handshake.server.InitCryptoComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 19/09/2016.
 */
public class InitCrytoEvent implements IMessageEventHandler {

    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        session.sendMessage(new InitCryptoComposer(session));
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
