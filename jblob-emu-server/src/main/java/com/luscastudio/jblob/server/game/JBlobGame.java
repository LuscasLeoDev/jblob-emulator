package com.luscastudio.jblob.server.game;

import com.luscastudio.jblob.api.sercurity.StringFilter;
import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.api.utils.console.InitializeBar;
import com.luscastudio.jblob.api.utils.io.Configuration;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.api.utils.time.DateTimeUtils;
import com.luscastudio.jblob.server.communication.client.builds.ClientBuildManager;
import com.luscastudio.jblob.server.database.DBConnectionManager;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.events.EventHandler;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogManager;
import com.luscastudio.jblob.server.game.habbohotel.landingview.promoarticle.PromoArticleManager;
import com.luscastudio.jblob.server.game.habbohotel.navigator.NavigatorManager;
import com.luscastudio.jblob.server.game.habbohotel.pet.PetManager;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.pets.PetAvatarHandler;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.ChatCommandManager;
import com.luscastudio.jblob.server.game.habbohotel.rooms.forwarding.RoomForwardService;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.RoomItemFactory;
import com.luscastudio.jblob.server.game.habbohotel.rooms.models.RoomModelManager;
import com.luscastudio.jblob.server.game.permissions.PermissionManager;
import com.luscastudio.jblob.server.game.players.cache.UserCacheFactory;
import com.luscastudio.jblob.server.game.sessions.PlayerSessionManager;
import com.luscastudio.jblob.server.manager.ClientMessageManager;
import com.luscastudio.jblob.server.manager.ItemBaseManager;
import com.luscastudio.jblob.server.manager.ProcessManager;
import com.luscastudio.jblob.server.manager.RoomManager;
import com.luscastudio.jblob.server.network.client.AsyncClientConnectionHandler;
import com.luscastudio.jblob.server.network.client.ClientConnectionHandler;
import com.luscastudio.jblob.server.network.client.SyncClientConnectionHandler;
import com.luscastudio.jblob.server.network.server.ServerClientManager;
import com.luscastudio.jblob.server.settings.DBConfig;
import org.apache.log4j.Logger;

import java.net.InetSocketAddress;
import java.util.List;

/**
 * Created by Lucas on 13/09/2016.
 */
public class JBlobGame {

    private Logger logger = Logger.getLogger(JBlobGame.class);

    private EventHandler eventHandler;

    private Configuration config;

    private ServerClientManager serverClientManager;
    
    private ClientMessageManager messageEventManager;

    private PlayerSessionManager sessionManager;

    private ClientBuildManager buildManager;

    private DBConnectionManager dbConn;

    private UserCacheFactory userCacheManager;

    private RoomManager roomManager;

    //region In Game Region

    private NavigatorManager navigatorManager;

    private RoomModelManager modelManager;

    private ProcessManager processManager;

    private ItemBaseManager itemBaseManager;

    private RoomItemFactory roomItemFactory;

    private PetManager petManager;

    private PetAvatarHandler petAvatarHandler;

    private CatalogManager catalogManager;

    private ChatCommandManager chatCommandManager;

    private DBConfig dbConfig;

    private int startedTime;

    private PermissionManager permissionManager;

    private StringFilter filter;

    private PromoArticleManager promoArticleManager;

    private RoomForwardService roomForwardService;

    //endregion

    public JBlobGame() {

    }

    //region Getters Region

    public Logger getLogger() {
        return logger;
    }

    public Configuration getConfig() {
        return config;
    }

    public ClientBuildManager getBuildManager() {
        return buildManager;
    }

    public DBConnectionManager getDbConn() {
        return dbConn;
    }

    public ItemBaseManager getItemBaseManager() {
        return itemBaseManager;
    }

    public NavigatorManager getNavigatorManager() {
        return navigatorManager;
    }

    public PlayerSessionManager getSessionManager() {
        return sessionManager;
    }

    public ProcessManager getProcessManager() {
        return processManager;
    }

    public RoomItemFactory getRoomItemFactory() {
        return roomItemFactory;
    }

    public RoomManager getRoomManager() {
        return roomManager;
    }

    public ServerClientManager getServerClientManager() {
        return serverClientManager;
    }
    
    public ClientMessageManager getMessageEventManager() { return messageEventManager; }

    public RoomModelManager getModelManager() {
        return modelManager;
    }

    public UserCacheFactory getUserCacheManager() {
        return userCacheManager;
    }

    public CatalogManager getCatalogManager() {
        return catalogManager;
    }

    public ChatCommandManager getChatCommandManager() {
        return chatCommandManager;
    }

    public PetManager getPetManager() {
        return petManager;
    }

    public PetAvatarHandler getPetAvatarHandler() {
        return petAvatarHandler;
    }

    public DBConfig getDbConfig() {
        return dbConfig;
    }

    public int getStartedTime() {
        return startedTime;
    }

    public PermissionManager getPermissionManager() {
        return permissionManager;
    }

    public PromoArticleManager getPromoArticleManager() {
        return promoArticleManager;
    }

    public StringFilter getFilter() {
        return filter;
    }

    public EventHandler getEventHandler() {
        return eventHandler;
    }

    public RoomForwardService getRoomForwardService() {
        return roomForwardService;
    }

    //endregion

    public void init() {
        System.out.println("Initializing JBlob Emu!");
        try {

            InitializeBar bar = new InitializeBar(System.out::print);

            this.eventHandler = new EventHandler();

            this.config = new Configuration("./jblob.properties");

            this.sessionManager = new PlayerSessionManager();

            this.buildManager = new ClientBuildManager();
            bar.init("Initializing Build Manager", buildManager, () -> "Done! [ Loaded " + buildManager.getBuildCount() + " builds ]");


            short header;
            String headerStr;
            if((headerStr = config.get("client.handler.initial.header", null)) == null)
                header = ((short) 4000);
            else
                header = Short.parseShort(headerStr);
            this.messageEventManager = new ClientMessageManager(header);
            bar.init("Setting up Packet Events", messageEventManager);

            this.processManager = new ProcessManager(config);
            bar.init("Initializing Process Manager", processManager);


            this.dbConn = new DBConnectionManager(config);
            bar.init("Initializing conection with database...\n", dbConn, "Connected!");

            this.dbConfig = new DBConfig();
            bar.init("Setting up Emu Settings By Database", dbConfig);

            //InGame

            //Non-DB connection needed
            this.userCacheManager = new UserCacheFactory();

            this.roomManager = new RoomManager();

            //DB connection needed

            this.permissionManager = new PermissionManager();
            bar.init("Initializing Permissions", permissionManager);

            this.navigatorManager = new NavigatorManager();
            bar.init("Initializing Navigator", navigatorManager);

            this.modelManager = new RoomModelManager();
            bar.init("Initializing Room Model Manager", modelManager);

            this.itemBaseManager = new ItemBaseManager();
            bar.init("Initializing Item base Manager", itemBaseManager);

            this.roomItemFactory = new RoomItemFactory();
            bar.init("Initializing Room Item Factory", roomItemFactory);

            this.petManager = new PetManager();
            bar.init("Initializing Pet Base Manager", petManager);

            this.petAvatarHandler = new PetAvatarHandler();
            bar.init("Initializing Pet Avatar Handler", petAvatarHandler);

            this.catalogManager = new CatalogManager();
            bar.init("Initializing Catalog Manager", catalogManager);

            this.promoArticleManager = new PromoArticleManager();
            bar.init("Initializing Promo Articles", this.promoArticleManager);

            this.chatCommandManager = new ChatCommandManager();
            bar.init("Setting Up Chat Command Manager", chatCommandManager);

            this.roomForwardService = new RoomForwardService();
            this.roomForwardService.init();
            bar.write("Room Forwarding Service Runnung...\n");

            this.filter = new StringFilter();

            //Init Server

            List<InetSocketAddress> socketAddresses = BCollect.newList();

            String s = config.get("socket.listen.hosts", null);
            if(s == null)
                throw new Exception("No socket listener host detected!");

            for (String addrs : s.split(";")) {
                String[] spl = addrs.split(":");
                if(spl.length < 2)
                    continue;

                if(!NumberHelper.isInteger(spl[1]))
                    continue;

                socketAddresses.add(new InetSocketAddress(spl[0], Integer.parseInt(spl[1])));
            }

            ClientConnectionHandler clientHandler;
            if(config.getBool("socket.handling.async").equals("true"))
                clientHandler = new AsyncClientConnectionHandler();
            else
                clientHandler = new SyncClientConnectionHandler();

            this.serverClientManager = new ServerClientManager(socketAddresses,
                    addr -> BLogger.warn("Error when binding the host " + addr.getHostString() + ":" + addr.getPort(), this.getClass()),
                    addr -> BLogger.warn("Server socket listening to " + addr.getHostString() + ":" + addr.getPort(), this.getClass()),
                    clientHandler);
            bar.init("Initializing sockets", new IFlushable() {
                @Override
                public void flush() throws Exception {
                    serverClientManager.connect();
                }

                @Override
                public void performFlush() {

                }
            }, "The JBlob Server Is Ready!");

            this.startedTime = DateTimeUtils.getUnixTimestampInt();

        } catch (Exception e) {
            BLogger.error(e, this.getClass());
            System.exit(-1);
        }


    }

    public void close() {
        this.serverClientManager.close();
        this.roomManager.close();
        this.processManager.close();
        this.sessionManager.close();
        this.dbConn.close();
    }
}
