package com.luscastudio.jblob.server.game.sessions;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.validator.Validator;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.players.IMessageComposerHandler;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 19/09/2016.
 */
public class PlayerSessionManager implements IMessageComposerHandler {

    public List<PlayerSession> unAuthenticatedsessions;
    private Map<Integer, PlayerSession> sessions;

    public PlayerSessionManager() {
        sessions = BCollect.newMap();
        unAuthenticatedsessions = BCollect.newList();
    }

    public boolean tryCreateSession(ChannelHandlerContext ctx) {

        PlayerSession newSession = new PlayerSession(ctx);

        ctx.attr(PlayerSession.CLIENT_ATTR).set(newSession);

        this.unAuthenticatedsessions.add(newSession);

        return true;

    }

    public void onSessionAuthenticated(PlayerSession session) {

        if (!unAuthenticatedsessions.contains(session))
            return;

        unAuthenticatedsessions.remove(session);

        sessions.put(session.getAvatar().getId(), session);

    }

    public PlayerSession getSession(Validator<PlayerSession> sessionValidator){
        for (PlayerSession session : this.sessions.values()) {
            if(sessionValidator.validate(session))
                return session;
        }
        return null;
    }

    public List<PlayerSession> getSessions(Validator<PlayerSession> sessionValidator){
        List<PlayerSession> sessions = BCollect.newList();
        for (PlayerSession session : this.sessions.values()) {
            if(sessionValidator.validate(session))
                sessions.add(session);
        }
        return sessions;
    }

    public PlayerSession getSession(int id) {
        if (sessions.containsKey(id))
            return sessions.get(id);

        return null;
    }


    public void onSessionDisconnect(PlayerSession session) {

        if (session.getAvatar() == null)
            return;

        unAuthenticatedsessions.remove(session);

        if (sessions.containsKey(session.getAvatar().getId()))
            sessions.remove(session.getAvatar().getId());
    }

    @Override
    public IMessageComposerHandler sendQueueMessage(MessageComposer message) {
        for(PlayerSession session : sessions.values())
            session.sendQueueMessage(message);

        return this;
    }

    @Override
    public void flush() {
        for(PlayerSession session : sessions.values())
            session.flush();
    }

    @Override
    public void sendMessage(MessageComposer message) {
        for(PlayerSession session : sessions.values())
            session.sendMessage(message);
    }

    public Map<Integer, PlayerSession> getSessions() {
        return sessions;
    }

    public void close() {
        for(PlayerSession session : this.sessions.values()){
            session.getAvatar().dispose();
        }
    }
}
