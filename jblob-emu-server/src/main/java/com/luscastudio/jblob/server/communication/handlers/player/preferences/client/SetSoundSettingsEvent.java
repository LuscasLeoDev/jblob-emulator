package com.luscastudio.jblob.server.communication.handlers.player.preferences.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.habbohotel.avatar.preference.AvatarPreferences;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;


/**
 * Created by Lucas on 09/12/2016.
 */

public class SetSoundSettingsEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        AvatarPreferences p = session.getAvatar().getPreferences();

        p.setInt(AvatarPreferences.VOLUME_SYSTEM, packet.getInt());
        p.setInt(AvatarPreferences.VOLUME_FURNI, packet.getInt());
        p.setInt(AvatarPreferences.VOLUME_MUSIC, packet.getInt());

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
