package com.luscastudio.jblob.server.communication.handlers.player.inventory.server;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.server.MessageComposer;

import java.util.List;

/**
 * Created by Lucas on 31/01/2017 at 19:45.
 */
public class FurniListNotificationComposer extends MessageComposer {

    public static int FURNI = 1;
    public static int BOT = 5;
    public static int PET = 3;
    public static int BADGE = 1;

    @Override
    public String id() {
        return "FurniListNotificationMessageComposer";
    }

    private List<Integer> types;
    private List<Integer> itemIds;

    public FurniListNotificationComposer(){
        this.types = BCollect.newList();
        this.itemIds = BCollect.newList();
    }

    public FurniListNotificationComposer add(int type, int id){
        if(!this.types.contains(type))
            this.types.add(type);

        if(!this.itemIds.contains(id))
            this.itemIds.add(id);

        return this;
    }

    @Override
    public void compose() {

        message.putInt(types.size());
        for(int i : types)
            message.putInt(i);

        message.putInt(itemIds.size());
        for(int i : itemIds)
            message.putInt(i);

        super.compose();
    }
}
