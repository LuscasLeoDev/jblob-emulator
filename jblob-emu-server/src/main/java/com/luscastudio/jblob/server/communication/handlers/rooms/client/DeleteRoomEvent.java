package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryUpdateRequestComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.RemoveFloorItemComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.RemoveWallItemComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 17/02/2017 at 02:49.
 */
public class DeleteRoomEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int roomId = packet.getInt();

        Room room = JBlob.getGame().getRoomManager().loadRoom(roomId);
        if(room == null)
            return;

        if(!room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_delete_room"))
            return;

        Map<Integer, List<IRoomItem>> roomItemsMap = room.getRoomItemHandlerService().removeAll();

        //region Remove RoomItems
        {
            List<IRoomItem> items = BCollect.newList();



                Iterator<Map.Entry<Integer, List<IRoomItem>>> iteratorUsers = roomItemsMap.entrySet().iterator();

                while (iteratorUsers.hasNext()){
                    Map.Entry<Integer, List<IRoomItem>> itemsByUser = iteratorUsers.next();

                    PlayerSession targetSession = JBlob.getGame().getSessionManager().getSession(itemsByUser.getKey());

                    iteratorUsers.remove();

                    items.addAll(itemsByUser.getValue());

                    if(targetSession == null)
                        continue;

                    Iterator<IRoomItem> iteratorItems = itemsByUser.getValue().iterator();
                    FurniListNotificationComposer notificationComposer = new FurniListNotificationComposer();
                    while (iteratorItems.hasNext()){
                        IRoomItem item = iteratorItems.next();

                        iteratorItems.remove();

                        targetSession.getAvatar().getInventory().addRoomItem(item.getProperties());
                        notificationComposer.add(FurniListNotificationComposer.FURNI, item.getProperties().getId());

                        room.getRoomMap().updateSqHeight(item.getAffectedTiles());
                    }

                    targetSession.sendQueueMessage(notificationComposer);
                    targetSession.sendQueueMessage(new InventoryUpdateRequestComposer());
                    targetSession.flush();


            }

            Iterator<IRoomItem> itemIterator = items.iterator();
            while(itemIterator.hasNext()) {
                IRoomItem item = itemIterator.next();
                if(item.getBase().getType().equals("i"))
                    room.sendQueueMessage(new RemoveWallItemComposer(item.getId(), item.getOwnerId()));
                else
                    room.sendMessage(new RemoveFloorItemComposer(item.getId(), item.getOwnerId()));

                item.dispose();
            }

            room.flush();
        }

        //endregion

        //todo: complete pet pick up
        // region Remove Pets
        {
            Iterator<IRoomPetAvatar> petAvatarIterator = room.getRoomAvatarService().getPets().iterator();
            Map<Integer, IRoomPetAvatar> usersPetsAvatar = BCollect.newMap();

            while (petAvatarIterator.hasNext()) {

            }
        }
        //endregion

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare;
            prepare = reactor.prepare("DELETE FROM players_rooms WHERE id = ?");

            prepare.setInt(1, room.getProperties().getId());
            prepare.run();

            prepare = reactor.prepare("UPDATE avatars_pets_data SET room_id = ? WHERE room_id = ?");
            prepare.setInt(1, 0);
            prepare.setInt(2, room.getProperties().getId());
            prepare.run();

        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }

        JBlob.getGame().getRoomManager().unloadRoom(room.getProperties().getId());

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
