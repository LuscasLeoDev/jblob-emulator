package com.luscastudio.jblob.server.database.dao.rooms.items;

import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.ASyncQueryType;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.database.IASyncQueryCallback;
import org.apache.log4j.Logger;

import java.sql.ResultSet;

/**
 * Created by Lucas on 27/10/2016.
 */

public class RoomItemHandlerDao {

    private static Logger log = Logger.getLogger(RoomItemHandlerDao.class);

    public static boolean addItemToRoom(int roomid, int itemid, int x, int y, double z, int rot) {


        try (DBConnReactor reator = JBlob.getGame().getDbConn().getReactor()) {
            DBConnPrepare prepare = reator.prepare("UPDATE avatars_items_data SET room_id = ?, x = ?, y = ?, z = ?, rotation = ? WHERE id = ?");
            prepare.setInt(1, roomid);
            prepare.setInt(2, x);
            prepare.setInt(3, y);
            prepare.setString(4, NumberHelper.doubleToString(z));
            prepare.setInt(5, rot);
            prepare.setInt(6, itemid);
            prepare.run();

        } catch (Exception e) {
            log.error("Error while adding room item #" + itemid + " to room #" + roomid, e);
            return false;
        }

        return true;
    }

    public static boolean addItemToRoom(int roomId, int id, String wallCoordinate) {
        try (DBConnReactor reator = JBlob.getGame().getDbConn().getReactor()) {
            DBConnPrepare prepare = reator.prepare("UPDATE avatars_items_data SET room_id = ?, wall_coordinate = ? WHERE id = ?");
            prepare.setInt(1, roomId);
            prepare.setString(2, wallCoordinate);
            prepare.setInt(3, id);
            prepare.run();
        } catch (Exception e) {
            log.error("Error while adding room item #" + id + " to room #" + roomId, e);
            return false;
        }

        return true;
    }
}
