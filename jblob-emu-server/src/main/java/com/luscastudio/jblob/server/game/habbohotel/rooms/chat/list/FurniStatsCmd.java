package com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list;

import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.communication.handlers.notification.server.BroadcastMessageAlertComposer;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.IChatCommand;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 28/12/2016 at 20:20.
 */
public class FurniStatsCmd implements IChatCommand {
    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {

        String numStr = params.get(1, "");
        if(!NumberHelper.isInteger(numStr)) {
            session.sendMessage(new BubbleNotificationComposer("cmd.furnistats.error.invalidinput"));
            return false;
        }
        int itemId = Integer.parseInt(numStr);
        IRoomItem item = room.getRoomItemHandlerService().getRoomItemById(itemId);

        if(item == null){
            session.sendMessage(new BubbleNotificationComposer("cmd.furnistats.error.furninotfound"));
            return false;
        }

        StringBuilder stats = new StringBuilder();
        stats.append("\t\t\t<b>Furni Information</b>");
        stats.append("\nId: ").append(item.getProperties().getId());
        stats.append(" x: ").append(item.getPosition().getX()).append(" y: ").append(item.getPosition().getY()).append(" z: ").append(NumberHelper.doubleToString(item.getPosition().getZ())).append("\n");
        stats.append("\nDynamic Height: ").append(NumberHelper.doubleToString(item.getHeight()));
        stats.append("\nAffected tiles:\n");
        for (Point p : item.getAffectedTiles()){
            stats.append("[").append(p.getX()).append(", ").append(p.getY()).append("] ");
        }

        stats.append("\n");
        stats.append("\n\t\t\t <b>Base Information</b>");
        stats.append("\n Base Id: ").append(item.getProperties().getBase().getId());
        stats.append("\n Base Name: ").append(item.getProperties().getBase().getClassName());
        stats.append("\n Base Sprite Id: ").append(item.getProperties().getBase().getSpriteId());
        stats.append("\n Width: ").append(item.getProperties().getBase().getWidth());
        stats.append("\n Length: ").append(item.getProperties().getBase().getLength());
        stats.append("\n Fixed Height: ").append(NumberHelper.doubleToString(item.getProperties().getBase().getHeight()));
        stats.append("\n Height List: ");
        for (double h : item.getProperties().getBase().getHeights()){
            stats.append("[").append(NumberHelper.doubleToString(h)).append("] ");
        }
        stats.append("\n Interaction Type: ").append(item.getProperties().getBase().getInteractionType());
        stats.append("\n Interaction Class: ").append(item.getClass().getSimpleName());

        session.sendMessage(new BroadcastMessageAlertComposer(stats.toString()));

        return true;
    }
}
