package com.luscastudio.jblob.server.game.habbohotel.pet.custom;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.custom.PetCustomPart;

import java.util.Collection;
import java.util.List;

/**
 * Created by Lucas on 08/02/2017 at 21:27.
 */
public class PetCustomDataQuery {

    private ListMultimap<Integer, PetCustomPart> partSuggests;
    private List<Integer> requiredPartTypes;

    public PetCustomDataQuery(){
        this.partSuggests = ArrayListMultimap.create();
        this.requiredPartTypes = BCollect.newList();
    }
    public ListMultimap<Integer, PetCustomPart> getPartSuggests() {
        return partSuggests;
    }

    public void addPartSuggest(int partType, PetCustomPart partData){
        this.partSuggests.put(partType, partData);
    }

    public Collection<Integer> getRequiredPartTypes() {
        return requiredPartTypes;
    }

    public void addRequiredPartType(int partType){
        this.requiredPartTypes.add(partType);
    }
}
