package com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.client;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts.ISavableWiredCondition;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;

/**
 * Created by Lucas on 16/02/2017 at 00:02.
 */
public class SaveWiredConditionConfigEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int itemId = packet.getInt();

        int integersCount = packet.getInt();
        List<Integer> integerList = BCollect.newList();
        for (int i = 0; i < integersCount; i++)
            integerList.add(packet.getInt());

        String stringData = packet.getString();

        int selectedItemsLen = packet.getInt();
        List<Integer> selectedFurniList = BCollect.newList();
        for (int i = 0; i < selectedItemsLen; i++)
            selectedFurniList.add(packet.getInt());

        int lastInt1 = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        IRoomItem item = room.getRoomItemHandlerService().getRoomItemById(itemId);
        if(item == null)
            return;

        if((item instanceof ISavableWiredCondition))
            ((ISavableWiredCondition)item).saveWiredData(session, integerList, stringData, selectedFurniList, lastInt1);
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
