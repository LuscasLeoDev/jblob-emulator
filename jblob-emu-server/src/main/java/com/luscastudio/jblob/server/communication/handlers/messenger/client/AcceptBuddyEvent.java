package com.luscastudio.jblob.server.communication.handlers.messenger.client;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.messenger.server.FriendListUpdateComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.friendship.AvatarFriendshipItem;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.List;

/**
 * Created by Lucas on 27/02/2017 at 16:14.
 */
public class AcceptBuddyEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        int length = packet.getInt();

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {
            String sql = "DELETE FROM avatars_friendship_request WHERE ";
            boolean f = true;
            List<Integer> ids = BCollect.newList();
            for (int i = 0; i < length; i++) {
                if(!f)
                    sql += " OR ";
                sql += "(from_user_id = ? AND to_user_id = ?)";
                f = false;

                ids.add(packet.getInt());
            }

            int i = 0;
            DBConnPrepare prepare = reactor.prepare(sql);
            for (Integer id : ids) {
                prepare.setInt(i * 2 + 1, id);
                prepare.setInt(i * 2 + 2, session.getAvatar().getId());
                i++;
                session.getAvatar().getFriendship().removeFriendRequest(id);
            }
            prepare.run();


            FriendListUpdateComposer updateComposer = new FriendListUpdateComposer(BCollect.newMap());
            sql = "INSERT INTO avatars_friendships VALUES ";
            f = true;
            for (int i1 = 0; i1 < length; i1++) {
                if(!f)
                    sql += ", ";
                sql += "(?, ?)";
                f = false;
            }

            i = 0;
            prepare = reactor.prepare(sql);
            for (Integer id : ids) {
                prepare.setInt(i * 2 + 1, id);
                prepare.setInt(i * 2 + 2, session.getAvatar().getId());
                AvatarFriendshipItem friendshipItem = session.getAvatar().getFriendship().addFriend(id);
                updateComposer.addUpdate(friendshipItem, friendshipItem.getUserData(), 1);
            }

            prepare.run();
            session.sendMessage(updateComposer);

            for (Integer id : ids) {
                PlayerSession friendSession = JBlob.getGame().getSessionManager().getSession(id);
                if(friendSession == null)
                    continue;

                AvatarFriendshipItem friend = friendSession.getAvatar().getFriendship().addFriend(session.getAvatar().getId());
                friendSession.sendMessage(new FriendListUpdateComposer(BCollect.newMap()).addUpdate(friend, friend.getUserData(), 1));
            }

        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
