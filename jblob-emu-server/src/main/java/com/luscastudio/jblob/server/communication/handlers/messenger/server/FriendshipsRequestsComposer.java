package com.luscastudio.jblob.server.communication.handlers.messenger.server;

import com.luscastudio.jblob.api.utils.validator.ValueGetter;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;

import java.util.Collection;

/**
 * Created by Lucas on 02/10/2016.
 */
public class FriendshipsRequestsComposer extends MessageComposer {
    @Override
    public String id() {
        return "BuddyRequestsMessageComposer";
    }

    public FriendshipsRequestsComposer(Collection<Integer> requestsId, ValueGetter<Integer, FriendRequestData> requestDataGetter) {

        message.putInt(0); //unused

        message.putInt(requestsId.size());//count

        //region User request data
        for (Integer id : requestsId) {
            FriendRequestData data = requestDataGetter.getValue(id);
            message.putInt(data.getId());
            message.putString(data.getName());
            message.putString(data.getFigure());
        }

        //endregion

    }
}
