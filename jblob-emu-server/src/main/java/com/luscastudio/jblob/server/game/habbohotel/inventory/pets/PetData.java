package com.luscastudio.jblob.server.game.habbohotel.inventory.pets;

import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.custom.PetCustomData;
import com.luscastudio.jblob.server.game.habbohotel.pet.PetBase;

import java.util.List;

/**
 * Created by Lucas on 22/01/2017 at 18:38.
 */
public class PetData {

    protected int id;
    protected String name;
    private int ownerId;
    private String color;
    private int raceId;
    private PetCustomData custom;
    private String extradata;

    private PetBase petBase;

    private int level;
    private int rarityLevel;
    private int energy;
    private int happiness;

    private int experience;
    private int respects;
    private int createTimestamp;
    private int roomId;

    public PetData(PetBase petBase, int id, String name, int ownerId, int roomId, String color, PetCustomData custom, int level, int rarityLevel, int energy, int happiness, int experience, int respects, int createTimestamp, String extradata, int raceId){
        this.petBase = petBase;
        this.id = id;
        this.name = name;
        this.ownerId = ownerId;
        this.roomId = roomId;
        this.color = color;
        this.custom = custom;
        this.level = level;
        this.rarityLevel = rarityLevel;
        this.energy = energy;
        this.happiness = happiness;
        this.experience = experience;
        this.respects = respects;
        this.createTimestamp = createTimestamp;
        this.extradata = extradata;
        this.raceId = raceId;
    }

    public int getId() {
        return id;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getColor() {
        return color;
    }

    public PetCustomData getCustom() {
        return custom;
    }

    public String getExtradata() {
        return extradata;
    }

    public String getName() {
        return name;
    }

    public PetBase getPetBase() {
        return petBase;
    }

    public int getLevel() {
        return level;
    }

    public int getRarityLevel() {
        return rarityLevel;
    }

    public int getExperience() {
        return experience;
    }

    public int getMaxExperience() {
        return this.getPetBase().getLastMaxExperience(this.level);
    }

    public int getEnergy() {
        return energy;
    }

    public int getMaxEnergy() {
        return this.getPetBase().getMaxEnergy(this.level);
    }

    public int getHappiness() {
        return happiness;
    }

    public int getMaxHappiness() {
        return this.getPetBase().getMaxHappiness(this.level);
    }

    public int getRespects() {
        return respects;
    }

    public int getCreateTimestamp() {
        return createTimestamp;
    }

    public List<Integer> getAvaliableCommands() {
        return this.getPetBase().getCommandsForLevel(this.level);
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setExtradata(String extradata) {
        this.extradata = extradata;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public void setHappiness(int happiness) {
        this.happiness = happiness;
    }

    public void setRespects(int respects) {
        this.respects = respects;
    }

    public int getRaceId(){
        return this.raceId == -1 ? this.getPetBase().getRaceId() : this.raceId;
    }

    public void setRaceId(int raceId) {
        this.raceId = raceId;
    }

    public int getRealRaceId() {
        return this.raceId;
    }
}
