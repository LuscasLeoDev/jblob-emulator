package com.luscastudio.jblob.server.communication.handlers.player.profile.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

import java.util.Collection;

/**
 * Created by Lucas on 14/02/2017 at 16:55.
 */
public class UserTagsComposer extends MessageComposer {
    @Override
    public String id() {
        return "UserTagsMessageComposer";
    }

    public UserTagsComposer(int userId, Collection<String> tags) {
        message.putInt(userId);

        message.putInt(tags.size());
        for (String tag : tags) {
            message.putString(tag);
        }
    }
}
