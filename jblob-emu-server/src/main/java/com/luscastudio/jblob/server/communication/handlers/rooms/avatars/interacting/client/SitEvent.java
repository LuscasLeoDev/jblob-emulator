package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.client;

import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.server.SleepComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 12/12/2016.
 */

public class SitEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int unknown = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        RoomPlayerAvatar avatar = room.getRoomAvatarService().getRoomAvatarByUserId(session.getAvatar().getId());
        if(avatar == null)
            return;

        if(!avatar.getStatusses().hasStatus("sit")) {
            avatar.getStatusses().set("sit", NumberHelper.doubleToString(0.5));
        }
        avatar.setIdle(false);

        room.sendMessage(new SleepComposer(avatar.getVirtualId(), false));
        avatar.updateNeeded(true);

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
