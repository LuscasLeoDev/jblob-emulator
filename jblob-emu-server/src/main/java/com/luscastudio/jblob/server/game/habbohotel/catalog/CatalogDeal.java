package com.luscastudio.jblob.server.game.habbohotel.catalog;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 28/10/2016.
 */

public class CatalogDeal {

    private int id;
    private String name;
    private Map<Integer, CatalogDealItem> items;

    private int pageId;

    private int creditsCost;

    private int currencyCost;

    private int currencyId;

    private boolean offerEnabled;

    private boolean allowsGift;
    //Deprecated
    private boolean isForRent;

    private int clubLevel;

    String customData;
    private int offerId;

    public CatalogDeal(int id, String name, int pageId, int creditsCost, int currencyCost, int currencyId, boolean offerEnabled, boolean allowsGift, boolean isForRent, int clubLevel, String custom, int offerId){

        this.id = id;
        this.name = name;

        this.pageId = pageId;

        this.creditsCost = creditsCost;

        this.currencyCost = currencyCost;
        this.currencyId = currencyId;

        this.offerEnabled = offerEnabled;
        this.allowsGift = allowsGift;
        this.isForRent = isForRent;

        this.clubLevel = clubLevel;
        this.customData = custom;
        this.offerId = offerId;
    }

    protected String getCustomData() {
        return customData;
    }

    public void setItems(Map<Integer, CatalogDealItem> items) {
        this.items = items;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Map<Integer, CatalogDealItem> getItems() {
        return items;
    }

    public int getPageId() {
        return pageId;
    }

    public int getCreditsCost() {
        return creditsCost;
    }

    public int getCurrencyCost() {
        return currencyCost;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public boolean isOfferEnabled() {
        return offerEnabled;
    }

    public boolean isAllowsGift() {
        return allowsGift;
    }

    public boolean isForRent() {
        return isForRent;
    }

    public int getClubLevel() {
        return clubLevel;
    }

    public void parsePacket(PacketWriting message){
        message.putInt(this.getId());
        message.putString(this.getName());
        message.putBool(this.isForRent());

        message.putInt(this.getCreditsCost());
        message.putInt(this.getCurrencyCost());
        message.putInt(this.getCurrencyId());

        message.putBool(this.isAllowsGift());

        //region Writing Deal Items
        message.putInt(this.getItems().size());

        for (CatalogDealItem item : this.getItems().values()) {

            message.putString(item.getType());

            item.parsePageComposer(message);
        }

        message.putInt(this.getClubLevel());
        message.putBool(this.isOfferEnabled());
    }

    public boolean tryPurchase(HabboAvatar avatar, String pageExtradata, int amount, List<MessageComposer> resultComposers) {
        //Step 1: Give the items
        List<Object> items = BCollect.newList();
        Map<Class<? extends CatalogDealItem>, CatalogDealItem> usedDeals = BCollect.newMap();

        //int inputAmount = amount;
        boolean someSuccess = false;
        for (CatalogDealItem dealItem : this.getItems().values()) {

            if(dealItem.generateValue(avatar, pageExtradata, items, amount))
                someSuccess = true;

            if(!usedDeals.containsKey(dealItem.getClass()))
                usedDeals.put(dealItem.getClass(), dealItem);
        }

        if(!someSuccess)
            return false;

        for(CatalogDealItem item : usedDeals.values()){
            item.handleValues(avatar, items);
        }

        //Step 2: Take the Money
        avatar.getBalance().decrease(this.getCreditsCost());
        if(this.getCurrencyCost() != 0)
            avatar.getBalance().decrease(this.getCurrencyId(), this.getCurrencyCost());

        Iterator<Object> iterator = items.iterator();
        while(iterator.hasNext()){
            Object obj = iterator.next();
            if(obj instanceof MessageComposer){
                resultComposers.add((MessageComposer)obj);
                iterator.remove();
            }
        }

        return true;
    }

    public int getOfferId() {
        return offerId;
    }
}
