package com.luscastudio.jblob.server.communication.handlers.navigator.newnav.server;

import com.luscastudio.jblob.server.communication.handlers.navigator.RoomSerializer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.navigator.search.NavigatorCategorySearchResult;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;

import java.util.List;

/**
 * Created by Lucas on 02/10/2016.
 */

public class NewNavigatorSearchResultComposer extends MessageComposer {
    @Override
    public String id() {
        return "NavigatorSearchResultSetMessageComposer";
    }

    public NewNavigatorSearchResultComposer(String category, String junk, List<NavigatorCategorySearchResult> results) {

        message.putString(category);
        message.putString(junk);

        message.putInt(results.size()); //Result count

        for (NavigatorCategorySearchResult result : results) {

            message.putString(result.getCategory().getIdentifier());
            message.putString(result.getCategory().getName());
            message.putInt(result.getExpandStatus()); //0 | 1 | 2
            message.putBool(result.isCollapsed());//Collapsed
            message.putInt(result.getShowType()); //0 -> tiny | 1 -> thumbnail

            message.putInt(result.getRooms().size()); //Room count


            for (RoomProperties room : result.getRooms()) {

                //region Room Data

                RoomSerializer.serializeRoomData1(message, room);

                //endregion
            }
        }


    }
}
