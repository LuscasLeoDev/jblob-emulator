package com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomForwardComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.IChatCommand;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 12/12/2016.
 */

public class UnloadRoomCmd implements IChatCommand {

    private boolean reload;

    public UnloadRoomCmd(boolean reload){
        this.reload = reload;
    }

    public UnloadRoomCmd(){
        this(false);
    }

    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {

        JBlob.getGame().getRoomManager().unloadRoom(room.getProperties().getId());

        if(reload)
            session.sendMessage(new RoomForwardComposer(room.getProperties().getId()));

        return true;
    }
}
