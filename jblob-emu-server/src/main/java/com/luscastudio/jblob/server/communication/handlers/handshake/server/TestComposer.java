package com.luscastudio.jblob.server.communication.handlers.handshake.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 20/01/2017 at 02:09.
 */
public class TestComposer extends MessageComposer {
    @Override
    public String id() {
        return "GnomeBoxMessageComposer";
    }

    public TestComposer(){
        message.putInt(2296);

        message.putInt(0);
        message.putInt(0);

        message.putString("");


        message.putInt(0);
        message.putInt(0);
    }
}
