package com.luscastudio.jblob.server.communication.handlers.catalog.server;

/**
 * Created by Lucas on 07/12/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;

public class PurchaseErrorComposer extends MessageComposer {
    @Override
    public String id() {
        return "PurchaseErrorMessageComposer";
    }

    public PurchaseErrorComposer(int errorId) {
        message.putInt(errorId);
    }
}
