package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.engine.IObject2DPosition;
import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.api.utils.time.DateTimeUtils;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.server.RoomUsersComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.client.FlatAccessibleComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.*;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.events.list.*;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.AvatarObjectPosition;
import com.luscastudio.jblob.server.game.habbohotel.rooms.IAvatarObjectPosition;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomEnterErrorFuture;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.process.RunProcess;
import com.luscastudio.jblob.server.game.permissions.PermissionManagerPermissions;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * Created by Lucas on 04/10/2016.
 */

public class RoomAvatarService {

    private Room room;

    private Map<Integer, RoomPlayerAvatar> roomPlayerAvatars;
    private List<IRoomAvatar> avatarsEnteredToRoom;

    private Map<Integer, IRoomAvatar> bots;
    private Map<Integer, IRoomPetAvatar> pets;

    private Map<Integer, IRoomAvatar> avatarsByVirtualId;

    private Map<Integer, PlayerSession> sessionsInRoom;

    private Map<Integer, IRoomAvatar> avatarsToSave;

    private Map<String, HabboAvatar> doorBellMap;

    private int usersIndex;

    //Debug
    private boolean pause;

    public void setPause(boolean pause) {
        this.pause = pause;
    }

    public boolean isPause() {
        return pause;
    }

    public RoomAvatarService(Room room) {
        this.room = room;

        this.usersIndex = 0;
        this.roomPlayerAvatars = BCollect.newConcurrentMap();
        this.avatarsEnteredToRoom = BCollect.newList();
        this.sessionsInRoom = BCollect.newConcurrentMap();

        this.bots = BCollect.newConcurrentMap();
        this.pets = BCollect.newConcurrentMap();

        this.avatarsToSave = BCollect.newConcurrentMap();

        this.avatarsByVirtualId = BCollect.newMap();

        this.doorBellMap = BCollect.newLinkedMap();
    }

    public void init(){
        Map<PetData, IAvatarObjectPosition> pets = JBlob.getGame().getPetManager().getPetsDataForRoom(this.room.getProperties().getId());

        for(Map.Entry<PetData, IAvatarObjectPosition> petData : pets.entrySet()){
            try {
                IRoomPetAvatar petAvatar = JBlob.getGame().getPetAvatarHandler().generateRoomPetAvatar(petData.getKey().getPetBase().getPetBehavior(), this.room, ++this.usersIndex, petData.getKey());

                if(petAvatar != null) {
                    this.addPet(petAvatar, petData.getValue());
                    petAvatar.onEnter();
                }
            }catch (Exception e){
                BLogger.error(e, this.getClass());
            }
        }

        this.room.getRoomProcess().enqueue(new RunProcess(this::onCycle, 0, 500));
    }

    public int getUsersNow() {
        return roomPlayerAvatars.size();
    }

    public Set<Map.Entry<Integer, PlayerSession>> getSessions(){
        return this.sessionsInRoom.entrySet();
    }

    public Collection<RoomPlayerAvatar> getPlayersAvatars() {
        return this.roomPlayerAvatars.values();
    }

    public Collection<IRoomAvatar> getAllAvatarsList() {
        return avatarsByVirtualId.values();
    }

    public void saveAvatar(IRoomAvatar avatar){
        if(!this.avatarsToSave.containsKey(avatar.getVirtualId()))
            this.avatarsToSave.put(avatar.getVirtualId(), avatar);
    }

    //region Managing Room Pets

    public IRoomPetAvatar addPet(PetData petData, IAvatarObjectPosition p) {

        try {
            IRoomPetAvatar petAvatar = JBlob.getGame().getPetAvatarHandler().generateRoomPetAvatar(this.room, ++this.usersIndex, petData);
            this.addPet(petAvatar, p);
            return petAvatar;
        } catch (NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addPet(IRoomPetAvatar petAvatar, IAvatarObjectPosition p) {
        this.avatarsByVirtualId.put(petAvatar.getVirtualId(), petAvatar);
        this.pets.put(petAvatar.getPetData().getId(), petAvatar);

        if(!this.room.getRoomMap().isValidSquare(p.getPoint()))
            p = new AvatarObjectPosition(0,0, 0, 0, 0);

        this.room.getRoomMap().addAvatarToMap(petAvatar, p.getPoint());

        petAvatar.getPosition().setPosition(p.getX(), p.getY(), room.getRoomMap().getSquareDynamicHeight(p.getX(), p.getY()));
        petAvatar.setSetPosition(p.getX(), p.getY(), room.getRoomMap().getSquareDynamicHeight(p.getX(), p.getY()));
        petAvatar.getPosition().setHeadRotation(p.getRotation());
        petAvatar.getPosition().setRotation(p.getRotation());
    }

    public IRoomPetAvatar getPet(int petId) {
        return pets.get(petId);
    }

    public synchronized IRoomPetAvatar removePet(int petId) {
        synchronized (this.avatarsByVirtualId) {
            IRoomPetAvatar pet = this.pets.remove(petId);
            if(pet == null)
                return null;

            this.avatarsByVirtualId.remove(pet.getVirtualId());

            this.room.getRoomMap().removeAvatarFromMap(pet, pet.getSetPosition().getPoint());

            this.avatarsToSave.remove(pet.getVirtualId());

            pet.getEventHandler().fireEvent("avatar.leave.room");

            return pet;
        }
    }

    //endregion

    //region Managing Room Player

    public RoomEnterErrorFuture tryAddPlayerAvatar(HabboAvatar avatar) {
        return tryAddPlayerAvatar(avatar, this.room.getProperties().getModel().getDoorPosition(), this.room.getRoomMap().getSquareDynamicHeight(this.room.getProperties().getModel().getDoorPosition()));
    }

    public RoomEnterErrorFuture tryAddPlayerAvatar(HabboAvatar avatar, IObject2DPosition pos) {
        return this.tryAddPlayerAvatar(avatar, pos, this.room.getRoomMap().getSquareDynamicHeight(this.room.getProperties().getModel().getDoorPosition()));
    }

    public RoomEnterErrorFuture tryAddPlayerAvatar(HabboAvatar avatar, IObject2DPosition pos, double z) {

        if (getRoomAvatarByUserId(avatar.getId()) != null) {
            this.removePlayerAvatar(avatar.getId());
            return new RoomEnterErrorFuture(RoomEnterErrorAlertComposer.ERROR_UNKNOWN, "");
            //todo: log duplicated Player at room
        }

        this.usersIndex++;

        avatar.setCurrentRoomId(this.room.getProperties().getId());
        avatar.getFriendship().onUpdate();

        RoomPlayerAvatar roomUser = new RoomPlayerAvatar(avatar, room, usersIndex);

        roomUser.setPosition(pos.getX(), pos.getY(), z);
        roomUser.setSetPosition(pos.getX(), pos.getY(), z);
        roomUser.getPosition().setRotation(pos.getRotation());
        roomUser.getPosition().setHeadRotation(pos.getRotation());

        this.room.getEventHandler().fireEvent("room.player.avatar.enter", new PlayerAvatarEnterRoomEventArgs(avatar, roomUser));

        roomUser.updateNeeded(true);

        this.avatarsByVirtualId.put(usersIndex, roomUser);
        this.roomPlayerAvatars.put(avatar.getId(), roomUser);
        this.sessionsInRoom.put(avatar.getId(), avatar.getSession());

        //avatar.getFriendship().onUpdate();


        if(this.room.getProperties().avatarHasRight(roomUser.getAvatar().getId(), PermissionManagerPermissions.ROOM_OWNER_RIGHT)) {
            roomUser.getStatusses().set("flatctrl", "useradmin");
        } else {
            roomUser.getStatusses().set("flatctrl", String.valueOf(this.room.getProperties().getAvatarRank(roomUser.getId())));
        }

        return new RoomEnterErrorFuture();
    }

    public RoomPlayerAvatar getRoomAvatarByUserId(int id) {
        if (roomPlayerAvatars.containsKey(id))
            return roomPlayerAvatars.get(id);

        return null;
    }

    public void removeSession(int id){
        if(this.sessionsInRoom.containsKey(id))
            this.sessionsInRoom.remove(id);
    }

    public void removePlayerAvatar(HabboAvatar avatar) {
        avatar.setCurrentRoomId(0);
        removePlayerAvatar(avatar.getId());
    }

    public void removePlayerAvatar(int id, boolean removeSession) {

        RoomPlayerAvatar avatar = null;

        /*if(this.roomAvatarsByPlayerId.containsKey(id))
            avatar = this.roomAvatarsByPlayerId.cancel(id);*/

        if (this.roomPlayerAvatars.containsKey(id))
            avatar = this.roomPlayerAvatars.remove(id);

        if (avatar != null) {
            removeUserFromByVirtualId(avatar.getVirtualId());
            room.sendMessage(new UserRemoveComposer(avatar.getVirtualId()));
            if(removeSession && this.sessionsInRoom.containsKey(id))
                this.sessionsInRoom.remove(id);

            this.room.getRoomMap().removeAvatarFromMap(avatar, (Point)avatar.getSetPosition());

            avatar.getEventHandler().fireEvent("avatar.leave.room", new AvatarLeaveRoomEventArgs());
            this.room.getEventHandler().fireEvent("avatar.leave.room", new AvatarLeaveRoomEventArgs(avatar));

            avatar.dispose();
        }

    }

    public void removePlayerAvatar(int id) {
        this.removePlayerAvatar(id, true);
    }

    private void removeUserFromByVirtualId(int id) {
        if (this.avatarsByVirtualId.containsKey(id))
            this.avatarsByVirtualId.remove(id);
    }

    private boolean serializeAvatarsStatuses() {

        List<IRoomAvatar> avatars = BCollect.newList();

        for (IRoomAvatar avatar : getAllAvatarsList()) {
            if (avatar.updateNeeded())
                avatars.add(avatar);

            avatar.updateNeeded(false);
        }

        if (avatars.size() == 0)
            return false;

        room.sendQueueMessage(new UserUpdateComposer(avatars));
        return true;
    }

    public void addAvatarEnteringPacketQueue(IRoomAvatar avatar) {
        avatarsEnteredToRoom.add(avatar);
    }

    public void removeAllPlayers() {
        //todo: improve this shit
        for (RoomPlayerAvatar avatar : BCollect.newList(roomPlayerAvatars.values())) {
            avatar.getEventHandler().fireEvent("avatar.leave.room", new AvatarLeaveRoomEventArgs());
            room.getEventHandler().fireEvent("avatar.leave.room", new AvatarLeaveRoomEventArgs(avatar));
            this.roomPlayerAvatars.remove(avatar.getId());
            this.avatarsByVirtualId.remove(avatar.getVirtualId());
            avatar.getAvatar().setCurrentRoomId(0);
        }

    }

    //endregion

    private synchronized void onCycle() {
        if(pause)
            return;
        try {
             {
                for (IRoomAvatar avatar : this.avatarsByVirtualId.values()) {

                    if (avatar.getPath().isRecall()) {
                        if (avatar.getPath().isWalking()) {

                            avatar.getPath().setWalking(false);
                            avatar.setPosition(avatar.getSetPosition().getX(), avatar.getSetPosition().getY(), avatar.getSetPosition().getZ());
                            avatar.getStatusses().remove("mv");
                            avatar.updateNeeded(true);

                            this.handleOnRoomUserStandsOn(avatar, avatar.getSetPosition().getPoint());
                        }

                        List<Point> reversePath = room.getRoomMap().getPath(avatar.getSetPosition().getPoint(), avatar.getPath().getGoal(), avatar.getPath().getValidator(), 256);

                        if (reversePath == null) {
                            avatar.getPath().recall(false);
                            avatar.getPath().clearPath();

                            avatar.getEventHandler().fireEvent("path.create.fail", null);
                            continue;
                        }

                        avatar.getEventHandler().fireEvent("path.create.success", null);

                        List<Point> path = BCollect.newList();
                        for (int i = reversePath.size() - 2; i >= 0; i--) {
                            path.add(reversePath.get(i));
                        }


                        avatar.getPath().setPath(path);
                        avatar.getPath().recall(false);
                    }

                    if (avatar.getPath().hasNex()) {

                        //Gets the new point to walk in the path
                        Point p = avatar.getPath().next();
                        avatar.getStatusses().remove("mv");
                        avatar.getStatusses().remove("sit");
                        avatar.getStatusses().remove("lay");

                        if (!avatar.getPath().getValidator().validate(avatar.getSetPosition().getPoint(), p)) {
                            avatar.getPath().setWalking(false);
                            avatar.setPosition(avatar.getSetPosition().getX(), avatar.getSetPosition().getY(), avatar.getSetPosition().getZ());
                            //avatar.getPath().clearPath();
                            avatar.updateNeeded(true);
                            avatar.getPath().recall(true);
                            avatar.getEventHandler().fireEvent("avatar.path.blocked", new AvatarPathBlockedEventArgs(avatar.getPosition().getPoint()));
                            continue;
                        }

                        if (avatar.getPath().isWalking())
                            this.handleOnRoomUserStandsOn(avatar, avatar.getSetPosition().getPoint());

                        //Sinalize that the avatar is walking
                        avatar.getPath().setWalking(true);

                        this.handleOnRoomUserWalksOff(avatar, avatar.getSetPosition().getPoint());


                        //Set the previous position to the point 1
                        avatar.setPosition(avatar.getSetPosition().getX(), avatar.getSetPosition().getY(), avatar.getSetPosition().getZ());
                        avatar.getPosition().setDirection(avatar.getPosition().getPoint().getRotation(p), false);


                        //Set the new position to the point 2
                        avatar.setSetPosition(p.getX(), p.getY(), room.getRoomMap().getSquareDynamicHeight(p.getX(), p.getY()));

                        this.room.getRoomMap().updateAvatarCoordinate(avatar, (Point) avatar.getPosition(), (Point) avatar.getSetPosition());

                        this.room.getEventHandler().fireEvent("room.avatar.walk.on.square", new AvatarWalkOnSquareEventArgs(avatar, avatar.getSetPosition().getPoint()));

                        avatar.getEventHandler().fireEvent("avatar.walk.on.square", new AvatarWalkOnSquareEventArgs(avatar.getSetPosition().getPoint()));


                        this.handleOnRoomUserWalksOn(avatar, avatar.getSetPosition().getPoint());

                        //Add EventArgs status to make the walking effect
                        avatar.getStatusses().set("mv", avatar.getSetPosition().getX() + "," + avatar.getSetPosition().getY() + "," + NumberHelper.doubleToString(avatar.getSetPosition().getZ() + avatar.getAditionalHeight()));

                        avatar.updateNeeded(true);
                    } else if (avatar.getPath().isWalking()) {
                        avatar.getPosition().setDirection(avatar.getPosition().getPoint().getRotation(avatar.getSetPosition().getPoint()), false);
                        avatar.setPosition(avatar.getSetPosition().getX(), avatar.getSetPosition().getY(), avatar.getSetPosition().getZ());

                        EventArgs args = this.room.getEventHandler().fireEvent("room.avatar.walking.end", new AvatarWalkingEndEventArgs(avatar, avatar.getPosition()));

                        if (!args.isCancelled()) {
                            avatar.getPath().setWalking(false);
                            avatar.getStatusses().remove("mv");
                            avatar.updateNeeded(true);
                        }

                        this.handleOnRoomUserStandsOn(avatar, avatar.getSetPosition().getPoint());
                    }

                    avatar.getStatusses().getStatuses().values().removeIf(status -> {
                        return status.getExpire() > 0 && status.getExpire() <= DateTimeUtils.getUnixTimestampInt();
                    });

                /*Iterator<RoomAvatarStatus> iterator = avatar.getStatusses().getStatuses().values().iterator();
                while (iterator.hasNext()) {
                    RoomAvatarStatus status = iterator.next();
                    if (status.getExpire() > 0 && status.getExpire() <= DateTimeUtils.getUnixTimestampInt())
                        iterator.remove();
                }*/

                }

                if (this.flushUsersAtRoom() || this.serializeAvatarsStatuses())
                    this.room.flush();
            }
        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }
    }

    private boolean flushUsersAtRoom() {
        if(avatarsEnteredToRoom.size() == 0)
            return false;

        for (IRoomAvatar avatar : avatarsEnteredToRoom) {
            if(avatar == null)
                continue;
            room.sendQueueMessage(new RoomUsersComposer(avatar));
            room.sendQueueMessage(new UserUpdateComposer(avatar));
            room.getEventHandler().fireEvent("avatar.enter.room", new AvatarEnterRoomEventArgs(avatar));
        }

        avatarsEnteredToRoom.clear();
        return true;
    }

    //region Handling user movements

    private void handleOnRoomUserWalksOn(IRoomAvatar avatar, Point p) {

        //todo: make wired system and set it up here or not..

        IRoomItem item = this.room.getRoomMap().getHighestRoomItemBySq(p.getX(), p.getY());
        if (item != null)
            item.onUserWalkOn(avatar);
    }

    private void handleOnRoomUserStandsOn(IRoomAvatar avatar, Point p) {
        IRoomItem item = this.room.getRoomMap().getHighestRoomItemBySq(p.getX(), p.getY());
        if (item != null) {

            if (item.getBase().isCanSitOn() && !avatar.getPath().isWalking()) {
                avatar.getPosition().setDirection(item.getPosition().getRotation(), false);
                //avatar.getRotation().setPosition(avatar.getRotation().getX(), avatar.getRotation().getY(), );
                avatar.getStatusses().set("sit", NumberHelper.doubleToString(item.getHeight()));
            }

            item.onUserWalkStand(avatar);
        }
        this.room.getEventHandler().fireEvent("room.avatar.stand.on.square", new AvatarStandOnSquareEventArgs(avatar, new Point(p)));
        avatar.getEventHandler().fireEvent("avatar.stand.on.square", new AvatarStandOnSquareEventArgs(new Point(p)));
    }

    private void handleOnRoomUserWalksOff(IRoomAvatar avatar, Point p) {
        IRoomItem item = this.room.getRoomMap().getHighestRoomItemBySq(p.getX(), p.getY());
        if (item != null)
            item.onUserWalkOff(avatar);
    }

    public synchronized void dispose() {

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {
            this.avatarsToSave.forEach((integer, avatar) -> {
                avatar.save(reactor);
            });
        }catch (Exception e){
            BLogger.error(e, this.getClass());
        }
        this.avatarsByVirtualId.forEach((integer, avatar) -> {avatar.dispose();});

        this.sessionsInRoom.clear();
        this.roomPlayerAvatars.clear();
        this.bots.clear();
        this.pets.clear();
        this.avatarsEnteredToRoom.clear();
        this.doorBellMap.clear();
    }

    public Collection<IRoomPetAvatar> getPets() {
        return pets.values();
    }

    //endregion

    //region Handling Door Bell

    public boolean addDoorBellAvatar(String name, HabboAvatar avatar){
        boolean hasAvatarsWithRight = true;
        for (RoomPlayerAvatar roomAvatar : this.roomPlayerAvatars.values()) {
            if(this.room.getProperties().avatarHasRight(roomAvatar.getAvatar().getId(), "room_right_manage_doorbell")){
                hasAvatarsWithRight = true;
                break;
            }
        }

        if(!hasAvatarsWithRight)
            return false;

        if(this.doorBellMap.containsKey(name))
            return false;

        this.doorBellMap.put(name, avatar);

        avatar.getEventHandler().on("session.disconnect avatar.room.leave avatar.room.flat.change", (delegate, eventArgs) -> {
            this.doorBellMap.remove(name);
            this.performDeniedAvatarFromDoorBell(name);
            delegate.cancel();
        });

        return true;
    }

    public Collection<String> getDoorBellQueue(){
        return this.doorBellMap.keySet();
    }

    public void performAddAvatarToDoorBell(String name){
        for (RoomPlayerAvatar avatar : this.roomPlayerAvatars.values()) {
            if(this.room.getProperties().avatarHasRight(avatar.getAvatar().getId(), "room_right_manage_doorbell")){
                avatar.getAvatar().getSession().sendMessage(new DoorbellComposer(name));
            }
        }
    }

    public void performDeniedAvatarFromDoorBell(String name){
        for (RoomPlayerAvatar avatar : this.roomPlayerAvatars.values()) {
            if(this.room.getProperties().avatarHasRight(avatar.getAvatar().getId(), "room_right_manage_doorbell")){
                avatar.getAvatar().getSession().sendMessage(new FlatAccessDeniedComposer(name));
            }
        }
    }

    public void performAcceptedAvatarFromDoorBell(String name){
        for (RoomPlayerAvatar avatar : this.roomPlayerAvatars.values()) {
            if(this.room.getProperties().avatarHasRight(avatar.getAvatar().getId(), "room_right_manage_doorbell")){
                avatar.getAvatar().getSession().sendMessage(new FlatAccessibleComposer(name));
            }
        }
    }

    public HabboAvatar removeDoorBellAvatar(String name){
        return this.doorBellMap.remove(name);
    }

    //endregion

}
