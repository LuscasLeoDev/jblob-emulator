package com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list;

import com.luscastudio.jblob.server.communication.handlers.notification.server.BroadcastMessageAlertComposer;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.RoomNotificationComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.IChatCommand;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 20/02/2017 at 19:21.
 */
public class HotelAlertCmd implements IChatCommand {
    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {
        if(params.length() <= 1)
            return true;

        session.sendMessage(new RoomNotificationComposer("alert", "${generic.alert.title}", params.get(1, true)));
        return true;
    }
}
