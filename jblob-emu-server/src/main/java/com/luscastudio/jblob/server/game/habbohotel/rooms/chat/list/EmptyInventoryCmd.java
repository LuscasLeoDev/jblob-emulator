package com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryUpdateRequestComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.game.habbohotel.inventory.AvatarInventory;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.IChatCommand;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 21/01/2017 at 00:14.
 */
public class EmptyInventoryCmd implements IChatCommand {
    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {

        session.getAvatar().getInventory().clearRoomItems();

        session.sendMessage(new InventoryUpdateRequestComposer());

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare = reactor.prepare("DELETE FROM avatars_items_data WHERE user_id = ? AND room_id = ?");
            prepare.setInt(1, session.getAvatar().getId());
            prepare.setInt(2, AvatarInventory.ITEM_NOROOM_ID);
            prepare.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
