package com.luscastudio.jblob.server.game.players.cache;

import com.luscastudio.jblob.server.game.habbohotel.inventory.AvatarInventory;

/**
 * Created by Lucas on 02/10/2016.
 */
public class AvatarDataCache implements IAvatarDataCache {

    private int id;
    private String username;
    private String figureString;
    private String gender;
    private String motto;
    private int lastOnline;
    private int rank;


    AvatarInventory inventory;

    //For Bots os Unknowns profiles
    public AvatarDataCache(int id, String username, String figureString, String motto, int lastOnline, String gender) {
        this(id, username, figureString, motto, lastOnline, gender, 0);
    }

    public AvatarDataCache(int id, String username, String figureString, String motto, int lastOnline, String gender, int rank) {
        this.id = id;
        this.username = username;
        this.figureString = figureString;
        this.gender = gender;
        this.motto = motto;
        this.lastOnline = lastOnline;
        this.rank = rank;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getFigureString() {
        return figureString;
    }

    @Override
    public String getMotto() {
        return motto;
    }

    @Override
    public int getLastOnlineTimestamp() {
        return lastOnline;
    }

    @Override
    public String getGender() {
        return gender;
    }

    @Override
    public boolean isInRoom() {
        return false;
    }

    @Override
    public boolean isOnline() {
        return false;
    }

    @Override
    public AvatarInventory getInventory() {

        if(inventory == null){
            this.inventory = new AvatarInventory(getId());
            this.inventory.load();
        }

        return inventory;

    }

    @Override
    public int getRank() {
        return this.rank;
    }
}
