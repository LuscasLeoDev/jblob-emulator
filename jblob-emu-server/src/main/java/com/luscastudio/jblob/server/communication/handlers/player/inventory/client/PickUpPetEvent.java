package com.luscastudio.jblob.server.communication.handlers.player.inventory.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.AddInventoryPetComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.UserRemoveComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import static com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer.PET;

/**
 * Created by Lucas on 31/01/2017 at 14:08.
 */
public class PickUpPetEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int petId = packet.getInt();

        Room room;
        if((room = session.getAvatar().getCurrentRoom()) == null)
            return;

        IRoomPetAvatar petAvatar = room.getRoomAvatarService().getPet(petId);

        if(petAvatar == null)
            return;

        petAvatar.onLeave();

        PetData petData = petAvatar.getPetData();

        room.getRoomAvatarService().removePet(petAvatar.getPetData().getId());

        petAvatar.onRemove(session.getAvatar());

        room.sendMessage(new UserRemoveComposer(petAvatar.getVirtualId()));
        session.getAvatar().getInventory().addPet(petAvatar.getPetData());
        petAvatar.getPetData().setRoomId(0);

        session.sendQueueMessage(new AddInventoryPetComposer(petAvatar.getPetData()));
        session.sendQueueMessage(new FurniListNotificationComposer().add(PET, petData.getId()));


        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){

            petAvatar.save(reactor);
            /*DBConnPrepare prepare = reactor.prepare("UPDATE avatars_pets_data SET x = ?, y = ?, rotation = ?, level = ?, energy = ?, happiness = ?, experience = ?, respects = ?, extradata = ?, custom_data = ?, room_id = ? WHERE id = ?");
            prepare.setInt(1, 0);
            prepare.setInt(2, 0);
            prepare.setInt(3, 0);

            prepare.setInt(4, petData.getLevel());
            prepare.setInt(5, petData.getEnergy());
            prepare.setInt(6, petData.getHappiness());
            prepare.setInt(7, petData.getExperience());
            prepare.setInt(8, petData.getRespects());
            prepare.setString(9, petData.getExtradata());
            prepare.setString(10, petData.getCustom().toDbString());
            prepare.setInt(11, 0);
            prepare.setInt(12, petData.getId());

            prepare.run();*/
        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }


        petAvatar.dispose();
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
