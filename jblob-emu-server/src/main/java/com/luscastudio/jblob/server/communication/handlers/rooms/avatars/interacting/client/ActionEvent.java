package com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.server.ActionComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.avatars.interacting.server.SleepComposer;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.RoomPlayerAvatar;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 12/12/2016.
 */

public class ActionEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        int actionId = packet.getInt();

        Room room = session.getAvatar().getCurrentRoom();
        if(room == null)
            return;

        RoomPlayerAvatar avatar = room.getRoomAvatarService().getRoomAvatarByUserId(session.getAvatar().getId());
        if(avatar == null)
            return;

        if(actionId == 5){
            avatar.setIdle(!avatar.isIdle());
            room.sendMessage(new SleepComposer(avatar.getVirtualId(), avatar.isIdle()));
        } else {
            avatar.setIdle(false);
            room.sendQueueMessage(new SleepComposer(avatar.getVirtualId(), avatar.isIdle()));
            room.sendQueueMessage(new ActionComposer(avatar.getVirtualId(), actionId)).flush();
        }
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
