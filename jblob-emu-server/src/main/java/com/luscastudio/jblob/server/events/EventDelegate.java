//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events;

/**
 * Created by Lucas on 06/03/2017 at 20:47.
 */
public class EventDelegate{
    private boolean cancelled;

    public boolean done(){
        return this.isCancelled();
    }

    public boolean isCancelled(){
        return this.cancelled;
    }

    public void cancel(){
        this.cancelled = true;
    }
}
