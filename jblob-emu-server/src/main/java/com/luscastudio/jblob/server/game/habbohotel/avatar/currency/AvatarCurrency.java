package com.luscastudio.jblob.server.game.habbohotel.avatar.currency;

/**
 * Created by Lucas on 07/12/2016.
 */

public class AvatarCurrency {

    private int currencyId;
    private int value;

    public AvatarCurrency(int currencyId, int value){
        this.currencyId = currencyId;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getCurrencyId() {
        return currencyId;
    }
}
