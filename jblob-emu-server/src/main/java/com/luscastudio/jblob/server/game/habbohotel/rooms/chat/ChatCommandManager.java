package com.luscastudio.jblob.server.game.habbohotel.rooms.chat;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.server.boot.JBlobSettings;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list.*;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Map;

/**
 * Created by Lucas on 11/12/2016.
 */

public class ChatCommandManager implements IFlushable {

    private Map<String, IChatCommand> commands;

    public ChatCommandManager(){
        this.commands = BCollect.newMap();
    }

    public boolean add(String cmd, IChatCommand command){
        if(this.commands.containsKey(cmd))
            return false;

        this.commands.put(cmd, command);
        return true;
    }

    public boolean addOrReplace(String cmd, IChatCommand command){
        if(this.commands.containsKey(cmd)){
            this.commands.replace(cmd, command);
            return true;
        }

        this.commands.put(cmd, command);
        return false;
    }

    public boolean tryRemove(String key){
        return this.remove(key) != null;
    }

    public IChatCommand remove(String key){
        return this.commands.remove(key);
    }

    private void setDefaults() {
        this.add(":about", new AboutCmd());
        this.add(":unload", new UnloadRoomCmd());
        this.add(":reload", new UnloadRoomCmd(true));
        this.add(":pickall", new PickAllCmd());
        this.add(":pickallpets", new PickupPetsCmd());
        this.add(":update", new UpdateCmd());
        this.add(":furnistats", new FurniStatsCmd());
        this.add(":test", new TestCmd());
        this.add(":shutdown", new ShutDownCmd());
        this.add(":maintenance", new MaintenanceAlertCmd());
        this.add(":empty", new EmptyInventoryCmd());
        this.add(":makebundle", new MakeBundleCmd());
        this.add(":makepack", new MakeCatalogDealPackCmd());
        this.add(":makedeals", new MakeCatalogDealsCmd());
        this.add(":currency", new GetCurrencyCmd());
        this.add(":getfurni", new GenerateItemCmd());

        this.add(":link", new OpenClientLinkCmd());
        this.add(":ha", new HotelAlertCmd());
    }

    public boolean handleCommand(PlayerSession session, IRoomAvatar avatar, Room room, String message) {

        message = message.toLowerCase().trim();
        if(message.equals(JBlobSettings.UTIL_EMPTY_STRING))
            return false;

        String[] params = message.split(JBlobSettings.UTIL_CHAR_SPACE);

        if(!this.commands.containsKey(params[0])){
            return false;
        }

        boolean done = this.commands.get(params[0]).parse(session, avatar, room, new CommandParams(params));

        if(!done && JBlobSettings.COMMAND_MANAGER_HANDLER_RETURN_TRUE_IF_COMMAND_FOUND)
            return true;
        else if(done)
            return true;

        return false;
    }

    @Override
    public void flush() throws Exception {
        this.commands.clear();
        this.setDefaults();
    }

    @Override
    public void performFlush() {

    }
}
