package com.luscastudio.jblob.server.communication.handlers.player.inventory.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 31/01/2017 at 12:15.
 */
public class RemoveInventoryPetComposer extends MessageComposer {
    @Override
    public String id() {
        return "RemoveInventoryPetMessageComposer";
    }

    public RemoveInventoryPetComposer(int petId) {
        message.putInt(petId);
    }
}
