//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Lucas on 10/12/2016.
 */

public class EventHandler {

    private Map<String, List<IEventDelegateFuture>> delegateFutureMap;
    private Map<IEventDelegateFuture, EventDelegate> delegateMap;

    public EventHandler(){
        this.delegateFutureMap = BCollect.newConcurrentMap();
        this.delegateMap = BCollect.newConcurrentMap();
    }

    public EventDelegate on(String keys, IEventDelegateFuture eventDelegateFuture){
        EventDelegate delegate = new EventDelegate();
        for(String key : keys.split(" ")){
            this.checkEventKey(key);
            JBlob.getGame().getProcessManager().run(() -> {
            synchronized (this.delegateFutureMap.get(key)) {
                this.delegateFutureMap.get(key).add(eventDelegateFuture);
                this.delegateMap.put(eventDelegateFuture, delegate);
            }}, 1, TimeUnit.MILLISECONDS);
        }

        return delegate;
    }

    public EventArgs fireEvent(String key) {
        return this.fireEvent(key, new EventArgs());
    }

    public EventArgs fireEvent(String key, EventArgs args) {
        if(args == null)
            args = new EventArgs();

        if (!this.delegateFutureMap.containsKey(key))
            return args;

        args.setEventKey(key);
        synchronized (delegateFutureMap.get(key)) {
            Iterator<IEventDelegateFuture> iterator = delegateFutureMap.get(key).iterator();
            while (iterator.hasNext()) {
                IEventDelegateFuture delegateFuture = iterator.next();
                EventDelegate eventDelegate = this.delegateMap.get(delegateFuture);

                if (eventDelegate == null || eventDelegate.done()) {
                    iterator.remove();
                    this.delegateMap.remove(delegateFuture);
                } else {
                    delegateFuture.fire(eventDelegate, args);
                }
            }

            return args;
        }
    }

    private void checkEventKey(String key){
        if(!this.delegateFutureMap.containsKey(key))
            this.delegateFutureMap.put(key, BCollect.newList());
    }


    public void dispose() {
        this.delegateFutureMap.clear();
        this.delegateMap.clear();
    }
}
