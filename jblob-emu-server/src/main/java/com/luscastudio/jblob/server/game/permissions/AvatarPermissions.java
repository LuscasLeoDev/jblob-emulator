package com.luscastudio.jblob.server.game.permissions;

import com.luscastudio.jblob.api.utils.collect.BCollect;

import java.util.Map;

/**
 * Created by Lucas on 12/12/2016.
 */

public class AvatarPermissions {
    private Map<String, PermissionData> permissions;

    public AvatarPermissions(){
        this.permissions = BCollect.newMap();
    }

    public void setPermissions(Map<String, PermissionData> permissions) {
        this.permissions = permissions;
    }

    public boolean hasPermission(String name){
        return this.permissions.containsKey(name);
    }
}
