package com.luscastudio.jblob.server.network.server;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.api.utils.io.Configuration;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.manager.ClientMessageManager;
import com.luscastudio.jblob.server.network.client.ClientConnectionHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.DefaultMessageSizeEstimator;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

import java.net.InetSocketAddress;
import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;


/**
 * Created by Lucas on 13/09/2016.
 */
public class ServerClientManager {

    private ClientConnectionHandler clientHandler;

    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;
    private EventLoopGroup channelsGroup;

    private ServerBootstrap server;
    private List<InetSocketAddress> addressListeners;

    private Map<String, ChannelFuture> channels;

    private Map<InetSocketAddress, ChannelFuture> channelFutureMap;

    private Consumer<InetSocketAddress> onError;
    private Consumer<InetSocketAddress> onConnect;

    public ServerClientManager(Iterable<InetSocketAddress> addresses, Consumer<InetSocketAddress> onError, Consumer<InetSocketAddress> onConnect, ClientConnectionHandler clientConnectionHandler) {

        this.channels = BCollect.newMap();
        this.addressListeners = BCollect.newList(addresses);
        this.channelFutureMap = BCollect.newMap();
        this.onError = onError;
        this.onConnect = onConnect;
        this.clientHandler = clientConnectionHandler;
        this.setUpServer();
    }

    private void setUpServer(){

        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();
        channelsGroup = new NioEventLoopGroup();

        this.server = new ServerBootstrap();
        server.group(bossGroup, workerGroup);
        server.channel(NioServerSocketChannel.class);
        server.childHandler(new ClientConnectionInitializer(channelsGroup, clientHandler));
        server.option(ChannelOption.SO_BACKLOG, 500);
        server.option(ChannelOption.TCP_NODELAY, true);
        server.option(ChannelOption.SO_KEEPALIVE, true);
        server.option(ChannelOption.WRITE_BUFFER_LOW_WATER_MARK, 32 * 1024);
        server.option(ChannelOption.WRITE_BUFFER_HIGH_WATER_MARK, 64 * 1024);
        server.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
        server.option(ChannelOption.MESSAGE_SIZE_ESTIMATOR, DefaultMessageSizeEstimator.DEFAULT);
        server.childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);
        server.childOption(ChannelOption.WRITE_BUFFER_LOW_WATER_MARK, 32 * 1024);
        server.childOption(ChannelOption.WRITE_BUFFER_HIGH_WATER_MARK, 64 * 1024);
    }

    public void connect() {
        try {

            for (InetSocketAddress address : this.addressListeners) {
                ChannelFuture bind = this.server.bind(address);
                bind.addListener(future -> {
                   if(!future.isSuccess())
                       this.onError.accept(address);
                   else
                       this.onConnect.accept(address);
                });
            }


        } catch (Exception e) {
            BLogger.error(e, this.getClass());
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
            channelsGroup.shutdownGracefully();

        }

    }

    public void close() {
        workerGroup.shutdownGracefully();
        bossGroup.shutdownGracefully();
        channelsGroup.shutdownGracefully();
    }
}
