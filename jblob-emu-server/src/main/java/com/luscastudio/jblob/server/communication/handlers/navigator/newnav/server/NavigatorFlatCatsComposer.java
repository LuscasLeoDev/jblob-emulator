package com.luscastudio.jblob.server.communication.handlers.navigator.newnav.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 10/12/2016.
 */

public class NavigatorFlatCatsComposer extends MessageComposer {
    @Override
    public String id() {
        return "NavigatorFlatCatsMessageComposer";
    }

    public NavigatorFlatCatsComposer(){
        message.putInt(2);

        message.putInt(1);
        message.putString("Luscas");
        message.putBool(true);

        message.putInt(2);
        message.putString("Leo");
        message.putBool(true);


    }
}
