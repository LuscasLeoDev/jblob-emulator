package com.luscastudio.jblob.server.communication.handlers.navigator.newnav.client;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.navigator.newnav.server.NewNavigatorSearchResultComposer;
import com.luscastudio.jblob.server.game.habbohotel.navigator.tabs.NavigatorTopLevel;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 02/10/2016.
 */

public class NewNavigatorSearchEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        String topLevelName = packet.getString();
        String searchType = packet.getString();


        ///List<NavigatorCategorySearchResult> results = JBlob.getGame().getNavigatorManager().getResults(session, cat, searchType);
        NavigatorTopLevel topLevel = JBlob.getGame().getNavigatorManager().getTopLevel(topLevelName);

        if(topLevel == null){
            session.sendMessage(new NewNavigatorSearchResultComposer(topLevelName, searchType, BCollect.newList()));
            return;
        }
        session.sendMessage(new NewNavigatorSearchResultComposer(topLevelName, searchType, topLevel.getResults(session, searchType)));


    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
