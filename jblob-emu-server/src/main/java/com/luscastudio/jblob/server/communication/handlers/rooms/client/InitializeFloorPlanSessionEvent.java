package com.luscastudio.jblob.server.communication.handlers.rooms.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 02/02/2017 at 18:01.
 */
public class InitializeFloorPlanSessionEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
