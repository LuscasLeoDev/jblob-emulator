package com.luscastudio.jblob.server.communication.handlers.messenger.server;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.friendship.AvatarFriendshipItem;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;

import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 02/10/2016.
 */
public class FriendListUpdateComposer extends MessageComposer {
    @Override
    public String id() {
        return "FriendListUpdateMessageComposer";
    }

    private Map<Integer, String> categories;
    private List<FriendShipUpdate> updates;

    /*public FriendListUpdateComposer(AvatarFriendship main, IAvatarDataCache avatar, AvatarFriendshipItem friendData, int code) {

        message.putInt(0); //Category Count

        /*message.putInt(1);
        message.putString("Family");

        message.putInt(1); //Update List

        message.putInt(code);

        if (code != -1)
            FriendListUpdater.serializeFriend(message, avatar, friendData.relationType);
        else
            message.putInt(avatar.getId());
    }*/

    public FriendListUpdateComposer(Map<Integer, String> categories){
        this.categories = categories;
        this.updates = BCollect.newList();
    }

    public FriendListUpdateComposer addUpdate(int removeId){
        this.updates.add(new FriendShipUpdate(removeId));
        return this;
    }

    public FriendListUpdateComposer addUpdate(AvatarFriendshipItem friendshipItem, IAvatarDataCache cache, int updateCode){
        this.updates.add(new FriendShipUpdate(cache, friendshipItem, updateCode));
        return this;
    }

    @Override
    public void compose() {

        message.putInt(this.categories.size());

        for (Map.Entry<Integer, String> entry : this.categories.entrySet()) {
            message.putInt(entry.getKey());
            message.putString(entry.getValue());
        }

        message.putInt(this.updates.size());

        for (FriendShipUpdate update : this.updates) {
            message.putInt(update.code);
            if(update.code == -1)
                message.putInt(update.friendId);
            else
                FriendListUpdater.serializeFriend(message, update.avatarDataCache, update.friendshipItem.getRelationType());

            update.dispose();
        }

        this.categories.clear();
        this.updates.clear();

        super.compose();
    }

    public static class FriendShipUpdate {
        private int code;
        private int friendId;
        private AvatarFriendshipItem friendshipItem;
        private IAvatarDataCache avatarDataCache;

        public FriendShipUpdate(int removeId){
            this.friendshipItem = null;
            this.avatarDataCache = null;
            this.friendId = removeId;
            this.code = -1;
        }

        public FriendShipUpdate(IAvatarDataCache avatarDataCache, AvatarFriendshipItem friendshipItem, int updateCode){
            this.avatarDataCache = avatarDataCache;
            this.friendshipItem = friendshipItem;
            this.code = updateCode;
            this.friendId = this.avatarDataCache.getId();
        }

        public void dispose() {
            this.friendshipItem = null;
            this.avatarDataCache = null;
        }
    }
}
