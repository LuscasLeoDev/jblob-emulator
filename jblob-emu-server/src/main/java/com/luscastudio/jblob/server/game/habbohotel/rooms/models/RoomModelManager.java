package com.luscastudio.jblob.server.game.habbohotel.rooms.models;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.util.Map;

/**
 * Created by Lucas on 03/10/2016.
 */

public class RoomModelManager implements IFlushable {

    Logger log = Logger.getLogger(this.getClass());

    private Map<String, RoomModel> models;

    public RoomModelManager() {
        models = BCollect.newMap();

        flush();
    }

    public void flush() {

        models.clear();

        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

            DBConnPrepare prepare = reactor.prepare("SELECT * FROM rooms_models WHERE custom = 0");

            ResultSet set = prepare.runQuery();

            while (set.next()) {

                models.put(set.getString("name"), new RoomModel(set));

            }

        } catch (Exception e) {
            log.error("Error while getting room model data", e);
        }

    }

    @Override
    public void performFlush() {

    }

    public RoomModel getModel(String modelName) {

        if (models.containsKey(modelName))
            return models.get(modelName);

        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

            DBConnPrepare prepare = reactor.prepare("SELECT * FROM rooms_models WHERE name = ?");
            prepare.setString(1, modelName);
            ResultSet set = prepare.runQuery();

            if(set.next()) {
                RoomModel roomModel = new RoomModel(set);
                models.put(roomModel.getName(), roomModel);
                return roomModel;
            }

        } catch (Exception e) {
            log.error("Error while getting room model data '" + modelName + "'", e);
        }

        return null;
    }

    public void updateModel(String modelName, RoomModel model){
        RoomModel oldModel = this.models.remove(modelName);
        if(oldModel != null)
            oldModel.dispose();

        models.put(modelName, model);

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare = reactor.prepare("REPLACE INTO rooms_models (name, floor_data, wall_height, door_x, door_y, door_rotation, custom) VALUES (?, ?, ?, ?, ?, ?, ?)");

            prepare.setString(1, model.getName());
            prepare.setString(2, model.getModelString());
            prepare.setInt(3, model.getWallHeight());
            prepare.setInt(4, model.getDoorPosition().getX());
            prepare.setInt(5, model.getDoorPosition().getY());
            prepare.setInt(6, model.getDoorPosition().getRotation());
            prepare.setInt(7, model.isCustom() ? 1 : 0);

            prepare.run();

        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }
    }
}
