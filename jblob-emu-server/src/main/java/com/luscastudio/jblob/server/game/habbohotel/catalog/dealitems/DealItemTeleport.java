package com.luscastudio.jblob.server.game.habbohotel.catalog.dealitems;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;

import java.util.List;

import static com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer.FURNI;

/**
 * Created by Lucas on 22/12/2016 at 21:18.
 */
public class DealItemTeleport extends DealItemFurni {
    public DealItemTeleport(int id, int dealId, int baseId, String extradata, int limitedAmount, int amount, String type, boolean isRare) {
        super(id, dealId, baseId, extradata, limitedAmount, amount, type, isRare);
    }

    @Override
    public boolean generateValue(HabboAvatar avatar, String pageExtradata, List<Object> outValues, int amount) {

        List<FurniProperties> teleporItems = JBlob.getGame().getRoomItemFactory().createFurniPropertiesList(this.getBase(), avatar.getId(), "", 0, this.isRare(), amount * 2 * this.getAmount());

        if(amount > 0) {

            StringBuilder builder = new StringBuilder("INSERT INTO teleports_links (tele_one_id, tele_two_id) VALUES ");
            int i = 0;
            boolean first = true;
            FurniProperties lastProp = null;
            for (FurniProperties prop : teleporItems) {
                if (lastProp == null) {
                    lastProp = prop;
                    continue;
                }

                builder.append((!first ? "," : "") + "(?, ?), (?, ?)");
                first = false;
            }

            lastProp = null;
            i = 0;
            for (FurniProperties prop : teleporItems) {
                if (lastProp == null) {
                    lastProp = prop;
                    continue;
                }
                try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {
                    DBConnPrepare prepare = reactor.prepare(builder.toString(), true);
                    prepare.setInt(i * 4 + 1, lastProp.getId());
                    prepare.setInt(i * 4 + 2, prop.getId());

                    prepare.setInt(i * 4 + 3, prop.getId());
                    prepare.setInt(i * 4 + 4, lastProp.getId());

                    prepare.runInsert();
                }catch (Exception e){
                    BLogger.error(e, this.getClass());
                }
            }
        }

        FurniListNotificationComposer composer = new FurniListNotificationComposer();
        for(FurniProperties item : teleporItems)
            composer.add(FURNI, item.getId());

        avatar.getSession().sendQueueMessage(composer);

        outValues.addAll(teleporItems);
        return true;
    }

    @Override
    public void handleValues(HabboAvatar avatar, List<Object> values) {
        super.handleValues(avatar, values);
    }
}
