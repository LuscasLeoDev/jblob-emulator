package com.luscastudio.jblob.server.game.habbohotel.rooms.engine.validator;

import com.luscastudio.jblob.api.utils.engine.Point;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;

/**
 * Created by Lucas on 09/10/2016.
 */

public class SimpleAvatarPathValidator implements IFloorValidator {

    Room room;
    IRoomAvatar avatar;

    public SimpleAvatarPathValidator(Room room, IRoomAvatar avatar) {
        this.room = room;
        this.avatar = avatar;
    }

    @Override
    public boolean validate(Point from, Point to) {
        if (!room.getRoomMap().isValidSquare(to.getX(), to.getY()))
            return false;

        int fromHeight = (int) room.getRoomMap().getSquareDynamicHeight(from.getX(), from.getY());
        int toHeight = (int) room.getRoomMap().getSquareDynamicHeight(to.getX(), to.getY());

        int diff = Math.abs(fromHeight - toHeight);

        if (diff > 1.5 && fromHeight < toHeight) {
            return false;
        }

        IRoomItem item = room.getRoomMap().getHighestRoomItemBySq(to.getX(), to.getY());
        if (item != null) {
            if (!item.canWalk(this.avatar, from))
                return false;
        }

        return !(room.getRoomMap().getRoomAvatars(to).size() > 0 && room.getProperties().getAllowWalkTrough() == 0);
    }

    @Override
    public boolean validatePathStep(Point from, Point to, boolean finalStep) {
        if (!room.getRoomMap().isValidSquare(to.getX(), to.getY()))
            return false;

        int fromHeight = (int) room.getRoomMap().getSquareDynamicHeight(from.getX(), from.getY());
        int toHeight = (int) room.getRoomMap().getSquareDynamicHeight(to.getX(), to.getY());

        int diff = Math.abs(fromHeight - toHeight);

        if (diff > 1.5 && fromHeight < toHeight) {
            return false;
        }

        IRoomItem item = room.getRoomMap().getHighestRoomItemBySq(to.getX(), to.getY());
        if (item != null) {
            if (!item.canContinuePath(this.avatar, from, finalStep))
                return false;
        }

        return !(room.getRoomMap().getRoomAvatars(to).size() > 0 && room.getProperties().getAllowWalkTrough() == 0);
    }
}
