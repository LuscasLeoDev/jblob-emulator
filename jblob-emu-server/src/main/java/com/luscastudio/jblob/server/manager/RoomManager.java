package com.luscastudio.jblob.server.manager;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.RoomPropertyComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.events.EventHandler;
import com.luscastudio.jblob.server.events.list.RoomUnloadEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.avatar.HabboAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomEnterErrorFuture;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.RoomType;
import com.luscastudio.jblob.server.game.habbohotel.rooms.models.RoomModel;
import com.luscastudio.jblob.server.game.players.IMessageComposerHandler;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 03/10/2016.
 */

public class RoomManager {

    Logger log = Logger.getLogger(this.getClass());
    private Map<Integer, Room> activeRooms;
    private Map<Integer, RoomProperties> activeRoomProperties;
    private Map<Integer, RoomProperties> cachedRooms;

    private EventHandler eventHandler;

    public RoomManager() {

        this.eventHandler = new EventHandler();

        this.activeRooms = BCollect.newConcurrentMap();
        this.activeRoomProperties = BCollect.newConcurrentMap();
        this.cachedRooms = BCollect.newConcurrentMap();
    }

    public RoomProperties getRoomData(int id) {

        if (cachedRooms.containsKey(id))
            return cachedRooms.get(id);
        if (activeRooms.containsKey(id))
            return activeRooms.get(id).getProperties();


        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

            DBConnPrepare prepare = reactor.prepare("SELECT * FROM players_rooms WHERE id = ?");
            prepare.setInt(1, id);

            ResultSet set = prepare.runQuery();

            if (!set.next())
                return null;

            String modelName = set.getString("model_name");

            RoomModel model = JBlob.getGame().getModelManager().getModel(modelName);

            if (model == null) {
                //todo: Alert owner os room visiter the room is with invalid model
                log.warn("Room #" + id + " has EventArgs invalid model! [" + modelName + "]");
                return null;
            }

            RoomProperties prop = new RoomProperties(set, model);

            cachedRooms.put(prop.getId(), prop);

            return prop;

        } catch (Exception e) {
            log.error("Error while parsing room", e);
            return null;
        }
    }

    public void unloadRoom(int id) {
        Room room = getRoom(id);
        if(room != null)
            this.unloadRoom(room);
    }

    public void unloadRoom(Room room){

        room.dispose();
        this.activeRooms.remove(room.getProperties().getId());
        this.activeRoomProperties.remove(room.getProperties().getId());
        eventHandler.fireEvent("room.unload", new RoomUnloadEventArgs(room.getProperties()));
    }

    public Room loadRoom(int id) {
        RoomProperties roomData = getRoomData(id);

        if (roomData == null)
            return null;

        if (roomData.getRoom() != null)
            return roomData.getRoom();
        else {

            cachedRooms.remove(roomData.getId());

            Room roomEngine = new Room(roomData);
            roomData.setRoom(roomEngine);
            activeRooms.put(roomData.getId(), roomEngine);
            activeRoomProperties.put(roomData.getId(), roomData);

            return roomEngine;

        }
    }

    public List<RoomProperties> getRoomsByCategory(int id, boolean featured) {
        List<RoomProperties> rooms = BCollect.newList();
        if (!featured) {

            for (Room room : this.activeRooms.values()) {

                if (room.getProperties().getCategoryId() == id)
                    rooms.add(room.getProperties());

            }
            for (RoomProperties room : this.cachedRooms.values()) {

                if (room.getCategoryId() == id)
                    rooms.add(room);
            }
        } else {

            try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

                DBConnPrepare prepare = reactor.prepare("SELECT id FROM players_rooms WHERE category_id = ?").setInt(1, id).back();
                ResultSet set = prepare.runQuery();

                while (set.next()) {
                    RoomProperties roomData = getRoomData(set.getInt("id"));

                    rooms.add(roomData);
                }


            } catch (Exception e) {
                log.error("Error getting room by category from database", e);
            }

        }

        return rooms;
    }

    public Room getRoom(int id) {
        if (activeRooms.containsKey(id))
            return activeRooms.get(id);

        return null;
    }

    public EventHandler getEventHandler() {
        return eventHandler;
    }

    public int getRoomCount() {
        return this.activeRooms.size();
    }

    public Map<Integer, Room> getActiveRooms() {
        return activeRooms;
    }

    public Collection<RoomProperties> getActiveRoomList() {
        return activeRoomProperties.values();
    }

    public void close() {
        List<Integer> ids = BCollect.newList(this.getActiveRooms().keySet());
        for (Integer id : ids) {
            this.unloadRoom(id);
        }
    }

    public static RoomEnterErrorFuture addHabboToRoom(HabboAvatar avatar, Room room){
        return room.getRoomAvatarService().tryAddPlayerAvatar(avatar);
    }

    public static void serializeRoomData(IMessageComposerHandler handler, Room room){

        handler.sendQueueMessage(new RoomPropertyComposer("floor", room.getProperties().getFloorData()));
        handler.sendQueueMessage(new RoomPropertyComposer("wallpaper", room.getProperties().getWallData()));
        handler.sendQueueMessage(new RoomPropertyComposer("landscape", room.getProperties().getLandscapeData()));
    }

    public RoomProperties createRoom(String name, String description, int ownerId, String modelName, int lockType, String password, int maxUsers, int tradeType, int categoryId, String wallData, String floorData, String landscapeData, RoomType type, int allowPets, int allowPetsEating, int hideWall, int wallThickness, int floorThickness, int allowWalkTrough, int whoCanMute, int whoCanKick, int whoCanBan, int chatMode, int chatSize, int chatSpeed, int chatExtraFlood, int chatDistance, String jsonAvatarsLike, String jsonAvatarsRight, String tagsData, RoomModel model) {

        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare = reactor.prepare("INSERT INTO `players_rooms` (`name`, `description`, `owner_id`, `model_name`, `lock_type`, `password`, `max_users`, `trade_type`, `score`, `tags`, `wallpaper_data`, `landscape_data`, `floor_data`, `who_can_kick`, `who_can_mute`, `who_can_ban`, `chat_mode`, `chat_size`, `chat_speed`, `chat_extra_flood`, `chat_distance`, `allow_pets`, `allow_pets_eating`, `hide_wall`, `wall_thickness`, `floor_thickness`, `allow_walk_trough`, `avatars_rights`, `avatars_likes`, `habbotv_enabled`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", true);

            prepare.setString(1, name);
            prepare.setString(2, description);
            prepare.setInt(3, ownerId);
            prepare.setString(4, modelName);
            prepare.setInt(5, lockType);
            prepare.setString(6, password);
            prepare.setInt(7, maxUsers);
            prepare.setInt(8, tradeType);
            prepare.setString(9, tagsData);
            prepare.setString(10, wallData);
            prepare.setString(11, landscapeData);
            prepare.setString(12, floorData);
            prepare.setInt(13, whoCanKick);
            prepare.setInt(14, whoCanMute);
            prepare.setInt(15, whoCanBan);
            prepare.setInt(16, chatMode);
            prepare.setInt(17, chatSize);
            prepare.setInt(18, chatSpeed);
            prepare.setInt(19, chatExtraFlood);
            prepare.setInt(20, chatDistance);
            prepare.setInt(21, allowPets);
            prepare.setInt(22, allowPetsEating);
            prepare.setInt(23, hideWall);
            prepare.setInt(24, wallThickness);
            prepare.setInt(25, floorThickness);
            prepare.setInt(26, allowWalkTrough);
            prepare.setString(27, jsonAvatarsRight);
            prepare.setString(28, jsonAvatarsLike);
            prepare.setInt(39, 0);

            int roomId = prepare.runInsert();

            RoomProperties roomProperties = new RoomProperties(roomId, name, description, ownerId, modelName, lockType, password, maxUsers, tradeType, categoryId, wallData, floorData, landscapeData, type, allowPets, allowPetsEating, hideWall, wallThickness, floorThickness, allowWalkTrough, whoCanMute, whoCanKick, whoCanBan, chatMode, chatSize, chatSpeed, chatExtraFlood, chatDistance, jsonAvatarsLike, jsonAvatarsRight, tagsData, model);

            this.cachedRooms.put(roomId, roomProperties);
            return roomProperties;

        } catch (Exception e){
            BLogger.error(e, this.getClass());
            return null;
        }
    }
}
