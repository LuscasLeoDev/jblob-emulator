package com.luscastudio.jblob.server.communication.handlers.rooms.groups.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.rooms.groups.server.HabboGroupBadgesComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 10/12/2016.
 */

public class GetHabboGroupBadgesEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        session.sendMessage(new HabboGroupBadgesComposer(1, ""));
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
