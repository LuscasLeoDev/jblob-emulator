//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.network.websockets;

import io.netty.handler.codec.DecoderResult;
import io.netty.handler.codec.http.*;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Lucas on 12/03/2017 at 19:37.
 */
public class WebMessage extends DefaultHttpResponse {

    public WebMessage(HttpVersion version, HttpResponseStatus status) {
        super(version, status);
    }
}
