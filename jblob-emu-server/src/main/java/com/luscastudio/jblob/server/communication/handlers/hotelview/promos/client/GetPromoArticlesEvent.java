package com.luscastudio.jblob.server.communication.handlers.hotelview.promos.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.hotelview.promos.server.PromoArticlesComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 14/02/2017 at 18:36.
 */
public class GetPromoArticlesEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        session.sendMessage(new PromoArticlesComposer(JBlob.getGame().getPromoArticleManager().getArticleList()));
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
