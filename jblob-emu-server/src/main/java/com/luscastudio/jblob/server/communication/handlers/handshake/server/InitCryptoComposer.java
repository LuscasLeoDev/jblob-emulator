package com.luscastudio.jblob.server.communication.handlers.handshake.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 19/09/2016.
 */
public class InitCryptoComposer extends MessageComposer {

    public InitCryptoComposer(PlayerSession session) {
        String p = session.getBuild().getRsa().sign(session.getDiffieHellman().getPrime().toString());
        String g = session.getBuild().getRsa().sign(session.getDiffieHellman().getGenerator().toString());

        message.putString(p);
        message.putString(g);

    }

    @Override
    public String id() {
        return "InitCryptoMessageComposer";
    }


}
