package com.luscastudio.jblob.server.game.habbohotel.avatar.preference;

/**
 * Created by Lucas on 09/12/2016.
 */

public class AvatarPreference {

    private String name;
    private String value;

    public AvatarPreference(String key, String value) {
        this.name = key;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }
}
