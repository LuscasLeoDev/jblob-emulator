package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars;

import java.util.List;

/**
 * Created by Lucas on 03/10/2016.
 */
public interface IBotData extends IRoomAvatar{

    int getOwnerId();

    int getOwnerUsername();

    List<Integer> getActions();

}
