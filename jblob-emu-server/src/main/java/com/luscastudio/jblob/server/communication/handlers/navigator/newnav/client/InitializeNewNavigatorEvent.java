package com.luscastudio.jblob.server.communication.handlers.navigator.newnav.client;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.navigator.newnav.server.NavigatorMetaDataParserComposer;
import com.luscastudio.jblob.server.communication.handlers.navigator.newnav.server.NavigatorPreferencesComposer;
import com.luscastudio.jblob.server.game.habbohotel.navigator.tabs.NavigatorTopLevel;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;

/**
 * Created by Lucas on 02/10/2016.
 */

public class InitializeNewNavigatorEvent implements IMessageEventHandler {

    @Override
    public void parse(PlayerSession session, MessageEvent packet) {

        Collection<NavigatorTopLevel> topLevels = JBlob.getGame().getNavigatorManager().getTopLevels(session.getAvatar().getId());
        session.sendQueueMessage(new NavigatorMetaDataParserComposer(topLevels));

        session.sendQueueMessage(new NavigatorPreferencesComposer(session.getAvatar().getPreferences()));
        session.flush();

    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
