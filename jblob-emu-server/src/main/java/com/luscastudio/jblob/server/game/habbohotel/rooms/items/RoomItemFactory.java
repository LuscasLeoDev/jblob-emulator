package com.luscastudio.jblob.server.game.habbohotel.rooms.items;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniBase;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata.*;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.*;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.conditions.ConditionFurniHasAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.conditions.ConditionFurnisStatePositionMatch;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.effects.EffectMatchFurnisStatePosition;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.effects.EffectShowMessageToAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.effects.EffectTeleportAvatarTo;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.effects.EffectToggleState;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.triggers.TriggerAvatarSaysSomething;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.triggers.TriggerAvatarWalksOnFurni;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.triggers.TriggerPeriodically;
import org.apache.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 10/10/2016.
 */

public class RoomItemFactory implements IFlushable {
    private static Logger log = Logger.getLogger(RoomItemFactory.class);
    private static String DEFAULT_INTERACTION = "default";

    private Map<String, Class<? extends IRoomItem>> interactors;
    private Map<String, Class<? extends IRoomItemExtradata>> extradatas;

    public RoomItemFactory() {
        this.interactors = BCollect.newMap();
        this.extradatas = BCollect.newMap();
    }

    public void addInteractor(String key, Class<? extends IRoomItem> itemClass) {

        if (this.interactors.containsKey(key))
            this.interactors.replace(key, itemClass);
        else
            this.interactors.put(key, itemClass);
    }

    public void addExtradata(String key, Class<? extends IRoomItemExtradata> itemClass) {

        if (this.extradatas.containsKey(key))
            this.extradatas.replace(key, itemClass);
        else
            this.extradatas.put(key, itemClass);
    }


    private void addDefault() {

        //Simple Room item
        this.addInteractor("default", SimpleRoomItem.class);
        this.addInteractor("none", SimpleRoomItem.class);

        this.addExtradata("default", SimpleRoomItemExtradata.class);
        this.addExtradata("none", SimpleRoomItemExtradata.class);

        //Gate
        this.addInteractor("gate", GateRoomItem.class);

        //Teleport
        this.addInteractor("teleport", TeleportRoomItem.class);

        //Sticky note
        this.addExtradata("postit", PostItRoomItemExtradata.class);

        //VendingMachine
        this.addInteractor("vendingmachine", VendingMachineRoomItem.class);

        //Pressure Tile
        this.addInteractor("pressure_tile", PressureTileRoomItem.class);

        //Dimmer
        this.addInteractor("dimmer", DimmerRoomItem.class);
        this.addExtradata("dimmer", DimmerRoomItemExtradata.class);

        //Monster Plant Seed
        this.addInteractor("monsterplant_seed", MonsterPlantSeedRoomItem.class);
        this.addExtradata("monsterplant_seed", MonsterPlantSeedRoomItemExtradata.class);

        //Pet Item
        this.addInteractor("pet_item", PetItemRoomItem.class);

        //Toner
        this.addInteractor("room_toner", TonerRoomItem.class);
        this.addExtradata("room_toner", TonerRoomItemExtradata.class);

        //Internal Link
        this.addInteractor("internal_link", InternalLinkRoomItem.class);
        this.addExtradata("internal_link", InternalLinkRoomItemExtradata.class);

        //Wired Triggers

        this.addInteractor("wf_trg_walks_on_furni", TriggerAvatarWalksOnFurni.class);
        this.addExtradata("wf_trg_walks_on_furni", WiredBoxRoomItemExtradata.class);

        this.addInteractor("wf_trg_says_something", TriggerAvatarSaysSomething.class);
        this.addExtradata("wf_trg_says_something", WiredBoxRoomItemExtradata.class);

        this.addInteractor("wf_trg_periodically", TriggerPeriodically.class);
        this.addExtradata("wf_trg_periodically", WiredBoxRoomItemExtradata.class);

        this.addInteractor("wf_act_toggle_state", EffectToggleState.class);
        this.addExtradata("wf_act_toggle_state", WiredBoxRoomItemExtradata.class);



        //Wired Effects
        this.addInteractor("wf_act_teleport_to", EffectTeleportAvatarTo.class);
        this.addExtradata("wf_act_teleport_to", WiredBoxRoomItemExtradata.class);

        this.addInteractor("wf_act_show_message", EffectShowMessageToAvatar.class);
        this.addExtradata("wf_act_show_message", WiredBoxRoomItemExtradata.class);

        this.addInteractor("wf_act_match_to_sshot", EffectMatchFurnisStatePosition.class);
        this.addExtradata("wf_act_match_to_sshot", WiredBoxRoomItemExtradata.class);


        //Wired Conditions
        this.addInteractor("wf_cnd_match_snapshot", ConditionFurnisStatePositionMatch.class);
        this.addExtradata("wf_cnd_match_snapshot", WiredBoxRoomItemExtradata.class);

        this.addInteractor("wf_cnd_furnis_hv_avtrs", ConditionFurniHasAvatar.class);
        this.addExtradata("wf_cnd_furnis_hv_avtrs", WiredBoxRoomItemExtradata.class);



    }

    //region FurniProperties Generating

    public void hideFurniProperties(int itemId){
        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {
            DBConnPrepare prepare = reactor.prepare("UPDATE avatars_items_data SET room_id = ?, user_id = ? WHERE id = ?");
            prepare.setInt(1, 0);
            prepare.setInt(2, 0);
            prepare.setInt(3, itemId);
            prepare.run();

        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }
    }

    public void showFurniProperties(int itemId){
        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {
            DBConnPrepare prepare = reactor.prepare("UPDATE avatars_items_data SET room_id = ?, user_id = owner_id WHERE id = ?");
            prepare.setInt(1, 0);
            prepare.setInt(2, itemId);
            prepare.run();

        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }
    }

    public FurniProperties getFurniProperties(int itemId){
        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {
            DBConnPrepare prepare = reactor.prepare("SELECT * FROM avatars_items_data WHERE id = ?");
            prepare.setInt(1, itemId);
            ResultSet set = prepare.runQuery();
            if(set.next())
                return this.generateFurniProperties(set);

        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }

        return null;
    }

    public List<FurniProperties> createFurniPropertiesList(List<FurniPropertiesCreateData> createDatas) {
        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

            List<FurniProperties> propertiesList = BCollect.newList();
            DBConnPrepare prepare;
            FurniProperties properties;
            IRoomItemExtradata extradataCls;

            StringBuilder sqlBuilder = new StringBuilder("INSERT INTO avatars_items_data (user_id, owner_id, base_id, extradata, group_id, is_rare) VALUES ");
            int i = createDatas.size();
            boolean twice = false;
            while (i-- > 0) {
                if (twice)
                    sqlBuilder.append(", ");
                sqlBuilder.append("(?, ?, ?, ?, ?, ?)");
                twice = true;
            }


            prepare = reactor.prepare(sqlBuilder.toString(), true);

            i = 0;
            for (FurniPropertiesCreateData createData : createDatas) {

                int n = i * 6;
                prepare.setInt(1 + n, createData.getOwnerId());
                prepare.setInt(2 + n, createData.getOwnerId());
                prepare.setInt(3 + n, createData.getBase().getId());
                prepare.setString(4 + n, createData.getExtradata());
                prepare.setInt(5 + n, createData.getGroupId());
                prepare.setInt(6 + n, createData.isRare() ? 1 : 0);
                i++;
            }

            ResultSet keys = prepare.runInsertKeys();
            //i = 0;
            for (FurniPropertiesCreateData createData : createDatas) {
                if (!keys.next())
                    continue;
                extradataCls = generateExtradata(createData.getBase().getInteractionType(), createData.getExtradata(), true);
                properties = new FurniProperties(keys.getInt(1), createData.getBase(), createData.getOwnerId(), extradataCls, createData.isRare(), createData.getGroupId());

                //todo: think about this
                extradataCls.init(properties);

                propertiesList.add(properties);
            }

            return propertiesList;

        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }

        return null;
    }

    public List<FurniProperties> createFurniPropertiesList(FurniBase base, int userId, String extradata, int groupId, boolean isRare, int amount){
        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

            List<FurniProperties> propertiesList = BCollect.newList();
            DBConnPrepare prepare;
            FurniProperties properties;
            IRoomItemExtradata extradataCls;

            StringBuilder sqlBuilder = new StringBuilder("INSERT INTO avatars_items_data (owner_id, user_id, base_id, extradata, group_id, is_rare) VALUES ");
            int i = amount;
            boolean twice = false;
            while(i-- > 0) {
                if(twice)
                    sqlBuilder.append(", ");
                sqlBuilder.append("(?, ?, ?, ?, ?, ?)");
                twice = true;
            }

            i = amount;
            prepare = reactor.prepare(sqlBuilder.toString(), true);

            while (i-- > 0) {
                //prepare = reactor.prepare("INSERT INTO avatars_items_data (user_id, base_id, extradata, group_id) VALUES (?, ?, ?, ?)", true);
                int n = i * 6;
                prepare.setInt(1 + n, userId);
                prepare.setInt(2 + n, userId);
                prepare.setInt(3 + n, base.getId());
                prepare.setString(4 + n, extradata);
                prepare.setInt(5 + n, groupId);
                prepare.setInt(6 + n, isRare ? 1 : 0);
            }

            ResultSet keys = prepare.runInsertKeys();

            while (keys.next()) {
                extradataCls = generateExtradata(base.getInteractionType(), extradata, true);
                properties = new FurniProperties(keys.getInt(1), base, userId, extradataCls, isRare, groupId);

                //todo: think about this
                extradataCls.init(properties);

                propertiesList.add(properties);
            }

            return propertiesList;

        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }

        return null;
    }

    public FurniProperties createFurniProperties(FurniBase base, int userId, String extradata, boolean isRare, int groupId) {
        try (DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()) {

            DBConnPrepare prepare = reactor.prepare("INSERT INTO avatars_items_data (owner_id, user_id, base_id, extradata, group_id, is_rare) VALUES (?, ?, ?, ?, ?, ?)", true);

            prepare.setInt(1, userId);
            prepare.setInt(2, userId);
            prepare.setInt(3, base.getId());
            prepare.setString(4, extradata);
            prepare.setInt(5, groupId);
            prepare.setInt(6, isRare ? 1 : 0);

            int insertId = prepare.runInsert();

            IRoomItemExtradata extradataCls = generateExtradata(base.getInteractionType(), extradata, true);
            FurniProperties properties = new FurniProperties(insertId, base, userId, extradataCls, isRare, groupId);
            extradataCls.init(properties);

            return properties;

        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }

        return null;
    }

    public FurniProperties generateFurniProperties(ResultSet set) throws SQLException {

        int baseId = set.getInt("base_id");
        FurniBase base = JBlob.getGame().getItemBaseManager().getItemBase(baseId);
        if (base == null)
            return null;

        return generateFurniProperties(set, base);
    }

    public FurniProperties generateFurniProperties(ResultSet set, FurniBase base) throws SQLException {

        int id = set.getInt("id");
        int ownerId = set.getInt("owner_id");
        int groupId = set.getInt("group_id");
        boolean isRare = set.getInt("is_rare") == 1;
        String extradata = set.getString("extradata");


        IRoomItemExtradata item = generateExtradata(base, extradata);

        if(item == null){
            //BLogger.warn("The Extradata for furni of '" + base.getInteractionType() + "' type was not found!!", this.getClass());

            item = generateExtradata(DEFAULT_INTERACTION, extradata);
        }

        FurniProperties furniProperties = new FurniProperties(id, base, ownerId, item, isRare, groupId);
        item.init(furniProperties);

        return furniProperties;

    }
    //endregion


    //region Furni Extradata Generating
    public IRoomItemExtradata generateExtradata(FurniBase base, String extradata){
        return generateExtradata(base.getInteractionType(), extradata, false);
    }

    public IRoomItemExtradata generateExtradata(String base, String extradata, boolean orDefault) {
        IRoomItemExtradata e;
        return (e = generateExtradata(base, extradata)) != null || !orDefault ? e : generateExtradata(DEFAULT_INTERACTION, extradata);
    }

    public IRoomItemExtradata generateExtradata(String base, String extradata) {
        Class<? extends IRoomItemExtradata> extradataClass;
        if (!extradatas.containsKey(base))
            extradataClass = extradatas.get(DEFAULT_INTERACTION);
        else
            extradataClass = extradatas.get(base);

        return generateExtradataInstance(extradataClass, extradata);
    }
    public IRoomItemExtradata generateExtradataInstance(Class< ? extends IRoomItemExtradata> classe, String extradata) {
        try {
            Constructor<?> constructor = classe.getConstructor(String.class);

            if(constructor == null)
                return  null;

            return (IRoomItemExtradata)constructor.newInstance(extradata);

        } catch (Exception e) {
            BLogger.error(e, this.getClass());
        }
        return null;
    }

    //endregion


    //region RoomItem Generating
    public IRoomItem createRoomItemFromDb(ResultSet set, Room room) throws SQLException {
        int baseId = set.getInt("base_id");

        FurniBase base = JBlob.getGame().getItemBaseManager().getItemBase(baseId);
        if (base == null)
            return null;

        return createRoomItemInteractor(set, base, room);
    }

    private IRoomItem createRoomItemInteractor(ResultSet set, FurniBase base, Room room) throws SQLException {

        Class<? extends IRoomItem> interactor;
        if (!interactors.containsKey(base.getInteractionType()))
            interactor = interactors.get(DEFAULT_INTERACTION);
        else
            interactor = interactors.get(base.getInteractionType());

        int id = set.getInt("id");
        int ownerId = set.getInt("user_id");
        int groupId = set.getInt("group_id");
        boolean isRare = set.getInt("is_rare") == 1;
        String extradata = set.getString("extradata");
        int x = set.getInt("x");
        int y = set.getInt("y");
        double z = NumberHelper.parseDouble(set.getString("z"));
        int dir = set.getInt("rotation");
        String wallData = set.getString("wall_coordinate");

        IRoomItemExtradata extradataHandler = generateExtradata(base.getInteractionType(), extradata);
        FurniProperties properties = new FurniProperties(id, base, ownerId, extradataHandler, isRare, groupId);
        extradataHandler.init(properties);
        try {


            IRoomItem item = generateRoomItemInstance(interactor, properties, room, x, y, z, dir, wallData);

            return item;

        } catch (Exception e) {
            log.error("Error while parsing interactor '" + base.getInteractionType() + "' : ", e);
            return null;
        }
    }

    public IRoomItem createRoomItemFromProperties(FurniProperties furni, Room room, int x, int y, double z, int dir, String wallCoords) {
        Class<? extends IRoomItem> interactor;
        if (!interactors.containsKey(furni.getBase().getInteractionType())){
            ///BLogger.warn("There is no interactor for '" + furni.getBase().getInteractionType(), this.getClass());
            interactor = interactors.get(DEFAULT_INTERACTION);
        } else
            interactor = interactors.get(furni.getBase().getInteractionType());

        try {
            IRoomItem item = generateRoomItemInstance(interactor, furni, room, x, y, z, dir, wallCoords);

            return item;

        } catch (Exception e) {
            log.error("Error while parsing interactor '" + furni.getBase().getInteractionType() + "' : ", e);

            return null;
        }
    }

    private IRoomItem generateRoomItemInstance(Class<? extends  IRoomItem> interactor, FurniProperties properties, Room room, int x, int y, double z, int rotation, String wallCoords) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        return (IRoomItem) interactor.getConstructors()[0].newInstance(properties, room, x, y, z, rotation, wallCoords);
    }

    //endregion

    @Override
    public void flush() {
        this.extradatas.clear();
        this.interactors.clear();
        this.addDefault();
    }

    @Override
    public void performFlush() {

    }

    public static class FurniPropertiesCreateData {
        private int ownerId;
        private FurniBase base;
        private String extradata;
        private int groupId;
        private boolean rare;

        public FurniPropertiesCreateData(int ownerId, FurniBase base, String extradata, boolean isRare, int groupId){
            this.ownerId = ownerId;
            this.base = base;
            this.extradata = extradata;
            this.groupId = groupId;
            this.rare = isRare;
        }

        public FurniBase getBase() {
            return base;
        }

        public int getGroupId() {
            return groupId;
        }

        public int getOwnerId() {
            return ownerId;
        }

        public String getExtradata() {
            return extradata;
        }

        public boolean isRare() {
            return rare;
        }
    }
}
