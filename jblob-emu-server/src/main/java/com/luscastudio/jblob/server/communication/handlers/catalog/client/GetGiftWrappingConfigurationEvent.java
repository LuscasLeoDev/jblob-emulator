package com.luscastudio.jblob.server.communication.handlers.catalog.client;

import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.catalog.server.GiftWrappingConfigurationComposer;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

/**
 * Created by Lucas on 31/01/2017 at 18:41.
 */
public class GetGiftWrappingConfigurationEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        session.sendMessage(new GiftWrappingConfigurationComposer());
    }

    @Override
    public boolean isAsync() {
        return false;
    }
}
