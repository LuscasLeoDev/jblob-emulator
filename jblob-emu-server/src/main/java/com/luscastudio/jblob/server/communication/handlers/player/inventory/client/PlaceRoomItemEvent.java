package com.luscastudio.jblob.server.communication.handlers.player.inventory.client;

import com.luscastudio.jblob.api.utils.numbers.StringSplitter;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.client.IMessageEventHandler;
import com.luscastudio.jblob.server.communication.client.MessageEvent;
import com.luscastudio.jblob.server.communication.handlers.handshake.server.TriggerClientEventComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.InventoryUpdateRequestComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.RemoveInventoryFurniComposer;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.AddFloorItemComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.AddWallItemComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.server.RemoveFloorItemComposer;
import com.luscastudio.jblob.server.events.list.PurchasedCatalogDealFurniEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogDealItem;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.players.cache.IAvatarDataCache;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;


/**
 * Created by Lucas on 17/10/2016.
 */

public class PlaceRoomItemEvent implements IMessageEventHandler {
    @Override
    public void parse(PlayerSession session, MessageEvent packet) {
        String itemDataStr = packet.getString();

        StringSplitter spl = new StringSplitter(itemDataStr, " ");

        if (spl.getCount() == 0)
            return;

        Room room = session.getAvatar().getCurrentRoom();
        if (room == null)
            return;

        if(!room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_place_item")) {
            session.sendMessage(new BubbleNotificationComposer("furni_placement_error").add("message", "${room.error.cant_set_not_owner}"));
            return;
        }

        int itemId = spl.getInt();
        String wallData = itemDataStr.substring(String.valueOf(itemId).length() + 1);
        if(itemId < 0){

            session.sendMessage(new RemoveFloorItemComposer(itemId, 0, false, 5000 ));
            this.registerPurchaseEvent(session, -itemId, spl, wallData, room);

            return;
        }

        FurniProperties furniprop = session.getAvatar().getInventory().getFurni(itemId);

        if (furniprop == null) {
            return;
        }

        if(!room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_place_item")) {
            session.sendMessage(new BubbleNotificationComposer("furni_placement_error").add("message", "${room.error.cant_set_not_owner}"));
            return;
        }

        IRoomItem item = room.getRoomItemHandlerService().getRoomItemById(itemId);
        if (item != null) {
            //todo: cancel this and send to external_flash_texts
            session.sendQueueMessage(new BubbleNotificationComposer("room.furni.placing.error"));

            session.sendQueueMessage(new AddFloorItemComposer(item, "Unknown"));
            session.sendQueueMessage(new RemoveInventoryFurniComposer(item.getId())).flush();

            return;
        }

        if (furniprop.getBase().getType().equals("s")) {
            tryPlaceFloorItem(session, spl, room, furniprop);
        } else {

            tryPlaceWallItem(session, wallData, room, furniprop);

        }
    }

    @Override
    public boolean isAsync() {
        return false;
    }

    private void tryPlaceFloorItem(PlayerSession session, StringSplitter spl, Room room, FurniProperties furniprop){
        int x = spl.getInt();
        int y = spl.getInt();
        int rot = spl.getInt();

        IRoomItem roomItem = JBlob.getGame().getRoomItemFactory().createRoomItemFromProperties(furniprop, room, 0, 0, 0, 0, "");

        if (roomItem == null)
            return;

        if(!roomItem.onPlace(session)) {
            roomItem.dispose();
            return;
        }

        if (!room.getRoomItemHandlerService().addItemToRoom(roomItem, x, y, rot)) {
            session.sendMessage(new BubbleNotificationComposer("room.furni.placing.error"));
            roomItem.dispose();
            return;
        }

        session.getAvatar().getInventory().removeRoomItem(roomItem.getId());
        session.sendQueueMessage(new RemoveInventoryFurniComposer(roomItem.getId()));
        session.sendQueueMessage(new InventoryUpdateRequestComposer());

        IAvatarDataCache cache = JBlob.getGame().getUserCacheManager().getUserCache(roomItem.getOwnerId());

        if (cache != null)
            room.sendMessage(new AddFloorItemComposer(roomItem, cache.getUsername()));
        else
            room.sendMessage(new AddFloorItemComposer(roomItem, "Unknown#" + roomItem.getOwnerId()));


        roomItem.onPlaced();
    }

    private void tryPlaceWallItem(PlayerSession session, String data, Room room, FurniProperties furniprop){

        IRoomItem roomItem = JBlob.getGame().getRoomItemFactory().createRoomItemFromProperties(furniprop, room, 0, 0, 0, 0, data);

        if(roomItem == null)
            return;

        if(!roomItem.onPlace(session)) {
            roomItem.dispose();
            return;
        }

        if (!room.getRoomItemHandlerService().addItemToRoom(roomItem, data)) {
            //todo: send this to external_texts
            session.sendMessage(new BubbleNotificationComposer("room.furni.placing.error"));
            roomItem.dispose();
            return;
        }

        session.getAvatar().getInventory().removeRoomItem(roomItem.getId());
        session.sendQueueMessage(new RemoveInventoryFurniComposer(roomItem.getId()));
        session.sendQueueMessage(new InventoryUpdateRequestComposer());

        IAvatarDataCache cache = JBlob.getGame().getUserCacheManager().getUserCache(roomItem.getOwnerId());

        if (cache != null)
            room.sendMessage(new AddWallItemComposer(roomItem, cache.getUsername()));
        else
            room.sendMessage(new AddWallItemComposer(roomItem, "Unknown#" + roomItem.getOwnerId()));

        roomItem.onPlaced();
    }

    private void registerPurchaseEvent(PlayerSession session, int dealId, StringSplitter spl, String wallData, Room room) {

        session.getAvatar().getEventHandler().on("catalog.deal.purchase.furni", (delegate, eventArgs) -> {

            PurchasedCatalogDealFurniEventArgs args = (PurchasedCatalogDealFurniEventArgs) eventArgs;

            if(session.getAvatar().getCurrentRoom() != room){
                delegate.cancel();
                return;
            }

            CatalogDealItem dealItem = args.getDeal();

            if(dealItem.getDealId() == dealId){
                FurniProperties furniProperties = args.getProperties();
                session.sendMessage(new TriggerClientEventComposer("inventory/open/furni/" + furniProperties.getId()));
                delegate.cancel();
            }


        });

    }
}
