package com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.abstracts;

import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Collection;

/**
 * Created by Lucas on 15/02/2017 at 16:56.
 */
public interface ISavableWiredTrigger {
    void saveWiredData(PlayerSession session, Collection<Integer> integerList, String stringData, Collection<Integer> selectedFurniList, int someInt);
}
