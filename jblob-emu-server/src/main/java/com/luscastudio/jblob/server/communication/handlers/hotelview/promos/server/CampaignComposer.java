package com.luscastudio.jblob.server.communication.handlers.hotelview.promos.server;

import com.luscastudio.jblob.server.communication.server.MessageComposer;

/**
 * Created by Lucas on 01/10/2016.
 */
public class CampaignComposer extends MessageComposer {
    @Override
    public String id() {
        return "CampaignMessageComposer";
    }

    public CampaignComposer(String str, String name) {
        message.putString(str).putString(name);
    }
}
