package com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.console.IFlushable;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.notification.server.BroadcastMessageAlertComposer;
import com.luscastudio.jblob.server.communication.handlers.notification.server.ModeratorSupportTicketResponseComposer;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.IChatCommand;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Map;

/**
 * Created by Lucas on 16/12/2016.
 */

public class UpdateCmd implements IChatCommand {

    private Map<String, IFlushable> updates;

    public UpdateCmd(){
        this.updates = BCollect.newMap();

        this.updates.put("furnis", JBlob.getGame().getItemBaseManager());
        this.updates.put("navigator", JBlob.getGame().getNavigatorManager());
        this.updates.put("catalog", JBlob.getGame().getCatalogManager());
        this.updates.put("commands", JBlob.getGame().getChatCommandManager());
        this.updates.put("dbconfig", JBlob.getGame().getDbConfig());
        this.updates.put("builds", JBlob.getGame().getBuildManager());
        this.updates.put("roomitems", JBlob.getGame().getRoomItemFactory());
        this.updates.put("permissions", JBlob.getGame().getPermissionManager());
        this.updates.put("rmodels", JBlob.getGame().getModelManager());
        this.updates.put("petbases", JBlob.getGame().getPetManager());
        this.updates.put("petavatars", JBlob.getGame().getPetAvatarHandler());
        this.updates.put("packets", JBlob.getGame().getMessageEventManager());
        this.updates.put("promos", JBlob.getGame().getPromoArticleManager());
    }

    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {

        String updateKey = params.get(1).toLowerCase();
        boolean dontPerformFlush = params.length() > 2 && (params.get(2).toLowerCase().equals("no") || params.get(2).toLowerCase().equals("false"));
        if (updateKey.equals("?")) {

            StringBuilder builder = new StringBuilder("These are the update commands:\n");
            for (String key : this.updates.keySet()) {
                builder.append("¤ <b>" + key + "</b>\n");
            }
            session.sendMessage(new BroadcastMessageAlertComposer(builder.toString()));

            return true;
        }

        if (!this.updates.containsKey(updateKey)) {
            session.sendMessage(new ModeratorSupportTicketResponseComposer("Update code not found"));
            return true;
        }

        try {
            this.updates.get(updateKey).flush();
            if(!dontPerformFlush)
                this.updates.get(updateKey).performFlush();
        } catch (Exception e) {
            BLogger.error(e, this.getClass());
            session.sendMessage(new ModeratorSupportTicketResponseComposer("Error when updating :c\n" + e.toString()));
            return false;
        }
        session.sendMessage(new ModeratorSupportTicketResponseComposer("Update did sucessfully"));
        return true;
    }
}
