package com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers;

import com.luscastudio.jblob.server.communication.server.PacketWriting;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;

/**
 * Created by Lucas on 10/01/2017 at 00:06.
 */
public class PostItItemMessageParser extends SimpleItemMessageParser {

    public PostItItemMessageParser(FurniProperties item) {
        super(item);
    }

    @Override
    public void parse(PacketWriting message) {
        message.putString(this.item.getExtradata().toString().split(" ")[0]);
    }
}
