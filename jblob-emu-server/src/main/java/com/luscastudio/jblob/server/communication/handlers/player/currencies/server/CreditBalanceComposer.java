package com.luscastudio.jblob.server.communication.handlers.player.currencies.server;

/**
 * Created by Lucas on 07/12/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;

public class CreditBalanceComposer extends MessageComposer {
    @Override
    public String id() {
        return "CreditBalanceMessageComposer";
    }

    public CreditBalanceComposer(int value) {
        message.putString(value + ".0");
    }

    public CreditBalanceComposer(String value){
        message.putString(value);
    }
}
