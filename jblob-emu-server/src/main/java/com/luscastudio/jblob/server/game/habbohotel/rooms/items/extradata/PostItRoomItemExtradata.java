package com.luscastudio.jblob.server.game.habbohotel.rooms.items.extradata;

import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniProperties;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.IItemMessageParser;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.parsers.PostItItemMessageParser;

/**
 * Created by Lucas on 09/01/2017 at 23:38.
 */
public class PostItRoomItemExtradata extends SimpleRoomItemExtradata {

    private PostItItemMessageParser messageParser;

    public PostItRoomItemExtradata(String extradata) {
        super(extradata);
    }

    @Override
    public void init(FurniProperties properties) {
        super.init(properties);
        this.messageParser = new PostItItemMessageParser(properties);
    }

    @Override
    public synchronized String getExtradata() {
        if(this.extradata.equals(""))
            return JBlob.getGame().getConfig().get("furni.postit.default.model", "367897 JBlob!");
        return this.extradata;
    }

    @Override
    public IItemMessageParser getMessageParser() {
        return this.messageParser;
    }
}
