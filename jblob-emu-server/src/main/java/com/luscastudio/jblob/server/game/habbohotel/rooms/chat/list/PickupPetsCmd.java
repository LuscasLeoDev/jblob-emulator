package com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.AddInventoryPetComposer;
import com.luscastudio.jblob.server.communication.handlers.player.inventory.server.FurniListNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.server.UserRemoveComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.game.habbohotel.inventory.pets.PetData;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomPetAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.IChatCommand;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 04/02/2017 at 14:52.
 */
public class PickupPetsCmd implements IChatCommand {
    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {

        if(!room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_pickup_pets"))
            return false;

        Iterator<IRoomPetAvatar> pets = room.getRoomAvatarService().getPets().iterator();
        List<PetData> petIds = BCollect.newList();

        Map<Integer, List<IRoomPetAvatar>> sessionsPets = BCollect.newMap();

        sessionsPets.put(session.getAvatar().getId(), BCollect.newList());

        boolean roomRightPickupAnyPets = room.getProperties().avatarHasRight(session.getAvatar().getId(), "room_right_pickup_any_pets");

        while(pets.hasNext()){
            IRoomPetAvatar petAvatar = pets.next();

            if(petAvatar.getPetData().getOwnerId() != session.getAvatar().getId()){
                if(!roomRightPickupAnyPets){
                    if(sessionsPets.containsKey(petAvatar.getPetData().getOwnerId()))
                        sessionsPets.put(petAvatar.getPetData().getOwnerId(), BCollect.newList());

                    sessionsPets.get(petAvatar.getPetData().getOwnerId()).add(petAvatar);
                }
            } else{
                sessionsPets.get(session.getAvatar().getId()).add(petAvatar);
            }

            petIds.add(petAvatar.getPetData());
        }

        Iterator<Map.Entry<Integer, List<IRoomPetAvatar>>> iterator = sessionsPets.entrySet().iterator();

        while(iterator.hasNext()){
            Map.Entry<Integer, List<IRoomPetAvatar>> entry = iterator.next();

            PlayerSession targetSession = JBlob.getGame().getSessionManager().getSession(entry.getKey());

            FurniListNotificationComposer inventoryNotification = new FurniListNotificationComposer();

            if(targetSession != null){
                entry.getValue().forEach(petAvatar -> {
                    targetSession.getAvatar().getInventory().addPet(petAvatar.getPetData());
                    room.getRoomAvatarService().removePet(petAvatar.getId());
                    inventoryNotification.add(FurniListNotificationComposer.PET, petAvatar.getPetData().getId());
                    targetSession.sendQueueMessage(new AddInventoryPetComposer(petAvatar.getPetData()));
                    room.sendQueueMessage(new UserRemoveComposer(petAvatar.getVirtualId()));
                });

                targetSession.sendQueueMessage(inventoryNotification);
                targetSession.flush();
                room.flush();
            }

            try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
                DBConnPrepare prepare = reactor.prepare("UPDATE avatars_pets_data SET room_id = ? WHERE owner_id = ?");
                prepare.setInt(1, 0);
                prepare.setInt(2, targetSession.getAvatar().getId());
                prepare.run();
            } catch (Exception e){
                BLogger.error(e, this.getClass());
            }
        }


        return true;
    }
}
