//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.IAvatarObjectPosition;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;

/**
 * Created by Lucas on 08/03/2017 at 13:24.
 */
public class AvatarWalkingEndEventArgs extends EventArgs {
    private IRoomAvatar avatar;
    private IAvatarObjectPosition position;

    public AvatarWalkingEndEventArgs(IRoomAvatar avatar, IAvatarObjectPosition position) {
        this.avatar = avatar;
        this.position = position;
    }

    public IRoomAvatar getAvatar() {
        return avatar;
    }

    public IAvatarObjectPosition getPosition() {
        return position;
    }
}
