package com.luscastudio.jblob.server.communication.handlers.catalog.server;

/**
 * Created by Lucas on 28/10/2016.
 */

import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogDeal;
import com.luscastudio.jblob.server.game.habbohotel.catalog.CatalogNode;

public class CatalogPageComposer extends MessageComposer {
    @Override
    public String id() {
return "CatalogPageMessageComposer";
    }

    public CatalogPageComposer(CatalogNode node, String mode) {

        message.putInt(node.getId());
        message.putString(mode);
        message.putString(node.getModel());


        message.putInt(node.getImageSet().size());
        for(String image : node.getImageSet())
            message.putString(image);

        message.putInt(node.getTextSet().size());
        for(String text : node.getTextSet())
            message.putString(text);

        //region Writing Deals

        message.putInt(node.getDeals().size());

        for(CatalogDeal deal : node.getDeals().values()) {

            deal.parsePacket(message);

            //New release
            message.putBool(false);
            message.putString("");

            //endregion

        }


        message.putInt(0);
        message.putBool(false); //AcceptCurrencyAsCredit
        //endregion

    }
}
