//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.server.events.list;

import com.luscastudio.jblob.server.events.EventArgs;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;

/**
 * Created by Lucas on 08/03/2017 at 13:05.
 */
public class AvatarEnterRoomEventArgs extends EventArgs {

    private IRoomAvatar roomAvatar;

    public AvatarEnterRoomEventArgs(IRoomAvatar roomAvatar) {
        this.roomAvatar = roomAvatar;
    }

    public IRoomAvatar getPlayerAvatar() {
        return roomAvatar;
    }
}
