package com.luscastudio.jblob.server.game.habbohotel.rooms.chat.list;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.server.boot.JBlob;
import com.luscastudio.jblob.server.communication.handlers.player.notifications.server.BubbleNotificationComposer;
import com.luscastudio.jblob.server.communication.handlers.rooms.items.wired.server.HideWiredConfigComposer;
import com.luscastudio.jblob.server.communication.server.MessageComposer;
import com.luscastudio.jblob.server.database.DBConnPrepare;
import com.luscastudio.jblob.server.database.DBConnReactor;
import com.luscastudio.jblob.server.debug.BLogger;
import com.luscastudio.jblob.server.events.EventDelegate;
import com.luscastudio.jblob.server.events.list.WiredSaveDataEventArgs;
import com.luscastudio.jblob.server.game.habbohotel.furnis.FurniBase;
import com.luscastudio.jblob.server.game.habbohotel.rooms.Room;
import com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.IRoomAvatar;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.CommandParams;
import com.luscastudio.jblob.server.game.habbohotel.rooms.chat.IChatCommand;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.IRoomItem;
import com.luscastudio.jblob.server.game.habbohotel.rooms.items.interactor.wired.WiredSaveData;
import com.luscastudio.jblob.server.game.sessions.PlayerSession;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Lucas on 17/02/2017 at 17:39.
 */
public class MakeCatalogDealPackCmd implements IChatCommand {
    @Override
    public boolean parse(PlayerSession session, IRoomAvatar avatar, Room room, CommandParams params) {

        int id = -session.getAvatar().getId();

        if(params.length() > 1) {
            if (!NumberHelper.isInteger(params.get(1))) {
                session.sendMessage(new BubbleNotificationComposer("debug", "Invalid page Id!"));
                return true;
            }
        }
        else
            session.sendMessage(new BubbleNotificationComposer("debug", "You can use :makedeals X to set the page Id."));


        session.sendMessage(new CustomWiredTriggerConfigComposer(id));

        session.getAvatar().getEventHandler().fireEvent("command.makedealpack.cancel", null);

        session.sendMessage(new BubbleNotificationComposer("debug", "Type the deal name in the Text input.\nYou have 30 minutes to complete the wired box and save."));

        EventDelegate saveDelegate = session.getAvatar().getEventHandler().on("save.wired.trigger", (delegate, eventArgs) -> {
            if(!(eventArgs instanceof WiredSaveDataEventArgs))
                return;

            WiredSaveDataEventArgs wiredSaveDataEventArgs = (WiredSaveDataEventArgs) eventArgs;
            if(wiredSaveDataEventArgs.getItemId() != id)
                return;

            WiredSaveData saveData = wiredSaveDataEventArgs.getSaveData();

            int pageId = 0;
            if(params.length() > 1 && NumberHelper.isInteger(params.get(1)))
                pageId = Integer.parseInt(params.get(1));
            int dealId = this.makeDeal(saveData, room, pageId);

            session.sendMessage(new BubbleNotificationComposer("debug", "Deal id: " + dealId));
            session.sendMessage(new HideWiredConfigComposer());

            delegate.cancel();
        });

        session.getAvatar().getEventHandler().on("command.makedealpack.cancel", (delegate, eventArgs) -> {
            saveDelegate.cancel();
            delegate.cancel();
        });

        avatar.getEventHandler().on("avatar.leave.room", (delegate, eventArgs) -> {
            delegate.cancel();
            saveDelegate.cancel();
        });
        JBlob.getGame().getProcessManager().run(saveDelegate::cancel, 30, TimeUnit.MINUTES);

        return true;
    }

    private int makeDeal(WiredSaveData data, Room room, int pageId){


        Map<FurniBase, Integer> baseToDeal = BCollect.newMap();
        for (Integer integer : data.getItemsId()) {
            IRoomItem item = room.getRoomItemHandlerService().getRoomItemById(integer);
            if(item == null)
                continue;

            if(!baseToDeal.containsKey(item.getProperties().getBase()))
                baseToDeal.put(item.getProperties().getBase(), 0);

            baseToDeal.replace(item.getProperties().getBase(), baseToDeal.get(item.getBase()) + 1);


        }
        try(DBConnReactor reactor = JBlob.getGame().getDbConn().getReactor()){
            DBConnPrepare prepare;
            prepare = reactor.prepare("INSERT INTO catalog_deals (type, name, page_id) VALUES (?, ?, ?)", true);
            prepare.setString(1, "default");
            prepare.setString(2, data.getStringData());
            prepare.setInt(3, pageId);

            int dealId = prepare.runInsert();

            String insertQuery = "INSERT INTO catalog_deals_items (deal_id, type, base_id, amount) VALUES ";
            boolean first = true;
            for (FurniBase base : baseToDeal.keySet()) {
                if(!first)
                    insertQuery += ", ";

                insertQuery += "(?, ?, ?, ?)";

                first = false;
            }

            prepare = reactor.prepare(insertQuery, true);

            int i = 0;
            for (Map.Entry<FurniBase, Integer> entry : baseToDeal.entrySet()) {

                prepare.setInt(i * 4 + 1, dealId);
                prepare.setString(i * 4 + 2, entry.getKey().getType());
                prepare.setInt(i * 4 + 3, entry.getKey().getId());
                prepare.setInt(i * 4 + 4, entry.getValue());

                i++;
            }

            prepare.runInsertKeys();

            return dealId;

        } catch (Exception e){
            BLogger.error(e, this.getClass());
        }

        return 0;
    }

    private class CustomWiredTriggerConfigComposer extends MessageComposer {
        @Override
        public String id() {
            return "WiredTriggerConfigMessageComposer";
        }

        public CustomWiredTriggerConfigComposer(int windowId){

            //region header

            message.putBool(false); //Some boolean related to the comment XX about
            message.putInt(700);//Limit of Selectable items

            message.putInt(0);//items ids count


            message.putInt(0);
            message.putInt(windowId);


            message.putString("");

            message.putInt(0); //Integers Data Count

            message.putInt(0); //Comment XX: Something on external flash texts like wiredfurni.pickfurnis. effect/trigger/conditon . x

            //endregion

            message.putInt(13);

            message.putInt(0); //Incompatible wireds on stack list count

        }
    }
}
