package com.luscastudio.jblob.server.game.habbohotel.rooms.avatars.statusses;

/**
 * Created by Lucas on 12/12/2016.
 */

public class RoomAvatarStatus {

    private String name;
    private String value;
    private int expire;

    public RoomAvatarStatus(String name, String value, int expire){
        this.name = name;
        this.value = value;
        this.expire = expire;
    }

    public String getName() {
        return name;
    }

    public int getExpire() {
        return expire;
    }

    public String getValue() {
        return value;
    }
}
