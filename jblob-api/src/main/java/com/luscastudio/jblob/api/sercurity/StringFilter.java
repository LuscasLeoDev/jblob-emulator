package com.luscastudio.jblob.api.sercurity;

import com.luscastudio.jblob.api.utils.console.IFlushable;

/**
 * Created by Lucas on 17/12/2016.
 */

public class StringFilter implements IFlushable{

    //todo: do the filter
    public String filterMessage(String message){
        return message;
    }

    public String filterFigureString(String figure) {
        return figure;
    }

    public String filterFigureGender(String gender) {
        return gender;
    }

    public PetNameValidation filterPetName(String name) {
        return new PetNameValidation(0, "Success!");
    }

    public String filterString(String string) {
        return string;
    }

    @Override
    public void flush() throws Exception {

    }

    @Override
    public void performFlush() {

    }

    public static class PetNameValidation{
        private int errorCode;
        private String validationInfo;

        public int getErrorCode() {
            return errorCode;
        }

        public String getValidationInfo() {
            return validationInfo;
        }

        public boolean isValid(){
            return errorCode == 0;
        }

        public PetNameValidation(int errorCode, String validationInfo){
            this.errorCode = errorCode;
            this.validationInfo = validationInfo;
        }
    }
}
