package com.luscastudio.jblob.api.utils.engine;

/**
 * Created by Lucas on 04/10/2016.
 */

public class Point implements IPoint{

    protected int x;
    protected int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point from) {
        this.x = from.x;
        this.y = from.y;
    }

    
    public int getDistance(Point point) {
        if(point.x == this.x || point.y == this.y)
            return Math.abs(this.x - point.x) + Math.abs(this.y - point.y);
        return Math.abs(this.x - point.x) + Math.abs(this.y - point.y) - 1;
    }

    
    public boolean equals(Object obj) {
        return obj instanceof Point && (((Point) obj).x == x && ((Point) obj).y == y);
    }

    
    public int getRotation(Point p) {
        /*
            0 >> >>
            1 >> vv
            2 vv vv
            3 << vv
            4 << <<
            5 << ^^
            6 ^^ ^^
            7 >> ^^
         */

        //int internPeoduct = (p.x * this.x) + (this.y * this.y);
        int defaultDir = 2;

        if (p.y > this.y) { //Up section
            if (p.x == this.x)// Just Up
                return 4;
            else if (p.x > this.x)
                return 3;
            else if (p.x < this.x)
                return 5;
        } else if (p.y == this.y) { //Rect
            if (p.x == this.x)// Just Up
                return defaultDir; //Default dir
            else if (p.x > this.x)
                return 2;
            else if (p.x < this.x)
                return 6;
        } else if (p.y < this.y) {
            if (p.x == this.x)// Just Up
                return 0;
            else if (p.x > this.x)
                return 1;
            else if (p.x < this.x)
                return 7;
        }

        return defaultDir;
    }

    
    public String toString() {
        return "[ " + this.x + ", " + this.y + " ]";
    }

    
    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    
    public int getX() {
        return x;
    }

    
    public int getY() {
        return y;
    }

    
    public void dispose(){
        try {

        } catch (Throwable throwable) {
            //
        }
    }
}
