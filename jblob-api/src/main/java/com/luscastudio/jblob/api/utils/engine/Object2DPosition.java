package com.luscastudio.jblob.api.utils.engine;

/**
 * Created by Lucas on 02/02/2017 at 20:39.
 */
public class Object2DPosition extends Point implements IObject2DPosition, IPoint {

    int rotation;

    public Object2DPosition(int x, int y, int rotation) {
        super(x, y);
        this.rotation = rotation;
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }

    @Override
    public int getRotation() {
        return rotation;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }
}
