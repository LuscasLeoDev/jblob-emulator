//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import org.w3c.dom.Element;

/**
 * Created by Lucas on 02/03/2017 at 08:02.
 */
public class FigureColor {

    private int id;
    private String value;

    public FigureColor(Element item) {
        if(item.hasAttribute("id") && NumberHelper.isInteger(item.getAttribute("id")))
            this.id = Integer.parseInt(item.getAttribute("id"));
        else
            this.id = 0;

        this.value = item.getTextContent();
    }

    public int getId() {
        return id;
    }

    public String getValue() {
        return value;
    }
}
