//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import org.w3c.dom.Element;

/**
 * Created by Lucas on 02/03/2017 at 12:01.
 */
public class FigureMapPart {
    private String id;
    private String type;
    public FigureMapPart(Element element) {
        if(element.hasAttribute("id"))
            this.id = element.getAttribute("id");
        else
            this.id = "null";

        if(element.hasAttribute("type"))
            this.type = element.getAttribute("type");
        else
            this.type = "null";
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }
}
