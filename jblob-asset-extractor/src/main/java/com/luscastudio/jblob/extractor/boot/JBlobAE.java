package com.luscastudio.jblob.extractor.boot;

import com.luscastudio.jblob.api.utils.json.JSONUtils;
import com.luscastudio.jblob.extractor.commands.CommandHandler;
import com.luscastudio.jblob.extractor.commands.CommandInput;
import com.luscastudio.jblob.extractor.process.ProcessExecutor;

import java.io.File;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * Created by Lucas on 03/11/2016.
 */

public class JBlobAE {

    private static Scanner scanner;

    public static void main(String[] args) {

        try {
            JSONUtils.init();
            ProcessExecutor.init();
            scanner = new Scanner(System.in);
            CommandHandler handler = new CommandHandler();
            if(args.length > 0){
                handler.handleCommand(new CommandInput(args[0]));
                return;
            }
            //region #Extracting from XML
            /*
            ProcessExecutor.init();
            FileInputStream fileInputStream = new FileInputStream(furnidataPath);
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            xml = builder.parse(fileInputStream);
            fileInputStream.dispose();

            xml.normalize();

            NodeList floorItemsLists = xml.getElementsByTagName("roomitemtypes");

            NodeList wallItemsLists= xml.getElementsByTagName("wallitemtypes");

            for(int i = 0; i < floorItemsLists.getLength(); i++){
                Element element = (Element)floorItemsLists.item(i);

                NodeList furnis = element.getElementsByTagName("furnitype");

                for(int o = 0; o < furnis.getLength(); o++){
                    FurniDataElement furniDataElement = new FurniDataElement((Element)furnis.item(o));

                    SwfIO furniIo = getSwfIOFromXml(furniDataElement);




                }
            }*/

            //endregion




            while (true) {
                try {
                    write("Input command:");
                    handler.handleCommand(new CommandInput(scanner.nextLine()));
                    write("Command end");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Path getOrCreatePath(String path) {
        try {
            File fpath = new File(path);
            if (!fpath.exists())
                if(!fpath.mkdirs())
                    throw new Exception("Could not create Paths for '" + path + "'");

            return fpath.toPath();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void write(String text) {
        System.out.println(text);
    }

    public static String readKey() {
        return scanner.next();
    }

    public static String readLine() {
        return scanner.nextLine();
    }
}