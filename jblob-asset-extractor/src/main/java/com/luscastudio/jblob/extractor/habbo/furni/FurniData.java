package com.luscastudio.jblob.extractor.habbo.furni;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.api.utils.numbers.NumberHelper;
import com.luscastudio.jblob.extractor.habbo.furni.xml.*;
import com.luscastudio.jblob.extractor.io.SwfIO;
import com.luscastudio.jblob.extractor.swf.tags.DefineBinaryDataTag;
import com.luscastudio.jblob.extractor.swf.tags.DefineBitsLossless2Tag;
import com.luscastudio.jblob.extractor.swf.tags.SwfTag;

import java.awt.image.BufferedImage;
import java.util.*;

/**
 * Created by Lucas on 04/11/2016.
 */

public class FurniData {

    private SwfIO swfIO;
    private SpriteImageCollection images;

    private BinaryVisualizationXmlReader visualizations;
    private BinaryAssetXmlReader assets;

    public FurniData(SwfIO io) throws Exception {
        this.images = new SpriteImageCollection();
        this.swfIO = io;

        for (SwfTag tag : io.getTags().values()) {
            if (tag instanceof DefineBitsLossless2Tag)
                this.images.add(new SpriteImage((DefineBitsLossless2Tag) tag));
        }

        String assetsBinaryName = swfIO.getFrameName() + "_" + swfIO.getFrameName() + "_assets";
        String visualizationsBinaryName = swfIO.getFrameName() + "_" + swfIO.getFrameName() + "_visualization";
        DefineBinaryDataTag assetsBinary = this.swfIO.getBinary(assetsBinaryName);
        DefineBinaryDataTag visualizationsBinary = this.swfIO.getBinary(visualizationsBinaryName);

        //What if this binary file doesn't exists? '-'

        if(assetsBinary == null)
            throw new Exception("The Swf file doesn't contains the assets binary file '" + assetsBinaryName);

        if(visualizationsBinary == null)
            throw new Exception("The Swf file doesn't contains the visualizations binary file '" + visualizationsBinaryName);


        this.assets = new BinaryAssetXmlReader(assetsBinary.getContent(), swfIO.getFrameName());
        this.visualizations = new BinaryVisualizationXmlReader(visualizationsBinary.getContent());
    }

    public Map<Integer, BufferedImage> getIcons(){
        Map<Integer, BufferedImage> iconImages = BCollect.newLinkedMap();

        List<FurniAssetData> iconAssets = BCollect.newList();

        SpriteVisualization visualization = visualizations.getVisualization(spriteVisualization -> spriteVisualization.getSize().equals("1"));

        List<FurniSpriteAsset> iconsCoord = assets.getAssets(spriteAsset -> spriteAsset.getNameData().getType().equals("icon"));
        iconsCoord.sort((o1, o2) -> {
            int i1 = NumberHelper.hexToInt(o1.getNameData().getOrder().charAt(0));
            int i2 = NumberHelper.hexToInt(o2.getNameData().getOrder().charAt(0));
            if(i1 > i2)
                return 1;
            if(i1 < i2)
                return -1;

            return 0;
        });


        /*for(SwfTag tag : this.swfIO.getTags().values()){
            if((tag instanceof DefineBitsLossless2Tag)) {
                DefineBitsLossless2Tag imageTag = (DefineBitsLossless2Tag) tag;
                SpriteImage sprImg = new SpriteImage(imageTag);
                if((sprImg.isIcon() || sprImg.getNameData().getSize().equals("32"))) {
                    for (FurniSpriteAsset asset : iconsCoord) {
                        if((asset.getNameData().getOrder().equals(sprImg.getNameData().getOrder()) && (sprImg.getNameData().getRotation() == 0 || sprImg.getNameData().getRotation() == 2))) {
                            if(!iconAssets.containsKey(asset.getFile()) ||
                                    sprImg.getNameData().getType().equals("icon") ||
                                    (iconAssets.containsKey(asset.getFile()) && sprImg.getNameData().getRotation() == visualization.getRotation())) {
                                iconAssets.put(asset.getFile(), sprImg);
                            }
                        }
                    }

                }
            }
        }*/

        SpriteImageCollection spriteImages = new SpriteImageCollection();
        for (SwfTag tag : this.swfIO.getTags().values()) {
            if(!(tag instanceof DefineBitsLossless2Tag))
                continue;

            DefineBitsLossless2Tag imageTag = (DefineBitsLossless2Tag)tag;
            SpriteImage sprImg = new SpriteImage(imageTag);

            spriteImages.push(sprImg);
        }

        int lastItemRotation = 2;
        for (FurniSpriteAsset asset : iconsCoord) {
            SpriteImageCollection byOrder = spriteImages.getByStateIndex(0).getByOrder(asset.getNameData().getOrder());
            SpriteImageCollection byType = byOrder.getByType(asset.getNameData().getType());

            if(byType.size() == 0){
                byType = byOrder.getByType("32");

                if(byType.size() > 0) {
                    SpriteImageCollection byRotation = byType.getByRotation(lastItemRotation);
                    if (byRotation.size() > 0)
                        iconAssets.add(new FurniAssetData(asset, byRotation.get(0)));
                    else
                        iconAssets.add(new FurniAssetData(asset, byType.get(0)));
                } else {
                    byType = byOrder.getByType("64");
                    if(byType.size() > 0) {
                        SpriteImageCollection byRotation = byType.getByRotation(lastItemRotation);
                        if (byRotation.size() > 0)
                            iconAssets.add(new FurniAssetData(asset, byRotation.get(0)));
                        else
                            iconAssets.add(new FurniAssetData(asset, byType.get(0)));
                    }
                }
            } else {
                iconAssets.add(new FurniAssetData(asset, byType.get(0)));
                lastItemRotation = byType.get(0).getNameData().getRotation();
            }


            byType.size();
        }


        FurniSpriteImageDrawing imageDrawing = new FurniSpriteImageDrawing(this.swfIO, visualization, iconAssets);
        for (SpriteColor color : visualization.getColors().values()) {
            iconImages.put(color.getId(), imageDrawing.draw(color));
        }

        if(iconImages.size() == 0)
            iconImages.put(-1, imageDrawing.draw(null));


        return iconImages;
    }

    private int getReverseRotation(int rotation) {
        switch (rotation){
            case 0:
                return 6;
            case 6:
            default:
                return 0;

            case 4:
                return 2;
            case 2:
                return 4;
        }
    }

    public SwfIO getSwfIO() {
        return swfIO;
    }

    public BinaryAssetXmlReader getAssets() {
        return assets;
    }

    public BinaryVisualizationXmlReader getVisualizations() {
        return visualizations;
    }

    public void close() {
        this.images.clear();
        this.swfIO = null;
    }

    public String getName() {
        return this.swfIO.getFrameName();
    }
}
