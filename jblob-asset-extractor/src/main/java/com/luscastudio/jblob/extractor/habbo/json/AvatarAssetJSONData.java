//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.json;

/**
 * Created by Lucas on 05/03/2017 at 11:47.
 */
public class AvatarAssetJSONData {
    private String libName;
    private String size;
    private String type;
    private String partId;
    private int rotation;
    private int frameIndex;
    private int x;
    private int y;

    public AvatarAssetJSONData(String libName, String size, String type, String partId, int rotation, int frameIndex, int x, int y) {
        this.libName = libName;
        this.size = size;
        this.type = type;
        this.partId = partId;
        this.rotation = rotation;
        this.frameIndex = frameIndex;
        this.x = x;
        this.y = y;
    }
}
