package com.luscastudio.jblob.extractor.habbo.furni.xml;

/**
 * Created by Lucas on 06/11/2016.
 */

public class SpriteColorLayer {

    private int id;
    private String color;

    public SpriteColorLayer(int id, String color){
        this.id = id;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public String getColor() {
        return color;
    }
}
