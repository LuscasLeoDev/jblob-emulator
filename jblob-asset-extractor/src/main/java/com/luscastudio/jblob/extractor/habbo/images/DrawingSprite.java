//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.images;

import java.awt.image.BufferedImage;

/**
 * Created by Lucas on 02/03/2017 at 23:49.
 */
public interface DrawingSprite {
    int getX();
    int getY();

    void setX(int x);
    void setY(int y);

    BufferedImage getImage();
}
