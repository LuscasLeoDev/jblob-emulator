package com.luscastudio.jblob.extractor.habbo.furni;

import com.luscastudio.jblob.extractor.habbo.furni.xml.FurniSpriteAsset;
import com.luscastudio.jblob.extractor.habbo.images.DrawingSprite;

import java.awt.image.BufferedImage;

/**
 * Created by Lucas on 02/03/2017 at 00:15.
 */
public class FurniDrawingSprite implements DrawingSprite {
    private SpriteImage image;
    private FurniSpriteAsset asset;

    private int x;
    private int y;

    public FurniDrawingSprite(SpriteImage image, FurniSpriteAsset asset, boolean colorable, String color){
        this.image = image;
        this.asset = asset;

        this.x = asset.getX();
        this.y = asset.getY();
    }

    public FurniSpriteAsset getAsset() {
        return asset;
    }

    public BufferedImage getImage() {
        return image.getImageTag().getImage();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
