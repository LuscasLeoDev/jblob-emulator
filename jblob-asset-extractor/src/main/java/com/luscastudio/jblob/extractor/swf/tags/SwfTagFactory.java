package com.luscastudio.jblob.extractor.swf.tags;

/**
 * Created by Lucas on 31/10/2016.
 */

public class SwfTagFactory {

    public static SwfTag generateTag(SwfTagHeader header) {

        switch(header.getCode()){

            case 36:
                return new DefineBitsLossless2Tag(header);

            case 76:
                return new SymbolClassTag(header);

            case 87:
                return new DefineBinaryDataTag(header);

            case 43:
                return new FrameLabelTag(header);

            case 35:
                return new DefineBitsJPEG3(header);

            case 82:
                return new DoAbcTag(header);

            default:
                return null;
        }

    }
}
