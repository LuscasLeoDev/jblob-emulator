package com.luscastudio.jblob.extractor.boot;

/**
 * Created by Lucas on 04/11/2016.
 */

public class Configurations {

    public static String BINARY_DATA_FILE_FORMAT = ".txt";
    public static String LOSSLESS2_DATA_FILE_FORMAT = ".png";

    public static String BINARY_DATA_FILE_PATH = "binaries";
    public static String LOSSLESS2_DATA_FILE_PATH = "images";
    public static String JPEG3_DATA_FILE_PATH = "images";

}
