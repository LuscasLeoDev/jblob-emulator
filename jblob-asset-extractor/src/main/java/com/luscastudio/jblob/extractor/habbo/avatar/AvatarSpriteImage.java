//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import java.awt.image.BufferedImage;

/**
 * Created by Lucas on 05/03/2017 at 10:34.
 */
public class AvatarSpriteImage {

    private BufferedImage image;
    private String libId;
    private String size;
    private String action;
    private String partId;
    private String type;
    private int rotation;
    private int frameIndex;

    public AvatarSpriteImage(BufferedImage image, String libId, String size, String action, String partId, String type, int rotation, int frameIndex) {
        this.image = image;
        this.libId = libId;
        this.size = size;
        this.action = action;
        this.partId = partId;
        this.type = type;
        this.rotation = rotation;
        this.frameIndex = frameIndex;
    }

    public BufferedImage getImage() {
        return image;
    }

    public String getLibId() {
        return libId;
    }

    public String getSize() {
        return size;
    }

    public String getAction() {
        return action;
    }

    public String getPartId() {
        return partId;
    }

    public String getType() {
        return type;
    }

    public int getRotation() {
        return rotation;
    }

    public int getFrameIndex() {
        return frameIndex;
    }
}
