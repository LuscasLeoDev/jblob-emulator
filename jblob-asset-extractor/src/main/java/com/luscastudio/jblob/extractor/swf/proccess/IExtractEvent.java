package com.luscastudio.jblob.extractor.swf.proccess;

/**
 * Created by Lucas on 04/11/2016.
 */
public interface IExtractEvent {

    void onSuccess();

    void onError(Exception e);

    void onFinalize();

}
