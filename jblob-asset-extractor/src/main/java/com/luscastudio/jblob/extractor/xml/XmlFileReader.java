//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.xml;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

/**
 * Created by Lucas on 04/11/2016.
 */

public class XmlFileReader {

    protected Document xml;
    protected DocumentBuilderFactory documentBuilderFactory;
    protected DocumentBuilder documentBuilder;

    public XmlFileReader(String xml){
        try (StringReader reader = new StringReader(xml)){

            this.documentBuilderFactory = DocumentBuilderFactory.newInstance();
            this.documentBuilder = documentBuilderFactory.newDocumentBuilder();

            this.xml = documentBuilder.parse(new InputSource(reader));
            this.xml.normalizeDocument();

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
