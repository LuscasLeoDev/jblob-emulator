/*
 * Copyright (c) 2017.
 */

package com.luscastudio.jblob.extractor.commands;

import com.luscastudio.jblob.api.utils.io.BFileReader;
import com.luscastudio.jblob.extractor.boot.JBlobAE;
import com.luscastudio.jblob.extractor.habbo.furni.FurniData;
import com.luscastudio.jblob.extractor.io.SwfIO;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.Map;

public class ExtractIconCommand implements Command {

    private boolean debug;

    @Override
    public synchronized void handleCommand(CommandInput input) {
        String path;
        String output;

        this.debug = input.setParam("-debug");

        if(!input.hasAllParams("-path -output")){
            JBlobAE.write("Please ensure that you are using the main params -path and -output");
            return;
        }

        path = input.getNext("-path");
        output = input.getNext("-output");

        File swfPath = new File(path);
        if(!swfPath.exists()) {
            JBlobAE.write("The path " + swfPath.getAbsolutePath() + "doesn't exits!");
            return;
        }

        IconSaveNameDelegate delegate = (partColor, assetData) ->
                new File(output.replace("%name", assetData.getName()).replace("%partcolor", partColor > -1 ? "_" + partColor.toString() : ""));


        if(swfPath.isFile())
            extractIcon(swfPath, delegate);
        else {
            File[] files;
            if (input.setParam("-regex")) {
                String pattern = input.getNext();
                files = swfPath.listFiles((dir, name) -> name.matches(pattern));
            } else
                files = swfPath.listFiles((dir, name) -> name.endsWith(".swf"));
            if (files != null)
                for (File file : files) {
                    extractIcon(file, delegate);
                }
        }
    }

    private synchronized void extractIcon(File file, IconSaveNameDelegate delegate) {

        try {
            ByteBuffer swfFileBytes = BFileReader.getFileBytes(file.getAbsolutePath());
            if(swfFileBytes == null){
                JBlobAE.write("The file " + file.getAbsolutePath() + " is invalid!");
                return;
            }
            SwfIO swfIO = new SwfIO(swfFileBytes);
            if (this.debug)
                swfIO.setDebugLogger(JBlobAE::write);
            swfIO.init();

            FurniData furniData = new FurniData(swfIO);

            for (Map.Entry<Integer, BufferedImage> entry : furniData.getIcons().entrySet()) {
                File name = delegate.getFile(entry.getKey(), furniData);
                if(!name.getParentFile().exists() && !name.getParentFile().mkdirs()){
                    JBlobAE.write("Count not create the directory to the file " + name.getAbsolutePath());
                }
                ImageIO.write(entry.getValue(), "png", name);
            }

            swfFileBytes.clear();
            swfIO.close();
            furniData.close();


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private interface IconSaveNameDelegate {
        File getFile(Integer partColor, FurniData assetData);
    }
}
