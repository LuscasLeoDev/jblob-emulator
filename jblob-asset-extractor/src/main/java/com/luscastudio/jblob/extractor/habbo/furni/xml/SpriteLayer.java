package com.luscastudio.jblob.extractor.habbo.furni.xml;

/**
 * Created by Lucas on 06/11/2016.
 */

public class SpriteLayer {

    private int layerId;
    private String ink;
    private int z;
    private int alpha;

    public SpriteLayer(int layerId, String ink, int z, int alpha){
        this.layerId = layerId;
        this.ink = ink;
        this.z = z;
        this.alpha = alpha;
    }

    public int getLayerId() {
        return layerId;
    }

    public int getZ() {
        return z;
    }

    public String getInk() {
        return ink;
    }

    public int getAlpha() {
        return alpha;
    }
}
