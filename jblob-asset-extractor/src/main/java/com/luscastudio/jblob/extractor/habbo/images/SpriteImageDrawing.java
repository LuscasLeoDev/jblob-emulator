//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.images;


import java.util.Collection;

/**
 * Created by Lucas on 02/03/2017 at 23:43.
 */
public abstract class SpriteImageDrawing {
    private int cropWidth;
    private int cropHeight;
    private int startCropX;
    private int startCropY;
    private int baseImageWidth;
    private int baseImageHeight;

    public void prepareSize(int maxWidth, int maxHeight){
        baseImageWidth = maxWidth;
        baseImageHeight = maxHeight;

        cropWidth = 1;
        cropHeight = 1;
        startCropX = -1;
        startCropY = -1;
        redoCoords(baseImageWidth, baseImageHeight);
    }

    private void redoCoords(int width, int height){
        for(DrawingSprite item : this.getDrawingSprites()) {

            item.setX(width / 2 - item.getX());
            item.setY(height / 2 - item.getY());
        }

        for(DrawingSprite item : this.getDrawingSprites()) {
            int addX = 0;
            int addY = 0;
            if(item.getX() < 0) {
                addX = -item.getX();
            } else if(item.getX() + item.getImage().getWidth() > baseImageWidth) {
                baseImageWidth = item.getX() + item.getImage().getWidth();
            }

            if(item.getY() < 0) {
                addY = -item.getY();
            } else if(item.getY() + item.getImage().getHeight() > baseImageHeight) {
                baseImageHeight = item.getY() + item.getImage().getHeight();
            }
            addCoordToItems(addX, addY);
        }

    }

    private void addCoordToItems(int x, int y){
        for(DrawingSprite item : this.getDrawingSprites()){
            item.setX(item.getX() + x);
            item.setY(item.getY() + y);
        }
    }

    protected void reviewSize(int x, int y, int width, int height){
        if(x + width > cropWidth)
            cropWidth = x + width;

        if(y + height > cropHeight)
            cropHeight = y + height;

        if(x < startCropX || startCropX == -1)
            startCropX = x;

        if(y < startCropY || startCropY == -1)
            startCropY = y;
    }

    public abstract Collection<? extends DrawingSprite> getDrawingSprites();

    public int getCropWidth() {
        return cropWidth;
    }

    public int getCropHeight() {
        return cropHeight;
    }

    public int getStartCropX() {
        return startCropX;
    }

    public int getStartCropY() {
        return startCropY;
    }

    public int getBaseImageWidth() {
        return baseImageWidth;
    }

    public int getBaseImageHeight() {
        return baseImageHeight;
    }

    public void setCropWidth(int cropWidth) {
        this.cropWidth = cropWidth;
    }

    public void setCropHeight(int cropHeight) {
        this.cropHeight = cropHeight;
    }

    public void setStartCropX(int startCropX) {
        this.startCropX = startCropX;
    }

    public void setStartCropY(int startCropY) {
        this.startCropY = startCropY;
    }

    public void setBaseImageWidth(int baseImageWidth) {
        this.baseImageWidth = baseImageWidth;
    }

    public void setBaseImageHeight(int baseImageHeight) {
        this.baseImageHeight = baseImageHeight;
    }
}
