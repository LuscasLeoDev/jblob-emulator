package com.luscastudio.jblob.extractor.swf.tags;

import com.luscastudio.jblob.extractor.boot.Configurations;
import com.luscastudio.jblob.extractor.boot.JBlobAE;
import com.luscastudio.jblob.extractor.compress.Compressing;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by Lucas on 31/10/2016.
 */

public class DefineBitsLossless2Tag extends SwfTag {

    private BufferedImage image;

    public DefineBitsLossless2Tag(SwfTagHeader header) {
        super(header);

        this.id = buffer.getShort();

        byte format = buffer.get();

        int width = buffer.getShort();
        int height = buffer.getShort();

        if(width < 0 || height < 0){
            JBlobAE.write("Invalid Size for image: #" + getId());
            return;
        }

        int colors = 0;

        if(format == 3)
            colors = buffer.get();

        byte[] imagedata = new byte[buffer.remaining()];

        try {
            ByteBuffer imagebuffer;
            buffer.get(imagedata);
            imagebuffer = ByteBuffer.wrap(imagedata);
            imagebuffer =  Compressing.ZLibDecompress(imagebuffer).order(ByteOrder.BIG_ENDIAN);

            switch(format){

                case 5: {

                    this.image = new BufferedImage(width, height, 3);

                    int alpha;
                    int red;
                    int green;
                    int blue;
                    //int color;

                    int i = 0;
                    for (int y = 0; y < height; y++)
                        for (int x = 0; x < width; x++) {

                            alpha = imagebuffer.get() & 255;
                            red = (imagebuffer.get() & 255);
                            green = (imagebuffer.get() & 255);
                            blue = (imagebuffer.get() & 255);

                            if(alpha > 0) {
                                red = red * 255 / alpha;
                                green = green * 255 / alpha;
                                blue = blue * 255 / alpha;
                            }

                            /*red = red * alpha / 255;
                            green = green * alpha / 255;
                            blue = blue * alpha / 255;*/

                            //Color color = new Color(red, green, blue, alpha);
                            image.setRGB(x, y, (alpha << 24 | red << 16 | green << 8 | blue));



                        }


                }
                break;

                default:

                    JBlobAE.write("Color format not supported: " + format);

                    break;
            }


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void save(File file) throws Exception{
        if(image == null) {

            JBlobAE.write("Invalid Image: " + getName());

            return;
        }
        ImageIO.write(image, "png", file);
    }

    @Override
    public String getSuffix() {
        return Configurations.LOSSLESS2_DATA_FILE_FORMAT;
    }

    @Override
    public String getFolderName() {
        return Configurations.LOSSLESS2_DATA_FILE_PATH;
    }

    public BufferedImage getImageCopy() {
        return image.getSubimage(0, 0, image.getWidth(), image.getHeight());
    }

    public BufferedImage getImage() {
        return image;
    }

    @Override
    public void dispose() {
        super.dispose();
        this.image = null;
    }

    @Override
    public boolean isSavable() {
        return true;
    }

    @Override
    public String toString() {
        return "PNG Image #" + this.id + " " + this.image.getWidth() + " x " + this.image.getHeight();
    }
}
