package com.luscastudio.jblob.extractor.io;

import java.nio.ByteBuffer;

/**
 * Created by Lucas on 31/10/2016.
 */

public class BitIO {

    private int cByte;
    private int cPos;

    private ByteBuffer buffer;

    public BitIO(ByteBuffer buffer){

        this.buffer = buffer;
    }

    public void resetBytes(){
        this.cByte = buffer.get();
        this.cPos = 1;
    }

    public int readBits(int nBytes){
        int i = 0;
        int r = 0;

        while(i++ < nBytes){
            r = (r << 1) + (this.cByte >> 8 - this.cPos & 1);
            if(++this.cPos > 8 && i < nBytes){
                this.resetBytes();
            }
        }

        return r;
    }

    public void end(){
        if(this.cPos != 1)
            this.resetBytes();
    }


}
