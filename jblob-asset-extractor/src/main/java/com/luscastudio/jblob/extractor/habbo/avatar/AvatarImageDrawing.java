package com.luscastudio.jblob.extractor.habbo.avatar;

import com.google.common.collect.*;
import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.extractor.habbo.images.DrawingSprite;
import com.luscastudio.jblob.extractor.habbo.images.ImageDrawningUtils;
import com.luscastudio.jblob.extractor.habbo.images.SpriteImageDrawing;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Lucas on 02/03/2017 at 21:20.
 */
public class AvatarImageDrawing extends SpriteImageDrawing {

    private List<AvatarDrawingSprite> avatarPartSprites;

    public Collection<? extends DrawingSprite> getDrawingSprites() {
        return this.avatarPartSprites;
    }

    public AvatarImageDrawing(int startWidth, int startHeight, List<SpriteAsset> spriteAssets) {

        this.avatarPartSprites = BCollect.newList();
        for (SpriteAsset asset : spriteAssets) {
            AvatarPartDrawingSprite sprite = new AvatarPartDrawingSprite(asset);
            this.avatarPartSprites.add(sprite);
        }

        this.prepareSize(startWidth, startHeight);
    }

    public BufferedImage draw(){
        BufferedImage image = new BufferedImage(this.getBaseImageWidth(), this.getBaseImageHeight(), BufferedImage.TYPE_INT_ARGB_PRE);
        Graphics gp = image.getGraphics();
        for (AvatarDrawingSprite part : this.avatarPartSprites) {
            BufferedImage imagePart = ImageDrawningUtils.copyImage(part.getImage());

            if(part.isColorable())
                ImageDrawningUtils.colorateImage(imagePart, part.getColor());

            gp.drawImage(imagePart, part.getX(), part.getY(), null);
            this.reviewSize(part.getX(), part.getY(), imagePart.getWidth(), imagePart.getHeight());
        }

        if(!this.avatarPartSprites.isEmpty())
            image = image.getSubimage(this.getStartCropX(), this.getStartCropY(), this.getCropWidth() - this.getStartCropX(), this.getCropHeight() - this.getStartCropY());
        gp.dispose();
        return image;
    }
}
