package com.luscastudio.jblob.extractor.swf;

import com.luscastudio.jblob.extractor.io.BitIO;
import com.luscastudio.jblob.extractor.io.SwfIO;

/**
 * Created by Lucas on 31/10/2016.
 */

public class SwfRectangle {

    private SwfIO swf;

    private int minX;
    private int minY;
    private int maxX;
    private int maxY;

    private int nBits;

    public SwfRectangle(SwfIO swf){
        this.swf = swf;
    }

    public void readRectangle() {

        BitIO io = new BitIO(swf.getBuffer().getByteBuffer());

        io.resetBytes();
        this.nBits = io.readBits(5);

        this.minX = io.readBits(nBits) / 20;
        this.maxX = io.readBits(nBits) / 20;
        this.minY = io.readBits(nBits) / 20;
        this.maxY = io.readBits(nBits) / 20;

        //io.end();
    }


    public int getMinX() {
        return minX;
    }

    public int getMinY() {
        return minY;
    }

    public int getMaxX() {
        return maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public void dispose(){
        this.swf = null;
    }

    @Override
    public String toString() {
        return "[Min: " + minX + "x" + minY + "] [Max: " + this.maxX + "x" + this.maxY + "]";
    }
}
