//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// Copyright Lucas Leonardo(c) 2017.                                           :
// You will die if you ctrl c and ctrl v this :)                               :
//                                                                             :
//                                                                             :
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

package com.luscastudio.jblob.extractor.habbo.avatar;

import com.luscastudio.jblob.api.utils.collect.BCollect;
import com.luscastudio.jblob.extractor.xml.XmlFileReader;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Map;

/**
 * Created by Lucas on 02/03/2017 at 20:47.
 */
public class ImageAssetManifest extends XmlFileReader {

    private Map<String, ImageSpriteAssetData> assetDataMap;

    public ImageAssetManifest(String xml) {
        super(xml);
        this.assetDataMap = BCollect.newMap();
        NodeList assets = this.xml.getElementsByTagName("asset");
        for (int i = 0; i < assets.getLength(); i++) {
            Element item = (Element) assets.item(i);

            ImageSpriteAssetData spriteAssetData = new ImageSpriteAssetData(item);
            this.assetDataMap.put(spriteAssetData.getAssetName(), spriteAssetData);
        }
    }

    public ImageSpriteAssetData getSpriteAssetData(String name){
        return this.assetDataMap.get(name);
    }
}
